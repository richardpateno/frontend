// npm install react-bootstrap
import React, { useContext } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Link from "next/link";
import UserContext from "../UserContext";

export default function NavBar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
      <Link href="/">
        <a className="navbar-brand"> Budget Tracker</a>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {user.id !== null ? (
            <React.Fragment>
              <Link href="/category">
                <a className="nav-link" role="button">
                  Category
                </a>
              </Link>
              <Link href="/record">
                <a className="nav-link" role="button">
                  Record
                </a>
              </Link>
              <Link href="/trend">
                <a className="nav-link" role="button">
                  Monthly Trend
                </a>
              </Link>
              <Link href="/breakdown">
                <a className="nav-link" role="button">
                  Breakdown
                </a>
              </Link>
              <Link href="/logout">
                <a className="nav-link" role="button">
                  Logout
                </a>
              </Link>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Link href="/login">
                <a className="nav-link" role="button">
                  Login
                </a>
              </Link>
              <Link href="/register">
                <a className="nav-link" role="button">
                  Register
                </a>
              </Link>
            </React.Fragment>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
