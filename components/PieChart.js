import { useState, useEffect } from "react";
import { Doughnut, Pie } from "react-chartjs-2";
import ColorRandomizer from "../helpers/ColorRandomizer";

export default function PieChart({ chartType, rawData, labelKey, amountKey }) {
  const [label, setLabel] = useState([]);
  const [amount, setAmount] = useState([]);
  const [bgColors, setBgColors] = useState([]);

  useEffect(() => {
    if (rawData) {
      setLabel(rawData.map((element) => element[labelKey]));
      setAmount(rawData.map((element) => Math.abs(element[amountKey])));

      let colors = rawData.map((element) => {
        if (element.color) {
          return element.color;
        } else {
          return `#${ColorRandomizer()}`;
        }
      });

      setBgColors(colors);
    } else {
      setLabel([]);
      setAmount([]);
      setBgColors([]);
    }
  }, [rawData]);

  if (chartType == "Pie") {
    return (
      <Pie
        data={{
          labels: label,
          legend: {
            position: "left",
          },
          datasets: [
            {
              data: amount,
              backgroundColor: bgColors,
              hoverBackground: bgColors,
            },
          ],
        }}
        redraw={false}
      />
    );
  } else {
    return (
      <Doughnut
        data={{
          labels: label,
          legend: {
            position: "left",
          },
          datasets: [
            {
              data: amount,
              backgroundColor: bgColors,
              hoverBackground: bgColors,
            },
          ],
        }}
      />
    );
  }
}
