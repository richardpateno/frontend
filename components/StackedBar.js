import { useState, useEffect } from "react";
import { HorizontalBar } from "react-chartjs-2";
import ColorRandomizer from "../helpers/ColorRandomizer";

export default function PieChart({ income, expenses }) {
  // State for 2nd data either savings/overspent
  const [consumption, setConsumption] = useState([]);
  const [label2, setLabel2] = useState([]);
  const [color2, setcolor2] = useState([]);

  useEffect(() => {
    let savings = income - expenses;

    if (savings < 0) {
      setConsumption(income);
      setLabel2("overspent");
      setcolor2("#e30707");
    } else {
      setConsumption(expenses);
      setLabel2("savings");
      setcolor2("#47b83d");
    }
  }, [income, expenses]);

  const arbitraryStackKey = "stack1";
  const data = {
    options: {
      scales: {
        x: {
          stacked: true,
        },
        y: {
          stacked: true,
        },
      },
      indexAxis: "x",
    },
    labels: [""],
    datasets: [
      // These two will be in the same stack.
      {
        stack: "",
        label: "consumption",
        data: [consumption],
        backgroundColor: "#f56f36",
        borderWidth: 0.01,
      },
      {
        stack: "",
        label: label2,
        data: [Math.abs(income - expenses)],
        backgroundColor: color2,
        borderWidth: 0.01,
      },
    ],
  };

  return (
    <HorizontalBar
      className="container-fluid"
      width="70%"
      height="8px"
      data={data}
    />
  );
}
