import React, { useState, useEffect, useRef } from "react";
import { Form, Button, Row, Col, Card } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
import moment from "moment";
// import moment from 'moment'

export default function editCategory() {
  // const{user} = useContext(UserContext)
  // Form input state hooks

  const [type, setType] = useState("");
  const [categories, setCategories] = useState([]);
  const [catOnType, setCatOnType] = useState([]);
  const [categoryId, setCategoryId] = useState("");

  const [oldName, setOldName] = useState("");
  const [oldBudget, setOldBudget] = useState(0);

  const [newName, setNewName] = useState("");
  const [newBudget, setNewBudget] = useState("");

  const [isActive, setIsActive] = useState(false);

  // Validate form input whenever email, password1, or password2 is changed
  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCategories(data.categories);
      });
  }, []);

  useEffect(() => {
    setCategoryId("");
    if (categories.length > 0) {
      setCatOnType(categories.filter((category) => category.type == type));
    } else {
      setCatOnType([]);
    }
    if (type == "income") {
      setNewBudget(0);
    }
  }, [type]);

  useEffect(() => {
    if (categoryId == "") {
      setOldBudget(0);
    } else {
      let foundCategory = catOnType.find(
        (category) => category._id == categoryId
      );
      setOldBudget(foundCategory.budget);
      setOldName(foundCategory.name);
    }
  }, [categoryId]);

  // useEffect(() => {

  //     if(type == "expense" ){
  //         setAmountToSave(0-Math.abs(amount))
  //     }else{
  //         setAmountToSave(Math.abs(amount))
  //     }

  // }, [type,amount])

  useEffect(() => {
    if (
      type !== "" &&
      categoryId !== "" &&
      newName !== "" &&
      newBudget >= 0 &&
      !(newName == oldName && newBudget == oldBudget)
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [type, categoryId, newName, newBudget]);

  function editCategory() {
    fetch(`http://localhost:4000/api/users/editCategory`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        categoryId: categoryId,
        type: type,
        name: newName,
        budget: newBudget,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data == true) {
          Router.push(".");
        } else {
          Swal.fire("Error", `Something went wrong`, "error");
        }
      });
  }

  function checkDuplicate(e) {
    e.preventDefault();
    if (newName.toLowerCase() == oldName.toLowerCase()) {
      editCategory();
    } else {
      fetch(`http://localhost:4000/api/users/duplicateCategory`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          type: type,
          name: newName,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data == true) {
            Swal.fire("Duplicate", `Same Category already exist`, "error");
          } else if (data == false) {
            editCategory();
          } else {
            Swal.fire("Error", `Something went wrong`, "error");
          }
        });
    }
  }

  return (
    <React.Fragment>
      <Head>
        <title>Edit Category</title>
      </Head>
      <Form onSubmit={(e) => checkDuplicate(e)}>
        <Card>
          <Card.Header>
            <h5>To edit</h5>
          </Card.Header>
          <Card.Body>
            <Form.Group controlId="type">
              <Form.Label>Category Type</Form.Label>
              <Form.Control
                as="select"
                onChange={(e) => {
                  setType(e.target.value);
                }}
                required
              >
                <option value="" disabled selected>
                  Select Category Type
                </option>
                <option value="income">Income</option>
                <option value="expense">Expense</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="categoryId">
              <Form.Label>Category Name</Form.Label>
              <Form.Control
                as="select"
                value={categoryId}
                onChange={(e) => setCategoryId(e.target.value)}
                required
              >
                <option value="" disabled selected>
                  Select Category Type
                </option>
                {catOnType.length > 0
                  ? catOnType.map((category) => {
                      return (
                        <option value={category._id}>{category.name}</option>
                      );
                    })
                  : null}
              </Form.Control>
            </Form.Group>
            {type == "expense" ? (
              <Form.Group controlId="budget">
                <Form.Label>Budget</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Insert Budget"
                  value={oldBudget}
                  onChange={(e) => setOldBudget(e.target.value)}
                  disabled
                />
              </Form.Group>
            ) : null}
          </Card.Body>
        </Card>

        <Card className="my-5">
          <Card.Header>
            <h5>New Details</h5>
          </Card.Header>
          <Card.Body>
            <Form.Group controlId="type">
              <Form.Label>Category Type</Form.Label>
              <Form.Control
                type="string"
                value={type != "" ? type[0].toUpperCase() + type.slice(1) : ""}
                disabled
              />
            </Form.Group>

            <Form.Group controlId="newName">
              <Form.Label>New Category Name</Form.Label>
              <Form.Control
                type="category"
                placeholder="Enter Category Name"
                value={newName}
                onChange={(e) => setNewName(e.target.value)}
                required
              />
            </Form.Group>
            {type == "expense" ? (
              <Form.Group controlId="newBudget">
                <Form.Label>New Budget</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Insert Budget"
                  value={newBudget}
                  onChange={(e) => setNewBudget(e.target.value)}
                  required
                />
              </Form.Group>
            ) : null}
          </Card.Body>

          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Card>
      </Form>
    </React.Fragment>
  );
}
