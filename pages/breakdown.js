import React, { useEffect, useContext, useState } from "react";
import Head from "next/head";
import { Container, Form, Col, Row, Card, Nav, Button } from "react-bootstrap";
import styles from "../styles/Home.module.css";
import UserContext from "../UserContext";
import Login from "./login";
import Category from "./category";
import moment from "moment";
import sumByGroup from "../helpers/sumByGroup";
import PieChart from "../components/PieChart";
import StackedBar from "../components/StackedBar";

export default function Breakdown() {
  const { user, setUser } = useContext(UserContext);
  //States Set after fetching user details
  const [records, setRecords] = useState([]);
  const [incomeCategories, setIncCat] = useState([]);
  const [expensesCategories, setExpCat] = useState([]);

  //States for filtering Records by Date
  const [startDate, setStartDate] = useState(
    moment(user.startDate).startOf("days").format("yyyy-MM-DD")
  );
  const [endDate, setEndDate] = useState(
    moment(user.endDate).startOf("days").format("yyyy-MM-DD")
  );

  //States for Data Manipulation
  const [totalIncome, setTotalIncome] = useState(0);
  const [totalExpenses, setTotalExpenses] = useState(0);
  const [incomeRecords, setIncRec] = useState([]);
  const [expensesRecords, setExpRec] = useState([]);
  const [categoriesType, setCatType] = useState([]);
  const [recordsType, setRecType] = useState([]);
  const [recordsName, setRecName] = useState([]);
  const [categoriesRecords, setCatRec] = useState([]);
  const [budgetArray, setBudgetArray] = useState([]);

  //States for Chart filter
  const [type, setType] = useState("expense");
  const [catOption, setCatOption] = useState("all");
  const [categoryId, setCategoryId] = useState("all");
  const [budgetOption, setBudgetOption] = useState("all");
  const [budgetId, setBudgetId] = useState("all");

  // States for Chart Data
  const [categoryPie, setCatPie] = useState([]);
  const [recordDoughnut, setRecDoughnut] = useState([]);
  const [budgetPie, setBudgetPie] = useState([]);
  const [budgetDoughnut, setBudgetDoughnut] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setRecords(data.records);

        let filteredInc = data.categories.filter((category) => {
          return category.type == "income";
        });

        setIncCat(filteredInc);

        let filteredExp = data.categories.filter((category) => {
          return category.type == "expense";
        });
        setExpCat(filteredExp);

        setStartDate(
          moment(data.currentDate.startDate)
            .startOf("days")
            .format("yyyy-MM-DD")
        );
        setEndDate(
          moment(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD")
        );
      });
  }, []);

  useEffect(() => {
    if (startDate <= endDate) {
      let dateBefore = moment(startDate).subtract(1, "day");
      let dateAfter = moment(endDate).add(1, "day");

      let filteredRecords = records.filter((record) => {
        let updatedOn = moment(record.updatedOn).startOf("days");
        return moment(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });

      setIncRec(filteredRecords.filter((record) => record.amount >= 0));
      setExpRec(filteredRecords.filter((record) => record.amount < 0));

      setBudgetPie(expensesCategories);
      setBudgetOption(
        expensesCategories.map((category) => {
          return <option value={category._id}>{category.name}</option>;
        })
      );
    }
  }, [startDate, endDate, records, incomeCategories, expensesCategories]);

  useEffect(() => {
    setCatRec(
      sumByGroup(
        expensesCategories,
        "_id",
        expensesRecords,
        "categoryId",
        "amount"
      )
    );
  }, [startDate, endDate, expensesCategories, expensesRecords]);

  useEffect(() => {
    if (budgetId == "all") {
      setBudgetArray(categoriesRecords);
    } else if (budgetId == "budgetOnly") {
      setBudgetArray(
        categoriesRecords.filter((category) => category.budget > 0)
      );
    } else if (budgetId == "expensesOnly") {
      setBudgetArray(
        categoriesRecords.filter((category) => category.amount < 0)
      );
    } else if (budgetId == "budgetExpenses") {
      setBudgetArray(
        categoriesRecords.filter(
          (category) => category.amount < 0 && category.budget > 0
        )
      );
    } else {
      setBudgetArray(
        categoriesRecords.filter((category) => category._id == budgetId)
      );
    }
  }, [categoriesRecords, budgetId, startDate, endDate]);

  useEffect(() => {
    console.log(budgetId);
    console.log(budgetArray);
    let budget = budgetArray.reduce((total, budget) => {
      return total + budget.budget;
    }, 0);
    let expenses = budgetArray.reduce((total, budget) => {
      return total - budget.amount;
    }, 0);
    let savings = budget - expenses;

    let budgetSummarry = [
      {
        label: "consumption",
        amount: expenses,
        color: "#f56f36",
      },
    ];

    if (savings >= 0) {
      setBudgetDoughnut([
        {
          label: "consumption",
          amount: expenses,
          color: "#f56f36",
        },
        {
          label: "savings",
          amount: savings,
          color: "#47b83d",
        },
      ]);
    } else {
      setBudgetDoughnut([
        {
          label: "consumption",
          amount: budget,
          color: "#f56f36",
        },
        {
          label: "overspent",
          amount: Math.abs(savings),
          color: "#e30707",
        },
      ]);
    }
  }, [budgetArray, budgetId, startDate, endDate]);

  useEffect(() => {
    setTotalIncome(
      incomeRecords.reduce((total, record) => {
        return total + record.amount;
      }, 0)
    );
    setTotalExpenses(
      expensesRecords.reduce((total, record) => {
        return total - record.amount;
      }, 0)
    );
  }, [startDate, endDate, incomeRecords, expensesRecords]);

  useEffect(() => {
    if (type == "income") {
      setCatType(incomeCategories);
      setRecType(incomeRecords);
    } else {
      setCatType(expensesCategories);
      setRecType(expensesRecords);
    }
    setCategoryId("all");
  }, [type, records, incomeCategories, expensesCategories, startDate, endDate]);

  useEffect(() => {
    setCatPie(
      sumByGroup(categoriesType, "_id", recordsType, "categoryId", "amount")
    );
    setCatOption(
      categoriesType.map((category) => {
        return <option value={category._id}>{category.name}</option>;
      })
    );
  }, [type, categoriesType, recordsType, startDate, endDate]);

  useEffect(() => {
    if (categoryId == "all") {
      setRecName(recordsType);
    } else {
      setRecName(
        recordsType.filter((record) => record.categoryId == categoryId)
      );
    }
  }, [type, categoryId, categoriesType, recordsType, startDate, endDate]);

  useEffect(() => {
    setRecDoughnut(sumByGroup([], "", recordsName, "description", "amount"));
  }, [
    categoryId,
    categoriesType,
    recordsType,
    recordsName,
    startDate,
    endDate,
  ]);

  return (
    <React.Fragment>
      <Head>
        <title>Monthly Trend</title>
      </Head>
      <Form>
        <Row className="justify-content-md-center my-2">
          <Col md="auto">
            <Form.Label>Date:</Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              dateformat="yyyy-MM-DD"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
            />
          </Col>
          <Col md="auto">
            <Form.Label> to </Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              value={endDate}
              dateformat="myyyy-MM-DD"
              onChange={(e) => setEndDate(e.target.value)}
            />
          </Col>
        </Row>
      </Form>
      <Row className="justify-content-md-center my-2">
        <h5>Income Consumption</h5>
        <div className="container-fluid stackedBar">
          <StackedBar income={totalIncome} expenses={totalExpenses} />
        </div>
      </Row>
      <Row className="justify-content-md-center my-2">
        <Col>
          <Card className="m-2">
            <Card.Header>
              <Nav variant="tabs">
                <Nav.Item vlye="income">
                  <Nav.Link onClick={(e) => setType("income")}>Income</Nav.Link>
                </Nav.Item>
                <Nav.Item value="expense">
                  <Nav.Link onClick={(e) => setType("expenses")}>
                    Expenses
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Card.Header>
            <Card.Body>
              <h5 className="text-center">
                {type[0].toUpperCase() + type.slice(1)} Categories{" "}
              </h5>
              <PieChart
                chartType="Pie"
                rawData={categoryPie}
                labelKey={"name"}
                amountKey={"amount"}
              />
            </Card.Body>
          </Card>
        </Col>

        <Col>
          <Card className="my-2">
            <Card.Header>
              <Form.Group controlId="categoryId" className="my-0">
                <Form.Control
                  as="select"
                  value={categoryId}
                  onChange={(e) => setCategoryId(e.target.value)}
                  required
                >
                  <option value="all" selected>
                    All
                  </option>
                  {catOption}
                </Form.Control>
              </Form.Group>
            </Card.Header>
            <Card.Body>
              <h5 className="text-center">
                {type[0].toUpperCase() + type.slice(1)} Categories{" "}
              </h5>
              <PieChart
                chartType="Doughnut"
                rawData={recordDoughnut}
                labelKey={"description"}
                amountKey={"amount"}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Row className="justify-content-md-center my-2">
        <Col>
          <Card className="m-2">
            <Card.Header>
              <Nav variant="tabs">
                <Nav.Item vlye="income">
                  <Nav.Link>Budget</Nav.Link>
                </Nav.Item>
              </Nav>
            </Card.Header>
            <Card.Body>
              <h5 className="text-center">Budget Categories</h5>
              <PieChart
                chartType="Pie"
                rawData={budgetPie}
                labelKey={"name"}
                amountKey={"budget"}
              />
            </Card.Body>
          </Card>
        </Col>

        <Col>
          <Card className="m-2">
            <Card.Header>
              <Form.Group controlId="categoryId" className="my-0">
                <Form.Control
                  as="select"
                  value={budgetId}
                  onChange={(e) => setBudgetId(e.target.value)}
                  required
                >
                  <option value="all" selected>
                    All
                  </option>
                  <option value="budgetOnly" selected>
                    With Budget Only
                  </option>
                  <option value="expensesOnly" selected>
                    With Expenses Only
                  </option>
                  <option value="budgetExpenses" selected>
                    With Both Budget & Expesenses Only
                  </option>
                  {budgetOption}
                </Form.Control>
              </Form.Group>
            </Card.Header>
            <Card.Body>
              <h5 className="text-center">Budget Consumption </h5>
              <PieChart
                chartType="Doughnut"
                rawData={budgetDoughnut}
                labelKey={"label"}
                amountKey={"amount"}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
}
