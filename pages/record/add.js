import React, { useState, useEffect, useRef } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
import moment from "moment";
// import moment from 'moment'

export default function newCategory() {
  // const{user} = useContext(UserContext)
  // Form input state hooks
  const [type, setType] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [description, setDescription] = useState("");
  const [amount, setAmount] = useState(0);
  const [amountToSave, setAmountToSave] = useState(0);
  const [categories, setCategories] = useState([]);
  const [catOption, setCatOption] = useState(null);
  const [isActive, setIsActive] = useState(false);
  const [recordDate, setRecordDate] = useState(moment().format("YYYY-MM-DD"));
  const [recordTime, setRecordTime] = useState(moment().format("HH:mm"));

  // Validate form input whenever email, password1, or password2 is changed
  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCategories(data.categories);
      });
  }, []);

  useEffect(() => {
    let categoryList = categories.filter((category) => category.type == type);

    let catOnType = categoryList.map((category) => {
      return <option value={category._id}>{category.name}</option>;
    });

    console.log(catOnType);
    setCatOption(catOnType);
    setCategoryId("");
  }, [type]);

  useEffect(() => {
    if (type == "expense") {
      setAmountToSave(0 - Math.abs(amount));
    } else {
      setAmountToSave(Math.abs(amount));
    }
  }, [type, amount]);

  useEffect(() => {
    if (type !== "" && categoryId !== "" && description !== "" && amount > 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [type, categoryId, description, amount]);

  function addRecord(e) {
    e.preventDefault();
    console.log(categoryId);
    console.log(description);
    console.log(amount);
    let updatedOn = moment(recordDate + " " + recordTime);
    console.log(updatedOn);
    fetch(`http://localhost:4000/api/users/addRecord`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        categoryId: categoryId,
        description: description,
        amount: amountToSave,
        updatedOn: updatedOn,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data == true) {
          Router.push(".");
        } else {
          Swal.fire("Error", `Something went wrong`, "error");
        }
      });
  }

  return (
    <React.Fragment>
      <Head>
        <title>Add Record</title>
      </Head>
      <Form onSubmit={(e) => addRecord(e)}>
        <Form.Group controlId="type">
          <Form.Label>Category Type</Form.Label>
          <Form.Control
            as="select"
            onChange={(e) => {
              setType(e.target.value);
            }}
            required
          >
            <option value="" disabled selected>
              Select Category Type
            </option>
            <option value="income">Income</option>
            <option value="expense">Expense</option>
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="categoryId">
          <Form.Label>Category Name</Form.Label>
          <Form.Control
            as="select"
            value={categoryId}
            onChange={(e) => setCategoryId(e.target.value)}
            required
          >
            <option value="" disabled selected>
              Select Category Type
            </option>
            {catOption}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="string"
            placeholder="Enter Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="amount">
          <Form.Label>Amount</Form.Label>
          <Form.Control
            type="number"
            placeholder="Insert Amount"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="timestamp">
          <Form.Label>Date</Form.Label>
          <Row>
            <Col>
              <Form.Control
                type="date"
                value={recordDate}
                onChange={(e) => setRecordDate(e.target.value)}
              />
            </Col>
            <Col>
              <Form.Control
                type="time"
                value={recordTime}
                onChange={(e) => setRecordTime(e.target.value)}
              />
            </Col>
          </Row>
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button variant="primary" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </Form>
    </React.Fragment>
  );
}
