import React, { useState, useEffect } from "react";
import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "react-bootstrap";
import NavBar from "../components/NavBar";
import { UserProvider } from "../UserContext";

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({ id: null });

  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id) {
          //JWT validated
          setUser({
            id: data._id,
          });
        } else {
          //JWT is invalid or non-existent
          setUser({
            id: null,
          });
        }
      });
  }, [user.id]);

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
    });
  };

  return (
    <React.Fragment>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <NavBar />
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </React.Fragment>
  );
}

export default MyApp;
