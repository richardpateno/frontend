import React, { useState, useEffect, useContext } from "react";
import { Form, Button, Card, Row, Col } from "react-bootstrap";
import { GoogleLogin } from "react-google-login";
import Swal from "sweetalert2";
import Router from "next/router";
import Head from "next/head";
import UserContext from "../UserContext";
import View from "../components/View";
import AppHelper from "../app-helper";

export default function login() {
  return (
    <View title={"Login"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <LoginForm />
        </Col>
      </Row>
    </View>
  );
}

const LoginForm = () => {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    fetch(`http://localhost:4000/api/users/login`, options)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          setUser({ id: data._id });
          Router.push("/");
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authentication Failed", "User does not exist.", "error");
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure, try an alternative login procedure",
              "error"
            );
          } else if (data.error === "incorrect-password") {
            Swal.fire(
              "Authentication Failed",
              "password is incorrect",
              "error"
            );
          }
        }
      });
  }

  const aunthenticateGoogleToken = (response) => {
    const payload = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ tokenId: response.tokenId }),
    };

    fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload)
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          setUser({ id: data._id });
          Router.push("/");
        } else {
          if ((data.error = "google-auth-error")) {
            Swal.fire(
              "Google Auth Error",
              "Google authentication procedure failed.",
              "error"
            );
          } else if ((data.error = "login-type-error")) {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure.",
              "error"
            );
          }
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Card>
      <Card.Header>Login details</Card.Header>
      <Card.Body>
        <Head>
          <title>Budget Tracker</title>
        </Head>
        <Form onSubmit={(e) => authenticate(e)}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Row className="justify-content-center px-3">
            {isActive ? (
              <Button className="bg-primary btn-block" type="submit">
                Submit
              </Button>
            ) : (
              <Button className="bg-primary btn-block" type="submit" disabled>
                Submit
              </Button>
            )}
          </Row>
        </Form>

        <GoogleLogin
          clientId="656593662569-24gufu44evj4ujqs3k0113rgrurq2bnp.apps.googleusercontent.com"
          buttonText="Login"
          onSuccess={aunthenticateGoogleToken}
          onFailure={aunthenticateGoogleToken}
          cookiePolicy={"single_host_origin"}
          className="w-100 text-center d-flex justify-content-center"
        />
      </Card.Body>
    </Card>
  );
};
