import React, { useEffect, useContext, useState } from "react";
import Head from "next/head";
import { Container, Form, Col, Row, Card, Nav, Button } from "react-bootstrap";
import styles from "../styles/Home.module.css";
import UserContext from "../UserContext";
import Login from "./login";
import Category from "./category";
import moment from "moment";
import sumByGroup from "../helpers/sumByGroup";
import PieChart from "../components/PieChart";

export default function Breakdown() {
  const { user, setUser } = useContext(UserContext);

  //States Set after fetching user details
  const [categories, setCategories] = useState([]);
  const [records, setRecords] = useState([]);
  const [incomeCategories, setIncCat] = useState([]);
  const [expensesCategories, setExpCat] = useState([]);

  //States for filtering Records by Date
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  //States for filtering Records by Date & Category Type
  const [incomeRecords, setIncRec] = useState([]);
  const [expensesRecords, setExpRec] = useState([]);

  //States for Chart filter
  // const [categoryType, setCategoryType] = useState("expense")
  const [categoryId, setCategoryId] = useState("");

  // States for Chart Data
  const [incomePie, setIncomePie] = useState([]);
  const [expensesPie, setExpensesPie] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setRecords(data.records);
        setCategories(data.categories);
        setStartDate(moment(data.currentDate.startDate).format("yyyy-MM-DD"));
        setEndDate(moment(data.currentDate.endDate).format("yyyy-MM-DD"));

        let filteredInc = data.categories.filter((category) => {
          return category.type == "income";
        });

        setIncCat(filteredInc);

        let filteredExp = data.categories.filter((category) => {
          return category.type == "expense";
        });
        setExpCat(filteredExp);
      });
  }, []);

  useEffect(() => {
    if (startDate <= endDate) {
      let filteredRecords = records.filter((record) => {
        return moment(record.updatedOn).isBetween(
          startDate,
          moment(endDate).add(86399, "seconds")
        );
      });

      setIncRec(filteredRecords.filter((record) => record.amount >= 0));
      setExpRec(filteredRecords.filter((record) => record.amount < 0));
    }
  }, [startDate, endDate, records]);

  useEffect(() => {
    setIncomePie(
      sumByGroup(incomeCategories, "_id", incomeRecords, "categoryId")
    );
    setExpensesPie(
      sumByGroup(expensesCategories, "_id", expensesRecords, "categoryId")
    );
  }, [incomeRecords, expensesRecords, incomeCategories, expensesCategories]);

  return (
    <React.Fragment>
      <Head>
        <title>Monthly Trend</title>
      </Head>
      <Form>
        <Row className="justify-content-md-center m-2">
          <Col md="auto">
            <Form.Label>Date:</Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
            />
          </Col>
          <Col md="auto">
            <Form.Label> to </Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
            />
          </Col>
        </Row>
      </Form>

      <Row className="justify-content-md-center my-5">
        <Col>
          <div className="m-1 text-center">
            <h5>Income Categories</h5>
            <PieChart chartType="Pie" rawData={incomePie} labelKey={"name"} />
          </div>
        </Col>
        <Col>
          <div className="m-1 text-center">
            <h5>Expense Categories</h5>
            <PieChart chartType="Pie" rawData={expensesPie} labelKey={"name"} />
          </div>
        </Col>
      </Row>
      <Row className="justify-content-md-center my-5">
        <Col>
          <div className="m-1 text-center">
            <h5>Income Categories</h5>
            <PieChart chartType="Pie" rawData={incomePie} labelKey={"name"} />
          </div>
        </Col>
        <Col>
          <div className="m-1 text-center">
            <h5>Expense Categories</h5>
            <PieChart chartType="Pie" rawData={expensesPie} labelKey={"name"} />
          </div>
        </Col>
      </Row>
    </React.Fragment>
  );
}
