import React, { useEffect, useState, useContext } from "react";
import Head from "next/head";
import {
  Container,
  Form,
  Row,
  Col,
  Button,
  Jumbotron,
  Card,
  Alert,
} from "react-bootstrap";
import styles from "../styles/Home.module.css";
import UserContext from "../UserContext";
import Login from "./login";
import Category from "./category";
import moment from "moment";
import PieChart from "../components/PieChart";
import Router from "next/router";

export default function Home() {
  const { user, setUser } = useContext(UserContext);
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [records, setRecords] = useState([]);
  const [categories, setCategories] = useState([]);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [savedStart, setSavedStart] = useState("");
  const [savedEnd, setSavedEnd] = useState("");
  const [currentRecords, setCurrentRecords] = useState([]);
  const [oldRecords, setOldRecords] = useState([]);
  const [previousSavings, setPreviousSavings] = useState(0);
  const [currentIncome, setCurrentIncome] = useState(0);
  const [currentExpenses, setCurrentExpenses] = useState(0);
  const [budget, setBudget] = useState(0);
  const [actualPie, setActualPie] = useState([]);
  const [budgetPie, setBudgetPie] = useState([]);

  useEffect(() => {
    if (user.id != null) {
      fetch(`http://localhost:4000/api/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setStartDate(
            moment(data.currentDate.startDate)
              .startOf("days")
              .format("yyyy-MM-DD")
          );
          setEndDate(
            moment(data.currentDate.endDate)
              .startOf("days")
              .format("yyyy-MM-DD")
          );
          setSavedStart(
            moment(data.currentDate.startDate)
              .startOf("days")
              .format("yyyy-MM-DD")
          );
          setSavedEnd(
            moment(data.currentDate.endDate)
              .startOf("days")
              .format("yyyy-MM-DD")
          );
          setName(`${data.firstName} ${data.lastName}`);
          setEmail(data.email);
          setRecords(data.records);
          setCategories(data.categories);
        });
    }
  }, [user]);

  useEffect(() => {
    if ((name != "") & (email != "")) {
      setLoading(false);
    }
  }, [name, email]);

  useEffect(() => {
    let dateBefore = moment(startDate).subtract(1, "day");
    let dateAfter = moment(endDate).add(1, "day");

    if (startDate <= endDate) {
      if (records.length > 0) {
        setCurrentRecords(
          records.filter((record) => {
            let updatedOn = moment(record.updatedOn).startOf("days");
            return moment(updatedOn).isBetween(dateBefore, dateAfter, "day");
          })
        );

        setOldRecords(
          records.filter((record) => {
            let updatedOn = moment(record.updatedOn).startOf("days");
            return moment(updatedOn).isBefore(dateBefore, "day");
          })
        );
      }
    } else {
      setCurrentRecords([]);
      setOldRecords([]);
    }

    if (categories.length > 0) {
      setBudget(
        categories.reduce((total, category) => {
          return total + category.budget;
        }, 0)
      );
    } else {
      setBudget(0);
    }
  }, [categories, startDate, endDate]);

  useEffect(() => {
    if (oldRecords.length > 0) {
      setPreviousSavings(
        oldRecords.reduce((total, record) => {
          return total + record.amount;
        }, 0)
      );
    } else {
      setPreviousSavings(0);
    }
  }, [oldRecords]);

  useEffect(() => {
    if (currentRecords.length > 0) {
      let income = currentRecords.filter((record) => record.amount > 0);
      console.log(income);
      if (income.length > 0) {
        setCurrentIncome(
          income.reduce((total, record) => {
            return total + record.amount;
          }, 0)
        );
      } else {
        setCurrentIncome(0);
      }

      let expenses = currentRecords.filter((record) => record.amount < 0);
      if (expenses.length > 0) {
        setCurrentExpenses(
          expenses.reduce((total, record) => {
            return total - record.amount;
          }, 0)
        );
      } else {
        setCurrentExpenses(0);
      }
    } else {
      setCurrentIncome(0);
      setCurrentExpenses(0);
    }
  }, [currentRecords]);

  useEffect(() => {
    console.log(previousSavings);
    console.log(currentIncome);
    console.log(currentExpenses);
    if (previousSavings >= 0) {
      if (currentIncome > currentExpenses) {
        setActualPie([
          {
            label: "Current Savings",
            amount: currentIncome - currentExpenses,
            color: "#47b83d",
          },
          {
            label: "Current Expenses",
            amount: currentExpenses,
            color: "#f56f36",
          },
          {
            label: "Previous Savings",
            amount: previousSavings,
            color: "#0f5209",
          },
        ]);
      } else {
        if (currentExpenses < currentIncome + previousSavings) {
          setActualPie([
            {
              label: "Accumulated Savings",
              amount: currentIncome + previousSavings - currentExpenses,
              color: "#0f5209",
            },
            {
              label: "Current Expenses",
              amount: currentExpenses,
              color: "#f56f36",
            },
          ]);
        } else {
          setActualPie([
            {
              label: "Accumulated Deficit",
              amount: currentIncome + previousSavings - currentExpenses,
              color: "#e30707",
            },
            {
              label: "Consumed Savings and Income",
              amount: currentIncome + previousSavings,
              color: "#0f5209",
            },
          ]);
        }
      }
    } else {
      if (currentIncome > Math.abs(previousSavings) + currentExpenses) {
        setActualPie([
          {
            label: "Accumulated Savings",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#47b83d",
          },
          {
            label: "Previous Deficit + Current Expenses",
            amount: Math.abs(previousSavings) + currentExpenses,
            color: "#f56f36",
          },
        ]);
      } else {
        setActualPie([
          {
            label: "Consumed Income",
            amount: currentIncome,
            color: "#f56f36",
          },
          {
            label: "Accumulated Deficit",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#e30707",
          },
        ]);
      }
    }
  }, [previousSavings, currentIncome, currentExpenses]);

  useEffect(() => {
    if (budget >= currentExpenses) {
      setBudgetPie([
        {
          label: "Savings",
          amount: budget - currentExpenses,
          color: "#47b83d",
        },
        {
          label: "Expenses",
          amount: currentExpenses,
          color: "#f56f36",
        },
      ]);
    } else {
      setBudgetPie([
        {
          label: "Consumed Budget",
          amount: budget,
          color: "#f56f36",
        },
        {
          label: "Deficit",
          amount: Math.abs(budget - currentExpenses),
          color: "#e30707",
        },
      ]);
    }
  }, [budget, currentExpenses]);

  function saveDate(e) {
    console.log(e);
    e.preventDefault();

    fetch(`http://localhost:4000/api/users/setDate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        startDate: moment(startDate).startOf("day"),
        endDate: moment(endDate).startOf("day"),
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data == false) {
          Swal.fire("Error", `Something went wrong`, "error");
        } else {
          console.log(data);
          setSavedStart(
            moment(data.startDate).startOf("day").format("yyyy-MM-DD")
          );
          setSavedEnd(moment(data.endDate).startOf("day").format("yyyy-MM-DD"));
          setStartDate(
            moment(data.startDate).startOf("day").format("yyyy-MM-DD")
          );
          setEndDate(moment(data.endDate).startOf("day").format("yyyy-MM-DD"));
        }
      });
  }

  return (
    <React.Fragment>
      <Head>
        <title>Budget Tracker</title>
      </Head>

      {user.id !== null ? (
        <div>
          <Jumbotron className="justify-content-center">
            <h3>{name}</h3>
            <h3>{email}</h3>
            <h3>
              Balance: Php {currentIncome + previousSavings - currentExpenses}
            </h3>
          </Jumbotron>
          <Row className="justify-content-md-center my-2">
            <Form onSubmit={(e) => saveDate(e)}>
              <Row className="justify-content-md-center my-2">
                <Col md="auto">
                  <Form.Label>Budget Date:</Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    type="date"
                    dateformat="yyyy-MM-DD"
                    value={startDate}
                    onChange={(e) => setStartDate(e.target.value)}
                  />
                </Col>
                <Col md="auto">
                  <Form.Label> to </Form.Label>
                </Col>
                <Col>
                  <Form.Control
                    type="date"
                    value={endDate}
                    dateformat="myyyy-MM-DD"
                    onChange={(e) => setEndDate(e.target.value)}
                  />
                </Col>

                <Col>
                  {startDate != savedStart || endDate != savedEnd ? (
                    <Button variant="primary" type="submit" id="submitBtn">
                      Set as Default
                    </Button>
                  ) : (
                    <Button
                      variant="primary"
                      type="submit"
                      id="submitBtn"
                      disabled
                    >
                      Set as Default
                    </Button>
                  )}
                </Col>
              </Row>
            </Form>
          </Row>
          <Row>
            <Col>
              <Card className="homepageCard">
                <div>
                  {actualPie.label ? (
                    <Alert>No Records</Alert>
                  ) : (
                    <PieChart
                      chartType="Pie"
                      rawData={actualPie}
                      labelKey={"label"}
                      amountKey={"amount"}
                    />
                  )}
                </div>
                <Card.Body>
                  <div className="homepagePie">
                    <h5>Current Income: Php {currentIncome}</h5>
                    <h5>Current Expenses: Php {currentExpenses}</h5>
                    {currentIncome >= currentExpenses ? (
                      <h5>
                        Current Savings: Php {currentIncome - currentExpenses}
                      </h5>
                    ) : (
                      <h5>
                        Current Deficit: Php {currentIncome - currentExpenses}{" "}
                      </h5>
                    )}
                    {previousSavings >= 0 ? (
                      <h5>Previous Savings: Php {previousSavings}</h5>
                    ) : (
                      <h5>Previous Deficit: Php {previousSavings}</h5>
                    )}
                  </div>
                </Card.Body>
              </Card>
            </Col>
            <Col>
              <Card className="homepageCard">
                <div>
                  {actualPie.label ? (
                    <Alert>No Records</Alert>
                  ) : (
                    <PieChart
                      chartType="Pie"
                      rawData={budgetPie}
                      labelKey={"label"}
                      amountKey={"amount"}
                    />
                  )}
                </div>
                <Card.Body>
                  <div className="homepagePie">
                    <h5>Budget: Php {budget}</h5>
                    <h5>Expenses: Php {currentExpenses}</h5>
                    {budget >= currentExpenses ? (
                      <h5>Savings: Php {budget - currentExpenses}</h5>
                    ) : (
                      <h5>Deficit: Php {budget - currentExpenses} </h5>
                    )}
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          <Row className="justify-content-md-center my-2">
            <Col className="justify-content-md-center my-2">
              <Button
                className="pull-right"
                size="lg"
                variant="success"
                onClick={() => Router.push("./category/add")}
              >
                Add Category
              </Button>
            </Col>
            <Col>
              <Button
                className="pull-right"
                size="lg"
                variant="secondary"
                onClick={() => Router.push("./record/add")}
              >
                Add Record
              </Button>
            </Col>
          </Row>
        </div>
      ) : (
        <Login />
      )}
    </React.Fragment>
  );
}
