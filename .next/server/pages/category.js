module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/category/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./helpers/sumByGroup.js":
/*!*******************************!*\
  !*** ./helpers/sumByGroup.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return sumByGroup; });
function sumByGroup(labelArray, labelKey, array, key, keySum) {
  let sumCounts = [];
  array.reduce(function (res, value) {
    if (!res[value[key]]) {
      res[value[key]] = {
        [key]: value[key],
        [keySum]: 0,
        count: 0
      };
      sumCounts.push(res[value[key]]);
    }

    res[value[key]][keySum] += value[keySum];
    res[value[key]].count += 1;
    return res;
  }, {});

  if (labelArray != [] & labelKey != "") {
    let labelSumCounts = labelArray.map(label => {
      let foundSumCount = sumCounts.find(sumCount => sumCount[key] == label[labelKey]);

      if (foundSumCount == undefined) {
        label[keySum] = 0;
        label.count = 0;
      } else {
        label[keySum] = foundSumCount[keySum];
        label.count = foundSumCount.count;
      }

      return label;
    });
    return labelSumCounts;
  } else {
    return sumCounts;
  }
}
;

/***/ }),

/***/ "./pages/category/index.js":
/*!*********************************!*\
  !*** ./pages/category/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../helpers/sumByGroup */ "./helpers/sumByGroup.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\category\\index.js";







function category() {
  const {
    0: categories,
    1: setCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: currentRecords,
    1: setCurRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeCategories,
    1: setIncCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: expensesCategories,
    1: setExpCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeRows,
    1: setIncomeRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const {
    0: expensesRows,
    1: setExpensesRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setCategories(data.categories);
      let dateBefore = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.startDate).subtract(1, "day");
      let dateAfter = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.endDate).add(1, "day");
      let filteredRecords = data.records.filter(record => {
        let updatedOn = moment__WEBPACK_IMPORTED_MODULE_6___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_6___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });
      setCurRec(filteredRecords);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let categoryRecords = Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__["default"])(categories, "_id", currentRecords, "categoryId", "amount");
    setIncCat(categoryRecords.filter(category => category.type == "income"));
    setExpCat(categoryRecords.filter(category => category.type == "expense"));
  }, [categories, currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setIncomeRows(incomeCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 11
      }, this);
    }));
  }, [incomeCategories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setExpensesRows(expensesCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: Math.abs(category.amount)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount + category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 11
      }, this);
    }));
  }, [expensesCategories]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: " Categories"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], {
      className: "justify-content-md-end my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "success",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Add Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "secondary",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/edit"),
        children: "Edit Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "danger",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Delete Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "info",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("../record/add"),
        children: "Add Record"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 111,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Income:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 7
    }, this), incomeCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 122,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 127,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Income"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 128,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: incomeRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 132,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 9
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Expenses:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 135,
      columnNumber: 7
    }, this), expensesCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories under expenses yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 142,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Budget"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Expenses"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Savings"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 141,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: expensesRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 149,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./styles/Home.module.css":
/*!********************************!*\
  !*** ./styles/Home.module.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Exports
module.exports = {
	"container": "Home_container__1EcsU",
	"main": "Home_main__1x8gC",
	"footer": "Home_footer__1WdhD",
	"title": "Home_title__3DjR7",
	"description": "Home_description__17Z4F",
	"code": "Home_code__axx2Y",
	"grid": "Home_grid__2Ei2F",
	"card": "Home_card__2SdtB",
	"logo": "Home_logo__1YbrH",
	"stackedBar": "Home_stackedBar__3UO1l",
	"homepageCard": "Home_homepageCard__1vkLN"
};


/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9zdW1CeUdyb3VwLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2NhdGVnb3J5L2luZGV4LmpzIiwid2VicGFjazovLy8uL3N0eWxlcy9Ib21lLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibW9tZW50XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9oZWFkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIl0sIm5hbWVzIjpbInN1bUJ5R3JvdXAiLCJsYWJlbEFycmF5IiwibGFiZWxLZXkiLCJhcnJheSIsImtleSIsImtleVN1bSIsInN1bUNvdW50cyIsInJlZHVjZSIsInJlcyIsInZhbHVlIiwiY291bnQiLCJwdXNoIiwibGFiZWxTdW1Db3VudHMiLCJtYXAiLCJsYWJlbCIsImZvdW5kU3VtQ291bnQiLCJmaW5kIiwic3VtQ291bnQiLCJ1bmRlZmluZWQiLCJjYXRlZ29yeSIsImNhdGVnb3JpZXMiLCJzZXRDYXRlZ29yaWVzIiwidXNlU3RhdGUiLCJjdXJyZW50UmVjb3JkcyIsInNldEN1clJlYyIsImluY29tZUNhdGVnb3JpZXMiLCJzZXRJbmNDYXQiLCJleHBlbnNlc0NhdGVnb3JpZXMiLCJzZXRFeHBDYXQiLCJpbmNvbWVSb3dzIiwic2V0SW5jb21lUm93cyIsImV4cGVuc2VzUm93cyIsInNldEV4cGVuc2VzUm93cyIsInVzZUVmZmVjdCIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwidGhlbiIsImpzb24iLCJkYXRhIiwiZGF0ZUJlZm9yZSIsIm1vbWVudCIsImN1cnJlbnREYXRlIiwic3RhcnREYXRlIiwic3VidHJhY3QiLCJkYXRlQWZ0ZXIiLCJlbmREYXRlIiwiYWRkIiwiZmlsdGVyZWRSZWNvcmRzIiwicmVjb3JkcyIsImZpbHRlciIsInJlY29yZCIsInVwZGF0ZWRPbiIsInN0YXJ0T2YiLCJpc0JldHdlZW4iLCJjYXRlZ29yeVJlY29yZHMiLCJ0eXBlIiwibmFtZSIsImFtb3VudCIsIl9pZCIsImJ1ZGdldCIsIk1hdGgiLCJhYnMiLCJSb3V0ZXIiLCJsZW5ndGgiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFBQTtBQUFlLFNBQVNBLFVBQVQsQ0FBb0JDLFVBQXBCLEVBQStCQyxRQUEvQixFQUF3Q0MsS0FBeEMsRUFBOENDLEdBQTlDLEVBQWtEQyxNQUFsRCxFQUF5RDtBQUd2RSxNQUFJQyxTQUFTLEdBQUMsRUFBZDtBQUVHSCxPQUFLLENBQUNJLE1BQU4sQ0FBYSxVQUFTQyxHQUFULEVBQWNDLEtBQWQsRUFBcUI7QUFDcEMsUUFBSSxDQUFDRCxHQUFHLENBQUNDLEtBQUssQ0FBRUwsR0FBRixDQUFOLENBQVIsRUFBdUI7QUFDdEJJLFNBQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBSCxHQUFtQjtBQUFFLFNBQUNBLEdBQUQsR0FBT0ssS0FBSyxDQUFFTCxHQUFGLENBQWQ7QUFBc0IsU0FBQ0MsTUFBRCxHQUFVLENBQWhDO0FBQW1DSyxhQUFLLEVBQUM7QUFBekMsT0FBbkI7QUFDQUosZUFBUyxDQUFDSyxJQUFWLENBQWVILEdBQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBbEI7QUFDQTs7QUFFREksT0FBRyxDQUFDQyxLQUFLLENBQUVMLEdBQUYsQ0FBTixDQUFILENBQWtCQyxNQUFsQixLQUE2QkksS0FBSyxDQUFFSixNQUFGLENBQWxDO0FBQ0FHLE9BQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBSCxDQUFpQk0sS0FBakIsSUFBMEIsQ0FBMUI7QUFDQSxXQUFPRixHQUFQO0FBQ0EsR0FURSxFQVNBLEVBVEE7O0FBYUgsTUFBR1AsVUFBVSxJQUFFLEVBQVosR0FBaUJDLFFBQVEsSUFBRyxFQUEvQixFQUFrQztBQUUzQixRQUFJVSxjQUFjLEdBQUdYLFVBQVUsQ0FBQ1ksR0FBWCxDQUFlQyxLQUFLLElBQUU7QUFFdkMsVUFBSUMsYUFBYSxHQUFHVCxTQUFTLENBQUNVLElBQVYsQ0FBZUMsUUFBUSxJQUFJQSxRQUFRLENBQUViLEdBQUYsQ0FBUixJQUFpQlUsS0FBSyxDQUFFWixRQUFGLENBQWpELENBQXBCOztBQUVBLFVBQUdhLGFBQWEsSUFBSUcsU0FBcEIsRUFBOEI7QUFDMUJKLGFBQUssQ0FBRVQsTUFBRixDQUFMLEdBQWlCLENBQWpCO0FBQ0FTLGFBQUssQ0FBQ0osS0FBTixHQUFjLENBQWQ7QUFDSCxPQUhELE1BR0s7QUFDREksYUFBSyxDQUFFVCxNQUFGLENBQUwsR0FBaUJVLGFBQWEsQ0FBRVYsTUFBRixDQUE5QjtBQUNBUyxhQUFLLENBQUNKLEtBQU4sR0FBY0ssYUFBYSxDQUFDTCxLQUE1QjtBQUNIOztBQUVELGFBQU9JLEtBQVA7QUFDSCxLQWJvQixDQUFyQjtBQWVBLFdBQU9GLGNBQVA7QUFDTixHQWxCRCxNQWtCSztBQUNKLFdBQU9OLFNBQVA7QUFDQTtBQUdEO0FBQUEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNhLFFBQVQsR0FBb0I7QUFDakMsUUFBTTtBQUFBLE9BQUNDLFVBQUQ7QUFBQSxPQUFhQztBQUFiLE1BQThCQyxzREFBUSxDQUFDLEVBQUQsQ0FBNUM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsY0FBRDtBQUFBLE9BQWlCQztBQUFqQixNQUE4QkYsc0RBQVEsQ0FBQyxFQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUNHLGdCQUFEO0FBQUEsT0FBbUJDO0FBQW5CLE1BQWdDSixzREFBUSxDQUFDLEVBQUQsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBQ0ssa0JBQUQ7QUFBQSxPQUFxQkM7QUFBckIsTUFBa0NOLHNEQUFRLENBQUMsRUFBRCxDQUFoRDtBQUNBLFFBQU07QUFBQSxPQUFDTyxVQUFEO0FBQUEsT0FBYUM7QUFBYixNQUE4QlIsc0RBQVEsQ0FBQyxJQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUNTLFlBQUQ7QUFBQSxPQUFlQztBQUFmLE1BQWtDVixzREFBUSxDQUFDLElBQUQsQ0FBaEQ7QUFFQVcseURBQVMsQ0FBQyxNQUFNO0FBQ2RDLFNBQUssQ0FBRSx5Q0FBRixFQUE0QztBQUMvQ0MsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRHNDLEtBQTVDLENBQUwsQ0FLR0MsSUFMSCxDQUtTL0IsR0FBRCxJQUFTQSxHQUFHLENBQUNnQyxJQUFKLEVBTGpCLEVBTUdELElBTkgsQ0FNU0UsSUFBRCxJQUFVO0FBQ2RwQixtQkFBYSxDQUFDb0IsSUFBSSxDQUFDckIsVUFBTixDQUFiO0FBRUEsVUFBSXNCLFVBQVUsR0FBR0MsNkNBQU0sQ0FBQ0YsSUFBSSxDQUFDRyxXQUFMLENBQWlCQyxTQUFsQixDQUFOLENBQW1DQyxRQUFuQyxDQUE0QyxDQUE1QyxFQUErQyxLQUEvQyxDQUFqQjtBQUNBLFVBQUlDLFNBQVMsR0FBR0osNkNBQU0sQ0FBQ0YsSUFBSSxDQUFDRyxXQUFMLENBQWlCSSxPQUFsQixDQUFOLENBQWlDQyxHQUFqQyxDQUFxQyxDQUFyQyxFQUF3QyxLQUF4QyxDQUFoQjtBQUVBLFVBQUlDLGVBQWUsR0FBR1QsSUFBSSxDQUFDVSxPQUFMLENBQWFDLE1BQWIsQ0FBcUJDLE1BQUQsSUFBWTtBQUNwRCxZQUFJQyxTQUFTLEdBQUdYLDZDQUFNLENBQUNVLE1BQU0sQ0FBQ0MsU0FBUixDQUFOLENBQXlCQyxPQUF6QixDQUFpQyxNQUFqQyxDQUFoQjtBQUNBLGVBQU9aLDZDQUFNLENBQUNXLFNBQUQsQ0FBTixDQUFrQkUsU0FBbEIsQ0FBNEJkLFVBQTVCLEVBQXdDSyxTQUF4QyxFQUFtRCxLQUFuRCxDQUFQO0FBQ0QsT0FIcUIsQ0FBdEI7QUFLQXZCLGVBQVMsQ0FBQzBCLGVBQUQsQ0FBVDtBQUNELEtBbEJIO0FBbUJELEdBcEJRLEVBb0JOLEVBcEJNLENBQVQ7QUFzQkFqQix5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJd0IsZUFBZSxHQUFHekQsbUVBQVUsQ0FDOUJvQixVQUQ4QixFQUU5QixLQUY4QixFQUc5QkcsY0FIOEIsRUFJOUIsWUFKOEIsRUFLOUIsUUFMOEIsQ0FBaEM7QUFRQUcsYUFBUyxDQUFDK0IsZUFBZSxDQUFDTCxNQUFoQixDQUF3QmpDLFFBQUQsSUFBY0EsUUFBUSxDQUFDdUMsSUFBVCxJQUFpQixRQUF0RCxDQUFELENBQVQ7QUFDQTlCLGFBQVMsQ0FBQzZCLGVBQWUsQ0FBQ0wsTUFBaEIsQ0FBd0JqQyxRQUFELElBQWNBLFFBQVEsQ0FBQ3VDLElBQVQsSUFBaUIsU0FBdEQsQ0FBRCxDQUFUO0FBQ0QsR0FYUSxFQVdOLENBQUN0QyxVQUFELEVBQWFHLGNBQWIsQ0FYTSxDQUFUO0FBYUFVLHlEQUFTLENBQUMsTUFBTTtBQUNkSCxpQkFBYSxDQUNYTCxnQkFBZ0IsQ0FBQ1osR0FBakIsQ0FBc0JNLFFBQUQsSUFBYztBQUNqQywwQkFDRTtBQUFBLGdDQUNFO0FBQUEsb0JBQUtBLFFBQVEsQ0FBQ3dDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFO0FBQUEsb0JBQUt4QyxRQUFRLENBQUN5QztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQSxTQUFTekMsUUFBUSxDQUFDMEMsR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGO0FBTUQsS0FQRCxDQURXLENBQWI7QUFVRCxHQVhRLEVBV04sQ0FBQ3BDLGdCQUFELENBWE0sQ0FBVDtBQWFBUSx5REFBUyxDQUFDLE1BQU07QUFDZEQsbUJBQWUsQ0FDYkwsa0JBQWtCLENBQUNkLEdBQW5CLENBQXdCTSxRQUFELElBQWM7QUFDbkMsMEJBQ0U7QUFBQSxnQ0FDRTtBQUFBLG9CQUFLQSxRQUFRLENBQUN3QztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRTtBQUFBLG9CQUFLeEMsUUFBUSxDQUFDMkM7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGLGVBR0U7QUFBQSxvQkFBS0MsSUFBSSxDQUFDQyxHQUFMLENBQVM3QyxRQUFRLENBQUN5QyxNQUFsQjtBQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSEYsZUFJRTtBQUFBLG9CQUFLekMsUUFBUSxDQUFDeUMsTUFBVCxHQUFrQnpDLFFBQVEsQ0FBQzJDO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSkY7QUFBQSxTQUFTM0MsUUFBUSxDQUFDMEMsR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGO0FBUUQsS0FURCxDQURhLENBQWY7QUFZRCxHQWJRLEVBYU4sQ0FBQ2xDLGtCQUFELENBYk0sQ0FBVDtBQWVBLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyw2QkFBZjtBQUFBLDhCQUNFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsU0FIVjtBQUlFLGVBQU8sRUFBRSxNQUFNc0Msa0RBQU0sQ0FBQ3RELElBQVAsQ0FBWSxnQkFBWixDQUpqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBU0UscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxXQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU1zRCxrREFBTSxDQUFDdEQsSUFBUCxDQUFZLGlCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBVEYsZUFpQkUscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxRQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU1zRCxrREFBTSxDQUFDdEQsSUFBUCxDQUFZLGdCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakJGLGVBMEJFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsTUFIVjtBQUlFLGVBQU8sRUFBRSxNQUFNc0Qsa0RBQU0sQ0FBQ3RELElBQVAsQ0FBWSxlQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBMUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUpGLGVBdUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBdkNGLEVBd0NHYyxnQkFBZ0IsQ0FBQ3lDLE1BQWpCLElBQTJCLENBQTNCLGdCQUNDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxFQUFDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkFHQyxxRUFBQyxxREFBRDtBQUFPLGFBQU8sTUFBZDtBQUFlLGNBQVEsTUFBdkI7QUFBd0IsV0FBSyxNQUE3QjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBQSxrQ0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVFFO0FBQUEsa0JBQVFyQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUEzQ0osZUFzREU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF0REYsRUF1REdGLGtCQUFrQixDQUFDdUMsTUFBbkIsSUFBNkIsQ0FBN0IsZ0JBQ0MscUVBQUMscURBQUQ7QUFBTyxhQUFPLEVBQUMsTUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURELGdCQUdDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxNQUFkO0FBQWUsY0FBUSxNQUF2QjtBQUF3QixXQUFLLE1BQTdCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRixlQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVVFO0FBQUEsa0JBQVFuQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUExREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEwRUQsQzs7Ozs7Ozs7Ozs7QUN6SkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDYkEsbUM7Ozs7Ozs7Ozs7O0FDQUEsc0M7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsNEM7Ozs7Ozs7Ozs7O0FDQUEsa0QiLCJmaWxlIjoicGFnZXMvY2F0ZWdvcnkuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3BhZ2VzL2NhdGVnb3J5L2luZGV4LmpzXCIpO1xuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gc3VtQnlHcm91cChsYWJlbEFycmF5LGxhYmVsS2V5LGFycmF5LGtleSxrZXlTdW0pe1xyXG5cclxuXHRcclxuXHRsZXQgc3VtQ291bnRzPVtdXHJcblxyXG4gICAgYXJyYXkucmVkdWNlKGZ1bmN0aW9uKHJlcywgdmFsdWUpIHtcclxuXHRcdGlmICghcmVzW3ZhbHVlLltrZXldXSkge1xyXG5cdFx0XHRyZXNbdmFsdWUuW2tleV1dID0geyBba2V5XTogdmFsdWUuW2tleV0sIFtrZXlTdW1dOiAwLCBjb3VudDowIH07XHJcblx0XHRcdHN1bUNvdW50cy5wdXNoKHJlc1t2YWx1ZS5ba2V5XV0pXHJcblx0XHR9XHJcblx0XHRcclxuXHRcdHJlc1t2YWx1ZS5ba2V5XV0uW2tleVN1bV0gKz0gdmFsdWUuW2tleVN1bV1cclxuXHRcdHJlc1t2YWx1ZS5ba2V5XV0uY291bnQgKz0gMVxyXG5cdFx0cmV0dXJuIHJlc1xyXG5cdH0sIHt9KVxyXG5cclxuXHJcblx0XHJcblx0aWYobGFiZWxBcnJheSE9W10gJiBsYWJlbEtleSE9IFwiXCIpe1xyXG4gICAgICAgIFxyXG4gICAgICAgIGxldCBsYWJlbFN1bUNvdW50cyA9IGxhYmVsQXJyYXkubWFwKGxhYmVsPT57XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIGxldCBmb3VuZFN1bUNvdW50ID0gc3VtQ291bnRzLmZpbmQoc3VtQ291bnQgPT4gc3VtQ291bnQuW2tleV09PSBsYWJlbC5bbGFiZWxLZXldKVxyXG5cclxuICAgICAgICAgICAgaWYoZm91bmRTdW1Db3VudCA9PSB1bmRlZmluZWQpe1xyXG4gICAgICAgICAgICAgICAgbGFiZWwuW2tleVN1bV0gPSAwXHJcbiAgICAgICAgICAgICAgICBsYWJlbC5jb3VudCA9IDBcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBsYWJlbC5ba2V5U3VtXSA9IGZvdW5kU3VtQ291bnQuW2tleVN1bV1cclxuICAgICAgICAgICAgICAgIGxhYmVsLmNvdW50ID0gZm91bmRTdW1Db3VudC5jb3VudFxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gbGFiZWxcclxuICAgICAgICB9KVxyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybihsYWJlbFN1bUNvdW50cylcclxuXHR9ZWxzZXtcclxuXHRcdHJldHVybihzdW1Db3VudHMpXHJcblx0fVxyXG5cclxuXHRcclxufTtcclxuIiwiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgeyBUYWJsZSwgQWxlcnQsIENvbnRhaW5lciwgQnV0dG9uLCBDb2wsIFJvdyB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHN0eWxlcyBmcm9tIFwiLi4vLi4vc3R5bGVzL0hvbWUubW9kdWxlLmNzc1wiO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gXCJtb21lbnRcIjtcclxuaW1wb3J0IHN1bUJ5R3JvdXAgZnJvbSBcIi4uLy4uL2hlbHBlcnMvc3VtQnlHcm91cFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY2F0ZWdvcnkoKSB7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXMsIHNldENhdGVnb3JpZXNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtjdXJyZW50UmVjb3Jkcywgc2V0Q3VyUmVjXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbaW5jb21lQ2F0ZWdvcmllcywgc2V0SW5jQ2F0XSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbZXhwZW5zZXNDYXRlZ29yaWVzLCBzZXRFeHBDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtpbmNvbWVSb3dzLCBzZXRJbmNvbWVSb3dzXSA9IHVzZVN0YXRlKG51bGwpO1xyXG4gIGNvbnN0IFtleHBlbnNlc1Jvd3MsIHNldEV4cGVuc2VzUm93c10gPSB1c2VTdGF0ZShudWxsKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2RldGFpbHNgLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBzZXRDYXRlZ29yaWVzKGRhdGEuY2F0ZWdvcmllcyk7XHJcblxyXG4gICAgICAgIGxldCBkYXRlQmVmb3JlID0gbW9tZW50KGRhdGEuY3VycmVudERhdGUuc3RhcnREYXRlKS5zdWJ0cmFjdCgxLCBcImRheVwiKTtcclxuICAgICAgICBsZXQgZGF0ZUFmdGVyID0gbW9tZW50KGRhdGEuY3VycmVudERhdGUuZW5kRGF0ZSkuYWRkKDEsIFwiZGF5XCIpO1xyXG5cclxuICAgICAgICBsZXQgZmlsdGVyZWRSZWNvcmRzID0gZGF0YS5yZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJkYXlzXCIpO1xyXG4gICAgICAgICAgcmV0dXJuIG1vbWVudCh1cGRhdGVkT24pLmlzQmV0d2VlbihkYXRlQmVmb3JlLCBkYXRlQWZ0ZXIsIFwiZGF5XCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBzZXRDdXJSZWMoZmlsdGVyZWRSZWNvcmRzKTtcclxuICAgICAgfSk7XHJcbiAgfSwgW10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgbGV0IGNhdGVnb3J5UmVjb3JkcyA9IHN1bUJ5R3JvdXAoXHJcbiAgICAgIGNhdGVnb3JpZXMsXHJcbiAgICAgIFwiX2lkXCIsXHJcbiAgICAgIGN1cnJlbnRSZWNvcmRzLFxyXG4gICAgICBcImNhdGVnb3J5SWRcIixcclxuICAgICAgXCJhbW91bnRcIlxyXG4gICAgKTtcclxuXHJcbiAgICBzZXRJbmNDYXQoY2F0ZWdvcnlSZWNvcmRzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gXCJpbmNvbWVcIikpO1xyXG4gICAgc2V0RXhwQ2F0KGNhdGVnb3J5UmVjb3Jkcy5maWx0ZXIoKGNhdGVnb3J5KSA9PiBjYXRlZ29yeS50eXBlID09IFwiZXhwZW5zZVwiKSk7XHJcbiAgfSwgW2NhdGVnb3JpZXMsIGN1cnJlbnRSZWNvcmRzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRJbmNvbWVSb3dzKFxyXG4gICAgICBpbmNvbWVDYXRlZ29yaWVzLm1hcCgoY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgPHRyIGtleT17Y2F0ZWdvcnkuX2lkfT5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5uYW1lfTwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkuYW1vdW50fTwvdGQ+XHJcbiAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICk7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH0sIFtpbmNvbWVDYXRlZ29yaWVzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRFeHBlbnNlc1Jvd3MoXHJcbiAgICAgIGV4cGVuc2VzQ2F0ZWdvcmllcy5tYXAoKGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgIDx0ciBrZXk9e2NhdGVnb3J5Ll9pZH0+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkubmFtZX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5LmJ1ZGdldH08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e01hdGguYWJzKGNhdGVnb3J5LmFtb3VudCl9PC90ZD5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5hbW91bnQgKyBjYXRlZ29yeS5idWRnZXR9PC90ZD5cclxuICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW2V4cGVuc2VzQ2F0ZWdvcmllc10pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+IENhdGVnb3JpZXM8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWVuZCBteS0yXCI+XHJcbiAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgdmFyaWFudD1cInN1Y2Nlc3NcIlxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL2NhdGVnb3J5L2FkZFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBBZGQgQ2F0ZWdvcnlcclxuICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwic2Vjb25kYXJ5XCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9lZGl0XCIpfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIEVkaXQgQ2F0ZWdvcnlcclxuICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwiZGFuZ2VyXCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9hZGRcIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgRGVsZXRlIENhdGVnb3J5XHJcbiAgICAgICAgPC9CdXR0b24+XHJcblxyXG4gICAgICAgIDxCdXR0b25cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgIHZhcmlhbnQ9XCJpbmZvXCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi4vcmVjb3JkL2FkZFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBBZGQgUmVjb3JkXHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgIDwvUm93PlxyXG4gICAgICA8aDU+SW5jb21lOjwvaDU+XHJcbiAgICAgIHtpbmNvbWVDYXRlZ29yaWVzLmxlbmd0aCA9PSAwID8gKFxyXG4gICAgICAgIDxBbGVydCB2YXJpYW50PVwiaW5mb1wiPllvdSBoYXZlIG5vIGNhdGVnb3JpZXMgeWV0LjwvQWxlcnQ+XHJcbiAgICAgICkgOiAoXHJcbiAgICAgICAgPFRhYmxlIHN0cmlwZWQgYm9yZGVyZWQgaG92ZXI+XHJcbiAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICA8dGg+Q2F0ZWdvcnkgTmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgPHRoPkN1cnJlbnQgSW5jb21lPC90aD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIDwvdGhlYWQ+XHJcblxyXG4gICAgICAgICAgPHRib2R5PntpbmNvbWVSb3dzfTwvdGJvZHk+XHJcbiAgICAgICAgPC9UYWJsZT5cclxuICAgICAgKX1cclxuICAgICAgPGg1PkV4cGVuc2VzOjwvaDU+XHJcbiAgICAgIHtleHBlbnNlc0NhdGVnb3JpZXMubGVuZ3RoID09IDAgPyAoXHJcbiAgICAgICAgPEFsZXJ0IHZhcmlhbnQ9XCJpbmZvXCI+WW91IGhhdmUgbm8gY2F0ZWdvcmllcyB1bmRlciBleHBlbnNlcyB5ZXQuPC9BbGVydD5cclxuICAgICAgKSA6IChcclxuICAgICAgICA8VGFibGUgc3RyaXBlZCBib3JkZXJlZCBob3Zlcj5cclxuICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgIDx0aD5DYXRlZ29yeSBOYW1lPC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBCdWRnZXQ8L3RoPlxyXG4gICAgICAgICAgICAgIDx0aD5DdXJyZW50IEV4cGVuc2VzPC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBTYXZpbmdzPC90aD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIDwvdGhlYWQ+XHJcblxyXG4gICAgICAgICAgPHRib2R5PntleHBlbnNlc1Jvd3N9PC90Ym9keT5cclxuICAgICAgICA8L1RhYmxlPlxyXG4gICAgICApfVxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICApO1xyXG59XHJcbiIsIi8vIEV4cG9ydHNcbm1vZHVsZS5leHBvcnRzID0ge1xuXHRcImNvbnRhaW5lclwiOiBcIkhvbWVfY29udGFpbmVyX18xRWNzVVwiLFxuXHRcIm1haW5cIjogXCJIb21lX21haW5fXzF4OGdDXCIsXG5cdFwiZm9vdGVyXCI6IFwiSG9tZV9mb290ZXJfXzFXZGhEXCIsXG5cdFwidGl0bGVcIjogXCJIb21lX3RpdGxlX18zRGpSN1wiLFxuXHRcImRlc2NyaXB0aW9uXCI6IFwiSG9tZV9kZXNjcmlwdGlvbl9fMTdaNEZcIixcblx0XCJjb2RlXCI6IFwiSG9tZV9jb2RlX19heHgyWVwiLFxuXHRcImdyaWRcIjogXCJIb21lX2dyaWRfXzJFaTJGXCIsXG5cdFwiY2FyZFwiOiBcIkhvbWVfY2FyZF9fMlNkdEJcIixcblx0XCJsb2dvXCI6IFwiSG9tZV9sb2dvX18xWWJySFwiLFxuXHRcInN0YWNrZWRCYXJcIjogXCJIb21lX3N0YWNrZWRCYXJfXzNVTzFsXCIsXG5cdFwiaG9tZXBhZ2VDYXJkXCI6IFwiSG9tZV9ob21lcGFnZUNhcmRfXzF2a0xOXCJcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJtb21lbnRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9oZWFkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvcm91dGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==