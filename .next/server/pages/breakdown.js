module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/breakdown.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./UserContext.js":
/*!************************!*\
  !*** ./UserContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // creates a Context Object

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "./app-helper.js":
/*!***********************!*\
  !*** ./app-helper.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  getAccessToken: () => localStorage.getItem('token')
};

/***/ }),

/***/ "./components/PieChart.js":
/*!********************************!*\
  !*** ./components/PieChart.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChart; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/ColorRandomizer */ "./helpers/ColorRandomizer.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\PieChart.js";



function PieChart({
  chartType,
  rawData,
  labelKey,
  amountKey
}) {
  const {
    0: label,
    1: setLabel
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: amount,
    1: setAmount
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: bgColors,
    1: setBgColors
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (rawData) {
      setLabel(rawData.map(element => element[labelKey]));
      setAmount(rawData.map(element => Math.abs(element[amountKey])));
      let colors = rawData.map(element => {
        if (element.color) {
          return element.color;
        } else {
          return `#${Object(_helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__["default"])()}`;
        }
      });
      setBgColors(colors);
    } else {
      setLabel([]);
      setAmount([]);
      setBgColors([]);
    }
  }, [rawData]);

  if (chartType == "Pie") {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      },
      redraw: false
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 7
    }, this);
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Doughnut"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 7
    }, this);
  }
}

/***/ }),

/***/ "./components/StackedBar.js":
/*!**********************************!*\
  !*** ./components/StackedBar.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChart; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/ColorRandomizer */ "./helpers/ColorRandomizer.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\StackedBar.js";



function PieChart({
  income,
  expenses
}) {
  // State for 2nd data either savings/overspent
  const {
    0: consumption,
    1: setConsumption
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: label2,
    1: setLabel2
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: color2,
    1: setcolor2
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let savings = income - expenses;

    if (savings < 0) {
      setConsumption(income);
      setLabel2("overspent");
      setcolor2("#e30707");
    } else {
      setConsumption(expenses);
      setLabel2("savings");
      setcolor2("#47b83d");
    }
  }, [income, expenses]);
  const arbitraryStackKey = "stack1";
  const data = {
    options: {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      },
      indexAxis: "x"
    },
    labels: [""],
    datasets: [// These two will be in the same stack.
    {
      stack: "",
      label: "consumption",
      data: [consumption],
      backgroundColor: "#f56f36",
      borderWidth: 0.01
    }, {
      stack: "",
      label: label2,
      data: [Math.abs(income - expenses)],
      backgroundColor: color2,
      borderWidth: 0.01
    }]
  };
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["HorizontalBar"], {
    className: "container-fluid",
    width: "70%",
    height: "8px",
    data: data
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 59,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./components/View.js":
/*!****************************!*\
  !*** ./components/View.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\View.js";


 //other syntax

const View = ({
  title,
  children
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: title
      }, "title-tag", false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "viewport",
        content: "initial-scale=1.0, width=device-width"
      }, "title-meta", false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Container"], {
      className: "mt-5 pt-4 mb-5",
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 8,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./helpers/ColorRandomizer.js":
/*!************************************!*\
  !*** ./helpers/ColorRandomizer.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ColorRandomizer; });
function ColorRandomizer() {
  return Math.floor(Math.random() * 16777215).toString(16);
}

/***/ }),

/***/ "./helpers/sumByGroup.js":
/*!*******************************!*\
  !*** ./helpers/sumByGroup.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return sumByGroup; });
function sumByGroup(labelArray, labelKey, array, key, keySum) {
  let sumCounts = [];
  array.reduce(function (res, value) {
    if (!res[value[key]]) {
      res[value[key]] = {
        [key]: value[key],
        [keySum]: 0,
        count: 0
      };
      sumCounts.push(res[value[key]]);
    }

    res[value[key]][keySum] += value[keySum];
    res[value[key]].count += 1;
    return res;
  }, {});

  if (labelArray != [] & labelKey != "") {
    let labelSumCounts = labelArray.map(label => {
      let foundSumCount = sumCounts.find(sumCount => sumCount[key] == label[labelKey]);

      if (foundSumCount == undefined) {
        label[keySum] = 0;
        label.count = 0;
      } else {
        label[keySum] = foundSumCount[keySum];
        label.count = foundSumCount.count;
      }

      return label;
    });
    return labelSumCounts;
  } else {
    return sumCounts;
  }
}
;

/***/ }),

/***/ "./pages/breakdown.js":
/*!****************************!*\
  !*** ./pages/breakdown.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Breakdown; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login */ "./pages/login.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./category */ "./pages/category/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../helpers/sumByGroup */ "./helpers/sumByGroup.js");
/* harmony import */ var _components_PieChart__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/PieChart */ "./components/PieChart.js");
/* harmony import */ var _components_StackedBar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/StackedBar */ "./components/StackedBar.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\breakdown.js";











function Breakdown() {
  const {
    user,
    setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_5__["default"]); //States Set after fetching user details

  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeCategories,
    1: setIncCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: expensesCategories,
    1: setExpCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //States for filtering Records by Date

  const {
    0: startDate,
    1: setStartDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_8___default()(user.startDate).startOf("days").format("yyyy-MM-DD"));
  const {
    0: endDate,
    1: setEndDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_8___default()(user.endDate).startOf("days").format("yyyy-MM-DD")); //States for Data Manipulation

  const {
    0: totalIncome,
    1: setTotalIncome
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: totalExpenses,
    1: setTotalExpenses
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: incomeRecords,
    1: setIncRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: expensesRecords,
    1: setExpRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: categoriesType,
    1: setCatType
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordsType,
    1: setRecType
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordsName,
    1: setRecName
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: categoriesRecords,
    1: setCatRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: budgetArray,
    1: setBudgetArray
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //States for Chart filter

  const {
    0: type,
    1: setType
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("expense");
  const {
    0: catOption,
    1: setCatOption
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: categoryId,
    1: setCategoryId
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: budgetOption,
    1: setBudgetOption
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: budgetId,
    1: setBudgetId
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all"); // States for Chart Data

  const {
    0: categoryPie,
    1: setCatPie
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordDoughnut,
    1: setRecDoughnut
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: budgetPie,
    1: setBudgetPie
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: budgetDoughnut,
    1: setBudgetDoughnut
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      console.log(data);
      setRecords(data.records);
      let filteredInc = data.categories.filter(category => {
        return category.type == "income";
      });
      setIncCat(filteredInc);
      let filteredExp = data.categories.filter(category => {
        return category.type == "expense";
      });
      setExpCat(filteredExp);
      setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
      setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (startDate <= endDate) {
      let dateBefore = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).subtract(1, "day");
      let dateAfter = moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).add(1, "day");
      let filteredRecords = records.filter(record => {
        let updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });
      setIncRec(filteredRecords.filter(record => record.amount >= 0));
      setExpRec(filteredRecords.filter(record => record.amount < 0));
      setBudgetPie(expensesCategories);
      setBudgetOption(expensesCategories.map(category => {
        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
          value: category._id,
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 101,
          columnNumber: 18
        }, this);
      }));
    }
  }, [startDate, endDate, records, incomeCategories, expensesCategories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setCatRec(Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_9__["default"])(expensesCategories, "_id", expensesRecords, "categoryId", "amount"));
  }, [startDate, endDate, expensesCategories, expensesRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (budgetId == "all") {
      setBudgetArray(categoriesRecords);
    } else if (budgetId == "budgetOnly") {
      setBudgetArray(categoriesRecords.filter(category => category.budget > 0));
    } else if (budgetId == "expensesOnly") {
      setBudgetArray(categoriesRecords.filter(category => category.amount < 0));
    } else if (budgetId == "budgetExpenses") {
      setBudgetArray(categoriesRecords.filter(category => category.amount < 0 && category.budget > 0));
    } else {
      setBudgetArray(categoriesRecords.filter(category => category._id == budgetId));
    }
  }, [categoriesRecords, budgetId, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    console.log(budgetId);
    console.log(budgetArray);
    let budget = budgetArray.reduce((total, budget) => {
      return total + budget.budget;
    }, 0);
    let expenses = budgetArray.reduce((total, budget) => {
      return total - budget.amount;
    }, 0);
    let savings = budget - expenses;
    let budgetSummarry = [{
      label: "consumption",
      amount: expenses,
      color: "#f56f36"
    }];

    if (savings >= 0) {
      setBudgetDoughnut([{
        label: "consumption",
        amount: expenses,
        color: "#f56f36"
      }, {
        label: "savings",
        amount: savings,
        color: "#47b83d"
      }]);
    } else {
      setBudgetDoughnut([{
        label: "consumption",
        amount: budget,
        color: "#f56f36"
      }, {
        label: "overspent",
        amount: Math.abs(savings),
        color: "#e30707"
      }]);
    }
  }, [budgetArray, budgetId, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setTotalIncome(incomeRecords.reduce((total, record) => {
      return total + record.amount;
    }, 0));
    setTotalExpenses(expensesRecords.reduce((total, record) => {
      return total - record.amount;
    }, 0));
  }, [startDate, endDate, incomeRecords, expensesRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (type == "income") {
      setCatType(incomeCategories);
      setRecType(incomeRecords);
    } else {
      setCatType(expensesCategories);
      setRecType(expensesRecords);
    }

    setCategoryId("all");
  }, [type, records, incomeCategories, expensesCategories, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setCatPie(Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_9__["default"])(categoriesType, "_id", recordsType, "categoryId", "amount"));
    setCatOption(categoriesType.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
        value: category._id,
        children: category.name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 221,
        columnNumber: 16
      }, this);
    }));
  }, [type, categoriesType, recordsType, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (categoryId == "all") {
      setRecName(recordsType);
    } else {
      setRecName(recordsType.filter(record => record.categoryId == categoryId));
    }
  }, [type, categoryId, categoriesType, recordsType, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setRecDoughnut(Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_9__["default"])([], "", recordsName, "description", "amount"));
  }, [categoryId, categoriesType, recordsType, recordsName, startDate, endDate]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Monthly Trend"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 250,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 249,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          md: "auto",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
            children: "Date:"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 255,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 254,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
            type: "date",
            dateformat: "yyyy-MM-DD",
            value: startDate,
            onChange: e => setStartDate(e.target.value)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 258,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 257,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          md: "auto",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
            children: " to "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 266,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 265,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
            type: "date",
            value: endDate,
            dateformat: "myyyy-MM-DD",
            onChange: e => setEndDate(e.target.value)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 269,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 268,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 253,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 252,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
      className: "justify-content-md-center my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
        children: "Income Consumption"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 279,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "container-fluid stackedBar",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_StackedBar__WEBPACK_IMPORTED_MODULE_11__["default"], {
          income: totalIncome,
          expenses: totalExpenses
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 281,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 280,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 278,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
      className: "justify-content-md-center my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
          className: "m-2",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Header, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"], {
              variant: "tabs",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Item, {
                vlye: "income",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Link, {
                  onClick: e => setType("income"),
                  children: "Income"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 290,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 289,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Item, {
                value: "expense",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Link, {
                  onClick: e => setType("expenses"),
                  children: "Expenses"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 293,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 292,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 288,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 287,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
              className: "text-center",
              children: [type[0].toUpperCase() + type.slice(1), " Categories", " "]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 300,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_10__["default"], {
              chartType: "Pie",
              rawData: categoryPie,
              labelKey: "name",
              amountKey: "amount"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 303,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 299,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 286,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 285,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
          className: "my-2",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Header, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Group, {
              controlId: "categoryId",
              className: "my-0",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                as: "select",
                value: categoryId,
                onChange: e => setCategoryId(e.target.value),
                required: true,
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "all",
                  selected: true,
                  children: "All"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 323,
                  columnNumber: 19
                }, this), catOption]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 317,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 316,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 315,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
              className: "text-center",
              children: [type[0].toUpperCase() + type.slice(1), " Categories", " "]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 331,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_10__["default"], {
              chartType: "Doughnut",
              rawData: recordDoughnut,
              labelKey: "description",
              amountKey: "amount"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 334,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 330,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 314,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 313,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 284,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
      className: "justify-content-md-center my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
          className: "m-2",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Header, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"], {
              variant: "tabs",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Item, {
                vlye: "income",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Nav"].Link, {
                  children: "Budget"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 351,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 350,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 349,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 348,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
              className: "text-center",
              children: "Budget Categories"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 356,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_10__["default"], {
              chartType: "Pie",
              rawData: budgetPie,
              labelKey: "name",
              amountKey: "budget"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 357,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 355,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 347,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 346,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
          className: "m-2",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Header, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Group, {
              controlId: "categoryId",
              className: "my-0",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                as: "select",
                value: budgetId,
                onChange: e => setBudgetId(e.target.value),
                required: true,
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "all",
                  selected: true,
                  children: "All"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 377,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "budgetOnly",
                  selected: true,
                  children: "With Budget Only"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 380,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "expensesOnly",
                  selected: true,
                  children: "With Expenses Only"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 383,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "budgetExpenses",
                  selected: true,
                  children: "With Both Budget & Expesenses Only"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 386,
                  columnNumber: 19
                }, this), budgetOption]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 371,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 370,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 369,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
              className: "text-center",
              children: "Budget Consumption "
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 394,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_10__["default"], {
              chartType: "Doughnut",
              rawData: budgetDoughnut,
              labelKey: "label",
              amountKey: "amount"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 395,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 393,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 368,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 367,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 345,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 248,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/category/index.js":
/*!*********************************!*\
  !*** ./pages/category/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../helpers/sumByGroup */ "./helpers/sumByGroup.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\category\\index.js";







function category() {
  const {
    0: categories,
    1: setCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: currentRecords,
    1: setCurRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeCategories,
    1: setIncCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: expensesCategories,
    1: setExpCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeRows,
    1: setIncomeRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const {
    0: expensesRows,
    1: setExpensesRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setCategories(data.categories);
      let dateBefore = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.startDate).subtract(1, "day");
      let dateAfter = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.endDate).add(1, "day");
      let filteredRecords = data.records.filter(record => {
        let updatedOn = moment__WEBPACK_IMPORTED_MODULE_6___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_6___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });
      setCurRec(filteredRecords);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let categoryRecords = Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__["default"])(categories, "_id", currentRecords, "categoryId", "amount");
    setIncCat(categoryRecords.filter(category => category.type == "income"));
    setExpCat(categoryRecords.filter(category => category.type == "expense"));
  }, [categories, currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setIncomeRows(incomeCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 11
      }, this);
    }));
  }, [incomeCategories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setExpensesRows(expensesCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: Math.abs(category.amount)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount + category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 11
      }, this);
    }));
  }, [expensesCategories]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: " Categories"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], {
      className: "justify-content-md-end my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "success",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Add Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "secondary",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/edit"),
        children: "Edit Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "danger",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Delete Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "info",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("../record/add"),
        children: "Add Record"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 111,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Income:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 7
    }, this), incomeCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 122,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 127,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Income"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 128,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: incomeRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 132,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 9
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Expenses:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 135,
      columnNumber: 7
    }, this), expensesCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories under expenses yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 142,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Budget"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Expenses"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Savings"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 141,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: expensesRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 149,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/login.js":
/*!************************!*\
  !*** ./pages/login.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return login; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-google-login */ "react-google-login");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _components_View__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/View */ "./components/View.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app-helper */ "./app-helper.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_app_helper__WEBPACK_IMPORTED_MODULE_9__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\login.js";









function login() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_View__WEBPACK_IMPORTED_MODULE_8__["default"], {
    title: "Login",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      className: "justify-content-center",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: true,
        md: "6",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(LoginForm, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

const LoginForm = () => {
  const {
    user,
    setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_7__["default"]);
  const {
    0: email,
    1: setEmail
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: password,
    1: setPassword
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: isActive,
    1: setIsActive
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);

  function authenticate(e) {
    e.preventDefault();
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    };
    fetch(`http://localhost:4000/api/users/login`, options).then(res => res.json()).then(data => {
      console.log(data);

      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error === "does-not-exist") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "User does not exist.", "error");
        } else if (data.error === "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure, try an alternative login procedure", "error");
        } else if (data.error === "incorrect-password") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "password is incorrect", "error");
        }
      }
    });
  }

  const aunthenticateGoogleToken = response => {
    const payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    };
    fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload).then(res => res.json()).then(data => {
      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error = "google-auth-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Google Auth Error", "Google authentication procedure failed.", "error");
        } else if (data.error = "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure.", "error");
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Header, {
      children: "Login details"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_6___default.a, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
          children: "Budget Tracker"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 115,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 114,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
        onSubmit: e => authenticate(e),
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "userEmail",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Email address"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 119,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "email",
            placeholder: "Enter email",
            value: email,
            onChange: e => setEmail(e.target.value),
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 120,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 118,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "password",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Password"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "password",
            placeholder: "Password",
            value: password,
            onChange: e => setPassword(e.target.value),
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 131,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          className: "justify-content-center px-3",
          children: isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 141,
            columnNumber: 15
          }, undefined) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            disabled: true,
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 139,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_google_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLogin"], {
        clientId: "656593662569-24gufu44evj4ujqs3k0113rgrurq2bnp.apps.googleusercontent.com",
        buttonText: "Login",
        onSuccess: aunthenticateGoogleToken,
        onFailure: aunthenticateGoogleToken,
        cookiePolicy: "single_host_origin",
        className: "w-100 text-center d-flex justify-content-center"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 152,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 111,
    columnNumber: 5
  }, undefined);
};

/***/ }),

/***/ "./styles/Home.module.css":
/*!********************************!*\
  !*** ./styles/Home.module.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Exports
module.exports = {
	"container": "Home_container__1EcsU",
	"main": "Home_main__1x8gC",
	"footer": "Home_footer__1WdhD",
	"title": "Home_title__3DjR7",
	"description": "Home_description__17Z4F",
	"code": "Home_code__axx2Y",
	"grid": "Home_grid__2Ei2F",
	"card": "Home_card__2SdtB",
	"logo": "Home_logo__1YbrH",
	"stackedBar": "Home_stackedBar__3UO1l",
	"homepageCard": "Home_homepageCard__1vkLN"
};


/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-chartjs-2");

/***/ }),

/***/ "react-google-login":
/*!*************************************!*\
  !*** external "react-google-login" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-google-login");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwLWhlbHBlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1BpZUNoYXJ0LmpzIiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvU3RhY2tlZEJhci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1ZpZXcuanMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9Db2xvclJhbmRvbWl6ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9zdW1CeUdyb3VwLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2JyZWFrZG93bi5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9jYXRlZ29yeS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9sb2dpbi5qcyIsIndlYnBhY2s6Ly8vLi9zdHlsZXMvSG9tZS5tb2R1bGUuY3NzIiwid2VicGFjazovLy9leHRlcm5hbCBcIm1vbWVudFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQvaGVhZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQvcm91dGVyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1ib290c3RyYXBcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1jaGFydGpzLTJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1nb29nbGUtbG9naW5cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJzd2VldGFsZXJ0MlwiIl0sIm5hbWVzIjpbIlVzZXJDb250ZXh0IiwiUmVhY3QiLCJjcmVhdGVDb250ZXh0IiwiVXNlclByb3ZpZGVyIiwiUHJvdmlkZXIiLCJtb2R1bGUiLCJleHBvcnRzIiwiZ2V0QWNjZXNzVG9rZW4iLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiUGllQ2hhcnQiLCJjaGFydFR5cGUiLCJyYXdEYXRhIiwibGFiZWxLZXkiLCJhbW91bnRLZXkiLCJsYWJlbCIsInNldExhYmVsIiwidXNlU3RhdGUiLCJhbW91bnQiLCJzZXRBbW91bnQiLCJiZ0NvbG9ycyIsInNldEJnQ29sb3JzIiwidXNlRWZmZWN0IiwibWFwIiwiZWxlbWVudCIsIk1hdGgiLCJhYnMiLCJjb2xvcnMiLCJjb2xvciIsIkNvbG9yUmFuZG9taXplciIsImxhYmVscyIsImxlZ2VuZCIsInBvc2l0aW9uIiwiZGF0YXNldHMiLCJkYXRhIiwiYmFja2dyb3VuZENvbG9yIiwiaG92ZXJCYWNrZ3JvdW5kIiwiaW5jb21lIiwiZXhwZW5zZXMiLCJjb25zdW1wdGlvbiIsInNldENvbnN1bXB0aW9uIiwibGFiZWwyIiwic2V0TGFiZWwyIiwiY29sb3IyIiwic2V0Y29sb3IyIiwic2F2aW5ncyIsImFyYml0cmFyeVN0YWNrS2V5Iiwib3B0aW9ucyIsInNjYWxlcyIsIngiLCJzdGFja2VkIiwieSIsImluZGV4QXhpcyIsInN0YWNrIiwiYm9yZGVyV2lkdGgiLCJWaWV3IiwidGl0bGUiLCJjaGlsZHJlbiIsImZsb29yIiwicmFuZG9tIiwidG9TdHJpbmciLCJzdW1CeUdyb3VwIiwibGFiZWxBcnJheSIsImFycmF5Iiwia2V5Iiwia2V5U3VtIiwic3VtQ291bnRzIiwicmVkdWNlIiwicmVzIiwidmFsdWUiLCJjb3VudCIsInB1c2giLCJsYWJlbFN1bUNvdW50cyIsImZvdW5kU3VtQ291bnQiLCJmaW5kIiwic3VtQ291bnQiLCJ1bmRlZmluZWQiLCJCcmVha2Rvd24iLCJ1c2VyIiwic2V0VXNlciIsInVzZUNvbnRleHQiLCJyZWNvcmRzIiwic2V0UmVjb3JkcyIsImluY29tZUNhdGVnb3JpZXMiLCJzZXRJbmNDYXQiLCJleHBlbnNlc0NhdGVnb3JpZXMiLCJzZXRFeHBDYXQiLCJzdGFydERhdGUiLCJzZXRTdGFydERhdGUiLCJtb21lbnQiLCJzdGFydE9mIiwiZm9ybWF0IiwiZW5kRGF0ZSIsInNldEVuZERhdGUiLCJ0b3RhbEluY29tZSIsInNldFRvdGFsSW5jb21lIiwidG90YWxFeHBlbnNlcyIsInNldFRvdGFsRXhwZW5zZXMiLCJpbmNvbWVSZWNvcmRzIiwic2V0SW5jUmVjIiwiZXhwZW5zZXNSZWNvcmRzIiwic2V0RXhwUmVjIiwiY2F0ZWdvcmllc1R5cGUiLCJzZXRDYXRUeXBlIiwicmVjb3Jkc1R5cGUiLCJzZXRSZWNUeXBlIiwicmVjb3Jkc05hbWUiLCJzZXRSZWNOYW1lIiwiY2F0ZWdvcmllc1JlY29yZHMiLCJzZXRDYXRSZWMiLCJidWRnZXRBcnJheSIsInNldEJ1ZGdldEFycmF5IiwidHlwZSIsInNldFR5cGUiLCJjYXRPcHRpb24iLCJzZXRDYXRPcHRpb24iLCJjYXRlZ29yeUlkIiwic2V0Q2F0ZWdvcnlJZCIsImJ1ZGdldE9wdGlvbiIsInNldEJ1ZGdldE9wdGlvbiIsImJ1ZGdldElkIiwic2V0QnVkZ2V0SWQiLCJjYXRlZ29yeVBpZSIsInNldENhdFBpZSIsInJlY29yZERvdWdobnV0Iiwic2V0UmVjRG91Z2hudXQiLCJidWRnZXRQaWUiLCJzZXRCdWRnZXRQaWUiLCJidWRnZXREb3VnaG51dCIsInNldEJ1ZGdldERvdWdobnV0IiwiZmV0Y2giLCJoZWFkZXJzIiwiQXV0aG9yaXphdGlvbiIsInRoZW4iLCJqc29uIiwiY29uc29sZSIsImxvZyIsImZpbHRlcmVkSW5jIiwiY2F0ZWdvcmllcyIsImZpbHRlciIsImNhdGVnb3J5IiwiZmlsdGVyZWRFeHAiLCJjdXJyZW50RGF0ZSIsImRhdGVCZWZvcmUiLCJzdWJ0cmFjdCIsImRhdGVBZnRlciIsImFkZCIsImZpbHRlcmVkUmVjb3JkcyIsInJlY29yZCIsInVwZGF0ZWRPbiIsImlzQmV0d2VlbiIsIl9pZCIsIm5hbWUiLCJidWRnZXQiLCJ0b3RhbCIsImJ1ZGdldFN1bW1hcnJ5IiwiZSIsInRhcmdldCIsInRvVXBwZXJDYXNlIiwic2xpY2UiLCJzZXRDYXRlZ29yaWVzIiwiY3VycmVudFJlY29yZHMiLCJzZXRDdXJSZWMiLCJpbmNvbWVSb3dzIiwic2V0SW5jb21lUm93cyIsImV4cGVuc2VzUm93cyIsInNldEV4cGVuc2VzUm93cyIsImNhdGVnb3J5UmVjb3JkcyIsIlJvdXRlciIsImxlbmd0aCIsImxvZ2luIiwiTG9naW5Gb3JtIiwiZW1haWwiLCJzZXRFbWFpbCIsInBhc3N3b3JkIiwic2V0UGFzc3dvcmQiLCJpc0FjdGl2ZSIsInNldElzQWN0aXZlIiwiYXV0aGVudGljYXRlIiwicHJldmVudERlZmF1bHQiLCJtZXRob2QiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsImFjY2Vzc1Rva2VuIiwic2V0SXRlbSIsImlkIiwiZXJyb3IiLCJTd2FsIiwiZmlyZSIsImF1bnRoZW50aWNhdGVHb29nbGVUb2tlbiIsInJlc3BvbnNlIiwicGF5bG9hZCIsInRva2VuSWQiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFBQTtBQUFBO0FBQUE7Q0FFQTs7QUFDQSxNQUFNQSxXQUFXLGdCQUFHQyw0Q0FBSyxDQUFDQyxhQUFOLEVBQXBCO0FBRU8sTUFBTUMsWUFBWSxHQUFHSCxXQUFXLENBQUNJLFFBQWpDO0FBRVFKLDBFQUFmLEU7Ozs7Ozs7Ozs7O0FDUEFLLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQjtBQUNoQkMsZ0JBQWMsRUFBRSxNQUFNQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckI7QUFETixDQUFqQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUVlLFNBQVNDLFFBQVQsQ0FBa0I7QUFBRUMsV0FBRjtBQUFhQyxTQUFiO0FBQXNCQyxVQUF0QjtBQUFnQ0M7QUFBaEMsQ0FBbEIsRUFBK0Q7QUFDNUUsUUFBTTtBQUFBLE9BQUNDLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CQyxzREFBUSxDQUFDLEVBQUQsQ0FBbEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsTUFBRDtBQUFBLE9BQVNDO0FBQVQsTUFBc0JGLHNEQUFRLENBQUMsRUFBRCxDQUFwQztBQUNBLFFBQU07QUFBQSxPQUFDRyxRQUFEO0FBQUEsT0FBV0M7QUFBWCxNQUEwQkosc0RBQVEsQ0FBQyxFQUFELENBQXhDO0FBRUFLLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlWLE9BQUosRUFBYTtBQUNYSSxjQUFRLENBQUNKLE9BQU8sQ0FBQ1csR0FBUixDQUFhQyxPQUFELElBQWFBLE9BQU8sQ0FBQ1gsUUFBRCxDQUFoQyxDQUFELENBQVI7QUFDQU0sZUFBUyxDQUFDUCxPQUFPLENBQUNXLEdBQVIsQ0FBYUMsT0FBRCxJQUFhQyxJQUFJLENBQUNDLEdBQUwsQ0FBU0YsT0FBTyxDQUFDVixTQUFELENBQWhCLENBQXpCLENBQUQsQ0FBVDtBQUVBLFVBQUlhLE1BQU0sR0FBR2YsT0FBTyxDQUFDVyxHQUFSLENBQWFDLE9BQUQsSUFBYTtBQUNwQyxZQUFJQSxPQUFPLENBQUNJLEtBQVosRUFBbUI7QUFDakIsaUJBQU9KLE9BQU8sQ0FBQ0ksS0FBZjtBQUNELFNBRkQsTUFFTztBQUNMLGlCQUFRLElBQUdDLHdFQUFlLEVBQUcsRUFBN0I7QUFDRDtBQUNGLE9BTlksQ0FBYjtBQVFBUixpQkFBVyxDQUFDTSxNQUFELENBQVg7QUFDRCxLQWJELE1BYU87QUFDTFgsY0FBUSxDQUFDLEVBQUQsQ0FBUjtBQUNBRyxlQUFTLENBQUMsRUFBRCxDQUFUO0FBQ0FFLGlCQUFXLENBQUMsRUFBRCxDQUFYO0FBQ0Q7QUFDRixHQW5CUSxFQW1CTixDQUFDVCxPQUFELENBbkJNLENBQVQ7O0FBcUJBLE1BQUlELFNBQVMsSUFBSSxLQUFqQixFQUF3QjtBQUN0Qix3QkFDRSxxRUFBQyxtREFBRDtBQUNFLFVBQUksRUFBRTtBQUNKbUIsY0FBTSxFQUFFZixLQURKO0FBRUpnQixjQUFNLEVBQUU7QUFDTkMsa0JBQVEsRUFBRTtBQURKLFNBRko7QUFLSkMsZ0JBQVEsRUFBRSxDQUNSO0FBQ0VDLGNBQUksRUFBRWhCLE1BRFI7QUFFRWlCLHlCQUFlLEVBQUVmLFFBRm5CO0FBR0VnQix5QkFBZSxFQUFFaEI7QUFIbkIsU0FEUTtBQUxOLE9BRFI7QUFjRSxZQUFNLEVBQUU7QUFkVjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREY7QUFrQkQsR0FuQkQsTUFtQk87QUFDTCx3QkFDRSxxRUFBQyx3REFBRDtBQUNFLFVBQUksRUFBRTtBQUNKVSxjQUFNLEVBQUVmLEtBREo7QUFFSmdCLGNBQU0sRUFBRTtBQUNOQyxrQkFBUSxFQUFFO0FBREosU0FGSjtBQUtKQyxnQkFBUSxFQUFFLENBQ1I7QUFDRUMsY0FBSSxFQUFFaEIsTUFEUjtBQUVFaUIseUJBQWUsRUFBRWYsUUFGbkI7QUFHRWdCLHlCQUFlLEVBQUVoQjtBQUhuQixTQURRO0FBTE47QUFEUjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREY7QUFpQkQ7QUFDRixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BFRDtBQUNBO0FBQ0E7QUFFZSxTQUFTVixRQUFULENBQWtCO0FBQUUyQixRQUFGO0FBQVVDO0FBQVYsQ0FBbEIsRUFBd0M7QUFDckQ7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsV0FBRDtBQUFBLE9BQWNDO0FBQWQsTUFBZ0N2QixzREFBUSxDQUFDLEVBQUQsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBQ3dCLE1BQUQ7QUFBQSxPQUFTQztBQUFULE1BQXNCekIsc0RBQVEsQ0FBQyxFQUFELENBQXBDO0FBQ0EsUUFBTTtBQUFBLE9BQUMwQixNQUFEO0FBQUEsT0FBU0M7QUFBVCxNQUFzQjNCLHNEQUFRLENBQUMsRUFBRCxDQUFwQztBQUVBSyx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJdUIsT0FBTyxHQUFHUixNQUFNLEdBQUdDLFFBQXZCOztBQUVBLFFBQUlPLE9BQU8sR0FBRyxDQUFkLEVBQWlCO0FBQ2ZMLG9CQUFjLENBQUNILE1BQUQsQ0FBZDtBQUNBSyxlQUFTLENBQUMsV0FBRCxDQUFUO0FBQ0FFLGVBQVMsQ0FBQyxTQUFELENBQVQ7QUFDRCxLQUpELE1BSU87QUFDTEosb0JBQWMsQ0FBQ0YsUUFBRCxDQUFkO0FBQ0FJLGVBQVMsQ0FBQyxTQUFELENBQVQ7QUFDQUUsZUFBUyxDQUFDLFNBQUQsQ0FBVDtBQUNEO0FBQ0YsR0FaUSxFQVlOLENBQUNQLE1BQUQsRUFBU0MsUUFBVCxDQVpNLENBQVQ7QUFjQSxRQUFNUSxpQkFBaUIsR0FBRyxRQUExQjtBQUNBLFFBQU1aLElBQUksR0FBRztBQUNYYSxXQUFPLEVBQUU7QUFDUEMsWUFBTSxFQUFFO0FBQ05DLFNBQUMsRUFBRTtBQUNEQyxpQkFBTyxFQUFFO0FBRFIsU0FERztBQUlOQyxTQUFDLEVBQUU7QUFDREQsaUJBQU8sRUFBRTtBQURSO0FBSkcsT0FERDtBQVNQRSxlQUFTLEVBQUU7QUFUSixLQURFO0FBWVh0QixVQUFNLEVBQUUsQ0FBQyxFQUFELENBWkc7QUFhWEcsWUFBUSxFQUFFLENBQ1I7QUFDQTtBQUNFb0IsV0FBSyxFQUFFLEVBRFQ7QUFFRXRDLFdBQUssRUFBRSxhQUZUO0FBR0VtQixVQUFJLEVBQUUsQ0FBQ0ssV0FBRCxDQUhSO0FBSUVKLHFCQUFlLEVBQUUsU0FKbkI7QUFLRW1CLGlCQUFXLEVBQUU7QUFMZixLQUZRLEVBU1I7QUFDRUQsV0FBSyxFQUFFLEVBRFQ7QUFFRXRDLFdBQUssRUFBRTBCLE1BRlQ7QUFHRVAsVUFBSSxFQUFFLENBQUNULElBQUksQ0FBQ0MsR0FBTCxDQUFTVyxNQUFNLEdBQUdDLFFBQWxCLENBQUQsQ0FIUjtBQUlFSCxxQkFBZSxFQUFFUSxNQUpuQjtBQUtFVyxpQkFBVyxFQUFFO0FBTGYsS0FUUTtBQWJDLEdBQWI7QUFnQ0Esc0JBQ0UscUVBQUMsNkRBQUQ7QUFDRSxhQUFTLEVBQUMsaUJBRFo7QUFFRSxTQUFLLEVBQUMsS0FGUjtBQUdFLFVBQU0sRUFBQyxLQUhUO0FBSUUsUUFBSSxFQUFFcEI7QUFKUjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFRRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pFRDtBQUNBO0NBR0E7O0FBQ0EsTUFBTXFCLElBQUksR0FBRyxDQUFDO0FBQUVDLE9BQUY7QUFBU0M7QUFBVCxDQUFELEtBQXlCO0FBQ3BDLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsOEJBQ0U7QUFBQSxrQkFBd0JEO0FBQXhCLFNBQVcsV0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFFRSxZQUFJLEVBQUMsVUFGUDtBQUdFLGVBQU8sRUFBQztBQUhWLFNBQ00sWUFETjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQVNFLHFFQUFDLHlEQUFEO0FBQVcsZUFBUyxFQUFDLGdCQUFyQjtBQUFBLGdCQUF1Q0M7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFURjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQWFELENBZEQ7O0FBZ0JlRixtRUFBZixFOzs7Ozs7Ozs7Ozs7QUNyQkE7QUFBQTtBQUFlLFNBQVUxQixlQUFWLEdBQTRCO0FBQzFDLFNBQU9KLElBQUksQ0FBQ2lDLEtBQUwsQ0FBV2pDLElBQUksQ0FBQ2tDLE1BQUwsS0FBYyxRQUF6QixFQUFtQ0MsUUFBbkMsQ0FBNEMsRUFBNUMsQ0FBUDtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ0ZEO0FBQUE7QUFBZSxTQUFTQyxVQUFULENBQW9CQyxVQUFwQixFQUErQmpELFFBQS9CLEVBQXdDa0QsS0FBeEMsRUFBOENDLEdBQTlDLEVBQWtEQyxNQUFsRCxFQUF5RDtBQUd2RSxNQUFJQyxTQUFTLEdBQUMsRUFBZDtBQUVHSCxPQUFLLENBQUNJLE1BQU4sQ0FBYSxVQUFTQyxHQUFULEVBQWNDLEtBQWQsRUFBcUI7QUFDcEMsUUFBSSxDQUFDRCxHQUFHLENBQUNDLEtBQUssQ0FBRUwsR0FBRixDQUFOLENBQVIsRUFBdUI7QUFDdEJJLFNBQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBSCxHQUFtQjtBQUFFLFNBQUNBLEdBQUQsR0FBT0ssS0FBSyxDQUFFTCxHQUFGLENBQWQ7QUFBc0IsU0FBQ0MsTUFBRCxHQUFVLENBQWhDO0FBQW1DSyxhQUFLLEVBQUM7QUFBekMsT0FBbkI7QUFDQUosZUFBUyxDQUFDSyxJQUFWLENBQWVILEdBQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBbEI7QUFDQTs7QUFFREksT0FBRyxDQUFDQyxLQUFLLENBQUVMLEdBQUYsQ0FBTixDQUFILENBQWtCQyxNQUFsQixLQUE2QkksS0FBSyxDQUFFSixNQUFGLENBQWxDO0FBQ0FHLE9BQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBSCxDQUFpQk0sS0FBakIsSUFBMEIsQ0FBMUI7QUFDQSxXQUFPRixHQUFQO0FBQ0EsR0FURSxFQVNBLEVBVEE7O0FBYUgsTUFBR04sVUFBVSxJQUFFLEVBQVosR0FBaUJqRCxRQUFRLElBQUcsRUFBL0IsRUFBa0M7QUFFM0IsUUFBSTJELGNBQWMsR0FBR1YsVUFBVSxDQUFDdkMsR0FBWCxDQUFlUixLQUFLLElBQUU7QUFFdkMsVUFBSTBELGFBQWEsR0FBR1AsU0FBUyxDQUFDUSxJQUFWLENBQWVDLFFBQVEsSUFBSUEsUUFBUSxDQUFFWCxHQUFGLENBQVIsSUFBaUJqRCxLQUFLLENBQUVGLFFBQUYsQ0FBakQsQ0FBcEI7O0FBRUEsVUFBRzRELGFBQWEsSUFBSUcsU0FBcEIsRUFBOEI7QUFDMUI3RCxhQUFLLENBQUVrRCxNQUFGLENBQUwsR0FBaUIsQ0FBakI7QUFDQWxELGFBQUssQ0FBQ3VELEtBQU4sR0FBYyxDQUFkO0FBQ0gsT0FIRCxNQUdLO0FBQ0R2RCxhQUFLLENBQUVrRCxNQUFGLENBQUwsR0FBaUJRLGFBQWEsQ0FBRVIsTUFBRixDQUE5QjtBQUNBbEQsYUFBSyxDQUFDdUQsS0FBTixHQUFjRyxhQUFhLENBQUNILEtBQTVCO0FBQ0g7O0FBRUQsYUFBT3ZELEtBQVA7QUFDSCxLQWJvQixDQUFyQjtBQWVBLFdBQU95RCxjQUFQO0FBQ04sR0FsQkQsTUFrQks7QUFDSixXQUFPTixTQUFQO0FBQ0E7QUFHRDtBQUFBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6Q0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNXLFNBQVQsR0FBcUI7QUFDbEMsUUFBTTtBQUFFQyxRQUFGO0FBQVFDO0FBQVIsTUFBb0JDLHdEQUFVLENBQUNoRixvREFBRCxDQUFwQyxDQURrQyxDQUVsQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ2lGLE9BQUQ7QUFBQSxPQUFVQztBQUFWLE1BQXdCakUsc0RBQVEsQ0FBQyxFQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUNrRSxnQkFBRDtBQUFBLE9BQW1CQztBQUFuQixNQUFnQ25FLHNEQUFRLENBQUMsRUFBRCxDQUE5QztBQUNBLFFBQU07QUFBQSxPQUFDb0Usa0JBQUQ7QUFBQSxPQUFxQkM7QUFBckIsTUFBa0NyRSxzREFBUSxDQUFDLEVBQUQsQ0FBaEQsQ0FMa0MsQ0FPbEM7O0FBQ0EsUUFBTTtBQUFBLE9BQUNzRSxTQUFEO0FBQUEsT0FBWUM7QUFBWixNQUE0QnZFLHNEQUFRLENBQ3hDd0UsNkNBQU0sQ0FBQ1gsSUFBSSxDQUFDUyxTQUFOLENBQU4sQ0FBdUJHLE9BQXZCLENBQStCLE1BQS9CLEVBQXVDQyxNQUF2QyxDQUE4QyxZQUE5QyxDQUR3QyxDQUExQztBQUdBLFFBQU07QUFBQSxPQUFDQyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3QjVFLHNEQUFRLENBQ3BDd0UsNkNBQU0sQ0FBQ1gsSUFBSSxDQUFDYyxPQUFOLENBQU4sQ0FBcUJGLE9BQXJCLENBQTZCLE1BQTdCLEVBQXFDQyxNQUFyQyxDQUE0QyxZQUE1QyxDQURvQyxDQUF0QyxDQVhrQyxDQWVsQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ0csV0FBRDtBQUFBLE9BQWNDO0FBQWQsTUFBZ0M5RSxzREFBUSxDQUFDLENBQUQsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBQytFLGFBQUQ7QUFBQSxPQUFnQkM7QUFBaEIsTUFBb0NoRixzREFBUSxDQUFDLENBQUQsQ0FBbEQ7QUFDQSxRQUFNO0FBQUEsT0FBQ2lGLGFBQUQ7QUFBQSxPQUFnQkM7QUFBaEIsTUFBNkJsRixzREFBUSxDQUFDLEVBQUQsQ0FBM0M7QUFDQSxRQUFNO0FBQUEsT0FBQ21GLGVBQUQ7QUFBQSxPQUFrQkM7QUFBbEIsTUFBK0JwRixzREFBUSxDQUFDLEVBQUQsQ0FBN0M7QUFDQSxRQUFNO0FBQUEsT0FBQ3FGLGNBQUQ7QUFBQSxPQUFpQkM7QUFBakIsTUFBK0J0RixzREFBUSxDQUFDLEVBQUQsQ0FBN0M7QUFDQSxRQUFNO0FBQUEsT0FBQ3VGLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQTRCeEYsc0RBQVEsQ0FBQyxFQUFELENBQTFDO0FBQ0EsUUFBTTtBQUFBLE9BQUN5RixXQUFEO0FBQUEsT0FBY0M7QUFBZCxNQUE0QjFGLHNEQUFRLENBQUMsRUFBRCxDQUExQztBQUNBLFFBQU07QUFBQSxPQUFDMkYsaUJBQUQ7QUFBQSxPQUFvQkM7QUFBcEIsTUFBaUM1RixzREFBUSxDQUFDLEVBQUQsQ0FBL0M7QUFDQSxRQUFNO0FBQUEsT0FBQzZGLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQWdDOUYsc0RBQVEsQ0FBQyxFQUFELENBQTlDLENBeEJrQyxDQTBCbEM7O0FBQ0EsUUFBTTtBQUFBLE9BQUMrRixJQUFEO0FBQUEsT0FBT0M7QUFBUCxNQUFrQmhHLHNEQUFRLENBQUMsU0FBRCxDQUFoQztBQUNBLFFBQU07QUFBQSxPQUFDaUcsU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBNEJsRyxzREFBUSxDQUFDLEtBQUQsQ0FBMUM7QUFDQSxRQUFNO0FBQUEsT0FBQ21HLFVBQUQ7QUFBQSxPQUFhQztBQUFiLE1BQThCcEcsc0RBQVEsQ0FBQyxLQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUNxRyxZQUFEO0FBQUEsT0FBZUM7QUFBZixNQUFrQ3RHLHNEQUFRLENBQUMsS0FBRCxDQUFoRDtBQUNBLFFBQU07QUFBQSxPQUFDdUcsUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJ4RyxzREFBUSxDQUFDLEtBQUQsQ0FBeEMsQ0EvQmtDLENBaUNsQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ3lHLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQTJCMUcsc0RBQVEsQ0FBQyxFQUFELENBQXpDO0FBQ0EsUUFBTTtBQUFBLE9BQUMyRyxjQUFEO0FBQUEsT0FBaUJDO0FBQWpCLE1BQW1DNUcsc0RBQVEsQ0FBQyxFQUFELENBQWpEO0FBQ0EsUUFBTTtBQUFBLE9BQUM2RyxTQUFEO0FBQUEsT0FBWUM7QUFBWixNQUE0QjlHLHNEQUFRLENBQUMsRUFBRCxDQUExQztBQUNBLFFBQU07QUFBQSxPQUFDK0csY0FBRDtBQUFBLE9BQWlCQztBQUFqQixNQUFzQ2hILHNEQUFRLENBQUMsRUFBRCxDQUFwRDtBQUVBSyx5REFBUyxDQUFDLE1BQU07QUFDZDRHLFNBQUssQ0FBRSx5Q0FBRixFQUE0QztBQUMvQ0MsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBUzVILFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUE4QjtBQURoRDtBQURzQyxLQUE1QyxDQUFMLENBS0c0SCxJQUxILENBS1NqRSxHQUFELElBQVNBLEdBQUcsQ0FBQ2tFLElBQUosRUFMakIsRUFNR0QsSUFOSCxDQU1TbkcsSUFBRCxJQUFVO0FBQ2RxRyxhQUFPLENBQUNDLEdBQVIsQ0FBWXRHLElBQVo7QUFDQWdELGdCQUFVLENBQUNoRCxJQUFJLENBQUMrQyxPQUFOLENBQVY7QUFFQSxVQUFJd0QsV0FBVyxHQUFHdkcsSUFBSSxDQUFDd0csVUFBTCxDQUFnQkMsTUFBaEIsQ0FBd0JDLFFBQUQsSUFBYztBQUNyRCxlQUFPQSxRQUFRLENBQUM1QixJQUFULElBQWlCLFFBQXhCO0FBQ0QsT0FGaUIsQ0FBbEI7QUFJQTVCLGVBQVMsQ0FBQ3FELFdBQUQsQ0FBVDtBQUVBLFVBQUlJLFdBQVcsR0FBRzNHLElBQUksQ0FBQ3dHLFVBQUwsQ0FBZ0JDLE1BQWhCLENBQXdCQyxRQUFELElBQWM7QUFDckQsZUFBT0EsUUFBUSxDQUFDNUIsSUFBVCxJQUFpQixTQUF4QjtBQUNELE9BRmlCLENBQWxCO0FBR0ExQixlQUFTLENBQUN1RCxXQUFELENBQVQ7QUFFQXJELGtCQUFZLENBQ1ZDLDZDQUFNLENBQUN2RCxJQUFJLENBQUM0RyxXQUFMLENBQWlCdkQsU0FBbEIsQ0FBTixDQUNHRyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURVLENBQVo7QUFLQUUsZ0JBQVUsQ0FDUkosNkNBQU0sQ0FBQ3ZELElBQUksQ0FBQzRHLFdBQUwsQ0FBaUJsRCxPQUFsQixDQUFOLENBQWlDRixPQUFqQyxDQUF5QyxNQUF6QyxFQUFpREMsTUFBakQsQ0FBd0QsWUFBeEQsQ0FEUSxDQUFWO0FBR0QsS0E3Qkg7QUE4QkQsR0EvQlEsRUErQk4sRUEvQk0sQ0FBVDtBQWlDQXJFLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlpRSxTQUFTLElBQUlLLE9BQWpCLEVBQTBCO0FBQ3hCLFVBQUltRCxVQUFVLEdBQUd0RCw2Q0FBTSxDQUFDRixTQUFELENBQU4sQ0FBa0J5RCxRQUFsQixDQUEyQixDQUEzQixFQUE4QixLQUE5QixDQUFqQjtBQUNBLFVBQUlDLFNBQVMsR0FBR3hELDZDQUFNLENBQUNHLE9BQUQsQ0FBTixDQUFnQnNELEdBQWhCLENBQW9CLENBQXBCLEVBQXVCLEtBQXZCLENBQWhCO0FBRUEsVUFBSUMsZUFBZSxHQUFHbEUsT0FBTyxDQUFDMEQsTUFBUixDQUFnQlMsTUFBRCxJQUFZO0FBQy9DLFlBQUlDLFNBQVMsR0FBRzVELDZDQUFNLENBQUMyRCxNQUFNLENBQUNDLFNBQVIsQ0FBTixDQUF5QjNELE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsZUFBT0QsNkNBQU0sQ0FBQzRELFNBQUQsQ0FBTixDQUFrQkMsU0FBbEIsQ0FBNEJQLFVBQTVCLEVBQXdDRSxTQUF4QyxFQUFtRCxLQUFuRCxDQUFQO0FBQ0QsT0FIcUIsQ0FBdEI7QUFLQTlDLGVBQVMsQ0FBQ2dELGVBQWUsQ0FBQ1IsTUFBaEIsQ0FBd0JTLE1BQUQsSUFBWUEsTUFBTSxDQUFDbEksTUFBUCxJQUFpQixDQUFwRCxDQUFELENBQVQ7QUFDQW1GLGVBQVMsQ0FBQzhDLGVBQWUsQ0FBQ1IsTUFBaEIsQ0FBd0JTLE1BQUQsSUFBWUEsTUFBTSxDQUFDbEksTUFBUCxHQUFnQixDQUFuRCxDQUFELENBQVQ7QUFFQTZHLGtCQUFZLENBQUMxQyxrQkFBRCxDQUFaO0FBQ0FrQyxxQkFBZSxDQUNibEMsa0JBQWtCLENBQUM5RCxHQUFuQixDQUF3QnFILFFBQUQsSUFBYztBQUNuQyw0QkFBTztBQUFRLGVBQUssRUFBRUEsUUFBUSxDQUFDVyxHQUF4QjtBQUFBLG9CQUE4QlgsUUFBUSxDQUFDWTtBQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFQO0FBQ0QsT0FGRCxDQURhLENBQWY7QUFLRDtBQUNGLEdBcEJRLEVBb0JOLENBQUNqRSxTQUFELEVBQVlLLE9BQVosRUFBcUJYLE9BQXJCLEVBQThCRSxnQkFBOUIsRUFBZ0RFLGtCQUFoRCxDQXBCTSxDQUFUO0FBc0JBL0QseURBQVMsQ0FBQyxNQUFNO0FBQ2R1RixhQUFTLENBQ1BoRCxtRUFBVSxDQUNSd0Isa0JBRFEsRUFFUixLQUZRLEVBR1JlLGVBSFEsRUFJUixZQUpRLEVBS1IsUUFMUSxDQURILENBQVQ7QUFTRCxHQVZRLEVBVU4sQ0FBQ2IsU0FBRCxFQUFZSyxPQUFaLEVBQXFCUCxrQkFBckIsRUFBeUNlLGVBQXpDLENBVk0sQ0FBVDtBQVlBOUUseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSWtHLFFBQVEsSUFBSSxLQUFoQixFQUF1QjtBQUNyQlQsb0JBQWMsQ0FBQ0gsaUJBQUQsQ0FBZDtBQUNELEtBRkQsTUFFTyxJQUFJWSxRQUFRLElBQUksWUFBaEIsRUFBOEI7QUFDbkNULG9CQUFjLENBQ1pILGlCQUFpQixDQUFDK0IsTUFBbEIsQ0FBMEJDLFFBQUQsSUFBY0EsUUFBUSxDQUFDYSxNQUFULEdBQWtCLENBQXpELENBRFksQ0FBZDtBQUdELEtBSk0sTUFJQSxJQUFJakMsUUFBUSxJQUFJLGNBQWhCLEVBQWdDO0FBQ3JDVCxvQkFBYyxDQUNaSCxpQkFBaUIsQ0FBQytCLE1BQWxCLENBQTBCQyxRQUFELElBQWNBLFFBQVEsQ0FBQzFILE1BQVQsR0FBa0IsQ0FBekQsQ0FEWSxDQUFkO0FBR0QsS0FKTSxNQUlBLElBQUlzRyxRQUFRLElBQUksZ0JBQWhCLEVBQWtDO0FBQ3ZDVCxvQkFBYyxDQUNaSCxpQkFBaUIsQ0FBQytCLE1BQWxCLENBQ0dDLFFBQUQsSUFBY0EsUUFBUSxDQUFDMUgsTUFBVCxHQUFrQixDQUFsQixJQUF1QjBILFFBQVEsQ0FBQ2EsTUFBVCxHQUFrQixDQUR6RCxDQURZLENBQWQ7QUFLRCxLQU5NLE1BTUE7QUFDTDFDLG9CQUFjLENBQ1pILGlCQUFpQixDQUFDK0IsTUFBbEIsQ0FBMEJDLFFBQUQsSUFBY0EsUUFBUSxDQUFDVyxHQUFULElBQWdCL0IsUUFBdkQsQ0FEWSxDQUFkO0FBR0Q7QUFDRixHQXRCUSxFQXNCTixDQUFDWixpQkFBRCxFQUFvQlksUUFBcEIsRUFBOEJqQyxTQUE5QixFQUF5Q0ssT0FBekMsQ0F0Qk0sQ0FBVDtBQXdCQXRFLHlEQUFTLENBQUMsTUFBTTtBQUNkaUgsV0FBTyxDQUFDQyxHQUFSLENBQVloQixRQUFaO0FBQ0FlLFdBQU8sQ0FBQ0MsR0FBUixDQUFZMUIsV0FBWjtBQUNBLFFBQUkyQyxNQUFNLEdBQUczQyxXQUFXLENBQUMzQyxNQUFaLENBQW1CLENBQUN1RixLQUFELEVBQVFELE1BQVIsS0FBbUI7QUFDakQsYUFBT0MsS0FBSyxHQUFHRCxNQUFNLENBQUNBLE1BQXRCO0FBQ0QsS0FGWSxFQUVWLENBRlUsQ0FBYjtBQUdBLFFBQUluSCxRQUFRLEdBQUd3RSxXQUFXLENBQUMzQyxNQUFaLENBQW1CLENBQUN1RixLQUFELEVBQVFELE1BQVIsS0FBbUI7QUFDbkQsYUFBT0MsS0FBSyxHQUFHRCxNQUFNLENBQUN2SSxNQUF0QjtBQUNELEtBRmMsRUFFWixDQUZZLENBQWY7QUFHQSxRQUFJMkIsT0FBTyxHQUFHNEcsTUFBTSxHQUFHbkgsUUFBdkI7QUFFQSxRQUFJcUgsY0FBYyxHQUFHLENBQ25CO0FBQ0U1SSxXQUFLLEVBQUUsYUFEVDtBQUVFRyxZQUFNLEVBQUVvQixRQUZWO0FBR0VWLFdBQUssRUFBRTtBQUhULEtBRG1CLENBQXJCOztBQVFBLFFBQUlpQixPQUFPLElBQUksQ0FBZixFQUFrQjtBQUNoQm9GLHVCQUFpQixDQUFDLENBQ2hCO0FBQ0VsSCxhQUFLLEVBQUUsYUFEVDtBQUVFRyxjQUFNLEVBQUVvQixRQUZWO0FBR0VWLGFBQUssRUFBRTtBQUhULE9BRGdCLEVBTWhCO0FBQ0ViLGFBQUssRUFBRSxTQURUO0FBRUVHLGNBQU0sRUFBRTJCLE9BRlY7QUFHRWpCLGFBQUssRUFBRTtBQUhULE9BTmdCLENBQUQsQ0FBakI7QUFZRCxLQWJELE1BYU87QUFDTHFHLHVCQUFpQixDQUFDLENBQ2hCO0FBQ0VsSCxhQUFLLEVBQUUsYUFEVDtBQUVFRyxjQUFNLEVBQUV1SSxNQUZWO0FBR0U3SCxhQUFLLEVBQUU7QUFIVCxPQURnQixFQU1oQjtBQUNFYixhQUFLLEVBQUUsV0FEVDtBQUVFRyxjQUFNLEVBQUVPLElBQUksQ0FBQ0MsR0FBTCxDQUFTbUIsT0FBVCxDQUZWO0FBR0VqQixhQUFLLEVBQUU7QUFIVCxPQU5nQixDQUFELENBQWpCO0FBWUQ7QUFDRixHQTlDUSxFQThDTixDQUFDa0YsV0FBRCxFQUFjVSxRQUFkLEVBQXdCakMsU0FBeEIsRUFBbUNLLE9BQW5DLENBOUNNLENBQVQ7QUFnREF0RSx5REFBUyxDQUFDLE1BQU07QUFDZHlFLGtCQUFjLENBQ1pHLGFBQWEsQ0FBQy9CLE1BQWQsQ0FBcUIsQ0FBQ3VGLEtBQUQsRUFBUU4sTUFBUixLQUFtQjtBQUN0QyxhQUFPTSxLQUFLLEdBQUdOLE1BQU0sQ0FBQ2xJLE1BQXRCO0FBQ0QsS0FGRCxFQUVHLENBRkgsQ0FEWSxDQUFkO0FBS0ErRSxvQkFBZ0IsQ0FDZEcsZUFBZSxDQUFDakMsTUFBaEIsQ0FBdUIsQ0FBQ3VGLEtBQUQsRUFBUU4sTUFBUixLQUFtQjtBQUN4QyxhQUFPTSxLQUFLLEdBQUdOLE1BQU0sQ0FBQ2xJLE1BQXRCO0FBQ0QsS0FGRCxFQUVHLENBRkgsQ0FEYyxDQUFoQjtBQUtELEdBWFEsRUFXTixDQUFDcUUsU0FBRCxFQUFZSyxPQUFaLEVBQXFCTSxhQUFyQixFQUFvQ0UsZUFBcEMsQ0FYTSxDQUFUO0FBYUE5RSx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJMEYsSUFBSSxJQUFJLFFBQVosRUFBc0I7QUFDcEJULGdCQUFVLENBQUNwQixnQkFBRCxDQUFWO0FBQ0FzQixnQkFBVSxDQUFDUCxhQUFELENBQVY7QUFDRCxLQUhELE1BR087QUFDTEssZ0JBQVUsQ0FBQ2xCLGtCQUFELENBQVY7QUFDQW9CLGdCQUFVLENBQUNMLGVBQUQsQ0FBVjtBQUNEOztBQUNEaUIsaUJBQWEsQ0FBQyxLQUFELENBQWI7QUFDRCxHQVRRLEVBU04sQ0FBQ0wsSUFBRCxFQUFPL0IsT0FBUCxFQUFnQkUsZ0JBQWhCLEVBQWtDRSxrQkFBbEMsRUFBc0RFLFNBQXRELEVBQWlFSyxPQUFqRSxDQVRNLENBQVQ7QUFXQXRFLHlEQUFTLENBQUMsTUFBTTtBQUNkcUcsYUFBUyxDQUNQOUQsbUVBQVUsQ0FBQ3lDLGNBQUQsRUFBaUIsS0FBakIsRUFBd0JFLFdBQXhCLEVBQXFDLFlBQXJDLEVBQW1ELFFBQW5ELENBREgsQ0FBVDtBQUdBVyxnQkFBWSxDQUNWYixjQUFjLENBQUMvRSxHQUFmLENBQW9CcUgsUUFBRCxJQUFjO0FBQy9CLDBCQUFPO0FBQVEsYUFBSyxFQUFFQSxRQUFRLENBQUNXLEdBQXhCO0FBQUEsa0JBQThCWCxRQUFRLENBQUNZO0FBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FBUDtBQUNELEtBRkQsQ0FEVSxDQUFaO0FBS0QsR0FUUSxFQVNOLENBQUN4QyxJQUFELEVBQU9WLGNBQVAsRUFBdUJFLFdBQXZCLEVBQW9DakIsU0FBcEMsRUFBK0NLLE9BQS9DLENBVE0sQ0FBVDtBQVdBdEUseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSThGLFVBQVUsSUFBSSxLQUFsQixFQUF5QjtBQUN2QlQsZ0JBQVUsQ0FBQ0gsV0FBRCxDQUFWO0FBQ0QsS0FGRCxNQUVPO0FBQ0xHLGdCQUFVLENBQ1JILFdBQVcsQ0FBQ21DLE1BQVosQ0FBb0JTLE1BQUQsSUFBWUEsTUFBTSxDQUFDaEMsVUFBUCxJQUFxQkEsVUFBcEQsQ0FEUSxDQUFWO0FBR0Q7QUFDRixHQVJRLEVBUU4sQ0FBQ0osSUFBRCxFQUFPSSxVQUFQLEVBQW1CZCxjQUFuQixFQUFtQ0UsV0FBbkMsRUFBZ0RqQixTQUFoRCxFQUEyREssT0FBM0QsQ0FSTSxDQUFUO0FBVUF0RSx5REFBUyxDQUFDLE1BQU07QUFDZHVHLGtCQUFjLENBQUNoRSxtRUFBVSxDQUFDLEVBQUQsRUFBSyxFQUFMLEVBQVM2QyxXQUFULEVBQXNCLGFBQXRCLEVBQXFDLFFBQXJDLENBQVgsQ0FBZDtBQUNELEdBRlEsRUFFTixDQUNEVSxVQURDLEVBRURkLGNBRkMsRUFHREUsV0FIQyxFQUlERSxXQUpDLEVBS0RuQixTQUxDLEVBTURLLE9BTkMsQ0FGTSxDQUFUO0FBV0Esc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQUlFLHFFQUFDLG9EQUFEO0FBQUEsNkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxpQkFBUyxFQUFDLGdDQUFmO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxZQUFFLEVBQUMsTUFBUjtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFJLEVBQUMsTUFEUDtBQUVFLHNCQUFVLEVBQUMsWUFGYjtBQUdFLGlCQUFLLEVBQUVMLFNBSFQ7QUFJRSxvQkFBUSxFQUFHcUUsQ0FBRCxJQUFPcEUsWUFBWSxDQUFDb0UsQ0FBQyxDQUFDQyxNQUFGLENBQVN4RixLQUFWO0FBSi9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUpGLGVBWUUscUVBQUMsbURBQUQ7QUFBSyxZQUFFLEVBQUMsTUFBUjtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBWkYsZUFlRSxxRUFBQyxtREFBRDtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFJLEVBQUMsTUFEUDtBQUVFLGlCQUFLLEVBQUV1QixPQUZUO0FBR0Usc0JBQVUsRUFBQyxhQUhiO0FBSUUsb0JBQVEsRUFBR2dFLENBQUQsSUFBTy9ELFVBQVUsQ0FBQytELENBQUMsQ0FBQ0MsTUFBRixDQUFTeEYsS0FBVjtBQUo3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFmRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkYsZUE4QkUscUVBQUMsbURBQUQ7QUFBSyxlQUFTLEVBQUMsZ0NBQWY7QUFBQSw4QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBRUU7QUFBSyxpQkFBUyxFQUFDLDRCQUFmO0FBQUEsK0JBQ0UscUVBQUMsK0RBQUQ7QUFBWSxnQkFBTSxFQUFFeUIsV0FBcEI7QUFBaUMsa0JBQVEsRUFBRUU7QUFBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUE5QkYsZUFvQ0UscUVBQUMsbURBQUQ7QUFBSyxlQUFTLEVBQUMsZ0NBQWY7QUFBQSw4QkFDRSxxRUFBQyxtREFBRDtBQUFBLCtCQUNFLHFFQUFDLG9EQUFEO0FBQU0sbUJBQVMsRUFBQyxLQUFoQjtBQUFBLGtDQUNFLHFFQUFDLG9EQUFELENBQU0sTUFBTjtBQUFBLG1DQUNFLHFFQUFDLG1EQUFEO0FBQUsscUJBQU8sRUFBQyxNQUFiO0FBQUEsc0NBQ0UscUVBQUMsbURBQUQsQ0FBSyxJQUFMO0FBQVUsb0JBQUksRUFBQyxRQUFmO0FBQUEsdUNBQ0UscUVBQUMsbURBQUQsQ0FBSyxJQUFMO0FBQVUseUJBQU8sRUFBRzRELENBQUQsSUFBTzNDLE9BQU8sQ0FBQyxRQUFELENBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERixlQUlFLHFFQUFDLG1EQUFELENBQUssSUFBTDtBQUFVLHFCQUFLLEVBQUMsU0FBaEI7QUFBQSx1Q0FDRSxxRUFBQyxtREFBRCxDQUFLLElBQUw7QUFBVSx5QkFBTyxFQUFHMkMsQ0FBRCxJQUFPM0MsT0FBTyxDQUFDLFVBQUQsQ0FBakM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFhRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsYUFBZDtBQUFBLHlCQUNHRCxJQUFJLENBQUMsQ0FBRCxDQUFKLENBQVE4QyxXQUFSLEtBQXdCOUMsSUFBSSxDQUFDK0MsS0FBTCxDQUFXLENBQVgsQ0FEM0IsaUJBQ3FELEdBRHJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixlQUlFLHFFQUFDLDZEQUFEO0FBQ0UsdUJBQVMsRUFBQyxLQURaO0FBRUUscUJBQU8sRUFBRXJDLFdBRlg7QUFHRSxzQkFBUSxFQUFFLE1BSFo7QUFJRSx1QkFBUyxFQUFFO0FBSmI7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBYkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBNkJFLHFFQUFDLG1EQUFEO0FBQUEsK0JBQ0UscUVBQUMsb0RBQUQ7QUFBTSxtQkFBUyxFQUFDLE1BQWhCO0FBQUEsa0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxNQUFOO0FBQUEsbUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksdUJBQVMsRUFBQyxZQUF0QjtBQUFtQyx1QkFBUyxFQUFDLE1BQTdDO0FBQUEscUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0Usa0JBQUUsRUFBQyxRQURMO0FBRUUscUJBQUssRUFBRU4sVUFGVDtBQUdFLHdCQUFRLEVBQUd3QyxDQUFELElBQU92QyxhQUFhLENBQUN1QyxDQUFDLENBQUNDLE1BQUYsQ0FBU3hGLEtBQVYsQ0FIaEM7QUFJRSx3QkFBUSxNQUpWO0FBQUEsd0NBTUU7QUFBUSx1QkFBSyxFQUFDLEtBQWQ7QUFBb0IsMEJBQVEsTUFBNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTkYsRUFTRzZDLFNBVEg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFnQkUscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsb0NBQ0U7QUFBSSx1QkFBUyxFQUFDLGFBQWQ7QUFBQSx5QkFDR0YsSUFBSSxDQUFDLENBQUQsQ0FBSixDQUFROEMsV0FBUixLQUF3QjlDLElBQUksQ0FBQytDLEtBQUwsQ0FBVyxDQUFYLENBRDNCLGlCQUNxRCxHQURyRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFJRSxxRUFBQyw2REFBRDtBQUNFLHVCQUFTLEVBQUMsVUFEWjtBQUVFLHFCQUFPLEVBQUVuQyxjQUZYO0FBR0Usc0JBQVEsRUFBRSxhQUhaO0FBSUUsdUJBQVMsRUFBRTtBQUpiO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWhCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBN0JGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXBDRixlQWlHRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyxnQ0FBZjtBQUFBLDhCQUNFLHFFQUFDLG1EQUFEO0FBQUEsK0JBQ0UscUVBQUMsb0RBQUQ7QUFBTSxtQkFBUyxFQUFDLEtBQWhCO0FBQUEsa0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxNQUFOO0FBQUEsbUNBQ0UscUVBQUMsbURBQUQ7QUFBSyxxQkFBTyxFQUFDLE1BQWI7QUFBQSxxQ0FDRSxxRUFBQyxtREFBRCxDQUFLLElBQUw7QUFBVSxvQkFBSSxFQUFDLFFBQWY7QUFBQSx1Q0FDRSxxRUFBQyxtREFBRCxDQUFLLElBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFRRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsYUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixlQUVFLHFFQUFDLDZEQUFEO0FBQ0UsdUJBQVMsRUFBQyxLQURaO0FBRUUscUJBQU8sRUFBRUUsU0FGWDtBQUdFLHNCQUFRLEVBQUUsTUFIWjtBQUlFLHVCQUFTLEVBQUU7QUFKYjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFzQkUscUVBQUMsbURBQUQ7QUFBQSwrQkFDRSxxRUFBQyxvREFBRDtBQUFNLG1CQUFTLEVBQUMsS0FBaEI7QUFBQSxrQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE1BQU47QUFBQSxtQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSx1QkFBUyxFQUFDLFlBQXRCO0FBQW1DLHVCQUFTLEVBQUMsTUFBN0M7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxrQkFBRSxFQUFDLFFBREw7QUFFRSxxQkFBSyxFQUFFTixRQUZUO0FBR0Usd0JBQVEsRUFBR29DLENBQUQsSUFBT25DLFdBQVcsQ0FBQ21DLENBQUMsQ0FBQ0MsTUFBRixDQUFTeEYsS0FBVixDQUg5QjtBQUlFLHdCQUFRLE1BSlY7QUFBQSx3Q0FNRTtBQUFRLHVCQUFLLEVBQUMsS0FBZDtBQUFvQiwwQkFBUSxNQUE1QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFORixlQVNFO0FBQVEsdUJBQUssRUFBQyxZQUFkO0FBQTJCLDBCQUFRLE1BQW5DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVRGLGVBWUU7QUFBUSx1QkFBSyxFQUFDLGNBQWQ7QUFBNkIsMEJBQVEsTUFBckM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBWkYsZUFlRTtBQUFRLHVCQUFLLEVBQUMsZ0JBQWQ7QUFBK0IsMEJBQVEsTUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBZkYsRUFrQkdpRCxZQWxCSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQXlCRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsYUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixlQUVFLHFFQUFDLDZEQUFEO0FBQ0UsdUJBQVMsRUFBQyxVQURaO0FBRUUscUJBQU8sRUFBRVUsY0FGWDtBQUdFLHNCQUFRLEVBQUUsT0FIWjtBQUlFLHVCQUFTLEVBQUU7QUFKYjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkF6QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXRCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFqR0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFnS0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RaRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNZLFFBQVQsR0FBb0I7QUFDakMsUUFBTTtBQUFBLE9BQUNGLFVBQUQ7QUFBQSxPQUFhc0I7QUFBYixNQUE4Qi9JLHNEQUFRLENBQUMsRUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDZ0osY0FBRDtBQUFBLE9BQWlCQztBQUFqQixNQUE4QmpKLHNEQUFRLENBQUMsRUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDa0UsZ0JBQUQ7QUFBQSxPQUFtQkM7QUFBbkIsTUFBZ0NuRSxzREFBUSxDQUFDLEVBQUQsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBQ29FLGtCQUFEO0FBQUEsT0FBcUJDO0FBQXJCLE1BQWtDckUsc0RBQVEsQ0FBQyxFQUFELENBQWhEO0FBQ0EsUUFBTTtBQUFBLE9BQUNrSixVQUFEO0FBQUEsT0FBYUM7QUFBYixNQUE4Qm5KLHNEQUFRLENBQUMsSUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDb0osWUFBRDtBQUFBLE9BQWVDO0FBQWYsTUFBa0NySixzREFBUSxDQUFDLElBQUQsQ0FBaEQ7QUFFQUsseURBQVMsQ0FBQyxNQUFNO0FBQ2Q0RyxTQUFLLENBQUUseUNBQUYsRUFBNEM7QUFDL0NDLGFBQU8sRUFBRTtBQUNQQyxxQkFBYSxFQUFHLFVBQVM1SCxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBOEI7QUFEaEQ7QUFEc0MsS0FBNUMsQ0FBTCxDQUtHNEgsSUFMSCxDQUtTakUsR0FBRCxJQUFTQSxHQUFHLENBQUNrRSxJQUFKLEVBTGpCLEVBTUdELElBTkgsQ0FNU25HLElBQUQsSUFBVTtBQUNkOEgsbUJBQWEsQ0FBQzlILElBQUksQ0FBQ3dHLFVBQU4sQ0FBYjtBQUVBLFVBQUlLLFVBQVUsR0FBR3RELDZDQUFNLENBQUN2RCxJQUFJLENBQUM0RyxXQUFMLENBQWlCdkQsU0FBbEIsQ0FBTixDQUFtQ3lELFFBQW5DLENBQTRDLENBQTVDLEVBQStDLEtBQS9DLENBQWpCO0FBQ0EsVUFBSUMsU0FBUyxHQUFHeEQsNkNBQU0sQ0FBQ3ZELElBQUksQ0FBQzRHLFdBQUwsQ0FBaUJsRCxPQUFsQixDQUFOLENBQWlDc0QsR0FBakMsQ0FBcUMsQ0FBckMsRUFBd0MsS0FBeEMsQ0FBaEI7QUFFQSxVQUFJQyxlQUFlLEdBQUdqSCxJQUFJLENBQUMrQyxPQUFMLENBQWEwRCxNQUFiLENBQXFCUyxNQUFELElBQVk7QUFDcEQsWUFBSUMsU0FBUyxHQUFHNUQsNkNBQU0sQ0FBQzJELE1BQU0sQ0FBQ0MsU0FBUixDQUFOLENBQXlCM0QsT0FBekIsQ0FBaUMsTUFBakMsQ0FBaEI7QUFDQSxlQUFPRCw2Q0FBTSxDQUFDNEQsU0FBRCxDQUFOLENBQWtCQyxTQUFsQixDQUE0QlAsVUFBNUIsRUFBd0NFLFNBQXhDLEVBQW1ELEtBQW5ELENBQVA7QUFDRCxPQUhxQixDQUF0QjtBQUtBaUIsZUFBUyxDQUFDZixlQUFELENBQVQ7QUFDRCxLQWxCSDtBQW1CRCxHQXBCUSxFQW9CTixFQXBCTSxDQUFUO0FBc0JBN0gseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSWlKLGVBQWUsR0FBRzFHLG1FQUFVLENBQzlCNkUsVUFEOEIsRUFFOUIsS0FGOEIsRUFHOUJ1QixjQUg4QixFQUk5QixZQUo4QixFQUs5QixRQUw4QixDQUFoQztBQVFBN0UsYUFBUyxDQUFDbUYsZUFBZSxDQUFDNUIsTUFBaEIsQ0FBd0JDLFFBQUQsSUFBY0EsUUFBUSxDQUFDNUIsSUFBVCxJQUFpQixRQUF0RCxDQUFELENBQVQ7QUFDQTFCLGFBQVMsQ0FBQ2lGLGVBQWUsQ0FBQzVCLE1BQWhCLENBQXdCQyxRQUFELElBQWNBLFFBQVEsQ0FBQzVCLElBQVQsSUFBaUIsU0FBdEQsQ0FBRCxDQUFUO0FBQ0QsR0FYUSxFQVdOLENBQUMwQixVQUFELEVBQWF1QixjQUFiLENBWE0sQ0FBVDtBQWFBM0kseURBQVMsQ0FBQyxNQUFNO0FBQ2Q4SSxpQkFBYSxDQUNYakYsZ0JBQWdCLENBQUM1RCxHQUFqQixDQUFzQnFILFFBQUQsSUFBYztBQUNqQywwQkFDRTtBQUFBLGdDQUNFO0FBQUEsb0JBQUtBLFFBQVEsQ0FBQ1k7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBQSxvQkFBS1osUUFBUSxDQUFDMUg7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUEsU0FBUzBILFFBQVEsQ0FBQ1csR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGO0FBTUQsS0FQRCxDQURXLENBQWI7QUFVRCxHQVhRLEVBV04sQ0FBQ3BFLGdCQUFELENBWE0sQ0FBVDtBQWFBN0QseURBQVMsQ0FBQyxNQUFNO0FBQ2RnSixtQkFBZSxDQUNiakYsa0JBQWtCLENBQUM5RCxHQUFuQixDQUF3QnFILFFBQUQsSUFBYztBQUNuQywwQkFDRTtBQUFBLGdDQUNFO0FBQUEsb0JBQUtBLFFBQVEsQ0FBQ1k7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBQSxvQkFBS1osUUFBUSxDQUFDYTtBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkYsZUFHRTtBQUFBLG9CQUFLaEksSUFBSSxDQUFDQyxHQUFMLENBQVNrSCxRQUFRLENBQUMxSCxNQUFsQjtBQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSEYsZUFJRTtBQUFBLG9CQUFLMEgsUUFBUSxDQUFDMUgsTUFBVCxHQUFrQjBILFFBQVEsQ0FBQ2E7QUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFKRjtBQUFBLFNBQVNiLFFBQVEsQ0FBQ1csR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGO0FBUUQsS0FURCxDQURhLENBQWY7QUFZRCxHQWJRLEVBYU4sQ0FBQ2xFLGtCQUFELENBYk0sQ0FBVDtBQWVBLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyw2QkFBZjtBQUFBLDhCQUNFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsU0FIVjtBQUlFLGVBQU8sRUFBRSxNQUFNbUYsa0RBQU0sQ0FBQ2pHLElBQVAsQ0FBWSxnQkFBWixDQUpqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBU0UscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxXQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU1pRyxrREFBTSxDQUFDakcsSUFBUCxDQUFZLGlCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBVEYsZUFpQkUscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxRQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU1pRyxrREFBTSxDQUFDakcsSUFBUCxDQUFZLGdCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakJGLGVBMEJFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsTUFIVjtBQUlFLGVBQU8sRUFBRSxNQUFNaUcsa0RBQU0sQ0FBQ2pHLElBQVAsQ0FBWSxlQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBMUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUpGLGVBdUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBdkNGLEVBd0NHWSxnQkFBZ0IsQ0FBQ3NGLE1BQWpCLElBQTJCLENBQTNCLGdCQUNDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxFQUFDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkFHQyxxRUFBQyxxREFBRDtBQUFPLGFBQU8sTUFBZDtBQUFlLGNBQVEsTUFBdkI7QUFBd0IsV0FBSyxNQUE3QjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBQSxrQ0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVFFO0FBQUEsa0JBQVFOO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQTNDSixlQXNERTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXRERixFQXVERzlFLGtCQUFrQixDQUFDb0YsTUFBbkIsSUFBNkIsQ0FBN0IsZ0JBQ0MscUVBQUMscURBQUQ7QUFBTyxhQUFPLEVBQUMsTUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURELGdCQUdDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxNQUFkO0FBQWUsY0FBUSxNQUF2QjtBQUF3QixXQUFLLE1BQTdCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRixlQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVVFO0FBQUEsa0JBQVFKO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQTFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQTBFRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNLLEtBQVQsR0FBaUI7QUFDOUIsc0JBQ0UscUVBQUMsd0RBQUQ7QUFBTSxTQUFLLEVBQUUsT0FBYjtBQUFBLDJCQUNFLHFFQUFDLG1EQUFEO0FBQUssZUFBUyxFQUFDLHdCQUFmO0FBQUEsNkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxVQUFFLE1BQVA7QUFBUSxVQUFFLEVBQUMsR0FBWDtBQUFBLCtCQUNFLHFFQUFDLFNBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBU0Q7O0FBRUQsTUFBTUMsU0FBUyxHQUFHLE1BQU07QUFDdEIsUUFBTTtBQUFFN0YsUUFBRjtBQUFRQztBQUFSLE1BQW9CQyx3REFBVSxDQUFDaEYsb0RBQUQsQ0FBcEM7QUFFQSxRQUFNO0FBQUEsT0FBQzRLLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CNUosc0RBQVEsQ0FBQyxFQUFELENBQWxDO0FBQ0EsUUFBTTtBQUFBLE9BQUM2SixRQUFEO0FBQUEsT0FBV0M7QUFBWCxNQUEwQjlKLHNEQUFRLENBQUMsRUFBRCxDQUF4QztBQUNBLFFBQU07QUFBQSxPQUFDK0osUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJoSyxzREFBUSxDQUFDLEtBQUQsQ0FBeEM7O0FBRUEsV0FBU2lLLFlBQVQsQ0FBc0J0QixDQUF0QixFQUF5QjtBQUN2QkEsS0FBQyxDQUFDdUIsY0FBRjtBQUVBLFVBQU1wSSxPQUFPLEdBQUc7QUFDZHFJLFlBQU0sRUFBRSxNQURNO0FBRWRqRCxhQUFPLEVBQUU7QUFBRSx3QkFBZ0I7QUFBbEIsT0FGSztBQUdka0QsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNuQlgsYUFBSyxFQUFFQSxLQURZO0FBRW5CRSxnQkFBUSxFQUFFQTtBQUZTLE9BQWY7QUFIUSxLQUFoQjtBQVNBNUMsU0FBSyxDQUFFLHVDQUFGLEVBQTBDbkYsT0FBMUMsQ0FBTCxDQUNHc0YsSUFESCxDQUNTakUsR0FBRCxJQUFTQSxHQUFHLENBQUNrRSxJQUFKLEVBRGpCLEVBRUdELElBRkgsQ0FFU25HLElBQUQsSUFBVTtBQUNkcUcsYUFBTyxDQUFDQyxHQUFSLENBQVl0RyxJQUFaOztBQUNBLFVBQUksT0FBT0EsSUFBSSxDQUFDc0osV0FBWixLQUE0QixXQUFoQyxFQUE2QztBQUMzQ2hMLG9CQUFZLENBQUNpTCxPQUFiLENBQXFCLE9BQXJCLEVBQThCdkosSUFBSSxDQUFDc0osV0FBbkM7QUFDQXpHLGVBQU8sQ0FBQztBQUFFMkcsWUFBRSxFQUFFeEosSUFBSSxDQUFDcUg7QUFBWCxTQUFELENBQVA7QUFDQWlCLDBEQUFNLENBQUNqRyxJQUFQLENBQVksR0FBWjtBQUNELE9BSkQsTUFJTztBQUNMLFlBQUlyQyxJQUFJLENBQUN5SixLQUFMLEtBQWUsZ0JBQW5CLEVBQXFDO0FBQ25DQyw0REFBSSxDQUFDQyxJQUFMLENBQVUsdUJBQVYsRUFBbUMsc0JBQW5DLEVBQTJELE9BQTNEO0FBQ0QsU0FGRCxNQUVPLElBQUkzSixJQUFJLENBQUN5SixLQUFMLEtBQWUsa0JBQW5CLEVBQXVDO0FBQzVDQyw0REFBSSxDQUFDQyxJQUFMLENBQ0Usa0JBREYsRUFFRSxpR0FGRixFQUdFLE9BSEY7QUFLRCxTQU5NLE1BTUEsSUFBSTNKLElBQUksQ0FBQ3lKLEtBQUwsS0FBZSxvQkFBbkIsRUFBeUM7QUFDOUNDLDREQUFJLENBQUNDLElBQUwsQ0FDRSx1QkFERixFQUVFLHVCQUZGLEVBR0UsT0FIRjtBQUtEO0FBQ0Y7QUFDRixLQXpCSDtBQTBCRDs7QUFFRCxRQUFNQyx3QkFBd0IsR0FBSUMsUUFBRCxJQUFjO0FBQzdDLFVBQU1DLE9BQU8sR0FBRztBQUNkWixZQUFNLEVBQUUsTUFETTtBQUVkakQsYUFBTyxFQUFFO0FBQUUsd0JBQWdCO0FBQWxCLE9BRks7QUFHZGtELFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFBRVUsZUFBTyxFQUFFRixRQUFRLENBQUNFO0FBQXBCLE9BQWY7QUFIUSxLQUFoQjtBQU1BL0QsU0FBSyxDQUFFLHdEQUFGLEVBQTJEOEQsT0FBM0QsQ0FBTCxDQUNHM0QsSUFESCxDQUNTakUsR0FBRCxJQUFTQSxHQUFHLENBQUNrRSxJQUFKLEVBRGpCLEVBRUdELElBRkgsQ0FFU25HLElBQUQsSUFBVTtBQUNkLFVBQUksT0FBT0EsSUFBSSxDQUFDc0osV0FBWixLQUE0QixXQUFoQyxFQUE2QztBQUMzQ2hMLG9CQUFZLENBQUNpTCxPQUFiLENBQXFCLE9BQXJCLEVBQThCdkosSUFBSSxDQUFDc0osV0FBbkM7QUFDQXpHLGVBQU8sQ0FBQztBQUFFMkcsWUFBRSxFQUFFeEosSUFBSSxDQUFDcUg7QUFBWCxTQUFELENBQVA7QUFDQWlCLDBEQUFNLENBQUNqRyxJQUFQLENBQVksR0FBWjtBQUNELE9BSkQsTUFJTztBQUNMLFlBQUtyQyxJQUFJLENBQUN5SixLQUFMLEdBQWEsbUJBQWxCLEVBQXdDO0FBQ3RDQyw0REFBSSxDQUFDQyxJQUFMLENBQ0UsbUJBREYsRUFFRSx5Q0FGRixFQUdFLE9BSEY7QUFLRCxTQU5ELE1BTU8sSUFBSzNKLElBQUksQ0FBQ3lKLEtBQUwsR0FBYSxrQkFBbEIsRUFBdUM7QUFDNUNDLDREQUFJLENBQUNDLElBQUwsQ0FDRSxrQkFERixFQUVFLDhEQUZGLEVBR0UsT0FIRjtBQUtEO0FBQ0Y7QUFDRixLQXRCSDtBQXVCRCxHQTlCRDs7QUFnQ0F2Syx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJc0osS0FBSyxLQUFLLEVBQVYsSUFBZ0JFLFFBQVEsS0FBSyxFQUFqQyxFQUFxQztBQUNuQ0csaUJBQVcsQ0FBQyxJQUFELENBQVg7QUFDRCxLQUZELE1BRU87QUFDTEEsaUJBQVcsQ0FBQyxLQUFELENBQVg7QUFDRDtBQUNGLEdBTlEsRUFNTixDQUFDTCxLQUFELEVBQVFFLFFBQVIsQ0FOTSxDQUFUO0FBUUEsc0JBQ0UscUVBQUMsb0RBQUQ7QUFBQSw0QkFDRSxxRUFBQyxvREFBRCxDQUFNLE1BQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSw4QkFDRSxxRUFBQyxnREFBRDtBQUFBLCtCQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUlFLHFFQUFDLG9EQUFEO0FBQU0sZ0JBQVEsRUFBR2xCLENBQUQsSUFBT3NCLFlBQVksQ0FBQ3RCLENBQUQsQ0FBbkM7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxtQkFBUyxFQUFDLFdBQXRCO0FBQUEsa0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsZ0JBQUksRUFBQyxPQURQO0FBRUUsdUJBQVcsRUFBQyxhQUZkO0FBR0UsaUJBQUssRUFBRWdCLEtBSFQ7QUFJRSxvQkFBUSxFQUFHaEIsQ0FBRCxJQUFPaUIsUUFBUSxDQUFDakIsQ0FBQyxDQUFDQyxNQUFGLENBQVN4RixLQUFWLENBSjNCO0FBS0Usb0JBQVE7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERixlQVlFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLG1CQUFTLEVBQUMsVUFBdEI7QUFBQSxrQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxnQkFBSSxFQUFDLFVBRFA7QUFFRSx1QkFBVyxFQUFDLFVBRmQ7QUFHRSxpQkFBSyxFQUFFeUcsUUFIVDtBQUlFLG9CQUFRLEVBQUdsQixDQUFELElBQU9tQixXQUFXLENBQUNuQixDQUFDLENBQUNDLE1BQUYsQ0FBU3hGLEtBQVYsQ0FKOUI7QUFLRSxvQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVpGLGVBc0JFLHFFQUFDLG1EQUFEO0FBQUssbUJBQVMsRUFBQyw2QkFBZjtBQUFBLG9CQUNHMkcsUUFBUSxnQkFDUCxxRUFBQyxzREFBRDtBQUFRLHFCQUFTLEVBQUMsc0JBQWxCO0FBQXlDLGdCQUFJLEVBQUMsUUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRE8sZ0JBS1AscUVBQUMsc0RBQUQ7QUFBUSxxQkFBUyxFQUFDLHNCQUFsQjtBQUF5QyxnQkFBSSxFQUFDLFFBQTlDO0FBQXVELG9CQUFRLE1BQS9EO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTko7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkF0QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUpGLGVBdUNFLHFFQUFDLDhEQUFEO0FBQ0UsZ0JBQVEsRUFBQywwRUFEWDtBQUVFLGtCQUFVLEVBQUMsT0FGYjtBQUdFLGlCQUFTLEVBQUVjLHdCQUhiO0FBSUUsaUJBQVMsRUFBRUEsd0JBSmI7QUFLRSxvQkFBWSxFQUFFLG9CQUxoQjtBQU1FLGlCQUFTLEVBQUM7QUFOWjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQXZDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFxREQsQ0E1SUQsQzs7Ozs7Ozs7Ozs7QUN0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDYkEsbUM7Ozs7Ozs7Ozs7O0FDQUEsc0M7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsNEM7Ozs7Ozs7Ozs7O0FDQUEsNEM7Ozs7Ozs7Ozs7O0FDQUEsK0M7Ozs7Ozs7Ozs7O0FDQUEsa0Q7Ozs7Ozs7Ozs7O0FDQUEsd0MiLCJmaWxlIjoicGFnZXMvYnJlYWtkb3duLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9icmVha2Rvd24uanNcIik7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuLy8gY3JlYXRlcyBhIENvbnRleHQgT2JqZWN0XHJcbmNvbnN0IFVzZXJDb250ZXh0ID0gUmVhY3QuY3JlYXRlQ29udGV4dCgpO1xyXG5cclxuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyXHJcblxyXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDsiLCJtb2R1bGUuZXhwb3J0cyA9IHtcclxuXHRnZXRBY2Nlc3NUb2tlbjogKCkgPT4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJyksXHJcbn1cclxuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBEb3VnaG51dCwgUGllIH0gZnJvbSBcInJlYWN0LWNoYXJ0anMtMlwiO1xyXG5pbXBvcnQgQ29sb3JSYW5kb21pemVyIGZyb20gXCIuLi9oZWxwZXJzL0NvbG9yUmFuZG9taXplclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUGllQ2hhcnQoeyBjaGFydFR5cGUsIHJhd0RhdGEsIGxhYmVsS2V5LCBhbW91bnRLZXkgfSkge1xyXG4gIGNvbnN0IFtsYWJlbCwgc2V0TGFiZWxdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFthbW91bnQsIHNldEFtb3VudF0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2JnQ29sb3JzLCBzZXRCZ0NvbG9yc10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAocmF3RGF0YSkge1xyXG4gICAgICBzZXRMYWJlbChyYXdEYXRhLm1hcCgoZWxlbWVudCkgPT4gZWxlbWVudFtsYWJlbEtleV0pKTtcclxuICAgICAgc2V0QW1vdW50KHJhd0RhdGEubWFwKChlbGVtZW50KSA9PiBNYXRoLmFicyhlbGVtZW50W2Ftb3VudEtleV0pKSk7XHJcblxyXG4gICAgICBsZXQgY29sb3JzID0gcmF3RGF0YS5tYXAoKGVsZW1lbnQpID0+IHtcclxuICAgICAgICBpZiAoZWxlbWVudC5jb2xvcikge1xyXG4gICAgICAgICAgcmV0dXJuIGVsZW1lbnQuY29sb3I7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiBgIyR7Q29sb3JSYW5kb21pemVyKCl9YDtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgc2V0QmdDb2xvcnMoY29sb3JzKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldExhYmVsKFtdKTtcclxuICAgICAgc2V0QW1vdW50KFtdKTtcclxuICAgICAgc2V0QmdDb2xvcnMoW10pO1xyXG4gICAgfVxyXG4gIH0sIFtyYXdEYXRhXSk7XHJcblxyXG4gIGlmIChjaGFydFR5cGUgPT0gXCJQaWVcIikge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFBpZVxyXG4gICAgICAgIGRhdGE9e3tcclxuICAgICAgICAgIGxhYmVsczogbGFiZWwsXHJcbiAgICAgICAgICBsZWdlbmQ6IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IFwibGVmdFwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBkYXRhOiBhbW91bnQsXHJcbiAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBiZ0NvbG9ycyxcclxuICAgICAgICAgICAgICBob3ZlckJhY2tncm91bmQ6IGJnQ29sb3JzLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgXSxcclxuICAgICAgICB9fVxyXG4gICAgICAgIHJlZHJhdz17ZmFsc2V9XHJcbiAgICAgIC8+XHJcbiAgICApO1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8RG91Z2hudXRcclxuICAgICAgICBkYXRhPXt7XHJcbiAgICAgICAgICBsYWJlbHM6IGxhYmVsLFxyXG4gICAgICAgICAgbGVnZW5kOiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImxlZnRcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBkYXRhc2V0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgZGF0YTogYW1vdW50LFxyXG4gICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogYmdDb2xvcnMsXHJcbiAgICAgICAgICAgICAgaG92ZXJCYWNrZ3JvdW5kOiBiZ0NvbG9ycyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIF0sXHJcbiAgICAgICAgfX1cclxuICAgICAgLz5cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgSG9yaXpvbnRhbEJhciB9IGZyb20gXCJyZWFjdC1jaGFydGpzLTJcIjtcclxuaW1wb3J0IENvbG9yUmFuZG9taXplciBmcm9tIFwiLi4vaGVscGVycy9Db2xvclJhbmRvbWl6ZXJcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFBpZUNoYXJ0KHsgaW5jb21lLCBleHBlbnNlcyB9KSB7XHJcbiAgLy8gU3RhdGUgZm9yIDJuZCBkYXRhIGVpdGhlciBzYXZpbmdzL292ZXJzcGVudFxyXG4gIGNvbnN0IFtjb25zdW1wdGlvbiwgc2V0Q29uc3VtcHRpb25dID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtsYWJlbDIsIHNldExhYmVsMl0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2NvbG9yMiwgc2V0Y29sb3IyXSA9IHVzZVN0YXRlKFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGxldCBzYXZpbmdzID0gaW5jb21lIC0gZXhwZW5zZXM7XHJcblxyXG4gICAgaWYgKHNhdmluZ3MgPCAwKSB7XHJcbiAgICAgIHNldENvbnN1bXB0aW9uKGluY29tZSk7XHJcbiAgICAgIHNldExhYmVsMihcIm92ZXJzcGVudFwiKTtcclxuICAgICAgc2V0Y29sb3IyKFwiI2UzMDcwN1wiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldENvbnN1bXB0aW9uKGV4cGVuc2VzKTtcclxuICAgICAgc2V0TGFiZWwyKFwic2F2aW5nc1wiKTtcclxuICAgICAgc2V0Y29sb3IyKFwiIzQ3YjgzZFwiKTtcclxuICAgIH1cclxuICB9LCBbaW5jb21lLCBleHBlbnNlc10pO1xyXG5cclxuICBjb25zdCBhcmJpdHJhcnlTdGFja0tleSA9IFwic3RhY2sxXCI7XHJcbiAgY29uc3QgZGF0YSA9IHtcclxuICAgIG9wdGlvbnM6IHtcclxuICAgICAgc2NhbGVzOiB7XHJcbiAgICAgICAgeDoge1xyXG4gICAgICAgICAgc3RhY2tlZDogdHJ1ZSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIHk6IHtcclxuICAgICAgICAgIHN0YWNrZWQ6IHRydWUsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgaW5kZXhBeGlzOiBcInhcIixcclxuICAgIH0sXHJcbiAgICBsYWJlbHM6IFtcIlwiXSxcclxuICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgIC8vIFRoZXNlIHR3byB3aWxsIGJlIGluIHRoZSBzYW1lIHN0YWNrLlxyXG4gICAgICB7XHJcbiAgICAgICAgc3RhY2s6IFwiXCIsXHJcbiAgICAgICAgbGFiZWw6IFwiY29uc3VtcHRpb25cIixcclxuICAgICAgICBkYXRhOiBbY29uc3VtcHRpb25dLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgYm9yZGVyV2lkdGg6IDAuMDEsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBzdGFjazogXCJcIixcclxuICAgICAgICBsYWJlbDogbGFiZWwyLFxyXG4gICAgICAgIGRhdGE6IFtNYXRoLmFicyhpbmNvbWUgLSBleHBlbnNlcyldLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogY29sb3IyLFxyXG4gICAgICAgIGJvcmRlcldpZHRoOiAwLjAxLFxyXG4gICAgICB9LFxyXG4gICAgXSxcclxuICB9O1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPEhvcml6b250YWxCYXJcclxuICAgICAgY2xhc3NOYW1lPVwiY29udGFpbmVyLWZsdWlkXCJcclxuICAgICAgd2lkdGg9XCI3MCVcIlxyXG4gICAgICBoZWlnaHQ9XCI4cHhcIlxyXG4gICAgICBkYXRhPXtkYXRhfVxyXG4gICAgLz5cclxuICApO1xyXG59XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgeyBDb250YWluZXIgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcblxyXG4vL290aGVyIHN5bnRheFxyXG5jb25zdCBWaWV3ID0gKHsgdGl0bGUsIGNoaWxkcmVuIH0pID0+IHtcclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGUga2V5PVwidGl0bGUtdGFnXCI+e3RpdGxlfTwvdGl0bGU+XHJcbiAgICAgICAgPG1ldGFcclxuICAgICAgICAgIGtleT1cInRpdGxlLW1ldGFcIlxyXG4gICAgICAgICAgbmFtZT1cInZpZXdwb3J0XCJcclxuICAgICAgICAgIGNvbnRlbnQ9XCJpbml0aWFsLXNjYWxlPTEuMCwgd2lkdGg9ZGV2aWNlLXdpZHRoXCJcclxuICAgICAgICAvPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxDb250YWluZXIgY2xhc3NOYW1lPVwibXQtNSBwdC00IG1iLTVcIj57Y2hpbGRyZW59PC9Db250YWluZXI+XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBWaWV3O1xyXG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAgQ29sb3JSYW5kb21pemVyICgpe1xyXG5cdHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkqMTY3NzcyMTUpLnRvU3RyaW5nKDE2KTtcclxufVxyXG5cclxuXHJcbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHN1bUJ5R3JvdXAobGFiZWxBcnJheSxsYWJlbEtleSxhcnJheSxrZXksa2V5U3VtKXtcclxuXHJcblx0XHJcblx0bGV0IHN1bUNvdW50cz1bXVxyXG5cclxuICAgIGFycmF5LnJlZHVjZShmdW5jdGlvbihyZXMsIHZhbHVlKSB7XHJcblx0XHRpZiAoIXJlc1t2YWx1ZS5ba2V5XV0pIHtcclxuXHRcdFx0cmVzW3ZhbHVlLltrZXldXSA9IHsgW2tleV06IHZhbHVlLltrZXldLCBba2V5U3VtXTogMCwgY291bnQ6MCB9O1xyXG5cdFx0XHRzdW1Db3VudHMucHVzaChyZXNbdmFsdWUuW2tleV1dKVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRyZXNbdmFsdWUuW2tleV1dLltrZXlTdW1dICs9IHZhbHVlLltrZXlTdW1dXHJcblx0XHRyZXNbdmFsdWUuW2tleV1dLmNvdW50ICs9IDFcclxuXHRcdHJldHVybiByZXNcclxuXHR9LCB7fSlcclxuXHJcblxyXG5cdFxyXG5cdGlmKGxhYmVsQXJyYXkhPVtdICYgbGFiZWxLZXkhPSBcIlwiKXtcclxuICAgICAgICBcclxuICAgICAgICBsZXQgbGFiZWxTdW1Db3VudHMgPSBsYWJlbEFycmF5Lm1hcChsYWJlbD0+e1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICBsZXQgZm91bmRTdW1Db3VudCA9IHN1bUNvdW50cy5maW5kKHN1bUNvdW50ID0+IHN1bUNvdW50LltrZXldPT0gbGFiZWwuW2xhYmVsS2V5XSlcclxuXHJcbiAgICAgICAgICAgIGlmKGZvdW5kU3VtQ291bnQgPT0gdW5kZWZpbmVkKXtcclxuICAgICAgICAgICAgICAgIGxhYmVsLltrZXlTdW1dID0gMFxyXG4gICAgICAgICAgICAgICAgbGFiZWwuY291bnQgPSAwXHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgbGFiZWwuW2tleVN1bV0gPSBmb3VuZFN1bUNvdW50LltrZXlTdW1dXHJcbiAgICAgICAgICAgICAgICBsYWJlbC5jb3VudCA9IGZvdW5kU3VtQ291bnQuY291bnRcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGxhYmVsXHJcbiAgICAgICAgfSlcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4obGFiZWxTdW1Db3VudHMpXHJcblx0fWVsc2V7XHJcblx0XHRyZXR1cm4oc3VtQ291bnRzKVxyXG5cdH1cclxuXHJcblx0XHJcbn07XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZUNvbnRleHQsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IHsgQ29udGFpbmVyLCBGb3JtLCBDb2wsIFJvdywgQ2FyZCwgTmF2LCBCdXR0b24gfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBzdHlsZXMgZnJvbSBcIi4uL3N0eWxlcy9Ib21lLm1vZHVsZS5jc3NcIjtcclxuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gXCIuLi9Vc2VyQ29udGV4dFwiO1xyXG5pbXBvcnQgTG9naW4gZnJvbSBcIi4vbG9naW5cIjtcclxuaW1wb3J0IENhdGVnb3J5IGZyb20gXCIuL2NhdGVnb3J5XCI7XHJcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG5pbXBvcnQgc3VtQnlHcm91cCBmcm9tIFwiLi4vaGVscGVycy9zdW1CeUdyb3VwXCI7XHJcbmltcG9ydCBQaWVDaGFydCBmcm9tIFwiLi4vY29tcG9uZW50cy9QaWVDaGFydFwiO1xyXG5pbXBvcnQgU3RhY2tlZEJhciBmcm9tIFwiLi4vY29tcG9uZW50cy9TdGFja2VkQmFyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBCcmVha2Rvd24oKSB7XHJcbiAgY29uc3QgeyB1c2VyLCBzZXRVc2VyIH0gPSB1c2VDb250ZXh0KFVzZXJDb250ZXh0KTtcclxuICAvL1N0YXRlcyBTZXQgYWZ0ZXIgZmV0Y2hpbmcgdXNlciBkZXRhaWxzXHJcbiAgY29uc3QgW3JlY29yZHMsIHNldFJlY29yZHNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtpbmNvbWVDYXRlZ29yaWVzLCBzZXRJbmNDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtleHBlbnNlc0NhdGVnb3JpZXMsIHNldEV4cENhdF0gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIC8vU3RhdGVzIGZvciBmaWx0ZXJpbmcgUmVjb3JkcyBieSBEYXRlXHJcbiAgY29uc3QgW3N0YXJ0RGF0ZSwgc2V0U3RhcnREYXRlXSA9IHVzZVN0YXRlKFxyXG4gICAgbW9tZW50KHVzZXIuc3RhcnREYXRlKS5zdGFydE9mKFwiZGF5c1wiKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgKTtcclxuICBjb25zdCBbZW5kRGF0ZSwgc2V0RW5kRGF0ZV0gPSB1c2VTdGF0ZShcclxuICAgIG1vbWVudCh1c2VyLmVuZERhdGUpLnN0YXJ0T2YoXCJkYXlzXCIpLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICApO1xyXG5cclxuICAvL1N0YXRlcyBmb3IgRGF0YSBNYW5pcHVsYXRpb25cclxuICBjb25zdCBbdG90YWxJbmNvbWUsIHNldFRvdGFsSW5jb21lXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFt0b3RhbEV4cGVuc2VzLCBzZXRUb3RhbEV4cGVuc2VzXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtpbmNvbWVSZWNvcmRzLCBzZXRJbmNSZWNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtleHBlbnNlc1JlY29yZHMsIHNldEV4cFJlY10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXNUeXBlLCBzZXRDYXRUeXBlXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbcmVjb3Jkc1R5cGUsIHNldFJlY1R5cGVdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtyZWNvcmRzTmFtZSwgc2V0UmVjTmFtZV0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXNSZWNvcmRzLCBzZXRDYXRSZWNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtidWRnZXRBcnJheSwgc2V0QnVkZ2V0QXJyYXldID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICAvL1N0YXRlcyBmb3IgQ2hhcnQgZmlsdGVyXHJcbiAgY29uc3QgW3R5cGUsIHNldFR5cGVdID0gdXNlU3RhdGUoXCJleHBlbnNlXCIpO1xyXG4gIGNvbnN0IFtjYXRPcHRpb24sIHNldENhdE9wdGlvbl0gPSB1c2VTdGF0ZShcImFsbFwiKTtcclxuICBjb25zdCBbY2F0ZWdvcnlJZCwgc2V0Q2F0ZWdvcnlJZF0gPSB1c2VTdGF0ZShcImFsbFwiKTtcclxuICBjb25zdCBbYnVkZ2V0T3B0aW9uLCBzZXRCdWRnZXRPcHRpb25dID0gdXNlU3RhdGUoXCJhbGxcIik7XHJcbiAgY29uc3QgW2J1ZGdldElkLCBzZXRCdWRnZXRJZF0gPSB1c2VTdGF0ZShcImFsbFwiKTtcclxuXHJcbiAgLy8gU3RhdGVzIGZvciBDaGFydCBEYXRhXHJcbiAgY29uc3QgW2NhdGVnb3J5UGllLCBzZXRDYXRQaWVdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtyZWNvcmREb3VnaG51dCwgc2V0UmVjRG91Z2hudXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtidWRnZXRQaWUsIHNldEJ1ZGdldFBpZV0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2J1ZGdldERvdWdobnV0LCBzZXRCdWRnZXREb3VnaG51dF0gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9kZXRhaWxzYCwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgc2V0UmVjb3JkcyhkYXRhLnJlY29yZHMpO1xyXG5cclxuICAgICAgICBsZXQgZmlsdGVyZWRJbmMgPSBkYXRhLmNhdGVnb3JpZXMuZmlsdGVyKChjYXRlZ29yeSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIGNhdGVnb3J5LnR5cGUgPT0gXCJpbmNvbWVcIjtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgc2V0SW5jQ2F0KGZpbHRlcmVkSW5jKTtcclxuXHJcbiAgICAgICAgbGV0IGZpbHRlcmVkRXhwID0gZGF0YS5jYXRlZ29yaWVzLmZpbHRlcigoY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICAgIHJldHVybiBjYXRlZ29yeS50eXBlID09IFwiZXhwZW5zZVwiO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHNldEV4cENhdChmaWx0ZXJlZEV4cCk7XHJcblxyXG4gICAgICAgIHNldFN0YXJ0RGF0ZShcclxuICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLnN0YXJ0RGF0ZSlcclxuICAgICAgICAgICAgLnN0YXJ0T2YoXCJkYXlzXCIpXHJcbiAgICAgICAgICAgIC5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgKTtcclxuICAgICAgICBzZXRFbmREYXRlKFxyXG4gICAgICAgICAgbW9tZW50KGRhdGEuY3VycmVudERhdGUuZW5kRGF0ZSkuc3RhcnRPZihcImRheXNcIikuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChzdGFydERhdGUgPD0gZW5kRGF0ZSkge1xyXG4gICAgICBsZXQgZGF0ZUJlZm9yZSA9IG1vbWVudChzdGFydERhdGUpLnN1YnRyYWN0KDEsIFwiZGF5XCIpO1xyXG4gICAgICBsZXQgZGF0ZUFmdGVyID0gbW9tZW50KGVuZERhdGUpLmFkZCgxLCBcImRheVwiKTtcclxuXHJcbiAgICAgIGxldCBmaWx0ZXJlZFJlY29yZHMgPSByZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgbGV0IHVwZGF0ZWRPbiA9IG1vbWVudChyZWNvcmQudXBkYXRlZE9uKS5zdGFydE9mKFwiZGF5c1wiKTtcclxuICAgICAgICByZXR1cm4gbW9tZW50KHVwZGF0ZWRPbikuaXNCZXR3ZWVuKGRhdGVCZWZvcmUsIGRhdGVBZnRlciwgXCJkYXlcIik7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgc2V0SW5jUmVjKGZpbHRlcmVkUmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmFtb3VudCA+PSAwKSk7XHJcbiAgICAgIHNldEV4cFJlYyhmaWx0ZXJlZFJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHJlY29yZC5hbW91bnQgPCAwKSk7XHJcblxyXG4gICAgICBzZXRCdWRnZXRQaWUoZXhwZW5zZXNDYXRlZ29yaWVzKTtcclxuICAgICAgc2V0QnVkZ2V0T3B0aW9uKFxyXG4gICAgICAgIGV4cGVuc2VzQ2F0ZWdvcmllcy5tYXAoKGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gPG9wdGlvbiB2YWx1ZT17Y2F0ZWdvcnkuX2lkfT57Y2F0ZWdvcnkubmFtZX08L29wdGlvbj47XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuICAgIH1cclxuICB9LCBbc3RhcnREYXRlLCBlbmREYXRlLCByZWNvcmRzLCBpbmNvbWVDYXRlZ29yaWVzLCBleHBlbnNlc0NhdGVnb3JpZXNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldENhdFJlYyhcclxuICAgICAgc3VtQnlHcm91cChcclxuICAgICAgICBleHBlbnNlc0NhdGVnb3JpZXMsXHJcbiAgICAgICAgXCJfaWRcIixcclxuICAgICAgICBleHBlbnNlc1JlY29yZHMsXHJcbiAgICAgICAgXCJjYXRlZ29yeUlkXCIsXHJcbiAgICAgICAgXCJhbW91bnRcIlxyXG4gICAgICApXHJcbiAgICApO1xyXG4gIH0sIFtzdGFydERhdGUsIGVuZERhdGUsIGV4cGVuc2VzQ2F0ZWdvcmllcywgZXhwZW5zZXNSZWNvcmRzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoYnVkZ2V0SWQgPT0gXCJhbGxcIikge1xyXG4gICAgICBzZXRCdWRnZXRBcnJheShjYXRlZ29yaWVzUmVjb3Jkcyk7XHJcbiAgICB9IGVsc2UgaWYgKGJ1ZGdldElkID09IFwiYnVkZ2V0T25seVwiKSB7XHJcbiAgICAgIHNldEJ1ZGdldEFycmF5KFxyXG4gICAgICAgIGNhdGVnb3JpZXNSZWNvcmRzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LmJ1ZGdldCA+IDApXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2UgaWYgKGJ1ZGdldElkID09IFwiZXhwZW5zZXNPbmx5XCIpIHtcclxuICAgICAgc2V0QnVkZ2V0QXJyYXkoXHJcbiAgICAgICAgY2F0ZWdvcmllc1JlY29yZHMuZmlsdGVyKChjYXRlZ29yeSkgPT4gY2F0ZWdvcnkuYW1vdW50IDwgMClcclxuICAgICAgKTtcclxuICAgIH0gZWxzZSBpZiAoYnVkZ2V0SWQgPT0gXCJidWRnZXRFeHBlbnNlc1wiKSB7XHJcbiAgICAgIHNldEJ1ZGdldEFycmF5KFxyXG4gICAgICAgIGNhdGVnb3JpZXNSZWNvcmRzLmZpbHRlcihcclxuICAgICAgICAgIChjYXRlZ29yeSkgPT4gY2F0ZWdvcnkuYW1vdW50IDwgMCAmJiBjYXRlZ29yeS5idWRnZXQgPiAwXHJcbiAgICAgICAgKVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0QnVkZ2V0QXJyYXkoXHJcbiAgICAgICAgY2F0ZWdvcmllc1JlY29yZHMuZmlsdGVyKChjYXRlZ29yeSkgPT4gY2F0ZWdvcnkuX2lkID09IGJ1ZGdldElkKVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gIH0sIFtjYXRlZ29yaWVzUmVjb3JkcywgYnVkZ2V0SWQsIHN0YXJ0RGF0ZSwgZW5kRGF0ZV0pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgY29uc29sZS5sb2coYnVkZ2V0SWQpO1xyXG4gICAgY29uc29sZS5sb2coYnVkZ2V0QXJyYXkpO1xyXG4gICAgbGV0IGJ1ZGdldCA9IGJ1ZGdldEFycmF5LnJlZHVjZSgodG90YWwsIGJ1ZGdldCkgPT4ge1xyXG4gICAgICByZXR1cm4gdG90YWwgKyBidWRnZXQuYnVkZ2V0O1xyXG4gICAgfSwgMCk7XHJcbiAgICBsZXQgZXhwZW5zZXMgPSBidWRnZXRBcnJheS5yZWR1Y2UoKHRvdGFsLCBidWRnZXQpID0+IHtcclxuICAgICAgcmV0dXJuIHRvdGFsIC0gYnVkZ2V0LmFtb3VudDtcclxuICAgIH0sIDApO1xyXG4gICAgbGV0IHNhdmluZ3MgPSBidWRnZXQgLSBleHBlbnNlcztcclxuXHJcbiAgICBsZXQgYnVkZ2V0U3VtbWFycnkgPSBbXHJcbiAgICAgIHtcclxuICAgICAgICBsYWJlbDogXCJjb25zdW1wdGlvblwiLFxyXG4gICAgICAgIGFtb3VudDogZXhwZW5zZXMsXHJcbiAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICB9LFxyXG4gICAgXTtcclxuXHJcbiAgICBpZiAoc2F2aW5ncyA+PSAwKSB7XHJcbiAgICAgIHNldEJ1ZGdldERvdWdobnV0KFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJjb25zdW1wdGlvblwiLFxyXG4gICAgICAgICAgYW1vdW50OiBleHBlbnNlcyxcclxuICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGxhYmVsOiBcInNhdmluZ3NcIixcclxuICAgICAgICAgIGFtb3VudDogc2F2aW5ncyxcclxuICAgICAgICAgIGNvbG9yOiBcIiM0N2I4M2RcIixcclxuICAgICAgICB9LFxyXG4gICAgICBdKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEJ1ZGdldERvdWdobnV0KFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJjb25zdW1wdGlvblwiLFxyXG4gICAgICAgICAgYW1vdW50OiBidWRnZXQsXHJcbiAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJvdmVyc3BlbnRcIixcclxuICAgICAgICAgIGFtb3VudDogTWF0aC5hYnMoc2F2aW5ncyksXHJcbiAgICAgICAgICBjb2xvcjogXCIjZTMwNzA3XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgXSk7XHJcbiAgICB9XHJcbiAgfSwgW2J1ZGdldEFycmF5LCBidWRnZXRJZCwgc3RhcnREYXRlLCBlbmREYXRlXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRUb3RhbEluY29tZShcclxuICAgICAgaW5jb21lUmVjb3Jkcy5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICByZXR1cm4gdG90YWwgKyByZWNvcmQuYW1vdW50O1xyXG4gICAgICB9LCAwKVxyXG4gICAgKTtcclxuICAgIHNldFRvdGFsRXhwZW5zZXMoXHJcbiAgICAgIGV4cGVuc2VzUmVjb3Jkcy5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICByZXR1cm4gdG90YWwgLSByZWNvcmQuYW1vdW50O1xyXG4gICAgICB9LCAwKVxyXG4gICAgKTtcclxuICB9LCBbc3RhcnREYXRlLCBlbmREYXRlLCBpbmNvbWVSZWNvcmRzLCBleHBlbnNlc1JlY29yZHNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmICh0eXBlID09IFwiaW5jb21lXCIpIHtcclxuICAgICAgc2V0Q2F0VHlwZShpbmNvbWVDYXRlZ29yaWVzKTtcclxuICAgICAgc2V0UmVjVHlwZShpbmNvbWVSZWNvcmRzKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldENhdFR5cGUoZXhwZW5zZXNDYXRlZ29yaWVzKTtcclxuICAgICAgc2V0UmVjVHlwZShleHBlbnNlc1JlY29yZHMpO1xyXG4gICAgfVxyXG4gICAgc2V0Q2F0ZWdvcnlJZChcImFsbFwiKTtcclxuICB9LCBbdHlwZSwgcmVjb3JkcywgaW5jb21lQ2F0ZWdvcmllcywgZXhwZW5zZXNDYXRlZ29yaWVzLCBzdGFydERhdGUsIGVuZERhdGVdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldENhdFBpZShcclxuICAgICAgc3VtQnlHcm91cChjYXRlZ29yaWVzVHlwZSwgXCJfaWRcIiwgcmVjb3Jkc1R5cGUsIFwiY2F0ZWdvcnlJZFwiLCBcImFtb3VudFwiKVxyXG4gICAgKTtcclxuICAgIHNldENhdE9wdGlvbihcclxuICAgICAgY2F0ZWdvcmllc1R5cGUubWFwKChjYXRlZ29yeSkgPT4ge1xyXG4gICAgICAgIHJldHVybiA8b3B0aW9uIHZhbHVlPXtjYXRlZ29yeS5faWR9PntjYXRlZ29yeS5uYW1lfTwvb3B0aW9uPjtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW3R5cGUsIGNhdGVnb3JpZXNUeXBlLCByZWNvcmRzVHlwZSwgc3RhcnREYXRlLCBlbmREYXRlXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoY2F0ZWdvcnlJZCA9PSBcImFsbFwiKSB7XHJcbiAgICAgIHNldFJlY05hbWUocmVjb3Jkc1R5cGUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0UmVjTmFtZShcclxuICAgICAgICByZWNvcmRzVHlwZS5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcnlJZClcclxuICAgICAgKTtcclxuICAgIH1cclxuICB9LCBbdHlwZSwgY2F0ZWdvcnlJZCwgY2F0ZWdvcmllc1R5cGUsIHJlY29yZHNUeXBlLCBzdGFydERhdGUsIGVuZERhdGVdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldFJlY0RvdWdobnV0KHN1bUJ5R3JvdXAoW10sIFwiXCIsIHJlY29yZHNOYW1lLCBcImRlc2NyaXB0aW9uXCIsIFwiYW1vdW50XCIpKTtcclxuICB9LCBbXHJcbiAgICBjYXRlZ29yeUlkLFxyXG4gICAgY2F0ZWdvcmllc1R5cGUsXHJcbiAgICByZWNvcmRzVHlwZSxcclxuICAgIHJlY29yZHNOYW1lLFxyXG4gICAgc3RhcnREYXRlLFxyXG4gICAgZW5kRGF0ZSxcclxuICBdKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPk1vbnRobHkgVHJlbmQ8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxGb3JtPlxyXG4gICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICA8Q29sIG1kPVwiYXV0b1wiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5EYXRlOjwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICBkYXRlZm9ybWF0PVwieXl5eS1NTS1ERFwiXHJcbiAgICAgICAgICAgICAgdmFsdWU9e3N0YXJ0RGF0ZX1cclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFN0YXJ0RGF0ZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDxDb2wgbWQ9XCJhdXRvXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPiB0byA8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICB0eXBlPVwiZGF0ZVwiXHJcbiAgICAgICAgICAgICAgdmFsdWU9e2VuZERhdGV9XHJcbiAgICAgICAgICAgICAgZGF0ZWZvcm1hdD1cIm15eXl5LU1NLUREXCJcclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEVuZERhdGUoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgPC9Sb3c+XHJcbiAgICAgIDwvRm9ybT5cclxuICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtbWQtY2VudGVyIG15LTJcIj5cclxuICAgICAgICA8aDU+SW5jb21lIENvbnN1bXB0aW9uPC9oNT5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZCBzdGFja2VkQmFyXCI+XHJcbiAgICAgICAgICA8U3RhY2tlZEJhciBpbmNvbWU9e3RvdGFsSW5jb21lfSBleHBlbnNlcz17dG90YWxFeHBlbnNlc30gLz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9Sb3c+XHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgPENvbD5cclxuICAgICAgICAgIDxDYXJkIGNsYXNzTmFtZT1cIm0tMlwiPlxyXG4gICAgICAgICAgICA8Q2FyZC5IZWFkZXI+XHJcbiAgICAgICAgICAgICAgPE5hdiB2YXJpYW50PVwidGFic1wiPlxyXG4gICAgICAgICAgICAgICAgPE5hdi5JdGVtIHZseWU9XCJpbmNvbWVcIj5cclxuICAgICAgICAgICAgICAgICAgPE5hdi5MaW5rIG9uQ2xpY2s9eyhlKSA9PiBzZXRUeXBlKFwiaW5jb21lXCIpfT5JbmNvbWU8L05hdi5MaW5rPlxyXG4gICAgICAgICAgICAgICAgPC9OYXYuSXRlbT5cclxuICAgICAgICAgICAgICAgIDxOYXYuSXRlbSB2YWx1ZT1cImV4cGVuc2VcIj5cclxuICAgICAgICAgICAgICAgICAgPE5hdi5MaW5rIG9uQ2xpY2s9eyhlKSA9PiBzZXRUeXBlKFwiZXhwZW5zZXNcIil9PlxyXG4gICAgICAgICAgICAgICAgICAgIEV4cGVuc2VzXHJcbiAgICAgICAgICAgICAgICAgIDwvTmF2Lkxpbms+XHJcbiAgICAgICAgICAgICAgICA8L05hdi5JdGVtPlxyXG4gICAgICAgICAgICAgIDwvTmF2PlxyXG4gICAgICAgICAgICA8L0NhcmQuSGVhZGVyPlxyXG4gICAgICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAge3R5cGVbMF0udG9VcHBlckNhc2UoKSArIHR5cGUuc2xpY2UoMSl9IENhdGVnb3JpZXN7XCIgXCJ9XHJcbiAgICAgICAgICAgICAgPC9oNT5cclxuICAgICAgICAgICAgICA8UGllQ2hhcnRcclxuICAgICAgICAgICAgICAgIGNoYXJ0VHlwZT1cIlBpZVwiXHJcbiAgICAgICAgICAgICAgICByYXdEYXRhPXtjYXRlZ29yeVBpZX1cclxuICAgICAgICAgICAgICAgIGxhYmVsS2V5PXtcIm5hbWVcIn1cclxuICAgICAgICAgICAgICAgIGFtb3VudEtleT17XCJhbW91bnRcIn1cclxuICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0NhcmQuQm9keT5cclxuICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICA8L0NvbD5cclxuXHJcbiAgICAgICAgPENvbD5cclxuICAgICAgICAgIDxDYXJkIGNsYXNzTmFtZT1cIm15LTJcIj5cclxuICAgICAgICAgICAgPENhcmQuSGVhZGVyPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImNhdGVnb3J5SWRcIiBjbGFzc05hbWU9XCJteS0wXCI+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcclxuICAgICAgICAgICAgICAgICAgdmFsdWU9e2NhdGVnb3J5SWR9XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0Q2F0ZWdvcnlJZChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJhbGxcIiBzZWxlY3RlZD5cclxuICAgICAgICAgICAgICAgICAgICBBbGxcclxuICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgIHtjYXRPcHRpb259XHJcbiAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cclxuICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICAgIDwvQ2FyZC5IZWFkZXI+XHJcbiAgICAgICAgICAgIDxDYXJkLkJvZHk+XHJcbiAgICAgICAgICAgICAgPGg1IGNsYXNzTmFtZT1cInRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICB7dHlwZVswXS50b1VwcGVyQ2FzZSgpICsgdHlwZS5zbGljZSgxKX0gQ2F0ZWdvcmllc3tcIiBcIn1cclxuICAgICAgICAgICAgICA8L2g1PlxyXG4gICAgICAgICAgICAgIDxQaWVDaGFydFxyXG4gICAgICAgICAgICAgICAgY2hhcnRUeXBlPVwiRG91Z2hudXRcIlxyXG4gICAgICAgICAgICAgICAgcmF3RGF0YT17cmVjb3JkRG91Z2hudXR9XHJcbiAgICAgICAgICAgICAgICBsYWJlbEtleT17XCJkZXNjcmlwdGlvblwifVxyXG4gICAgICAgICAgICAgICAgYW1vdW50S2V5PXtcImFtb3VudFwifVxyXG4gICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDwvQ29sPlxyXG4gICAgICA8L1Jvdz5cclxuXHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgPENvbD5cclxuICAgICAgICAgIDxDYXJkIGNsYXNzTmFtZT1cIm0tMlwiPlxyXG4gICAgICAgICAgICA8Q2FyZC5IZWFkZXI+XHJcbiAgICAgICAgICAgICAgPE5hdiB2YXJpYW50PVwidGFic1wiPlxyXG4gICAgICAgICAgICAgICAgPE5hdi5JdGVtIHZseWU9XCJpbmNvbWVcIj5cclxuICAgICAgICAgICAgICAgICAgPE5hdi5MaW5rPkJ1ZGdldDwvTmF2Lkxpbms+XHJcbiAgICAgICAgICAgICAgICA8L05hdi5JdGVtPlxyXG4gICAgICAgICAgICAgIDwvTmF2PlxyXG4gICAgICAgICAgICA8L0NhcmQuSGVhZGVyPlxyXG4gICAgICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlclwiPkJ1ZGdldCBDYXRlZ29yaWVzPC9oNT5cclxuICAgICAgICAgICAgICA8UGllQ2hhcnRcclxuICAgICAgICAgICAgICAgIGNoYXJ0VHlwZT1cIlBpZVwiXHJcbiAgICAgICAgICAgICAgICByYXdEYXRhPXtidWRnZXRQaWV9XHJcbiAgICAgICAgICAgICAgICBsYWJlbEtleT17XCJuYW1lXCJ9XHJcbiAgICAgICAgICAgICAgICBhbW91bnRLZXk9e1wiYnVkZ2V0XCJ9XHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9DYXJkLkJvZHk+XHJcbiAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9Db2w+XHJcblxyXG4gICAgICAgIDxDb2w+XHJcbiAgICAgICAgICA8Q2FyZCBjbGFzc05hbWU9XCJtLTJcIj5cclxuICAgICAgICAgICAgPENhcmQuSGVhZGVyPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImNhdGVnb3J5SWRcIiBjbGFzc05hbWU9XCJteS0wXCI+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcclxuICAgICAgICAgICAgICAgICAgdmFsdWU9e2J1ZGdldElkfVxyXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEJ1ZGdldElkKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImFsbFwiIHNlbGVjdGVkPlxyXG4gICAgICAgICAgICAgICAgICAgIEFsbFxyXG4gICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImJ1ZGdldE9ubHlcIiBzZWxlY3RlZD5cclxuICAgICAgICAgICAgICAgICAgICBXaXRoIEJ1ZGdldCBPbmx5XHJcbiAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiZXhwZW5zZXNPbmx5XCIgc2VsZWN0ZWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgV2l0aCBFeHBlbnNlcyBPbmx5XHJcbiAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiYnVkZ2V0RXhwZW5zZXNcIiBzZWxlY3RlZD5cclxuICAgICAgICAgICAgICAgICAgICBXaXRoIEJvdGggQnVkZ2V0ICYgRXhwZXNlbnNlcyBPbmx5XHJcbiAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICB7YnVkZ2V0T3B0aW9ufVxyXG4gICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgICA8L0NhcmQuSGVhZGVyPlxyXG4gICAgICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDxoNSBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlclwiPkJ1ZGdldCBDb25zdW1wdGlvbiA8L2g1PlxyXG4gICAgICAgICAgICAgIDxQaWVDaGFydFxyXG4gICAgICAgICAgICAgICAgY2hhcnRUeXBlPVwiRG91Z2hudXRcIlxyXG4gICAgICAgICAgICAgICAgcmF3RGF0YT17YnVkZ2V0RG91Z2hudXR9XHJcbiAgICAgICAgICAgICAgICBsYWJlbEtleT17XCJsYWJlbFwifVxyXG4gICAgICAgICAgICAgICAgYW1vdW50S2V5PXtcImFtb3VudFwifVxyXG4gICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDwvQ29sPlxyXG4gICAgICA8L1Jvdz5cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCB7IFRhYmxlLCBBbGVydCwgQ29udGFpbmVyLCBCdXR0b24sIENvbCwgUm93IH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuLi8uLi9zdHlsZXMvSG9tZS5tb2R1bGUuY3NzXCI7XHJcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG5pbXBvcnQgc3VtQnlHcm91cCBmcm9tIFwiLi4vLi4vaGVscGVycy9zdW1CeUdyb3VwXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBjYXRlZ29yeSgpIHtcclxuICBjb25zdCBbY2F0ZWdvcmllcywgc2V0Q2F0ZWdvcmllc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2N1cnJlbnRSZWNvcmRzLCBzZXRDdXJSZWNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtpbmNvbWVDYXRlZ29yaWVzLCBzZXRJbmNDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtleHBlbnNlc0NhdGVnb3JpZXMsIHNldEV4cENhdF0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2luY29tZVJvd3MsIHNldEluY29tZVJvd3NdID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW2V4cGVuc2VzUm93cywgc2V0RXhwZW5zZXNSb3dzXSA9IHVzZVN0YXRlKG51bGwpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvZGV0YWlsc2AsIHtcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgIH0sXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIHNldENhdGVnb3JpZXMoZGF0YS5jYXRlZ29yaWVzKTtcclxuXHJcbiAgICAgICAgbGV0IGRhdGVCZWZvcmUgPSBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5zdGFydERhdGUpLnN1YnRyYWN0KDEsIFwiZGF5XCIpO1xyXG4gICAgICAgIGxldCBkYXRlQWZ0ZXIgPSBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5lbmREYXRlKS5hZGQoMSwgXCJkYXlcIik7XHJcblxyXG4gICAgICAgIGxldCBmaWx0ZXJlZFJlY29yZHMgPSBkYXRhLnJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgIGxldCB1cGRhdGVkT24gPSBtb21lbnQocmVjb3JkLnVwZGF0ZWRPbikuc3RhcnRPZihcImRheXNcIik7XHJcbiAgICAgICAgICByZXR1cm4gbW9tZW50KHVwZGF0ZWRPbikuaXNCZXR3ZWVuKGRhdGVCZWZvcmUsIGRhdGVBZnRlciwgXCJkYXlcIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHNldEN1clJlYyhmaWx0ZXJlZFJlY29yZHMpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBsZXQgY2F0ZWdvcnlSZWNvcmRzID0gc3VtQnlHcm91cChcclxuICAgICAgY2F0ZWdvcmllcyxcclxuICAgICAgXCJfaWRcIixcclxuICAgICAgY3VycmVudFJlY29yZHMsXHJcbiAgICAgIFwiY2F0ZWdvcnlJZFwiLFxyXG4gICAgICBcImFtb3VudFwiXHJcbiAgICApO1xyXG5cclxuICAgIHNldEluY0NhdChjYXRlZ29yeVJlY29yZHMuZmlsdGVyKChjYXRlZ29yeSkgPT4gY2F0ZWdvcnkudHlwZSA9PSBcImluY29tZVwiKSk7XHJcbiAgICBzZXRFeHBDYXQoY2F0ZWdvcnlSZWNvcmRzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gXCJleHBlbnNlXCIpKTtcclxuICB9LCBbY2F0ZWdvcmllcywgY3VycmVudFJlY29yZHNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldEluY29tZVJvd3MoXHJcbiAgICAgIGluY29tZUNhdGVnb3JpZXMubWFwKChjYXRlZ29yeSkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICA8dHIga2V5PXtjYXRlZ29yeS5faWR9PlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5Lm5hbWV9PC90ZD5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5hbW91bnR9PC90ZD5cclxuICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW2luY29tZUNhdGVnb3JpZXNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldEV4cGVuc2VzUm93cyhcclxuICAgICAgZXhwZW5zZXNDYXRlZ29yaWVzLm1hcCgoY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgPHRyIGtleT17Y2F0ZWdvcnkuX2lkfT5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5uYW1lfTwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkuYnVkZ2V0fTwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57TWF0aC5hYnMoY2F0ZWdvcnkuYW1vdW50KX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5LmFtb3VudCArIGNhdGVnb3J5LmJ1ZGdldH08L3RkPlxyXG4gICAgICAgICAgPC90cj5cclxuICAgICAgICApO1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9LCBbZXhwZW5zZXNDYXRlZ29yaWVzXSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDx0aXRsZT4gQ2F0ZWdvcmllczwvdGl0bGU+XHJcbiAgICAgIDwvSGVhZD5cclxuICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtbWQtZW5kIG15LTJcIj5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwic3VjY2Vzc1wiXHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBSb3V0ZXIucHVzaChcIi4vY2F0ZWdvcnkvYWRkXCIpfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIEFkZCBDYXRlZ29yeVxyXG4gICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgIDxCdXR0b25cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgIHZhcmlhbnQ9XCJzZWNvbmRhcnlcIlxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL2NhdGVnb3J5L2VkaXRcIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgRWRpdCBDYXRlZ29yeVxyXG4gICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgIDxCdXR0b25cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgIHZhcmlhbnQ9XCJkYW5nZXJcIlxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL2NhdGVnb3J5L2FkZFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBEZWxldGUgQ2F0ZWdvcnlcclxuICAgICAgICA8L0J1dHRvbj5cclxuXHJcbiAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgdmFyaWFudD1cImluZm9cIlxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuLi9yZWNvcmQvYWRkXCIpfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIEFkZCBSZWNvcmRcclxuICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgPC9Sb3c+XHJcbiAgICAgIDxoNT5JbmNvbWU6PC9oNT5cclxuICAgICAge2luY29tZUNhdGVnb3JpZXMubGVuZ3RoID09IDAgPyAoXHJcbiAgICAgICAgPEFsZXJ0IHZhcmlhbnQ9XCJpbmZvXCI+WW91IGhhdmUgbm8gY2F0ZWdvcmllcyB5ZXQuPC9BbGVydD5cclxuICAgICAgKSA6IChcclxuICAgICAgICA8VGFibGUgc3RyaXBlZCBib3JkZXJlZCBob3Zlcj5cclxuICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgIDx0aD5DYXRlZ29yeSBOYW1lPC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBJbmNvbWU8L3RoPlxyXG4gICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgPC90aGVhZD5cclxuXHJcbiAgICAgICAgICA8dGJvZHk+e2luY29tZVJvd3N9PC90Ym9keT5cclxuICAgICAgICA8L1RhYmxlPlxyXG4gICAgICApfVxyXG4gICAgICA8aDU+RXhwZW5zZXM6PC9oNT5cclxuICAgICAge2V4cGVuc2VzQ2F0ZWdvcmllcy5sZW5ndGggPT0gMCA/IChcclxuICAgICAgICA8QWxlcnQgdmFyaWFudD1cImluZm9cIj5Zb3UgaGF2ZSBubyBjYXRlZ29yaWVzIHVuZGVyIGV4cGVuc2VzIHlldC48L0FsZXJ0PlxyXG4gICAgICApIDogKFxyXG4gICAgICAgIDxUYWJsZSBzdHJpcGVkIGJvcmRlcmVkIGhvdmVyPlxyXG4gICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgPHRoPkNhdGVnb3J5IE5hbWU8L3RoPlxyXG4gICAgICAgICAgICAgIDx0aD5DdXJyZW50IEJ1ZGdldDwvdGg+XHJcbiAgICAgICAgICAgICAgPHRoPkN1cnJlbnQgRXhwZW5zZXM8L3RoPlxyXG4gICAgICAgICAgICAgIDx0aD5DdXJyZW50IFNhdmluZ3M8L3RoPlxyXG4gICAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICAgPC90aGVhZD5cclxuXHJcbiAgICAgICAgICA8dGJvZHk+e2V4cGVuc2VzUm93c308L3Rib2R5PlxyXG4gICAgICAgIDwvVGFibGU+XHJcbiAgICAgICl9XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgRm9ybSwgQnV0dG9uLCBDYXJkLCBSb3csIENvbCB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHsgR29vZ2xlTG9naW4gfSBmcm9tIFwicmVhY3QtZ29vZ2xlLWxvZ2luXCI7XHJcbmltcG9ydCBTd2FsIGZyb20gXCJzd2VldGFsZXJ0MlwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vVXNlckNvbnRleHRcIjtcclxuaW1wb3J0IFZpZXcgZnJvbSBcIi4uL2NvbXBvbmVudHMvVmlld1wiO1xyXG5pbXBvcnQgQXBwSGVscGVyIGZyb20gXCIuLi9hcHAtaGVscGVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBsb2dpbigpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPFZpZXcgdGl0bGU9e1wiTG9naW5cIn0+XHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgIDxDb2wgeHMgbWQ9XCI2XCI+XHJcbiAgICAgICAgICA8TG9naW5Gb3JtIC8+XHJcbiAgICAgICAgPC9Db2w+XHJcbiAgICAgIDwvUm93PlxyXG4gICAgPC9WaWV3PlxyXG4gICk7XHJcbn1cclxuXHJcbmNvbnN0IExvZ2luRm9ybSA9ICgpID0+IHtcclxuICBjb25zdCB7IHVzZXIsIHNldFVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpO1xyXG5cclxuICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtwYXNzd29yZCwgc2V0UGFzc3dvcmRdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2lzQWN0aXZlLCBzZXRJc0FjdGl2ZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIGZ1bmN0aW9uIGF1dGhlbnRpY2F0ZShlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgY29uc3Qgb3B0aW9ucyA9IHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgZW1haWw6IGVtYWlsLFxyXG4gICAgICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcclxuICAgICAgfSksXHJcbiAgICB9O1xyXG5cclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2xvZ2luYCwgb3B0aW9ucylcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICBpZiAodHlwZW9mIGRhdGEuYWNjZXNzVG9rZW4gIT09IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwidG9rZW5cIiwgZGF0YS5hY2Nlc3NUb2tlbik7XHJcbiAgICAgICAgICBzZXRVc2VyKHsgaWQ6IGRhdGEuX2lkIH0pO1xyXG4gICAgICAgICAgUm91dGVyLnB1c2goXCIvXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoZGF0YS5lcnJvciA9PT0gXCJkb2VzLW5vdC1leGlzdFwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcIkF1dGhlbnRpY2F0aW9uIEZhaWxlZFwiLCBcIlVzZXIgZG9lcyBub3QgZXhpc3QuXCIsIFwiZXJyb3JcIik7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09IFwibG9naW4tdHlwZS1lcnJvclwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcclxuICAgICAgICAgICAgICBcIkxvZ2luIFR5cGUgRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIllvdSBtYXkgaGF2ZSByZWdpc3RlcmVkIHRocm91Z2ggYSBkaWZmZXJlbnQgbG9naW4gcHJvY2VkdXJlLCB0cnkgYW4gYWx0ZXJuYXRpdmUgbG9naW4gcHJvY2VkdXJlXCIsXHJcbiAgICAgICAgICAgICAgXCJlcnJvclwiXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09IFwiaW5jb3JyZWN0LXBhc3N3b3JkXCIpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFxyXG4gICAgICAgICAgICAgIFwiQXV0aGVudGljYXRpb24gRmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgXCJwYXNzd29yZCBpcyBpbmNvcnJlY3RcIixcclxuICAgICAgICAgICAgICBcImVycm9yXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3QgYXVudGhlbnRpY2F0ZUdvb2dsZVRva2VuID0gKHJlc3BvbnNlKSA9PiB7XHJcbiAgICBjb25zdCBwYXlsb2FkID0ge1xyXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICBoZWFkZXJzOiB7IFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgdG9rZW5JZDogcmVzcG9uc2UudG9rZW5JZCB9KSxcclxuICAgIH07XHJcblxyXG4gICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvdmVyaWZ5LWdvb2dsZS1pZC10b2tlbmAsIHBheWxvYWQpXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhLmFjY2Vzc1Rva2VuICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInRva2VuXCIsIGRhdGEuYWNjZXNzVG9rZW4pO1xyXG4gICAgICAgICAgc2V0VXNlcih7IGlkOiBkYXRhLl9pZCB9KTtcclxuICAgICAgICAgIFJvdXRlci5wdXNoKFwiL1wiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaWYgKChkYXRhLmVycm9yID0gXCJnb29nbGUtYXV0aC1lcnJvclwiKSkge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJHb29nbGUgQXV0aCBFcnJvclwiLFxyXG4gICAgICAgICAgICAgIFwiR29vZ2xlIGF1dGhlbnRpY2F0aW9uIHByb2NlZHVyZSBmYWlsZWQuXCIsXHJcbiAgICAgICAgICAgICAgXCJlcnJvclwiXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKChkYXRhLmVycm9yID0gXCJsb2dpbi10eXBlLWVycm9yXCIpKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcclxuICAgICAgICAgICAgICBcIkxvZ2luIFR5cGUgRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIllvdSBtYXkgaGF2ZSByZWdpc3RlcmVkIHRocm91Z2ggYSBkaWZmZXJlbnQgbG9naW4gcHJvY2VkdXJlLlwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChlbWFpbCAhPT0gXCJcIiAmJiBwYXNzd29yZCAhPT0gXCJcIikge1xyXG4gICAgICBzZXRJc0FjdGl2ZSh0cnVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldElzQWN0aXZlKGZhbHNlKTtcclxuICAgIH1cclxuICB9LCBbZW1haWwsIHBhc3N3b3JkXSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Q2FyZD5cclxuICAgICAgPENhcmQuSGVhZGVyPkxvZ2luIGRldGFpbHM8L0NhcmQuSGVhZGVyPlxyXG4gICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgPHRpdGxlPkJ1ZGdldCBUcmFja2VyPC90aXRsZT5cclxuICAgICAgICA8L0hlYWQ+XHJcbiAgICAgICAgPEZvcm0gb25TdWJtaXQ9eyhlKSA9PiBhdXRoZW50aWNhdGUoZSl9PlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwidXNlckVtYWlsXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPkVtYWlsIGFkZHJlc3M8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIlxyXG4gICAgICAgICAgICAgIHZhbHVlPXtlbWFpbH1cclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEVtYWlsKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInBhc3N3b3JkXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPlBhc3N3b3JkPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17cGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRQYXNzd29yZChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlciBweC0zXCI+XHJcbiAgICAgICAgICAgIHtpc0FjdGl2ZSA/IChcclxuICAgICAgICAgICAgICA8QnV0dG9uIGNsYXNzTmFtZT1cImJnLXByaW1hcnkgYnRuLWJsb2NrXCIgdHlwZT1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9XCJiZy1wcmltYXJ5IGJ0bi1ibG9ja1wiIHR5cGU9XCJzdWJtaXRcIiBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICApfVxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgPC9Gb3JtPlxyXG5cclxuICAgICAgICA8R29vZ2xlTG9naW5cclxuICAgICAgICAgIGNsaWVudElkPVwiNjU2NTkzNjYyNTY5LTI0Z3VmdTQ0ZXZqNHVqcXMzazAxMTNyZ3J1cnEyYm5wLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tXCJcclxuICAgICAgICAgIGJ1dHRvblRleHQ9XCJMb2dpblwiXHJcbiAgICAgICAgICBvblN1Y2Nlc3M9e2F1bnRoZW50aWNhdGVHb29nbGVUb2tlbn1cclxuICAgICAgICAgIG9uRmFpbHVyZT17YXVudGhlbnRpY2F0ZUdvb2dsZVRva2VufVxyXG4gICAgICAgICAgY29va2llUG9saWN5PXtcInNpbmdsZV9ob3N0X29yaWdpblwifVxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIlxyXG4gICAgICAgIC8+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICk7XHJcbn07XHJcbiIsIi8vIEV4cG9ydHNcbm1vZHVsZS5leHBvcnRzID0ge1xuXHRcImNvbnRhaW5lclwiOiBcIkhvbWVfY29udGFpbmVyX18xRWNzVVwiLFxuXHRcIm1haW5cIjogXCJIb21lX21haW5fXzF4OGdDXCIsXG5cdFwiZm9vdGVyXCI6IFwiSG9tZV9mb290ZXJfXzFXZGhEXCIsXG5cdFwidGl0bGVcIjogXCJIb21lX3RpdGxlX18zRGpSN1wiLFxuXHRcImRlc2NyaXB0aW9uXCI6IFwiSG9tZV9kZXNjcmlwdGlvbl9fMTdaNEZcIixcblx0XCJjb2RlXCI6IFwiSG9tZV9jb2RlX19heHgyWVwiLFxuXHRcImdyaWRcIjogXCJIb21lX2dyaWRfXzJFaTJGXCIsXG5cdFwiY2FyZFwiOiBcIkhvbWVfY2FyZF9fMlNkdEJcIixcblx0XCJsb2dvXCI6IFwiSG9tZV9sb2dvX18xWWJySFwiLFxuXHRcInN0YWNrZWRCYXJcIjogXCJIb21lX3N0YWNrZWRCYXJfXzNVTzFsXCIsXG5cdFwiaG9tZXBhZ2VDYXJkXCI6IFwiSG9tZV9ob21lcGFnZUNhcmRfXzF2a0xOXCJcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJtb21lbnRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9oZWFkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvcm91dGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1jaGFydGpzLTJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtZ29vZ2xlLWxvZ2luXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9