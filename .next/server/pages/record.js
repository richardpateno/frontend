module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/record/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./UserContext.js":
/*!************************!*\
  !*** ./UserContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // creates a Context Object

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "./helpers/categoryRecord.js":
/*!***********************************!*\
  !*** ./helpers/categoryRecord.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return categoryRecord; });
// Function for converting string data from the enpoints to numbers
function categoryRecord(categories, records) {
  const combinedCatRec = records.map(record => {
    const category = categories.find(category => category._id == record.categoryId);

    if (category == undefined) {
      record.categoryType = "uncategorized";
      record.categoryName = "uncategorized";
    } else {
      record.categoryName = category.name;
      record.categoryType = category.type;
    }

    return record;
  });
  return combinedCatRec;
}

/***/ }),

/***/ "./pages/record/index.js":
/*!*******************************!*\
  !*** ./pages/record/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return record; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_bootstrap_DropdownButton__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/DropdownButton */ "react-bootstrap/DropdownButton");
/* harmony import */ var react_bootstrap_DropdownButton__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_DropdownButton__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "react-bootstrap/Dropdown");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../UserContext */ "./UserContext.js");
/* harmony import */ var _helpers_categoryRecord__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../helpers/categoryRecord */ "./helpers/categoryRecord.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\record\\index.js";










function record() {
  const {
    0: categories,
    1: setCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: startDate,
    1: setStartDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: endDate,
    1: setEndDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: type,
    1: setType
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: action,
    1: setAction
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: searchString,
    1: setSearchString
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: filteredCategories,
    1: setFilCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: categoryId,
    1: setCategoryId
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("all");
  const {
    0: recordResult,
    1: setRecordResult
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordsOnDate,
    1: setRecDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordsOnType,
    1: setRecType
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: recordsOnCategory,
    1: setRecCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: filteredRecords,
    1: setFilRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setCategories(data.categories);
      setRecords(data.records);
      setStartDate(moment__WEBPACK_IMPORTED_MODULE_10___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
      setEndDate(moment__WEBPACK_IMPORTED_MODULE_10___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setCategoryId("all");

    if (categories.length > 0) {
      if (type == "all") {
        setFilCat(categories);
      } else if (type == "income") {
        setFilCat(categories.filter(category => category.type == "income"));
      } else {
        setFilCat(categories.filter(category => category.type == "expense"));
      }
    }
  }, [type, categories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (action == "all") {
      setRecordResult(records);
    }
  }, [action, records]);

  function search(e) {
    e.preventDefault();

    if (records.length > 0) {
      setRecordResult(records.filter(record => {
        return record.description.toLowerCase().includes(searchString.toLowerCase());
      }));
    } else {
      setRecordResult([]);
    }
  }

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    console.log(recordResult);
    let dateBefore = moment__WEBPACK_IMPORTED_MODULE_10___default()(startDate).subtract(1, "day");
    let dateAfter = moment__WEBPACK_IMPORTED_MODULE_10___default()(endDate).add(1, "day");

    if (recordResult.length > 0) {
      setRecDate(recordResult.filter(record => {
        let updatedOn = moment__WEBPACK_IMPORTED_MODULE_10___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_10___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      }));
    } else {
      setRecDate([]);
    }
  }, [recordResult, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (recordsOnDate.length > 0) {
      if (type == "all") {
        setRecType(recordsOnDate);
      } else if (type == "income") {
        setRecType(recordsOnDate.filter(record => record.amount > 0));
      } else {
        setRecType(recordsOnDate.filter(record => record.amount < 0));
      }
    } else {
      setRecType([]);
    }
  }, [recordsOnDate, type]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (recordsOnType.length > 0) {
      if (categoryId == "all") {
        setRecCat(recordsOnType);
      } else {
        setRecCat(recordsOnType.filter(record => record.categoryId == categoryId));
      }
    } else {
      setRecCat([]);
    }
  }, [recordsOnType, categoryId]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    console.log(recordsOnCategory);

    if (recordsOnCategory.length > 0) {
      setFilRec(Object(_helpers_categoryRecord__WEBPACK_IMPORTED_MODULE_9__["default"])(filteredCategories, recordsOnCategory));
    } else {
      setFilRec([]);
    }
  }, [recordsOnCategory]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Search Records"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 142,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 141,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
      variant: "success",
      onClick: e => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./record/add"),
      children: "Add Record"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 145,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      onSubmit: e => search(e),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
        className: "justify-content-md-center my-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          xs: 3,
          md: 3,
          lg: 3,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "searchBy",
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              as: "select",
              required: true,
              onChange: e => setAction(e.target.value),
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                value: "all",
                children: "All Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 158,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                value: "search",
                children: "Search Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 159,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 153,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 152,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 151,
          columnNumber: 11
        }, this), action == "all" ? null : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
            xs: 8,
            md: 8,
            lg: 8,
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
              controlId: "searchString",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
                type: "string",
                placeholder: "Search...",
                value: searchString,
                onChange: e => setSearchString(e.target.value),
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 167,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 166,
              columnNumber: 17
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 165,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
            xs: 4,
            md: 4,
            lg: 4,
            children: searchString.length > 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              variant: "primary",
              type: "submit",
              id: "submitBtn",
              children: "Search"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 178,
              columnNumber: 19
            }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              variant: "primary",
              type: "submit",
              id: "submitBtn",
              disabled: true,
              children: "Search"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 182,
              columnNumber: 19
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 176,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 150,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
            className: "justify-content-md-center my-2",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
              lg: 4,
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
                controlId: "type",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
                  className: "justify-content-md-center",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                    lg: 3,
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
                      children: "Type:"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 202,
                      columnNumber: 23
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 201,
                    columnNumber: 21
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
                      as: "select",
                      onChange: e => {
                        setType(e.target.value);
                      },
                      required: true,
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                        value: "all",
                        children: "All"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 212,
                        columnNumber: 25
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                        value: "income",
                        children: "Income"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 213,
                        columnNumber: 25
                      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                        value: "expense",
                        children: "Expense"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 214,
                        columnNumber: 25
                      }, this)]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 205,
                      columnNumber: 23
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 204,
                    columnNumber: 21
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 200,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 199,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 198,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
              lg: 7,
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
                controlId: "categoryId",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
                  className: "justify-content-md-center",
                  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                    lg: 6,
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
                      children: "Category Name:"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 224,
                      columnNumber: 23
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 223,
                    columnNumber: 21
                  }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                    lg: 6,
                    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
                      as: "select",
                      value: categoryId,
                      onChange: e => setCategoryId(e.target.value),
                      required: true,
                      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                        value: "all",
                        children: "All"
                      }, void 0, false, {
                        fileName: _jsxFileName,
                        lineNumber: 233,
                        columnNumber: 25
                      }, this), filteredCategories.length == 0 ? null : filteredCategories.map(category => {
                        return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                          value: category._id,
                          children: category.name
                        }, void 0, false, {
                          fileName: _jsxFileName,
                          lineNumber: 238,
                          columnNumber: 33
                        }, this);
                      })]
                    }, void 0, true, {
                      fileName: _jsxFileName,
                      lineNumber: 227,
                      columnNumber: 23
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 226,
                    columnNumber: 21
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 222,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 221,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 220,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 197,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 196,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
                className: "justify-content-md-center my-2",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                  md: "auto",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
                    children: "Date:"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 255,
                    columnNumber: 21
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 254,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
                    type: "date",
                    dateformat: "yyyy-MM-DD",
                    value: startDate,
                    onChange: e => setStartDate(e.target.value)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 258,
                    columnNumber: 21
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 257,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                  md: "auto",
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
                    children: " to "
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 266,
                    columnNumber: 21
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 265,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
                  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
                    type: "date",
                    value: endDate,
                    dateformat: "myyyy-MM-DD",
                    onChange: e => setEndDate(e.target.value)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 269,
                    columnNumber: 21
                  }, this)
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 268,
                  columnNumber: 19
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 253,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 252,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 251,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 250,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 195,
        columnNumber: 9
      }, this), filteredRecords.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Alert"], {
        variant: "info",
        children: "No Records Found"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 282,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Table"], {
        striped: true,
        bordered: true,
        hover: true,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Type"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 287,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Category"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 288,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Description"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 289,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Amount"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 290,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
              children: "Date"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 291,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 286,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 285,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
          children: filteredRecords.map(record => {
            return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
                children: record.categoryType
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 299,
                columnNumber: 21
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
                children: record.categoryName
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 300,
                columnNumber: 21
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
                children: record.description
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 301,
                columnNumber: 21
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
                children: Math.abs(record.amount)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 302,
                columnNumber: 21
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
                children: moment__WEBPACK_IMPORTED_MODULE_10___default()(record.updatedOn).format("MMMM D, YYYY  hh:mm a")
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 303,
                columnNumber: 21
              }, this)]
            }, record._id, true, {
              fileName: _jsxFileName,
              lineNumber: 298,
              columnNumber: 19
            }, this);
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 295,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 284,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 149,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 140,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-bootstrap/Dropdown":
/*!*******************************************!*\
  !*** external "react-bootstrap/Dropdown" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/Dropdown");

/***/ }),

/***/ "react-bootstrap/DropdownButton":
/*!*************************************************!*\
  !*** external "react-bootstrap/DropdownButton" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap/DropdownButton");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9jYXRlZ29yeVJlY29yZC5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9yZWNvcmQvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibW9tZW50XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9oZWFkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcC9Ecm9wZG93blwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcC9Ecm9wZG93bkJ1dHRvblwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInN3ZWV0YWxlcnQyXCIiXSwibmFtZXMiOlsiVXNlckNvbnRleHQiLCJSZWFjdCIsImNyZWF0ZUNvbnRleHQiLCJVc2VyUHJvdmlkZXIiLCJQcm92aWRlciIsImNhdGVnb3J5UmVjb3JkIiwiY2F0ZWdvcmllcyIsInJlY29yZHMiLCJjb21iaW5lZENhdFJlYyIsIm1hcCIsInJlY29yZCIsImNhdGVnb3J5IiwiZmluZCIsIl9pZCIsImNhdGVnb3J5SWQiLCJ1bmRlZmluZWQiLCJjYXRlZ29yeVR5cGUiLCJjYXRlZ29yeU5hbWUiLCJuYW1lIiwidHlwZSIsInNldENhdGVnb3JpZXMiLCJ1c2VTdGF0ZSIsInNldFJlY29yZHMiLCJzdGFydERhdGUiLCJzZXRTdGFydERhdGUiLCJlbmREYXRlIiwic2V0RW5kRGF0ZSIsInNldFR5cGUiLCJhY3Rpb24iLCJzZXRBY3Rpb24iLCJzZWFyY2hTdHJpbmciLCJzZXRTZWFyY2hTdHJpbmciLCJmaWx0ZXJlZENhdGVnb3JpZXMiLCJzZXRGaWxDYXQiLCJzZXRDYXRlZ29yeUlkIiwicmVjb3JkUmVzdWx0Iiwic2V0UmVjb3JkUmVzdWx0IiwicmVjb3Jkc09uRGF0ZSIsInNldFJlY0RhdGUiLCJyZWNvcmRzT25UeXBlIiwic2V0UmVjVHlwZSIsInJlY29yZHNPbkNhdGVnb3J5Iiwic2V0UmVjQ2F0IiwiZmlsdGVyZWRSZWNvcmRzIiwic2V0RmlsUmVjIiwidXNlRWZmZWN0IiwiZmV0Y2giLCJoZWFkZXJzIiwiQXV0aG9yaXphdGlvbiIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJtb21lbnQiLCJjdXJyZW50RGF0ZSIsInN0YXJ0T2YiLCJmb3JtYXQiLCJsZW5ndGgiLCJmaWx0ZXIiLCJzZWFyY2giLCJlIiwicHJldmVudERlZmF1bHQiLCJkZXNjcmlwdGlvbiIsInRvTG93ZXJDYXNlIiwiaW5jbHVkZXMiLCJjb25zb2xlIiwibG9nIiwiZGF0ZUJlZm9yZSIsInN1YnRyYWN0IiwiZGF0ZUFmdGVyIiwiYWRkIiwidXBkYXRlZE9uIiwiaXNCZXR3ZWVuIiwiYW1vdW50IiwiUm91dGVyIiwicHVzaCIsInRhcmdldCIsInZhbHVlIiwiTWF0aCIsImFicyJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUFBO0FBQUE7QUFBQTtDQUVBOztBQUNBLE1BQU1BLFdBQVcsZ0JBQUdDLDRDQUFLLENBQUNDLGFBQU4sRUFBcEI7QUFFTyxNQUFNQyxZQUFZLEdBQUdILFdBQVcsQ0FBQ0ksUUFBakM7QUFFUUosMEVBQWYsRTs7Ozs7Ozs7Ozs7O0FDUEE7QUFBQTtBQUFBO0FBRWUsU0FBU0ssY0FBVCxDQUF3QkMsVUFBeEIsRUFBb0NDLE9BQXBDLEVBQTRDO0FBRTFELFFBQU1DLGNBQWMsR0FBR0QsT0FBTyxDQUFDRSxHQUFSLENBQVlDLE1BQU0sSUFBRztBQUMzQyxVQUFNQyxRQUFRLEdBQUdMLFVBQVUsQ0FBQ00sSUFBWCxDQUFnQkQsUUFBUSxJQUFJQSxRQUFRLENBQUNFLEdBQVQsSUFBZ0JILE1BQU0sQ0FBQ0ksVUFBbkQsQ0FBakI7O0FBRUEsUUFBR0gsUUFBUSxJQUFFSSxTQUFiLEVBQXVCO0FBQ3RCTCxZQUFNLENBQUNNLFlBQVAsR0FBc0IsZUFBdEI7QUFDQU4sWUFBTSxDQUFDTyxZQUFQLEdBQXNCLGVBQXRCO0FBQ0EsS0FIRCxNQUdLO0FBQ0pQLFlBQU0sQ0FBQ08sWUFBUCxHQUFzQk4sUUFBUSxDQUFDTyxJQUEvQjtBQUNBUixZQUFNLENBQUNNLFlBQVAsR0FBc0JMLFFBQVEsQ0FBQ1EsSUFBL0I7QUFDQTs7QUFFRCxXQUFPVCxNQUFQO0FBQ0EsR0Fac0IsQ0FBdkI7QUFjQSxTQUFPRixjQUFQO0FBRUEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEJEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsU0FBU0UsTUFBVCxHQUFrQjtBQUMvQixRQUFNO0FBQUEsT0FBQ0osVUFBRDtBQUFBLE9BQWFjO0FBQWIsTUFBOEJDLHNEQUFRLENBQUMsRUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDZCxPQUFEO0FBQUEsT0FBVWU7QUFBVixNQUF3QkQsc0RBQVEsQ0FBQyxFQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUNFLFNBQUQ7QUFBQSxPQUFZQztBQUFaLE1BQTRCSCxzREFBUSxDQUFDLEVBQUQsQ0FBMUM7QUFDQSxRQUFNO0FBQUEsT0FBQ0ksT0FBRDtBQUFBLE9BQVVDO0FBQVYsTUFBd0JMLHNEQUFRLENBQUMsRUFBRCxDQUF0QztBQUVBLFFBQU07QUFBQSxPQUFDRixJQUFEO0FBQUEsT0FBT1E7QUFBUCxNQUFrQk4sc0RBQVEsQ0FBQyxLQUFELENBQWhDO0FBQ0EsUUFBTTtBQUFBLE9BQUNPLE1BQUQ7QUFBQSxPQUFTQztBQUFULE1BQXNCUixzREFBUSxDQUFDLEtBQUQsQ0FBcEM7QUFDQSxRQUFNO0FBQUEsT0FBQ1MsWUFBRDtBQUFBLE9BQWVDO0FBQWYsTUFBa0NWLHNEQUFRLENBQUMsRUFBRCxDQUFoRDtBQUVBLFFBQU07QUFBQSxPQUFDVyxrQkFBRDtBQUFBLE9BQXFCQztBQUFyQixNQUFrQ1osc0RBQVEsQ0FBQyxFQUFELENBQWhEO0FBQ0EsUUFBTTtBQUFBLE9BQUNQLFVBQUQ7QUFBQSxPQUFhb0I7QUFBYixNQUE4QmIsc0RBQVEsQ0FBQyxLQUFELENBQTVDO0FBRUEsUUFBTTtBQUFBLE9BQUNjLFlBQUQ7QUFBQSxPQUFlQztBQUFmLE1BQWtDZixzREFBUSxDQUFDLEVBQUQsQ0FBaEQ7QUFDQSxRQUFNO0FBQUEsT0FBQ2dCLGFBQUQ7QUFBQSxPQUFnQkM7QUFBaEIsTUFBOEJqQixzREFBUSxDQUFDLEVBQUQsQ0FBNUM7QUFDQSxRQUFNO0FBQUEsT0FBQ2tCLGFBQUQ7QUFBQSxPQUFnQkM7QUFBaEIsTUFBOEJuQixzREFBUSxDQUFDLEVBQUQsQ0FBNUM7QUFDQSxRQUFNO0FBQUEsT0FBQ29CLGlCQUFEO0FBQUEsT0FBb0JDO0FBQXBCLE1BQWlDckIsc0RBQVEsQ0FBQyxFQUFELENBQS9DO0FBQ0EsUUFBTTtBQUFBLE9BQUNzQixlQUFEO0FBQUEsT0FBa0JDO0FBQWxCLE1BQStCdkIsc0RBQVEsQ0FBQyxFQUFELENBQTdDO0FBRUF3Qix5REFBUyxDQUFDLE1BQU07QUFDZEMsU0FBSyxDQUFFLHlDQUFGLEVBQTRDO0FBQy9DQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsRUFBRyxVQUFTQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBOEI7QUFEaEQ7QUFEc0MsS0FBNUMsQ0FBTCxDQUtHQyxJQUxILENBS1NDLEdBQUQsSUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBTGpCLEVBTUdGLElBTkgsQ0FNU0csSUFBRCxJQUFVO0FBQ2RsQyxtQkFBYSxDQUFDa0MsSUFBSSxDQUFDaEQsVUFBTixDQUFiO0FBQ0FnQixnQkFBVSxDQUFDZ0MsSUFBSSxDQUFDL0MsT0FBTixDQUFWO0FBQ0FpQixrQkFBWSxDQUNWK0IsOENBQU0sQ0FBQ0QsSUFBSSxDQUFDRSxXQUFMLENBQWlCakMsU0FBbEIsQ0FBTixDQUNHa0MsT0FESCxDQUNXLE1BRFgsRUFFR0MsTUFGSCxDQUVVLFlBRlYsQ0FEVSxDQUFaO0FBS0FoQyxnQkFBVSxDQUNSNkIsOENBQU0sQ0FBQ0QsSUFBSSxDQUFDRSxXQUFMLENBQWlCL0IsT0FBbEIsQ0FBTixDQUFpQ2dDLE9BQWpDLENBQXlDLE1BQXpDLEVBQWlEQyxNQUFqRCxDQUF3RCxZQUF4RCxDQURRLENBQVY7QUFHRCxLQWpCSDtBQWtCRCxHQW5CUSxFQW1CTixFQW5CTSxDQUFUO0FBcUJBYix5REFBUyxDQUFDLE1BQU07QUFDZFgsaUJBQWEsQ0FBQyxLQUFELENBQWI7O0FBQ0EsUUFBSTVCLFVBQVUsQ0FBQ3FELE1BQVgsR0FBb0IsQ0FBeEIsRUFBMkI7QUFDekIsVUFBSXhDLElBQUksSUFBSSxLQUFaLEVBQW1CO0FBQ2pCYyxpQkFBUyxDQUFDM0IsVUFBRCxDQUFUO0FBQ0QsT0FGRCxNQUVPLElBQUlhLElBQUksSUFBSSxRQUFaLEVBQXNCO0FBQzNCYyxpQkFBUyxDQUFDM0IsVUFBVSxDQUFDc0QsTUFBWCxDQUFtQmpELFFBQUQsSUFBY0EsUUFBUSxDQUFDUSxJQUFULElBQWlCLFFBQWpELENBQUQsQ0FBVDtBQUNELE9BRk0sTUFFQTtBQUNMYyxpQkFBUyxDQUFDM0IsVUFBVSxDQUFDc0QsTUFBWCxDQUFtQmpELFFBQUQsSUFBY0EsUUFBUSxDQUFDUSxJQUFULElBQWlCLFNBQWpELENBQUQsQ0FBVDtBQUNEO0FBQ0Y7QUFDRixHQVhRLEVBV04sQ0FBQ0EsSUFBRCxFQUFPYixVQUFQLENBWE0sQ0FBVDtBQWFBdUMseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSWpCLE1BQU0sSUFBSSxLQUFkLEVBQXFCO0FBQ25CUSxxQkFBZSxDQUFDN0IsT0FBRCxDQUFmO0FBQ0Q7QUFDRixHQUpRLEVBSU4sQ0FBQ3FCLE1BQUQsRUFBU3JCLE9BQVQsQ0FKTSxDQUFUOztBQU1BLFdBQVNzRCxNQUFULENBQWdCQyxDQUFoQixFQUFtQjtBQUNqQkEsS0FBQyxDQUFDQyxjQUFGOztBQUNBLFFBQUl4RCxPQUFPLENBQUNvRCxNQUFSLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3RCdkIscUJBQWUsQ0FDYjdCLE9BQU8sQ0FBQ3FELE1BQVIsQ0FBZ0JsRCxNQUFELElBQVk7QUFDekIsZUFBT0EsTUFBTSxDQUFDc0QsV0FBUCxDQUNKQyxXQURJLEdBRUpDLFFBRkksQ0FFS3BDLFlBQVksQ0FBQ21DLFdBQWIsRUFGTCxDQUFQO0FBR0QsT0FKRCxDQURhLENBQWY7QUFPRCxLQVJELE1BUU87QUFDTDdCLHFCQUFlLENBQUMsRUFBRCxDQUFmO0FBQ0Q7QUFDRjs7QUFFRFMseURBQVMsQ0FBQyxNQUFNO0FBQ2RzQixXQUFPLENBQUNDLEdBQVIsQ0FBWWpDLFlBQVo7QUFDQSxRQUFJa0MsVUFBVSxHQUFHZCw4Q0FBTSxDQUFDaEMsU0FBRCxDQUFOLENBQWtCK0MsUUFBbEIsQ0FBMkIsQ0FBM0IsRUFBOEIsS0FBOUIsQ0FBakI7QUFDQSxRQUFJQyxTQUFTLEdBQUdoQiw4Q0FBTSxDQUFDOUIsT0FBRCxDQUFOLENBQWdCK0MsR0FBaEIsQ0FBb0IsQ0FBcEIsRUFBdUIsS0FBdkIsQ0FBaEI7O0FBRUEsUUFBSXJDLFlBQVksQ0FBQ3dCLE1BQWIsR0FBc0IsQ0FBMUIsRUFBNkI7QUFDM0JyQixnQkFBVSxDQUNSSCxZQUFZLENBQUN5QixNQUFiLENBQXFCbEQsTUFBRCxJQUFZO0FBQzlCLFlBQUkrRCxTQUFTLEdBQUdsQiw4Q0FBTSxDQUFDN0MsTUFBTSxDQUFDK0QsU0FBUixDQUFOLENBQXlCaEIsT0FBekIsQ0FBaUMsTUFBakMsQ0FBaEI7QUFDQSxlQUFPRiw4Q0FBTSxDQUFDa0IsU0FBRCxDQUFOLENBQWtCQyxTQUFsQixDQUE0QkwsVUFBNUIsRUFBd0NFLFNBQXhDLEVBQW1ELEtBQW5ELENBQVA7QUFDRCxPQUhELENBRFEsQ0FBVjtBQU1ELEtBUEQsTUFPTztBQUNMakMsZ0JBQVUsQ0FBQyxFQUFELENBQVY7QUFDRDtBQUNGLEdBZlEsRUFlTixDQUFDSCxZQUFELEVBQWVaLFNBQWYsRUFBMEJFLE9BQTFCLENBZk0sQ0FBVDtBQWlCQW9CLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlSLGFBQWEsQ0FBQ3NCLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDNUIsVUFBSXhDLElBQUksSUFBSSxLQUFaLEVBQW1CO0FBQ2pCcUIsa0JBQVUsQ0FBQ0gsYUFBRCxDQUFWO0FBQ0QsT0FGRCxNQUVPLElBQUlsQixJQUFJLElBQUksUUFBWixFQUFzQjtBQUMzQnFCLGtCQUFVLENBQUNILGFBQWEsQ0FBQ3VCLE1BQWQsQ0FBc0JsRCxNQUFELElBQVlBLE1BQU0sQ0FBQ2lFLE1BQVAsR0FBZ0IsQ0FBakQsQ0FBRCxDQUFWO0FBQ0QsT0FGTSxNQUVBO0FBQ0xuQyxrQkFBVSxDQUFDSCxhQUFhLENBQUN1QixNQUFkLENBQXNCbEQsTUFBRCxJQUFZQSxNQUFNLENBQUNpRSxNQUFQLEdBQWdCLENBQWpELENBQUQsQ0FBVjtBQUNEO0FBQ0YsS0FSRCxNQVFPO0FBQ0xuQyxnQkFBVSxDQUFDLEVBQUQsQ0FBVjtBQUNEO0FBQ0YsR0FaUSxFQVlOLENBQUNILGFBQUQsRUFBZ0JsQixJQUFoQixDQVpNLENBQVQ7QUFjQTBCLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlOLGFBQWEsQ0FBQ29CLE1BQWQsR0FBdUIsQ0FBM0IsRUFBOEI7QUFDNUIsVUFBSTdDLFVBQVUsSUFBSSxLQUFsQixFQUF5QjtBQUN2QjRCLGlCQUFTLENBQUNILGFBQUQsQ0FBVDtBQUNELE9BRkQsTUFFTztBQUNMRyxpQkFBUyxDQUNQSCxhQUFhLENBQUNxQixNQUFkLENBQXNCbEQsTUFBRCxJQUFZQSxNQUFNLENBQUNJLFVBQVAsSUFBcUJBLFVBQXRELENBRE8sQ0FBVDtBQUdEO0FBQ0YsS0FSRCxNQVFPO0FBQ0w0QixlQUFTLENBQUMsRUFBRCxDQUFUO0FBQ0Q7QUFDRixHQVpRLEVBWU4sQ0FBQ0gsYUFBRCxFQUFnQnpCLFVBQWhCLENBWk0sQ0FBVDtBQWNBK0IseURBQVMsQ0FBQyxNQUFNO0FBQ2RzQixXQUFPLENBQUNDLEdBQVIsQ0FBWTNCLGlCQUFaOztBQUNBLFFBQUlBLGlCQUFpQixDQUFDa0IsTUFBbEIsR0FBMkIsQ0FBL0IsRUFBa0M7QUFDaENmLGVBQVMsQ0FBQ3ZDLHVFQUFjLENBQUMyQixrQkFBRCxFQUFxQlMsaUJBQXJCLENBQWYsQ0FBVDtBQUNELEtBRkQsTUFFTztBQUNMRyxlQUFTLENBQUMsRUFBRCxDQUFUO0FBQ0Q7QUFDRixHQVBRLEVBT04sQ0FBQ0gsaUJBQUQsQ0FQTSxDQUFUO0FBU0Esc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQUtFLHFFQUFDLHNEQUFEO0FBQVEsYUFBTyxFQUFDLFNBQWhCO0FBQTBCLGFBQU8sRUFBR3FCLENBQUQsSUFBT2Msa0RBQU0sQ0FBQ0MsSUFBUCxDQUFZLGNBQVosQ0FBMUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFMRixlQVNFLHFFQUFDLG9EQUFEO0FBQU0sY0FBUSxFQUFHZixDQUFELElBQU9ELE1BQU0sQ0FBQ0MsQ0FBRCxDQUE3QjtBQUFBLDhCQUNFLHFFQUFDLG1EQUFEO0FBQUssaUJBQVMsRUFBQyxnQ0FBZjtBQUFBLGdDQUNFLHFFQUFDLG1EQUFEO0FBQUssWUFBRSxFQUFFLENBQVQ7QUFBWSxZQUFFLEVBQUUsQ0FBaEI7QUFBbUIsWUFBRSxFQUFFLENBQXZCO0FBQUEsaUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVkscUJBQVMsRUFBQyxVQUF0QjtBQUFBLG1DQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFFLEVBQUMsUUFETDtBQUVFLHNCQUFRLE1BRlY7QUFHRSxzQkFBUSxFQUFHQSxDQUFELElBQU9qQyxTQUFTLENBQUNpQyxDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVYsQ0FINUI7QUFBQSxzQ0FLRTtBQUFRLHFCQUFLLEVBQUMsS0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFMRixlQU1FO0FBQVEscUJBQUssRUFBQyxRQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQU5GO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLEVBYUduRCxNQUFNLElBQUksS0FBVixHQUFrQixJQUFsQixnQkFDQyxxRUFBQyxtREFBRDtBQUFBLGtDQUNFLHFFQUFDLG1EQUFEO0FBQUssY0FBRSxFQUFFLENBQVQ7QUFBWSxjQUFFLEVBQUUsQ0FBaEI7QUFBbUIsY0FBRSxFQUFFLENBQXZCO0FBQUEsbUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksdUJBQVMsRUFBQyxjQUF0QjtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLG9CQUFJLEVBQUMsUUFEUDtBQUVFLDJCQUFXLEVBQUMsV0FGZDtBQUdFLHFCQUFLLEVBQUVFLFlBSFQ7QUFJRSx3QkFBUSxFQUFHZ0MsQ0FBRCxJQUFPL0IsZUFBZSxDQUFDK0IsQ0FBQyxDQUFDZ0IsTUFBRixDQUFTQyxLQUFWLENBSmxDO0FBS0Usd0JBQVE7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFZRSxxRUFBQyxtREFBRDtBQUFLLGNBQUUsRUFBRSxDQUFUO0FBQVksY0FBRSxFQUFFLENBQWhCO0FBQW1CLGNBQUUsRUFBRSxDQUF2QjtBQUFBLHNCQUNHakQsWUFBWSxDQUFDNkIsTUFBYixHQUFzQixDQUF0QixnQkFDQyxxRUFBQyxzREFBRDtBQUFRLHFCQUFPLEVBQUMsU0FBaEI7QUFBMEIsa0JBQUksRUFBQyxRQUEvQjtBQUF3QyxnQkFBRSxFQUFDLFdBQTNDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURELGdCQUtDLHFFQUFDLHNEQUFEO0FBQ0UscUJBQU8sRUFBQyxTQURWO0FBRUUsa0JBQUksRUFBQyxRQUZQO0FBR0UsZ0JBQUUsRUFBQyxXQUhMO0FBSUUsc0JBQVEsTUFKVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBWkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBOENFLHFFQUFDLG1EQUFEO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxtREFBRDtBQUFLLHFCQUFTLEVBQUMsZ0NBQWY7QUFBQSxvQ0FDRSxxRUFBQyxtREFBRDtBQUFLLGdCQUFFLEVBQUUsQ0FBVDtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHlCQUFTLEVBQUMsTUFBdEI7QUFBQSx1Q0FDRSxxRUFBQyxtREFBRDtBQUFLLDJCQUFTLEVBQUMsMkJBQWY7QUFBQSwwQ0FDRSxxRUFBQyxtREFBRDtBQUFLLHNCQUFFLEVBQUUsQ0FBVDtBQUFBLDJDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFBLDJDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLHdCQUFFLEVBQUMsUUFETDtBQUVFLDhCQUFRLEVBQUdHLENBQUQsSUFBTztBQUNmbkMsK0JBQU8sQ0FBQ21DLENBQUMsQ0FBQ2dCLE1BQUYsQ0FBU0MsS0FBVixDQUFQO0FBQ0QsdUJBSkg7QUFLRSw4QkFBUSxNQUxWO0FBQUEsOENBT0U7QUFBUSw2QkFBSyxFQUFDLEtBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBUEYsZUFRRTtBQUFRLDZCQUFLLEVBQUMsUUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4QkFSRixlQVNFO0FBQVEsNkJBQUssRUFBQyxTQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDhCQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUF1QkUscUVBQUMsbURBQUQ7QUFBSyxnQkFBRSxFQUFFLENBQVQ7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSx5QkFBUyxFQUFDLFlBQXRCO0FBQUEsdUNBQ0UscUVBQUMsbURBQUQ7QUFBSywyQkFBUyxFQUFDLDJCQUFmO0FBQUEsMENBQ0UscUVBQUMsbURBQUQ7QUFBSyxzQkFBRSxFQUFFLENBQVQ7QUFBQSwyQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQURGLGVBSUUscUVBQUMsbURBQUQ7QUFBSyxzQkFBRSxFQUFFLENBQVQ7QUFBQSwyQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSx3QkFBRSxFQUFDLFFBREw7QUFFRSwyQkFBSyxFQUFFakUsVUFGVDtBQUdFLDhCQUFRLEVBQUdnRCxDQUFELElBQU81QixhQUFhLENBQUM0QixDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVYsQ0FIaEM7QUFJRSw4QkFBUSxNQUpWO0FBQUEsOENBTUU7QUFBUSw2QkFBSyxFQUFDLEtBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOEJBTkYsRUFPRy9DLGtCQUFrQixDQUFDMkIsTUFBbkIsSUFBNkIsQ0FBN0IsR0FDRyxJQURILEdBRUczQixrQkFBa0IsQ0FBQ3ZCLEdBQW5CLENBQXdCRSxRQUFELElBQWM7QUFDbkMsNENBQ0U7QUFBUSwrQkFBSyxFQUFFQSxRQUFRLENBQUNFLEdBQXhCO0FBQUEsb0NBQ0dGLFFBQVEsQ0FBQ087QUFEWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdDQURGO0FBS0QsdUJBTkQsQ0FUTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQXZCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBdURFLHFFQUFDLG1EQUFEO0FBQUEsaUNBQ0UscUVBQUMsbURBQUQ7QUFBQSxtQ0FDRSxxRUFBQyxtREFBRDtBQUFBLHFDQUNFLHFFQUFDLG1EQUFEO0FBQUsseUJBQVMsRUFBQyxnQ0FBZjtBQUFBLHdDQUNFLHFFQUFDLG1EQUFEO0FBQUssb0JBQUUsRUFBQyxNQUFSO0FBQUEseUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERixlQUlFLHFFQUFDLG1EQUFEO0FBQUEseUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0Usd0JBQUksRUFBQyxNQURQO0FBRUUsOEJBQVUsRUFBQyxZQUZiO0FBR0UseUJBQUssRUFBRUssU0FIVDtBQUlFLDRCQUFRLEVBQUd1QyxDQUFELElBQU90QyxZQUFZLENBQUNzQyxDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVY7QUFKL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkYsZUFZRSxxRUFBQyxtREFBRDtBQUFLLG9CQUFFLEVBQUMsTUFBUjtBQUFBLHlDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBWkYsZUFlRSxxRUFBQyxtREFBRDtBQUFBLHlDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLHdCQUFJLEVBQUMsTUFEUDtBQUVFLHlCQUFLLEVBQUV0RCxPQUZUO0FBR0UsOEJBQVUsRUFBQyxhQUhiO0FBSUUsNEJBQVEsRUFBR3FDLENBQUQsSUFBT3BDLFVBQVUsQ0FBQ29DLENBQUMsQ0FBQ2dCLE1BQUYsQ0FBU0MsS0FBVjtBQUo3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFmRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdkRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTlDRixFQW9JR3BDLGVBQWUsQ0FBQ2dCLE1BQWhCLElBQTBCLENBQTFCLGdCQUNDLHFFQUFDLHFEQUFEO0FBQU8sZUFBTyxFQUFDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERCxnQkFHQyxxRUFBQyxxREFBRDtBQUFPLGVBQU8sTUFBZDtBQUFlLGdCQUFRLE1BQXZCO0FBQXdCLGFBQUssTUFBN0I7QUFBQSxnQ0FDRTtBQUFBLGlDQUNFO0FBQUEsb0NBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFGRixlQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUhGLGVBSUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkYsZUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBV0U7QUFBQSxvQkFDR2hCLGVBQWUsQ0FBQ2xDLEdBQWhCLENBQXFCQyxNQUFELElBQVk7QUFDL0IsZ0NBQ0U7QUFBQSxzQ0FDRTtBQUFBLDBCQUFLQSxNQUFNLENBQUNNO0FBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERixlQUVFO0FBQUEsMEJBQUtOLE1BQU0sQ0FBQ087QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZGLGVBR0U7QUFBQSwwQkFBS1AsTUFBTSxDQUFDc0Q7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUhGLGVBSUU7QUFBQSwwQkFBS2dCLElBQUksQ0FBQ0MsR0FBTCxDQUFTdkUsTUFBTSxDQUFDaUUsTUFBaEI7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUpGLGVBS0U7QUFBQSwwQkFDR3BCLDhDQUFNLENBQUM3QyxNQUFNLENBQUMrRCxTQUFSLENBQU4sQ0FBeUJmLE1BQXpCLENBQWdDLHVCQUFoQztBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBTEY7QUFBQSxlQUFTaEQsTUFBTSxDQUFDRyxHQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGO0FBV0QsV0FaQTtBQURIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBWEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBdklKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBZ0xELEM7Ozs7Ozs7Ozs7O0FDMVRELG1DOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLDJEOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLHdDIiwiZmlsZSI6InBhZ2VzL3JlY29yZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvcmVjb3JkL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuXHJcbi8vIGNyZWF0ZXMgYSBDb250ZXh0IE9iamVjdFxyXG5jb25zdCBVc2VyQ29udGV4dCA9IFJlYWN0LmNyZWF0ZUNvbnRleHQoKTtcclxuXHJcbmV4cG9ydCBjb25zdCBVc2VyUHJvdmlkZXIgPSBVc2VyQ29udGV4dC5Qcm92aWRlclxyXG5cclxuZXhwb3J0IGRlZmF1bHQgVXNlckNvbnRleHQ7IiwiLy8gRnVuY3Rpb24gZm9yIGNvbnZlcnRpbmcgc3RyaW5nIGRhdGEgZnJvbSB0aGUgZW5wb2ludHMgdG8gbnVtYmVyc1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY2F0ZWdvcnlSZWNvcmQoY2F0ZWdvcmllcywgcmVjb3Jkcyl7XHJcblxyXG5cdGNvbnN0IGNvbWJpbmVkQ2F0UmVjID0gcmVjb3Jkcy5tYXAocmVjb3JkID0+e1xyXG5cdFx0Y29uc3QgY2F0ZWdvcnkgPSBjYXRlZ29yaWVzLmZpbmQoY2F0ZWdvcnkgPT4gY2F0ZWdvcnkuX2lkID09IHJlY29yZC5jYXRlZ29yeUlkKVxyXG5cclxuXHRcdGlmKGNhdGVnb3J5PT11bmRlZmluZWQpe1xyXG5cdFx0XHRyZWNvcmQuY2F0ZWdvcnlUeXBlID0gXCJ1bmNhdGVnb3JpemVkXCJcclxuXHRcdFx0cmVjb3JkLmNhdGVnb3J5TmFtZSA9IFwidW5jYXRlZ29yaXplZFwiXHJcblx0XHR9ZWxzZXtcclxuXHRcdFx0cmVjb3JkLmNhdGVnb3J5TmFtZSA9IGNhdGVnb3J5Lm5hbWVcclxuXHRcdFx0cmVjb3JkLmNhdGVnb3J5VHlwZSA9IGNhdGVnb3J5LnR5cGVcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gcmVjb3JkXHJcblx0fSlcclxuXHJcblx0cmV0dXJuIGNvbWJpbmVkQ2F0UmVjXHJcblxyXG59IiwiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZVJlZiB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGb3JtLCBCdXR0b24sIFRhYmxlLCBSb3csIENvbCwgQWxlcnQgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IERyb3Bkb3duQnV0dG9uIGZyb20gXCJyZWFjdC1ib290c3RyYXAvRHJvcGRvd25CdXR0b25cIjtcclxuaW1wb3J0IERyb3Bkb3duIGZyb20gXCJyZWFjdC1ib290c3RyYXAvRHJvcGRvd25cIjtcclxuaW1wb3J0IFN3YWwgZnJvbSBcInN3ZWV0YWxlcnQyXCI7XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vLi4vVXNlckNvbnRleHRcIjtcclxuaW1wb3J0IGNhdGVnb3J5UmVjb3JkIGZyb20gXCIuLi8uLi9oZWxwZXJzL2NhdGVnb3J5UmVjb3JkXCI7XHJcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiByZWNvcmQoKSB7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXMsIHNldENhdGVnb3JpZXNdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3JlY29yZHMsIHNldFJlY29yZHNdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3N0YXJ0RGF0ZSwgc2V0U3RhcnREYXRlXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtlbmREYXRlLCBzZXRFbmREYXRlXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG5cclxuICBjb25zdCBbdHlwZSwgc2V0VHlwZV0gPSB1c2VTdGF0ZShcImFsbFwiKTtcclxuICBjb25zdCBbYWN0aW9uLCBzZXRBY3Rpb25dID0gdXNlU3RhdGUoXCJhbGxcIik7XHJcbiAgY29uc3QgW3NlYXJjaFN0cmluZywgc2V0U2VhcmNoU3RyaW5nXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG5cclxuICBjb25zdCBbZmlsdGVyZWRDYXRlZ29yaWVzLCBzZXRGaWxDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtjYXRlZ29yeUlkLCBzZXRDYXRlZ29yeUlkXSA9IHVzZVN0YXRlKFwiYWxsXCIpO1xyXG5cclxuICBjb25zdCBbcmVjb3JkUmVzdWx0LCBzZXRSZWNvcmRSZXN1bHRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtyZWNvcmRzT25EYXRlLCBzZXRSZWNEYXRlXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbcmVjb3Jkc09uVHlwZSwgc2V0UmVjVHlwZV0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW3JlY29yZHNPbkNhdGVnb3J5LCBzZXRSZWNDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtmaWx0ZXJlZFJlY29yZHMsIHNldEZpbFJlY10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9kZXRhaWxzYCwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgc2V0Q2F0ZWdvcmllcyhkYXRhLmNhdGVnb3JpZXMpO1xyXG4gICAgICAgIHNldFJlY29yZHMoZGF0YS5yZWNvcmRzKTtcclxuICAgICAgICBzZXRTdGFydERhdGUoXHJcbiAgICAgICAgICBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5zdGFydERhdGUpXHJcbiAgICAgICAgICAgIC5zdGFydE9mKFwiZGF5c1wiKVxyXG4gICAgICAgICAgICAuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgc2V0RW5kRGF0ZShcclxuICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLmVuZERhdGUpLnN0YXJ0T2YoXCJkYXlzXCIpLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICApO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRDYXRlZ29yeUlkKFwiYWxsXCIpO1xyXG4gICAgaWYgKGNhdGVnb3JpZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICBpZiAodHlwZSA9PSBcImFsbFwiKSB7XHJcbiAgICAgICAgc2V0RmlsQ2F0KGNhdGVnb3JpZXMpO1xyXG4gICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gXCJpbmNvbWVcIikge1xyXG4gICAgICAgIHNldEZpbENhdChjYXRlZ29yaWVzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gXCJpbmNvbWVcIikpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNldEZpbENhdChjYXRlZ29yaWVzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gXCJleHBlbnNlXCIpKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0sIFt0eXBlLCBjYXRlZ29yaWVzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoYWN0aW9uID09IFwiYWxsXCIpIHtcclxuICAgICAgc2V0UmVjb3JkUmVzdWx0KHJlY29yZHMpO1xyXG4gICAgfVxyXG4gIH0sIFthY3Rpb24sIHJlY29yZHNdKTtcclxuXHJcbiAgZnVuY3Rpb24gc2VhcmNoKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIGlmIChyZWNvcmRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgc2V0UmVjb3JkUmVzdWx0KFxyXG4gICAgICAgIHJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgIHJldHVybiByZWNvcmQuZGVzY3JpcHRpb25cclxuICAgICAgICAgICAgLnRvTG93ZXJDYXNlKClcclxuICAgICAgICAgICAgLmluY2x1ZGVzKHNlYXJjaFN0cmluZy50b0xvd2VyQ2FzZSgpKTtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0UmVjb3JkUmVzdWx0KFtdKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhyZWNvcmRSZXN1bHQpO1xyXG4gICAgbGV0IGRhdGVCZWZvcmUgPSBtb21lbnQoc3RhcnREYXRlKS5zdWJ0cmFjdCgxLCBcImRheVwiKTtcclxuICAgIGxldCBkYXRlQWZ0ZXIgPSBtb21lbnQoZW5kRGF0ZSkuYWRkKDEsIFwiZGF5XCIpO1xyXG5cclxuICAgIGlmIChyZWNvcmRSZXN1bHQubGVuZ3RoID4gMCkge1xyXG4gICAgICBzZXRSZWNEYXRlKFxyXG4gICAgICAgIHJlY29yZFJlc3VsdC5maWx0ZXIoKHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgbGV0IHVwZGF0ZWRPbiA9IG1vbWVudChyZWNvcmQudXBkYXRlZE9uKS5zdGFydE9mKFwiZGF5c1wiKTtcclxuICAgICAgICAgIHJldHVybiBtb21lbnQodXBkYXRlZE9uKS5pc0JldHdlZW4oZGF0ZUJlZm9yZSwgZGF0ZUFmdGVyLCBcImRheVwiKTtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0UmVjRGF0ZShbXSk7XHJcbiAgICB9XHJcbiAgfSwgW3JlY29yZFJlc3VsdCwgc3RhcnREYXRlLCBlbmREYXRlXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAocmVjb3Jkc09uRGF0ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGlmICh0eXBlID09IFwiYWxsXCIpIHtcclxuICAgICAgICBzZXRSZWNUeXBlKHJlY29yZHNPbkRhdGUpO1xyXG4gICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gXCJpbmNvbWVcIikge1xyXG4gICAgICAgIHNldFJlY1R5cGUocmVjb3Jkc09uRGF0ZS5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmFtb3VudCA+IDApKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZXRSZWNUeXBlKHJlY29yZHNPbkRhdGUuZmlsdGVyKChyZWNvcmQpID0+IHJlY29yZC5hbW91bnQgPCAwKSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldFJlY1R5cGUoW10pO1xyXG4gICAgfVxyXG4gIH0sIFtyZWNvcmRzT25EYXRlLCB0eXBlXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAocmVjb3Jkc09uVHlwZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGlmIChjYXRlZ29yeUlkID09IFwiYWxsXCIpIHtcclxuICAgICAgICBzZXRSZWNDYXQocmVjb3Jkc09uVHlwZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2V0UmVjQ2F0KFxyXG4gICAgICAgICAgcmVjb3Jkc09uVHlwZS5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmNhdGVnb3J5SWQgPT0gY2F0ZWdvcnlJZClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRSZWNDYXQoW10pO1xyXG4gICAgfVxyXG4gIH0sIFtyZWNvcmRzT25UeXBlLCBjYXRlZ29yeUlkXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhyZWNvcmRzT25DYXRlZ29yeSk7XHJcbiAgICBpZiAocmVjb3Jkc09uQ2F0ZWdvcnkubGVuZ3RoID4gMCkge1xyXG4gICAgICBzZXRGaWxSZWMoY2F0ZWdvcnlSZWNvcmQoZmlsdGVyZWRDYXRlZ29yaWVzLCByZWNvcmRzT25DYXRlZ29yeSkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0RmlsUmVjKFtdKTtcclxuICAgIH1cclxuICB9LCBbcmVjb3Jkc09uQ2F0ZWdvcnldKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPlNlYXJjaCBSZWNvcmRzPC90aXRsZT5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAgPEJ1dHRvbiB2YXJpYW50PVwic3VjY2Vzc1wiIG9uQ2xpY2s9eyhlKSA9PiBSb3V0ZXIucHVzaChcIi4vcmVjb3JkL2FkZFwiKX0+XHJcbiAgICAgICAgQWRkIFJlY29yZFxyXG4gICAgICA8L0J1dHRvbj5cclxuXHJcbiAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gc2VhcmNoKGUpfT5cclxuICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgPENvbCB4cz17M30gbWQ9ezN9IGxnPXszfT5cclxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwic2VhcmNoQnlcIj5cclxuICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRBY3Rpb24oZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJhbGxcIj5BbGwgUmVjb3Jkczwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cInNlYXJjaFwiPlNlYXJjaCBSZWNvcmRzPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAge2FjdGlvbiA9PSBcImFsbFwiID8gbnVsbCA6IChcclxuICAgICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgICA8Q29sIHhzPXs4fSBtZD17OH0gbGc9ezh9PlxyXG4gICAgICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwic2VhcmNoU3RyaW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3RyaW5nXCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlNlYXJjaC4uLlwiXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3NlYXJjaFN0cmluZ31cclxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFNlYXJjaFN0cmluZyhlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICA8Q29sIHhzPXs0fSBtZD17NH0gbGc9ezR9PlxyXG4gICAgICAgICAgICAgICAge3NlYXJjaFN0cmluZy5sZW5ndGggPiAwID8gKFxyXG4gICAgICAgICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiIGlkPVwic3VibWl0QnRuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgU2VhcmNoXHJcbiAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcclxuICAgICAgICAgICAgICAgICAgICBpZD1cInN1Ym1pdEJ0blwiXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIFNlYXJjaFxyXG4gICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgICAgKX1cclxuICAgICAgICA8L1Jvdz5cclxuICAgICAgICA8Um93PlxyXG4gICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtbWQtY2VudGVyIG15LTJcIj5cclxuICAgICAgICAgICAgICA8Q29sIGxnPXs0fT5cclxuICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInR5cGVcIj5cclxuICAgICAgICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtbWQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPENvbCBsZz17M30+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD5UeXBlOjwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHNldFR5cGUoZS50YXJnZXQudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiYWxsXCI+QWxsPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJpbmNvbWVcIj5JbmNvbWU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImV4cGVuc2VcIj5FeHBlbnNlPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cclxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgPENvbCBsZz17N30+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJjYXRlZ29yeUlkXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxDb2wgbGc9ezZ9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+Q2F0ZWdvcnkgTmFtZTo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICAgICAgPENvbCBsZz17Nn0+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2NhdGVnb3J5SWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0Q2F0ZWdvcnlJZChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJhbGxcIj5BbGw8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAge2ZpbHRlcmVkQ2F0ZWdvcmllcy5sZW5ndGggPT0gMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgID8gbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogZmlsdGVyZWRDYXRlZ29yaWVzLm1hcCgoY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPXtjYXRlZ29yeS5faWR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2NhdGVnb3J5Lm5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgIDxSb3c+XHJcbiAgICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxDb2wgbWQ9XCJhdXRvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+RGF0ZTo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICAgICAgICAgIGRhdGVmb3JtYXQ9XCJ5eXl5LU1NLUREXCJcclxuICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtzdGFydERhdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFN0YXJ0RGF0ZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICAgIDxDb2wgbWQ9XCJhdXRvXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+IHRvIDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImRhdGVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2VuZERhdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICBkYXRlZm9ybWF0PVwibXl5eXktTU0tRERcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRFbmREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgIDwvUm93PlxyXG4gICAgICAgIHtmaWx0ZXJlZFJlY29yZHMubGVuZ3RoID09IDAgPyAoXHJcbiAgICAgICAgICA8QWxlcnQgdmFyaWFudD1cImluZm9cIj5ObyBSZWNvcmRzIEZvdW5kPC9BbGVydD5cclxuICAgICAgICApIDogKFxyXG4gICAgICAgICAgPFRhYmxlIHN0cmlwZWQgYm9yZGVyZWQgaG92ZXI+XHJcbiAgICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgICA8dGg+VHlwZTwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGg+Q2F0ZWdvcnk8L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRoPkRlc2NyaXB0aW9uPC90aD5cclxuICAgICAgICAgICAgICAgIDx0aD5BbW91bnQ8L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRoPkRhdGU8L3RoPlxyXG4gICAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICAgIDwvdGhlYWQ+XHJcblxyXG4gICAgICAgICAgICA8dGJvZHk+XHJcbiAgICAgICAgICAgICAge2ZpbHRlcmVkUmVjb3Jkcy5tYXAoKHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgPHRyIGtleT17cmVjb3JkLl9pZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPntyZWNvcmQuY2F0ZWdvcnlUeXBlfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPntyZWNvcmQuY2F0ZWdvcnlOYW1lfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPntyZWNvcmQuZGVzY3JpcHRpb259PC90ZD5cclxuICAgICAgICAgICAgICAgICAgICA8dGQ+e01hdGguYWJzKHJlY29yZC5hbW91bnQpfTwvdGQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPHRkPlxyXG4gICAgICAgICAgICAgICAgICAgICAge21vbWVudChyZWNvcmQudXBkYXRlZE9uKS5mb3JtYXQoXCJNTU1NIEQsIFlZWVkgIGhoOm1tIGFcIil9XHJcbiAgICAgICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgIDwvdGJvZHk+XHJcbiAgICAgICAgICA8L1RhYmxlPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvRm9ybT5cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufVxyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJtb21lbnRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9oZWFkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvcm91dGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1ib290c3RyYXAvRHJvcGRvd25cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtYm9vdHN0cmFwL0Ryb3Bkb3duQnV0dG9uXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9