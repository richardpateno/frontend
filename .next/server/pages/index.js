module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./UserContext.js":
/*!************************!*\
  !*** ./UserContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // creates a Context Object

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "./app-helper.js":
/*!***********************!*\
  !*** ./app-helper.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  getAccessToken: () => localStorage.getItem('token')
};

/***/ }),

/***/ "./components/PieChart.js":
/*!********************************!*\
  !*** ./components/PieChart.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChart; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/ColorRandomizer */ "./helpers/ColorRandomizer.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\PieChart.js";



function PieChart({
  chartType,
  rawData,
  labelKey,
  amountKey
}) {
  const {
    0: label,
    1: setLabel
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: amount,
    1: setAmount
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: bgColors,
    1: setBgColors
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (rawData) {
      setLabel(rawData.map(element => element[labelKey]));
      setAmount(rawData.map(element => Math.abs(element[amountKey])));
      let colors = rawData.map(element => {
        if (element.color) {
          return element.color;
        } else {
          return `#${Object(_helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__["default"])()}`;
        }
      });
      setBgColors(colors);
    } else {
      setLabel([]);
      setAmount([]);
      setBgColors([]);
    }
  }, [rawData]);

  if (chartType == "Pie") {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      },
      redraw: false
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 7
    }, this);
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Doughnut"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 7
    }, this);
  }
}

/***/ }),

/***/ "./components/View.js":
/*!****************************!*\
  !*** ./components/View.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\View.js";


 //other syntax

const View = ({
  title,
  children
}) => {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: title
      }, "title-tag", false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("meta", {
        name: "viewport",
        content: "initial-scale=1.0, width=device-width"
      }, "title-meta", false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Container"], {
      className: "mt-5 pt-4 mb-5",
      children: children
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 8,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./helpers/ColorRandomizer.js":
/*!************************************!*\
  !*** ./helpers/ColorRandomizer.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ColorRandomizer; });
function ColorRandomizer() {
  return Math.floor(Math.random() * 16777215).toString(16);
}

/***/ }),

/***/ "./helpers/sumByGroup.js":
/*!*******************************!*\
  !*** ./helpers/sumByGroup.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return sumByGroup; });
function sumByGroup(labelArray, labelKey, array, key, keySum) {
  let sumCounts = [];
  array.reduce(function (res, value) {
    if (!res[value[key]]) {
      res[value[key]] = {
        [key]: value[key],
        [keySum]: 0,
        count: 0
      };
      sumCounts.push(res[value[key]]);
    }

    res[value[key]][keySum] += value[keySum];
    res[value[key]].count += 1;
    return res;
  }, {});

  if (labelArray != [] & labelKey != "") {
    let labelSumCounts = labelArray.map(label => {
      let foundSumCount = sumCounts.find(sumCount => sumCount[key] == label[labelKey]);

      if (foundSumCount == undefined) {
        label[keySum] = 0;
        label.count = 0;
      } else {
        label[keySum] = foundSumCount[keySum];
        label.count = foundSumCount.count;
      }

      return label;
    });
    return labelSumCounts;
  } else {
    return sumCounts;
  }
}
;

/***/ }),

/***/ "./pages/category/index.js":
/*!*********************************!*\
  !*** ./pages/category/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../helpers/sumByGroup */ "./helpers/sumByGroup.js");

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\category\\index.js";







function category() {
  const {
    0: categories,
    1: setCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: currentRecords,
    1: setCurRec
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeCategories,
    1: setIncCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: expensesCategories,
    1: setExpCat
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: incomeRows,
    1: setIncomeRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  const {
    0: expensesRows,
    1: setExpensesRows
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setCategories(data.categories);
      let dateBefore = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.startDate).subtract(1, "day");
      let dateAfter = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.endDate).add(1, "day");
      let filteredRecords = data.records.filter(record => {
        let updatedOn = moment__WEBPACK_IMPORTED_MODULE_6___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_6___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });
      setCurRec(filteredRecords);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let categoryRecords = Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__["default"])(categories, "_id", currentRecords, "categoryId", "amount");
    setIncCat(categoryRecords.filter(category => category.type == "income"));
    setExpCat(categoryRecords.filter(category => category.type == "expense"));
  }, [categories, currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setIncomeRows(incomeCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 11
      }, this);
    }));
  }, [incomeCategories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setExpensesRows(expensesCategories.map(category => {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: Math.abs(category.amount)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount + category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 13
        }, this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 11
      }, this);
    }));
  }, [expensesCategories]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: " Categories"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], {
      className: "justify-content-md-end my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "success",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Add Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "secondary",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/edit"),
        children: "Edit Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "danger",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add"),
        children: "Delete Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "info",
        onClick: () => next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("../record/add"),
        children: "Add Record"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 111,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Income:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 7
    }, this), incomeCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 122,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 127,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Income"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 128,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: incomeRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 132,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 9
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Expenses:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 135,
      columnNumber: 7
    }, this), expensesCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories under expenses yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 142,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Budget"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Expenses"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Savings"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 141,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: expensesRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 149,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login */ "./pages/login.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./category */ "./pages/category/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_PieChart__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/PieChart */ "./components/PieChart.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_10__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\index.js";










function Home() {
  const {
    user,
    setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_5__["default"]);
  const {
    0: loading,
    1: setLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true);
  const {
    0: name,
    1: setName
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: email,
    1: setEmail
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: categories,
    1: setCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: startDate,
    1: setStartDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: endDate,
    1: setEndDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: savedStart,
    1: setSavedStart
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: savedEnd,
    1: setSavedEnd
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: currentRecords,
    1: setCurrentRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: oldRecords,
    1: setOldRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: previousSavings,
    1: setPreviousSavings
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: currentIncome,
    1: setCurrentIncome
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: currentExpenses,
    1: setCurrentExpenses
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: budget,
    1: setBudget
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0);
  const {
    0: actualPie,
    1: setActualPie
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: budgetPie,
    1: setBudgetPie
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (user.id != null) {
      fetch(`http://localhost:4000/api/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      }).then(res => res.json()).then(data => {
        console.log(data);
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setName(`${data.firstName} ${data.lastName}`);
        setEmail(data.email);
        setRecords(data.records);
        setCategories(data.categories);
      });
    }
  }, [user]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (name != "" & email != "") {
      setLoading(false);
    }
  }, [name, email]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    let dateBefore = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).subtract(1, "day");
    let dateAfter = moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).add(1, "day");

    if (startDate <= endDate) {
      if (records.length > 0) {
        setCurrentRecords(records.filter(record => {
          let updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
        }));
        setOldRecords(records.filter(record => {
          let updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBefore(dateBefore, "day");
        }));
      }
    } else {
      setCurrentRecords([]);
      setOldRecords([]);
    }

    if (categories.length > 0) {
      setBudget(categories.reduce((total, category) => {
        return total + category.budget;
      }, 0));
    } else {
      setBudget(0);
    }
  }, [categories, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (oldRecords.length > 0) {
      setPreviousSavings(oldRecords.reduce((total, record) => {
        return total + record.amount;
      }, 0));
    } else {
      setPreviousSavings(0);
    }
  }, [oldRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (currentRecords.length > 0) {
      let income = currentRecords.filter(record => record.amount > 0);
      console.log(income);

      if (income.length > 0) {
        setCurrentIncome(income.reduce((total, record) => {
          return total + record.amount;
        }, 0));
      } else {
        setCurrentIncome(0);
      }

      let expenses = currentRecords.filter(record => record.amount < 0);

      if (expenses.length > 0) {
        setCurrentExpenses(expenses.reduce((total, record) => {
          return total - record.amount;
        }, 0));
      } else {
        setCurrentExpenses(0);
      }
    } else {
      setCurrentIncome(0);
      setCurrentExpenses(0);
    }
  }, [currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    console.log(previousSavings);
    console.log(currentIncome);
    console.log(currentExpenses);

    if (previousSavings >= 0) {
      if (currentIncome > currentExpenses) {
        setActualPie([{
          label: "Current Savings",
          amount: currentIncome - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Current Expenses",
          amount: currentExpenses,
          color: "#f56f36"
        }, {
          label: "Previous Savings",
          amount: previousSavings,
          color: "#0f5209"
        }]);
      } else {
        if (currentExpenses < currentIncome + previousSavings) {
          setActualPie([{
            label: "Accumulated Savings",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#0f5209"
          }, {
            label: "Current Expenses",
            amount: currentExpenses,
            color: "#f56f36"
          }]);
        } else {
          setActualPie([{
            label: "Accumulated Deficit",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#e30707"
          }, {
            label: "Consumed Savings and Income",
            amount: currentIncome + previousSavings,
            color: "#0f5209"
          }]);
        }
      }
    } else {
      if (currentIncome > Math.abs(previousSavings) + currentExpenses) {
        setActualPie([{
          label: "Accumulated Savings",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Previous Deficit + Current Expenses",
          amount: Math.abs(previousSavings) + currentExpenses,
          color: "#f56f36"
        }]);
      } else {
        setActualPie([{
          label: "Consumed Income",
          amount: currentIncome,
          color: "#f56f36"
        }, {
          label: "Accumulated Deficit",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#e30707"
        }]);
      }
    }
  }, [previousSavings, currentIncome, currentExpenses]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (budget >= currentExpenses) {
      setBudgetPie([{
        label: "Savings",
        amount: budget - currentExpenses,
        color: "#47b83d"
      }, {
        label: "Expenses",
        amount: currentExpenses,
        color: "#f56f36"
      }]);
    } else {
      setBudgetPie([{
        label: "Consumed Budget",
        amount: budget,
        color: "#f56f36"
      }, {
        label: "Deficit",
        amount: Math.abs(budget - currentExpenses),
        color: "#e30707"
      }]);
    }
  }, [budget, currentExpenses]);

  function saveDate(e) {
    console.log(e);
    e.preventDefault();
    fetch(`http://localhost:4000/api/users/setDate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        startDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).startOf("day"),
        endDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).startOf("day")
      })
    }).then(res => res.json()).then(data => {
      if (data == false) {
        Swal.fire("Error", `Something went wrong`, "error");
      } else {
        console.log(data);
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Budget Tracker"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 312,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 311,
      columnNumber: 7
    }, this), user.id !== null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
        className: "justify-content-center",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 318,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: email
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 319,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: ["Balance: Php ", currentIncome + previousSavings - currentExpenses]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 320,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 317,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
          onSubmit: e => saveDate(e),
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
            className: "justify-content-md-center my-2",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: "Budget Date:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 328,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 327,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                dateformat: "yyyy-MM-DD",
                value: startDate,
                onChange: e => setStartDate(e.target.value)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 331,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 330,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: " to "
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 339,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 338,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                value: endDate,
                dateformat: "myyyy-MM-DD",
                onChange: e => setEndDate(e.target.value)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 342,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 341,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: startDate != savedStart || endDate != savedEnd ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 352,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                disabled: true,
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 356,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 350,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 326,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 325,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 324,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 374,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: actualPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 376,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 372,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Income: Php ", currentIncome]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 386,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 387,
                  columnNumber: 21
                }, this), currentIncome >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Savings: Php ", currentIncome - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 389,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Deficit: Php ", currentIncome - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 393,
                  columnNumber: 23
                }, this), previousSavings >= 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Savings: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 398,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Deficit: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 400,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 385,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 384,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 371,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 370,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 410,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: budgetPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 412,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 408,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Budget: Php ", budget]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 422,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 423,
                  columnNumber: 21
                }, this), budget >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Savings: Php ", budget - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 425,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Deficit: Php ", budget - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 427,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 421,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 420,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 407,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 406,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 369,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          className: "justify-content-md-center my-2",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "success",
            onClick: () => next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./category/add"),
            children: "Add Category"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 436,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 435,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "secondary",
            onClick: () => next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./record/add"),
            children: "Add Record"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 446,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 445,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 434,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 316,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_login__WEBPACK_IMPORTED_MODULE_6__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 458,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 310,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/login.js":
/*!************************!*\
  !*** ./pages/login.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return login; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-google-login */ "react-google-login");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _components_View__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/View */ "./components/View.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app-helper */ "./app-helper.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_app_helper__WEBPACK_IMPORTED_MODULE_9__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\login.js";









function login() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_View__WEBPACK_IMPORTED_MODULE_8__["default"], {
    title: "Login",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      className: "justify-content-center",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: true,
        md: "6",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(LoginForm, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

const LoginForm = () => {
  const {
    user,
    setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_7__["default"]);
  const {
    0: email,
    1: setEmail
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: password,
    1: setPassword
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: isActive,
    1: setIsActive
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);

  function authenticate(e) {
    e.preventDefault();
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    };
    fetch(`http://localhost:4000/api/users/login`, options).then(res => res.json()).then(data => {
      console.log(data);

      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error === "does-not-exist") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "User does not exist.", "error");
        } else if (data.error === "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure, try an alternative login procedure", "error");
        } else if (data.error === "incorrect-password") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "password is incorrect", "error");
        }
      }
    });
  }

  const aunthenticateGoogleToken = response => {
    const payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    };
    fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload).then(res => res.json()).then(data => {
      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error = "google-auth-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Google Auth Error", "Google authentication procedure failed.", "error");
        } else if (data.error = "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure.", "error");
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Header, {
      children: "Login details"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 7
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_6___default.a, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
          children: "Budget Tracker"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 115,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 114,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
        onSubmit: e => authenticate(e),
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "userEmail",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Email address"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 119,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "email",
            placeholder: "Enter email",
            value: email,
            onChange: e => setEmail(e.target.value),
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 120,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 118,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "password",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Password"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 13
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "password",
            placeholder: "Password",
            value: password,
            onChange: e => setPassword(e.target.value),
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 131,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 11
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          className: "justify-content-center px-3",
          children: isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 141,
            columnNumber: 15
          }, undefined) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            disabled: true,
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 139,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 9
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_google_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLogin"], {
        clientId: "656593662569-24gufu44evj4ujqs3k0113rgrurq2bnp.apps.googleusercontent.com",
        buttonText: "Login",
        onSuccess: aunthenticateGoogleToken,
        onFailure: aunthenticateGoogleToken,
        cookiePolicy: "single_host_origin",
        className: "w-100 text-center d-flex justify-content-center"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 152,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 111,
    columnNumber: 5
  }, undefined);
};

/***/ }),

/***/ "./styles/Home.module.css":
/*!********************************!*\
  !*** ./styles/Home.module.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Exports
module.exports = {
	"container": "Home_container__1EcsU",
	"main": "Home_main__1x8gC",
	"footer": "Home_footer__1WdhD",
	"title": "Home_title__3DjR7",
	"description": "Home_description__17Z4F",
	"code": "Home_code__axx2Y",
	"grid": "Home_grid__2Ei2F",
	"card": "Home_card__2SdtB",
	"logo": "Home_logo__1YbrH",
	"stackedBar": "Home_stackedBar__3UO1l",
	"homepageCard": "Home_homepageCard__1vkLN"
};


/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-chartjs-2");

/***/ }),

/***/ "react-google-login":
/*!*************************************!*\
  !*** external "react-google-login" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-google-login");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vYXBwLWhlbHBlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1BpZUNoYXJ0LmpzIiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvVmlldy5qcyIsIndlYnBhY2s6Ly8vLi9oZWxwZXJzL0NvbG9yUmFuZG9taXplci5qcyIsIndlYnBhY2s6Ly8vLi9oZWxwZXJzL3N1bUJ5R3JvdXAuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvY2F0ZWdvcnkvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvbG9naW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3R5bGVzL0hvbWUubW9kdWxlLmNzcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb21lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L2hlYWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtYm9vdHN0cmFwXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtY2hhcnRqcy0yXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtZ29vZ2xlLWxvZ2luXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic3dlZXRhbGVydDJcIiJdLCJuYW1lcyI6WyJVc2VyQ29udGV4dCIsIlJlYWN0IiwiY3JlYXRlQ29udGV4dCIsIlVzZXJQcm92aWRlciIsIlByb3ZpZGVyIiwibW9kdWxlIiwiZXhwb3J0cyIsImdldEFjY2Vzc1Rva2VuIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsIlBpZUNoYXJ0IiwiY2hhcnRUeXBlIiwicmF3RGF0YSIsImxhYmVsS2V5IiwiYW1vdW50S2V5IiwibGFiZWwiLCJzZXRMYWJlbCIsInVzZVN0YXRlIiwiYW1vdW50Iiwic2V0QW1vdW50IiwiYmdDb2xvcnMiLCJzZXRCZ0NvbG9ycyIsInVzZUVmZmVjdCIsIm1hcCIsImVsZW1lbnQiLCJNYXRoIiwiYWJzIiwiY29sb3JzIiwiY29sb3IiLCJDb2xvclJhbmRvbWl6ZXIiLCJsYWJlbHMiLCJsZWdlbmQiLCJwb3NpdGlvbiIsImRhdGFzZXRzIiwiZGF0YSIsImJhY2tncm91bmRDb2xvciIsImhvdmVyQmFja2dyb3VuZCIsIlZpZXciLCJ0aXRsZSIsImNoaWxkcmVuIiwiZmxvb3IiLCJyYW5kb20iLCJ0b1N0cmluZyIsInN1bUJ5R3JvdXAiLCJsYWJlbEFycmF5IiwiYXJyYXkiLCJrZXkiLCJrZXlTdW0iLCJzdW1Db3VudHMiLCJyZWR1Y2UiLCJyZXMiLCJ2YWx1ZSIsImNvdW50IiwicHVzaCIsImxhYmVsU3VtQ291bnRzIiwiZm91bmRTdW1Db3VudCIsImZpbmQiLCJzdW1Db3VudCIsInVuZGVmaW5lZCIsImNhdGVnb3J5IiwiY2F0ZWdvcmllcyIsInNldENhdGVnb3JpZXMiLCJjdXJyZW50UmVjb3JkcyIsInNldEN1clJlYyIsImluY29tZUNhdGVnb3JpZXMiLCJzZXRJbmNDYXQiLCJleHBlbnNlc0NhdGVnb3JpZXMiLCJzZXRFeHBDYXQiLCJpbmNvbWVSb3dzIiwic2V0SW5jb21lUm93cyIsImV4cGVuc2VzUm93cyIsInNldEV4cGVuc2VzUm93cyIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJ0aGVuIiwianNvbiIsImRhdGVCZWZvcmUiLCJtb21lbnQiLCJjdXJyZW50RGF0ZSIsInN0YXJ0RGF0ZSIsInN1YnRyYWN0IiwiZGF0ZUFmdGVyIiwiZW5kRGF0ZSIsImFkZCIsImZpbHRlcmVkUmVjb3JkcyIsInJlY29yZHMiLCJmaWx0ZXIiLCJyZWNvcmQiLCJ1cGRhdGVkT24iLCJzdGFydE9mIiwiaXNCZXR3ZWVuIiwiY2F0ZWdvcnlSZWNvcmRzIiwidHlwZSIsIm5hbWUiLCJfaWQiLCJidWRnZXQiLCJSb3V0ZXIiLCJsZW5ndGgiLCJIb21lIiwidXNlciIsInNldFVzZXIiLCJ1c2VDb250ZXh0IiwibG9hZGluZyIsInNldExvYWRpbmciLCJzZXROYW1lIiwiZW1haWwiLCJzZXRFbWFpbCIsInNldFJlY29yZHMiLCJzZXRTdGFydERhdGUiLCJzZXRFbmREYXRlIiwic2F2ZWRTdGFydCIsInNldFNhdmVkU3RhcnQiLCJzYXZlZEVuZCIsInNldFNhdmVkRW5kIiwic2V0Q3VycmVudFJlY29yZHMiLCJvbGRSZWNvcmRzIiwic2V0T2xkUmVjb3JkcyIsInByZXZpb3VzU2F2aW5ncyIsInNldFByZXZpb3VzU2F2aW5ncyIsImN1cnJlbnRJbmNvbWUiLCJzZXRDdXJyZW50SW5jb21lIiwiY3VycmVudEV4cGVuc2VzIiwic2V0Q3VycmVudEV4cGVuc2VzIiwic2V0QnVkZ2V0IiwiYWN0dWFsUGllIiwic2V0QWN0dWFsUGllIiwiYnVkZ2V0UGllIiwic2V0QnVkZ2V0UGllIiwiaWQiLCJjb25zb2xlIiwibG9nIiwiZm9ybWF0IiwiZmlyc3ROYW1lIiwibGFzdE5hbWUiLCJpc0JlZm9yZSIsInRvdGFsIiwiaW5jb21lIiwiZXhwZW5zZXMiLCJzYXZlRGF0ZSIsImUiLCJwcmV2ZW50RGVmYXVsdCIsIm1ldGhvZCIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiU3dhbCIsImZpcmUiLCJ0YXJnZXQiLCJsb2dpbiIsIkxvZ2luRm9ybSIsInBhc3N3b3JkIiwic2V0UGFzc3dvcmQiLCJpc0FjdGl2ZSIsInNldElzQWN0aXZlIiwiYXV0aGVudGljYXRlIiwib3B0aW9ucyIsImFjY2Vzc1Rva2VuIiwic2V0SXRlbSIsImVycm9yIiwiYXVudGhlbnRpY2F0ZUdvb2dsZVRva2VuIiwicmVzcG9uc2UiLCJwYXlsb2FkIiwidG9rZW5JZCJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUFBO0FBQUE7QUFBQTtDQUVBOztBQUNBLE1BQU1BLFdBQVcsZ0JBQUdDLDRDQUFLLENBQUNDLGFBQU4sRUFBcEI7QUFFTyxNQUFNQyxZQUFZLEdBQUdILFdBQVcsQ0FBQ0ksUUFBakM7QUFFUUosMEVBQWYsRTs7Ozs7Ozs7Ozs7QUNQQUssTUFBTSxDQUFDQyxPQUFQLEdBQWlCO0FBQ2hCQyxnQkFBYyxFQUFFLE1BQU1DLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQjtBQUROLENBQWpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBRWUsU0FBU0MsUUFBVCxDQUFrQjtBQUFFQyxXQUFGO0FBQWFDLFNBQWI7QUFBc0JDLFVBQXRCO0FBQWdDQztBQUFoQyxDQUFsQixFQUErRDtBQUM1RSxRQUFNO0FBQUEsT0FBQ0MsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0JDLHNEQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDQyxNQUFEO0FBQUEsT0FBU0M7QUFBVCxNQUFzQkYsc0RBQVEsQ0FBQyxFQUFELENBQXBDO0FBQ0EsUUFBTTtBQUFBLE9BQUNHLFFBQUQ7QUFBQSxPQUFXQztBQUFYLE1BQTBCSixzREFBUSxDQUFDLEVBQUQsQ0FBeEM7QUFFQUsseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSVYsT0FBSixFQUFhO0FBQ1hJLGNBQVEsQ0FBQ0osT0FBTyxDQUFDVyxHQUFSLENBQWFDLE9BQUQsSUFBYUEsT0FBTyxDQUFDWCxRQUFELENBQWhDLENBQUQsQ0FBUjtBQUNBTSxlQUFTLENBQUNQLE9BQU8sQ0FBQ1csR0FBUixDQUFhQyxPQUFELElBQWFDLElBQUksQ0FBQ0MsR0FBTCxDQUFTRixPQUFPLENBQUNWLFNBQUQsQ0FBaEIsQ0FBekIsQ0FBRCxDQUFUO0FBRUEsVUFBSWEsTUFBTSxHQUFHZixPQUFPLENBQUNXLEdBQVIsQ0FBYUMsT0FBRCxJQUFhO0FBQ3BDLFlBQUlBLE9BQU8sQ0FBQ0ksS0FBWixFQUFtQjtBQUNqQixpQkFBT0osT0FBTyxDQUFDSSxLQUFmO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsaUJBQVEsSUFBR0Msd0VBQWUsRUFBRyxFQUE3QjtBQUNEO0FBQ0YsT0FOWSxDQUFiO0FBUUFSLGlCQUFXLENBQUNNLE1BQUQsQ0FBWDtBQUNELEtBYkQsTUFhTztBQUNMWCxjQUFRLENBQUMsRUFBRCxDQUFSO0FBQ0FHLGVBQVMsQ0FBQyxFQUFELENBQVQ7QUFDQUUsaUJBQVcsQ0FBQyxFQUFELENBQVg7QUFDRDtBQUNGLEdBbkJRLEVBbUJOLENBQUNULE9BQUQsQ0FuQk0sQ0FBVDs7QUFxQkEsTUFBSUQsU0FBUyxJQUFJLEtBQWpCLEVBQXdCO0FBQ3RCLHdCQUNFLHFFQUFDLG1EQUFEO0FBQ0UsVUFBSSxFQUFFO0FBQ0ptQixjQUFNLEVBQUVmLEtBREo7QUFFSmdCLGNBQU0sRUFBRTtBQUNOQyxrQkFBUSxFQUFFO0FBREosU0FGSjtBQUtKQyxnQkFBUSxFQUFFLENBQ1I7QUFDRUMsY0FBSSxFQUFFaEIsTUFEUjtBQUVFaUIseUJBQWUsRUFBRWYsUUFGbkI7QUFHRWdCLHlCQUFlLEVBQUVoQjtBQUhuQixTQURRO0FBTE4sT0FEUjtBQWNFLFlBQU0sRUFBRTtBQWRWO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERjtBQWtCRCxHQW5CRCxNQW1CTztBQUNMLHdCQUNFLHFFQUFDLHdEQUFEO0FBQ0UsVUFBSSxFQUFFO0FBQ0pVLGNBQU0sRUFBRWYsS0FESjtBQUVKZ0IsY0FBTSxFQUFFO0FBQ05DLGtCQUFRLEVBQUU7QUFESixTQUZKO0FBS0pDLGdCQUFRLEVBQUUsQ0FDUjtBQUNFQyxjQUFJLEVBQUVoQixNQURSO0FBRUVpQix5QkFBZSxFQUFFZixRQUZuQjtBQUdFZ0IseUJBQWUsRUFBRWhCO0FBSG5CLFNBRFE7QUFMTjtBQURSO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERjtBQWlCRDtBQUNGLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEVEO0FBQ0E7Q0FHQTs7QUFDQSxNQUFNaUIsSUFBSSxHQUFHLENBQUM7QUFBRUMsT0FBRjtBQUFTQztBQUFULENBQUQsS0FBeUI7QUFDcEMsc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw4QkFDRTtBQUFBLGtCQUF3QkQ7QUFBeEIsU0FBVyxXQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUVFLFlBQUksRUFBQyxVQUZQO0FBR0UsZUFBTyxFQUFDO0FBSFYsU0FDTSxZQUROO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBU0UscUVBQUMseURBQUQ7QUFBVyxlQUFTLEVBQUMsZ0JBQXJCO0FBQUEsZ0JBQXVDQztBQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBYUQsQ0FkRDs7QUFnQmVGLG1FQUFmLEU7Ozs7Ozs7Ozs7OztBQ3JCQTtBQUFBO0FBQWUsU0FBVVIsZUFBVixHQUE0QjtBQUMxQyxTQUFPSixJQUFJLENBQUNlLEtBQUwsQ0FBV2YsSUFBSSxDQUFDZ0IsTUFBTCxLQUFjLFFBQXpCLEVBQW1DQyxRQUFuQyxDQUE0QyxFQUE1QyxDQUFQO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDRkQ7QUFBQTtBQUFlLFNBQVNDLFVBQVQsQ0FBb0JDLFVBQXBCLEVBQStCL0IsUUFBL0IsRUFBd0NnQyxLQUF4QyxFQUE4Q0MsR0FBOUMsRUFBa0RDLE1BQWxELEVBQXlEO0FBR3ZFLE1BQUlDLFNBQVMsR0FBQyxFQUFkO0FBRUdILE9BQUssQ0FBQ0ksTUFBTixDQUFhLFVBQVNDLEdBQVQsRUFBY0MsS0FBZCxFQUFxQjtBQUNwQyxRQUFJLENBQUNELEdBQUcsQ0FBQ0MsS0FBSyxDQUFFTCxHQUFGLENBQU4sQ0FBUixFQUF1QjtBQUN0QkksU0FBRyxDQUFDQyxLQUFLLENBQUVMLEdBQUYsQ0FBTixDQUFILEdBQW1CO0FBQUUsU0FBQ0EsR0FBRCxHQUFPSyxLQUFLLENBQUVMLEdBQUYsQ0FBZDtBQUFzQixTQUFDQyxNQUFELEdBQVUsQ0FBaEM7QUFBbUNLLGFBQUssRUFBQztBQUF6QyxPQUFuQjtBQUNBSixlQUFTLENBQUNLLElBQVYsQ0FBZUgsR0FBRyxDQUFDQyxLQUFLLENBQUVMLEdBQUYsQ0FBTixDQUFsQjtBQUNBOztBQUVESSxPQUFHLENBQUNDLEtBQUssQ0FBRUwsR0FBRixDQUFOLENBQUgsQ0FBa0JDLE1BQWxCLEtBQTZCSSxLQUFLLENBQUVKLE1BQUYsQ0FBbEM7QUFDQUcsT0FBRyxDQUFDQyxLQUFLLENBQUVMLEdBQUYsQ0FBTixDQUFILENBQWlCTSxLQUFqQixJQUEwQixDQUExQjtBQUNBLFdBQU9GLEdBQVA7QUFDQSxHQVRFLEVBU0EsRUFUQTs7QUFhSCxNQUFHTixVQUFVLElBQUUsRUFBWixHQUFpQi9CLFFBQVEsSUFBRyxFQUEvQixFQUFrQztBQUUzQixRQUFJeUMsY0FBYyxHQUFHVixVQUFVLENBQUNyQixHQUFYLENBQWVSLEtBQUssSUFBRTtBQUV2QyxVQUFJd0MsYUFBYSxHQUFHUCxTQUFTLENBQUNRLElBQVYsQ0FBZUMsUUFBUSxJQUFJQSxRQUFRLENBQUVYLEdBQUYsQ0FBUixJQUFpQi9CLEtBQUssQ0FBRUYsUUFBRixDQUFqRCxDQUFwQjs7QUFFQSxVQUFHMEMsYUFBYSxJQUFJRyxTQUFwQixFQUE4QjtBQUMxQjNDLGFBQUssQ0FBRWdDLE1BQUYsQ0FBTCxHQUFpQixDQUFqQjtBQUNBaEMsYUFBSyxDQUFDcUMsS0FBTixHQUFjLENBQWQ7QUFDSCxPQUhELE1BR0s7QUFDRHJDLGFBQUssQ0FBRWdDLE1BQUYsQ0FBTCxHQUFpQlEsYUFBYSxDQUFFUixNQUFGLENBQTlCO0FBQ0FoQyxhQUFLLENBQUNxQyxLQUFOLEdBQWNHLGFBQWEsQ0FBQ0gsS0FBNUI7QUFDSDs7QUFFRCxhQUFPckMsS0FBUDtBQUNILEtBYm9CLENBQXJCO0FBZUEsV0FBT3VDLGNBQVA7QUFDTixHQWxCRCxNQWtCSztBQUNKLFdBQU9OLFNBQVA7QUFDQTtBQUdEO0FBQUEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNXLFFBQVQsR0FBb0I7QUFDakMsUUFBTTtBQUFBLE9BQUNDLFVBQUQ7QUFBQSxPQUFhQztBQUFiLE1BQThCNUMsc0RBQVEsQ0FBQyxFQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUM2QyxjQUFEO0FBQUEsT0FBaUJDO0FBQWpCLE1BQThCOUMsc0RBQVEsQ0FBQyxFQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUMrQyxnQkFBRDtBQUFBLE9BQW1CQztBQUFuQixNQUFnQ2hELHNEQUFRLENBQUMsRUFBRCxDQUE5QztBQUNBLFFBQU07QUFBQSxPQUFDaUQsa0JBQUQ7QUFBQSxPQUFxQkM7QUFBckIsTUFBa0NsRCxzREFBUSxDQUFDLEVBQUQsQ0FBaEQ7QUFDQSxRQUFNO0FBQUEsT0FBQ21ELFVBQUQ7QUFBQSxPQUFhQztBQUFiLE1BQThCcEQsc0RBQVEsQ0FBQyxJQUFELENBQTVDO0FBQ0EsUUFBTTtBQUFBLE9BQUNxRCxZQUFEO0FBQUEsT0FBZUM7QUFBZixNQUFrQ3RELHNEQUFRLENBQUMsSUFBRCxDQUFoRDtBQUVBSyx5REFBUyxDQUFDLE1BQU07QUFDZGtELFNBQUssQ0FBRSx5Q0FBRixFQUE0QztBQUMvQ0MsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU2xFLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUE4QjtBQURoRDtBQURzQyxLQUE1QyxDQUFMLENBS0drRSxJQUxILENBS1N6QixHQUFELElBQVNBLEdBQUcsQ0FBQzBCLElBQUosRUFMakIsRUFNR0QsSUFOSCxDQU1TekMsSUFBRCxJQUFVO0FBQ2QyQixtQkFBYSxDQUFDM0IsSUFBSSxDQUFDMEIsVUFBTixDQUFiO0FBRUEsVUFBSWlCLFVBQVUsR0FBR0MsNkNBQU0sQ0FBQzVDLElBQUksQ0FBQzZDLFdBQUwsQ0FBaUJDLFNBQWxCLENBQU4sQ0FBbUNDLFFBQW5DLENBQTRDLENBQTVDLEVBQStDLEtBQS9DLENBQWpCO0FBQ0EsVUFBSUMsU0FBUyxHQUFHSiw2Q0FBTSxDQUFDNUMsSUFBSSxDQUFDNkMsV0FBTCxDQUFpQkksT0FBbEIsQ0FBTixDQUFpQ0MsR0FBakMsQ0FBcUMsQ0FBckMsRUFBd0MsS0FBeEMsQ0FBaEI7QUFFQSxVQUFJQyxlQUFlLEdBQUduRCxJQUFJLENBQUNvRCxPQUFMLENBQWFDLE1BQWIsQ0FBcUJDLE1BQUQsSUFBWTtBQUNwRCxZQUFJQyxTQUFTLEdBQUdYLDZDQUFNLENBQUNVLE1BQU0sQ0FBQ0MsU0FBUixDQUFOLENBQXlCQyxPQUF6QixDQUFpQyxNQUFqQyxDQUFoQjtBQUNBLGVBQU9aLDZDQUFNLENBQUNXLFNBQUQsQ0FBTixDQUFrQkUsU0FBbEIsQ0FBNEJkLFVBQTVCLEVBQXdDSyxTQUF4QyxFQUFtRCxLQUFuRCxDQUFQO0FBQ0QsT0FIcUIsQ0FBdEI7QUFLQW5CLGVBQVMsQ0FBQ3NCLGVBQUQsQ0FBVDtBQUNELEtBbEJIO0FBbUJELEdBcEJRLEVBb0JOLEVBcEJNLENBQVQ7QUFzQkEvRCx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJc0UsZUFBZSxHQUFHakQsbUVBQVUsQ0FDOUJpQixVQUQ4QixFQUU5QixLQUY4QixFQUc5QkUsY0FIOEIsRUFJOUIsWUFKOEIsRUFLOUIsUUFMOEIsQ0FBaEM7QUFRQUcsYUFBUyxDQUFDMkIsZUFBZSxDQUFDTCxNQUFoQixDQUF3QjVCLFFBQUQsSUFBY0EsUUFBUSxDQUFDa0MsSUFBVCxJQUFpQixRQUF0RCxDQUFELENBQVQ7QUFDQTFCLGFBQVMsQ0FBQ3lCLGVBQWUsQ0FBQ0wsTUFBaEIsQ0FBd0I1QixRQUFELElBQWNBLFFBQVEsQ0FBQ2tDLElBQVQsSUFBaUIsU0FBdEQsQ0FBRCxDQUFUO0FBQ0QsR0FYUSxFQVdOLENBQUNqQyxVQUFELEVBQWFFLGNBQWIsQ0FYTSxDQUFUO0FBYUF4Qyx5REFBUyxDQUFDLE1BQU07QUFDZCtDLGlCQUFhLENBQ1hMLGdCQUFnQixDQUFDekMsR0FBakIsQ0FBc0JvQyxRQUFELElBQWM7QUFDakMsMEJBQ0U7QUFBQSxnQ0FDRTtBQUFBLG9CQUFLQSxRQUFRLENBQUNtQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRTtBQUFBLG9CQUFLbkMsUUFBUSxDQUFDekM7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUEsU0FBU3lDLFFBQVEsQ0FBQ29DLEdBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERjtBQU1ELEtBUEQsQ0FEVyxDQUFiO0FBVUQsR0FYUSxFQVdOLENBQUMvQixnQkFBRCxDQVhNLENBQVQ7QUFhQTFDLHlEQUFTLENBQUMsTUFBTTtBQUNkaUQsbUJBQWUsQ0FDYkwsa0JBQWtCLENBQUMzQyxHQUFuQixDQUF3Qm9DLFFBQUQsSUFBYztBQUNuQywwQkFDRTtBQUFBLGdDQUNFO0FBQUEsb0JBQUtBLFFBQVEsQ0FBQ21DO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFO0FBQUEsb0JBQUtuQyxRQUFRLENBQUNxQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkYsZUFHRTtBQUFBLG9CQUFLdkUsSUFBSSxDQUFDQyxHQUFMLENBQVNpQyxRQUFRLENBQUN6QyxNQUFsQjtBQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSEYsZUFJRTtBQUFBLG9CQUFLeUMsUUFBUSxDQUFDekMsTUFBVCxHQUFrQnlDLFFBQVEsQ0FBQ3FDO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSkY7QUFBQSxTQUFTckMsUUFBUSxDQUFDb0MsR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGO0FBUUQsS0FURCxDQURhLENBQWY7QUFZRCxHQWJRLEVBYU4sQ0FBQzdCLGtCQUFELENBYk0sQ0FBVDtBQWVBLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyw2QkFBZjtBQUFBLDhCQUNFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsU0FIVjtBQUlFLGVBQU8sRUFBRSxNQUFNK0Isa0RBQU0sQ0FBQzVDLElBQVAsQ0FBWSxnQkFBWixDQUpqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBU0UscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxXQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU00QyxrREFBTSxDQUFDNUMsSUFBUCxDQUFZLGlCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBVEYsZUFpQkUscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxRQUhWO0FBSUUsZUFBTyxFQUFFLE1BQU00QyxrREFBTSxDQUFDNUMsSUFBUCxDQUFZLGdCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakJGLGVBMEJFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsTUFIVjtBQUlFLGVBQU8sRUFBRSxNQUFNNEMsa0RBQU0sQ0FBQzVDLElBQVAsQ0FBWSxlQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBMUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUpGLGVBdUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBdkNGLEVBd0NHVyxnQkFBZ0IsQ0FBQ2tDLE1BQWpCLElBQTJCLENBQTNCLGdCQUNDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxFQUFDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkFHQyxxRUFBQyxxREFBRDtBQUFPLGFBQU8sTUFBZDtBQUFlLGNBQVEsTUFBdkI7QUFBd0IsV0FBSyxNQUE3QjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBQSxrQ0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVFFO0FBQUEsa0JBQVE5QjtBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUEzQ0osZUFzREU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF0REYsRUF1REdGLGtCQUFrQixDQUFDZ0MsTUFBbkIsSUFBNkIsQ0FBN0IsZ0JBQ0MscUVBQUMscURBQUQ7QUFBTyxhQUFPLEVBQUMsTUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURELGdCQUdDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxNQUFkO0FBQWUsY0FBUSxNQUF2QjtBQUF3QixXQUFLLE1BQTdCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRixlQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVVFO0FBQUEsa0JBQVE1QjtBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUExREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEwRUQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pKRDtBQUNBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVM2QixJQUFULEdBQWdCO0FBQzdCLFFBQU07QUFBRUMsUUFBRjtBQUFRQztBQUFSLE1BQW9CQyx3REFBVSxDQUFDdEcsb0RBQUQsQ0FBcEM7QUFDQSxRQUFNO0FBQUEsT0FBQ3VHLE9BQUQ7QUFBQSxPQUFVQztBQUFWLE1BQXdCdkYsc0RBQVEsQ0FBQyxJQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUM2RSxJQUFEO0FBQUEsT0FBT1c7QUFBUCxNQUFrQnhGLHNEQUFRLENBQUMsRUFBRCxDQUFoQztBQUNBLFFBQU07QUFBQSxPQUFDeUYsS0FBRDtBQUFBLE9BQVFDO0FBQVIsTUFBb0IxRixzREFBUSxDQUFDLEVBQUQsQ0FBbEM7QUFDQSxRQUFNO0FBQUEsT0FBQ3FFLE9BQUQ7QUFBQSxPQUFVc0I7QUFBVixNQUF3QjNGLHNEQUFRLENBQUMsRUFBRCxDQUF0QztBQUNBLFFBQU07QUFBQSxPQUFDMkMsVUFBRDtBQUFBLE9BQWFDO0FBQWIsTUFBOEI1QyxzREFBUSxDQUFDLEVBQUQsQ0FBNUM7QUFDQSxRQUFNO0FBQUEsT0FBQytELFNBQUQ7QUFBQSxPQUFZNkI7QUFBWixNQUE0QjVGLHNEQUFRLENBQUMsRUFBRCxDQUExQztBQUNBLFFBQU07QUFBQSxPQUFDa0UsT0FBRDtBQUFBLE9BQVUyQjtBQUFWLE1BQXdCN0Ysc0RBQVEsQ0FBQyxFQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUM4RixVQUFEO0FBQUEsT0FBYUM7QUFBYixNQUE4Qi9GLHNEQUFRLENBQUMsRUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDZ0csUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJqRyxzREFBUSxDQUFDLEVBQUQsQ0FBeEM7QUFDQSxRQUFNO0FBQUEsT0FBQzZDLGNBQUQ7QUFBQSxPQUFpQnFEO0FBQWpCLE1BQXNDbEcsc0RBQVEsQ0FBQyxFQUFELENBQXBEO0FBQ0EsUUFBTTtBQUFBLE9BQUNtRyxVQUFEO0FBQUEsT0FBYUM7QUFBYixNQUE4QnBHLHNEQUFRLENBQUMsRUFBRCxDQUE1QztBQUNBLFFBQU07QUFBQSxPQUFDcUcsZUFBRDtBQUFBLE9BQWtCQztBQUFsQixNQUF3Q3RHLHNEQUFRLENBQUMsQ0FBRCxDQUF0RDtBQUNBLFFBQU07QUFBQSxPQUFDdUcsYUFBRDtBQUFBLE9BQWdCQztBQUFoQixNQUFvQ3hHLHNEQUFRLENBQUMsQ0FBRCxDQUFsRDtBQUNBLFFBQU07QUFBQSxPQUFDeUcsZUFBRDtBQUFBLE9BQWtCQztBQUFsQixNQUF3QzFHLHNEQUFRLENBQUMsQ0FBRCxDQUF0RDtBQUNBLFFBQU07QUFBQSxPQUFDK0UsTUFBRDtBQUFBLE9BQVM0QjtBQUFULE1BQXNCM0csc0RBQVEsQ0FBQyxDQUFELENBQXBDO0FBQ0EsUUFBTTtBQUFBLE9BQUM0RyxTQUFEO0FBQUEsT0FBWUM7QUFBWixNQUE0QjdHLHNEQUFRLENBQUMsRUFBRCxDQUExQztBQUNBLFFBQU07QUFBQSxPQUFDOEcsU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBNEIvRyxzREFBUSxDQUFDLEVBQUQsQ0FBMUM7QUFFQUsseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSThFLElBQUksQ0FBQzZCLEVBQUwsSUFBVyxJQUFmLEVBQXFCO0FBQ25CekQsV0FBSyxDQUFFLHlDQUFGLEVBQTRDO0FBQy9DQyxlQUFPLEVBQUU7QUFDUEMsdUJBQWEsRUFBRyxVQUFTbEUsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRHNDLE9BQTVDLENBQUwsQ0FLR2tFLElBTEgsQ0FLU3pCLEdBQUQsSUFBU0EsR0FBRyxDQUFDMEIsSUFBSixFQUxqQixFQU1HRCxJQU5ILENBTVN6QyxJQUFELElBQVU7QUFDZGdHLGVBQU8sQ0FBQ0MsR0FBUixDQUFZakcsSUFBWjtBQUNBMkUsb0JBQVksQ0FDVi9CLDZDQUFNLENBQUM1QyxJQUFJLENBQUM2QyxXQUFMLENBQWlCQyxTQUFsQixDQUFOLENBQ0dVLE9BREgsQ0FDVyxNQURYLEVBRUcwQyxNQUZILENBRVUsWUFGVixDQURVLENBQVo7QUFLQXRCLGtCQUFVLENBQ1JoQyw2Q0FBTSxDQUFDNUMsSUFBSSxDQUFDNkMsV0FBTCxDQUFpQkksT0FBbEIsQ0FBTixDQUNHTyxPQURILENBQ1csTUFEWCxFQUVHMEMsTUFGSCxDQUVVLFlBRlYsQ0FEUSxDQUFWO0FBS0FwQixxQkFBYSxDQUNYbEMsNkNBQU0sQ0FBQzVDLElBQUksQ0FBQzZDLFdBQUwsQ0FBaUJDLFNBQWxCLENBQU4sQ0FDR1UsT0FESCxDQUNXLE1BRFgsRUFFRzBDLE1BRkgsQ0FFVSxZQUZWLENBRFcsQ0FBYjtBQUtBbEIsbUJBQVcsQ0FDVHBDLDZDQUFNLENBQUM1QyxJQUFJLENBQUM2QyxXQUFMLENBQWlCSSxPQUFsQixDQUFOLENBQ0dPLE9BREgsQ0FDVyxNQURYLEVBRUcwQyxNQUZILENBRVUsWUFGVixDQURTLENBQVg7QUFLQTNCLGVBQU8sQ0FBRSxHQUFFdkUsSUFBSSxDQUFDbUcsU0FBVSxJQUFHbkcsSUFBSSxDQUFDb0csUUFBUyxFQUFwQyxDQUFQO0FBQ0EzQixnQkFBUSxDQUFDekUsSUFBSSxDQUFDd0UsS0FBTixDQUFSO0FBQ0FFLGtCQUFVLENBQUMxRSxJQUFJLENBQUNvRCxPQUFOLENBQVY7QUFDQXpCLHFCQUFhLENBQUMzQixJQUFJLENBQUMwQixVQUFOLENBQWI7QUFDRCxPQWhDSDtBQWlDRDtBQUNGLEdBcENRLEVBb0NOLENBQUN3QyxJQUFELENBcENNLENBQVQ7QUFzQ0E5RSx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFLd0UsSUFBSSxJQUFJLEVBQVQsR0FBZ0JZLEtBQUssSUFBSSxFQUE3QixFQUFrQztBQUNoQ0YsZ0JBQVUsQ0FBQyxLQUFELENBQVY7QUFDRDtBQUNGLEdBSlEsRUFJTixDQUFDVixJQUFELEVBQU9ZLEtBQVAsQ0FKTSxDQUFUO0FBTUFwRix5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJdUQsVUFBVSxHQUFHQyw2Q0FBTSxDQUFDRSxTQUFELENBQU4sQ0FBa0JDLFFBQWxCLENBQTJCLENBQTNCLEVBQThCLEtBQTlCLENBQWpCO0FBQ0EsUUFBSUMsU0FBUyxHQUFHSiw2Q0FBTSxDQUFDSyxPQUFELENBQU4sQ0FBZ0JDLEdBQWhCLENBQW9CLENBQXBCLEVBQXVCLEtBQXZCLENBQWhCOztBQUVBLFFBQUlKLFNBQVMsSUFBSUcsT0FBakIsRUFBMEI7QUFDeEIsVUFBSUcsT0FBTyxDQUFDWSxNQUFSLEdBQWlCLENBQXJCLEVBQXdCO0FBQ3RCaUIseUJBQWlCLENBQ2Y3QixPQUFPLENBQUNDLE1BQVIsQ0FBZ0JDLE1BQUQsSUFBWTtBQUN6QixjQUFJQyxTQUFTLEdBQUdYLDZDQUFNLENBQUNVLE1BQU0sQ0FBQ0MsU0FBUixDQUFOLENBQXlCQyxPQUF6QixDQUFpQyxNQUFqQyxDQUFoQjtBQUNBLGlCQUFPWiw2Q0FBTSxDQUFDVyxTQUFELENBQU4sQ0FBa0JFLFNBQWxCLENBQTRCZCxVQUE1QixFQUF3Q0ssU0FBeEMsRUFBbUQsS0FBbkQsQ0FBUDtBQUNELFNBSEQsQ0FEZSxDQUFqQjtBQU9BbUMscUJBQWEsQ0FDWC9CLE9BQU8sQ0FBQ0MsTUFBUixDQUFnQkMsTUFBRCxJQUFZO0FBQ3pCLGNBQUlDLFNBQVMsR0FBR1gsNkNBQU0sQ0FBQ1UsTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJDLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsaUJBQU9aLDZDQUFNLENBQUNXLFNBQUQsQ0FBTixDQUFrQjhDLFFBQWxCLENBQTJCMUQsVUFBM0IsRUFBdUMsS0FBdkMsQ0FBUDtBQUNELFNBSEQsQ0FEVyxDQUFiO0FBTUQ7QUFDRixLQWhCRCxNQWdCTztBQUNMc0MsdUJBQWlCLENBQUMsRUFBRCxDQUFqQjtBQUNBRSxtQkFBYSxDQUFDLEVBQUQsQ0FBYjtBQUNEOztBQUVELFFBQUl6RCxVQUFVLENBQUNzQyxNQUFYLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3pCMEIsZUFBUyxDQUNQaEUsVUFBVSxDQUFDWCxNQUFYLENBQWtCLENBQUN1RixLQUFELEVBQVE3RSxRQUFSLEtBQXFCO0FBQ3JDLGVBQU82RSxLQUFLLEdBQUc3RSxRQUFRLENBQUNxQyxNQUF4QjtBQUNELE9BRkQsRUFFRyxDQUZILENBRE8sQ0FBVDtBQUtELEtBTkQsTUFNTztBQUNMNEIsZUFBUyxDQUFDLENBQUQsQ0FBVDtBQUNEO0FBQ0YsR0FsQ1EsRUFrQ04sQ0FBQ2hFLFVBQUQsRUFBYW9CLFNBQWIsRUFBd0JHLE9BQXhCLENBbENNLENBQVQ7QUFvQ0E3RCx5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJOEYsVUFBVSxDQUFDbEIsTUFBWCxHQUFvQixDQUF4QixFQUEyQjtBQUN6QnFCLHdCQUFrQixDQUNoQkgsVUFBVSxDQUFDbkUsTUFBWCxDQUFrQixDQUFDdUYsS0FBRCxFQUFRaEQsTUFBUixLQUFtQjtBQUNuQyxlQUFPZ0QsS0FBSyxHQUFHaEQsTUFBTSxDQUFDdEUsTUFBdEI7QUFDRCxPQUZELEVBRUcsQ0FGSCxDQURnQixDQUFsQjtBQUtELEtBTkQsTUFNTztBQUNMcUcsd0JBQWtCLENBQUMsQ0FBRCxDQUFsQjtBQUNEO0FBQ0YsR0FWUSxFQVVOLENBQUNILFVBQUQsQ0FWTSxDQUFUO0FBWUE5Rix5REFBUyxDQUFDLE1BQU07QUFDZCxRQUFJd0MsY0FBYyxDQUFDb0MsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QixVQUFJdUMsTUFBTSxHQUFHM0UsY0FBYyxDQUFDeUIsTUFBZixDQUF1QkMsTUFBRCxJQUFZQSxNQUFNLENBQUN0RSxNQUFQLEdBQWdCLENBQWxELENBQWI7QUFDQWdILGFBQU8sQ0FBQ0MsR0FBUixDQUFZTSxNQUFaOztBQUNBLFVBQUlBLE1BQU0sQ0FBQ3ZDLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7QUFDckJ1Qix3QkFBZ0IsQ0FDZGdCLE1BQU0sQ0FBQ3hGLE1BQVAsQ0FBYyxDQUFDdUYsS0FBRCxFQUFRaEQsTUFBUixLQUFtQjtBQUMvQixpQkFBT2dELEtBQUssR0FBR2hELE1BQU0sQ0FBQ3RFLE1BQXRCO0FBQ0QsU0FGRCxFQUVHLENBRkgsQ0FEYyxDQUFoQjtBQUtELE9BTkQsTUFNTztBQUNMdUcsd0JBQWdCLENBQUMsQ0FBRCxDQUFoQjtBQUNEOztBQUVELFVBQUlpQixRQUFRLEdBQUc1RSxjQUFjLENBQUN5QixNQUFmLENBQXVCQyxNQUFELElBQVlBLE1BQU0sQ0FBQ3RFLE1BQVAsR0FBZ0IsQ0FBbEQsQ0FBZjs7QUFDQSxVQUFJd0gsUUFBUSxDQUFDeEMsTUFBVCxHQUFrQixDQUF0QixFQUF5QjtBQUN2QnlCLDBCQUFrQixDQUNoQmUsUUFBUSxDQUFDekYsTUFBVCxDQUFnQixDQUFDdUYsS0FBRCxFQUFRaEQsTUFBUixLQUFtQjtBQUNqQyxpQkFBT2dELEtBQUssR0FBR2hELE1BQU0sQ0FBQ3RFLE1BQXRCO0FBQ0QsU0FGRCxFQUVHLENBRkgsQ0FEZ0IsQ0FBbEI7QUFLRCxPQU5ELE1BTU87QUFDTHlHLDBCQUFrQixDQUFDLENBQUQsQ0FBbEI7QUFDRDtBQUNGLEtBdkJELE1BdUJPO0FBQ0xGLHNCQUFnQixDQUFDLENBQUQsQ0FBaEI7QUFDQUUsd0JBQWtCLENBQUMsQ0FBRCxDQUFsQjtBQUNEO0FBQ0YsR0E1QlEsRUE0Qk4sQ0FBQzdELGNBQUQsQ0E1Qk0sQ0FBVDtBQThCQXhDLHlEQUFTLENBQUMsTUFBTTtBQUNkNEcsV0FBTyxDQUFDQyxHQUFSLENBQVliLGVBQVo7QUFDQVksV0FBTyxDQUFDQyxHQUFSLENBQVlYLGFBQVo7QUFDQVUsV0FBTyxDQUFDQyxHQUFSLENBQVlULGVBQVo7O0FBQ0EsUUFBSUosZUFBZSxJQUFJLENBQXZCLEVBQTBCO0FBQ3hCLFVBQUlFLGFBQWEsR0FBR0UsZUFBcEIsRUFBcUM7QUFDbkNJLG9CQUFZLENBQUMsQ0FDWDtBQUNFL0csZUFBSyxFQUFFLGlCQURUO0FBRUVHLGdCQUFNLEVBQUVzRyxhQUFhLEdBQUdFLGVBRjFCO0FBR0U5RixlQUFLLEVBQUU7QUFIVCxTQURXLEVBTVg7QUFDRWIsZUFBSyxFQUFFLGtCQURUO0FBRUVHLGdCQUFNLEVBQUV3RyxlQUZWO0FBR0U5RixlQUFLLEVBQUU7QUFIVCxTQU5XLEVBV1g7QUFDRWIsZUFBSyxFQUFFLGtCQURUO0FBRUVHLGdCQUFNLEVBQUVvRyxlQUZWO0FBR0UxRixlQUFLLEVBQUU7QUFIVCxTQVhXLENBQUQsQ0FBWjtBQWlCRCxPQWxCRCxNQWtCTztBQUNMLFlBQUk4RixlQUFlLEdBQUdGLGFBQWEsR0FBR0YsZUFBdEMsRUFBdUQ7QUFDckRRLHNCQUFZLENBQUMsQ0FDWDtBQUNFL0csaUJBQUssRUFBRSxxQkFEVDtBQUVFRyxrQkFBTSxFQUFFc0csYUFBYSxHQUFHRixlQUFoQixHQUFrQ0ksZUFGNUM7QUFHRTlGLGlCQUFLLEVBQUU7QUFIVCxXQURXLEVBTVg7QUFDRWIsaUJBQUssRUFBRSxrQkFEVDtBQUVFRyxrQkFBTSxFQUFFd0csZUFGVjtBQUdFOUYsaUJBQUssRUFBRTtBQUhULFdBTlcsQ0FBRCxDQUFaO0FBWUQsU0FiRCxNQWFPO0FBQ0xrRyxzQkFBWSxDQUFDLENBQ1g7QUFDRS9HLGlCQUFLLEVBQUUscUJBRFQ7QUFFRUcsa0JBQU0sRUFBRXNHLGFBQWEsR0FBR0YsZUFBaEIsR0FBa0NJLGVBRjVDO0FBR0U5RixpQkFBSyxFQUFFO0FBSFQsV0FEVyxFQU1YO0FBQ0ViLGlCQUFLLEVBQUUsNkJBRFQ7QUFFRUcsa0JBQU0sRUFBRXNHLGFBQWEsR0FBR0YsZUFGMUI7QUFHRTFGLGlCQUFLLEVBQUU7QUFIVCxXQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0Y7QUFDRixLQWhERCxNQWdETztBQUNMLFVBQUk0RixhQUFhLEdBQUcvRixJQUFJLENBQUNDLEdBQUwsQ0FBUzRGLGVBQVQsSUFBNEJJLGVBQWhELEVBQWlFO0FBQy9ESSxvQkFBWSxDQUFDLENBQ1g7QUFDRS9HLGVBQUssRUFBRSxxQkFEVDtBQUVFRyxnQkFBTSxFQUFFc0csYUFBYSxHQUFHRixlQUFoQixHQUFrQ0ksZUFGNUM7QUFHRTlGLGVBQUssRUFBRTtBQUhULFNBRFcsRUFNWDtBQUNFYixlQUFLLEVBQUUscUNBRFQ7QUFFRUcsZ0JBQU0sRUFBRU8sSUFBSSxDQUFDQyxHQUFMLENBQVM0RixlQUFULElBQTRCSSxlQUZ0QztBQUdFOUYsZUFBSyxFQUFFO0FBSFQsU0FOVyxDQUFELENBQVo7QUFZRCxPQWJELE1BYU87QUFDTGtHLG9CQUFZLENBQUMsQ0FDWDtBQUNFL0csZUFBSyxFQUFFLGlCQURUO0FBRUVHLGdCQUFNLEVBQUVzRyxhQUZWO0FBR0U1RixlQUFLLEVBQUU7QUFIVCxTQURXLEVBTVg7QUFDRWIsZUFBSyxFQUFFLHFCQURUO0FBRUVHLGdCQUFNLEVBQUVzRyxhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQUY1QztBQUdFOUYsZUFBSyxFQUFFO0FBSFQsU0FOVyxDQUFELENBQVo7QUFZRDtBQUNGO0FBQ0YsR0FqRlEsRUFpRk4sQ0FBQzBGLGVBQUQsRUFBa0JFLGFBQWxCLEVBQWlDRSxlQUFqQyxDQWpGTSxDQUFUO0FBbUZBcEcseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSTBFLE1BQU0sSUFBSTBCLGVBQWQsRUFBK0I7QUFDN0JNLGtCQUFZLENBQUMsQ0FDWDtBQUNFakgsYUFBSyxFQUFFLFNBRFQ7QUFFRUcsY0FBTSxFQUFFOEUsTUFBTSxHQUFHMEIsZUFGbkI7QUFHRTlGLGFBQUssRUFBRTtBQUhULE9BRFcsRUFNWDtBQUNFYixhQUFLLEVBQUUsVUFEVDtBQUVFRyxjQUFNLEVBQUV3RyxlQUZWO0FBR0U5RixhQUFLLEVBQUU7QUFIVCxPQU5XLENBQUQsQ0FBWjtBQVlELEtBYkQsTUFhTztBQUNMb0csa0JBQVksQ0FBQyxDQUNYO0FBQ0VqSCxhQUFLLEVBQUUsaUJBRFQ7QUFFRUcsY0FBTSxFQUFFOEUsTUFGVjtBQUdFcEUsYUFBSyxFQUFFO0FBSFQsT0FEVyxFQU1YO0FBQ0ViLGFBQUssRUFBRSxTQURUO0FBRUVHLGNBQU0sRUFBRU8sSUFBSSxDQUFDQyxHQUFMLENBQVNzRSxNQUFNLEdBQUcwQixlQUFsQixDQUZWO0FBR0U5RixhQUFLLEVBQUU7QUFIVCxPQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0YsR0E1QlEsRUE0Qk4sQ0FBQ29FLE1BQUQsRUFBUzBCLGVBQVQsQ0E1Qk0sQ0FBVDs7QUE4QkEsV0FBU2lCLFFBQVQsQ0FBa0JDLENBQWxCLEVBQXFCO0FBQ25CVixXQUFPLENBQUNDLEdBQVIsQ0FBWVMsQ0FBWjtBQUNBQSxLQUFDLENBQUNDLGNBQUY7QUFFQXJFLFNBQUssQ0FBRSx5Q0FBRixFQUE0QztBQUMvQ3NFLFlBQU0sRUFBRSxLQUR1QztBQUUvQ3JFLGFBQU8sRUFBRTtBQUNQLHdCQUFnQixrQkFEVDtBQUVQQyxxQkFBYSxFQUFHLFVBQVNsRSxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBOEI7QUFGaEQsT0FGc0M7QUFNL0NzSSxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CakUsaUJBQVMsRUFBRUYsNkNBQU0sQ0FBQ0UsU0FBRCxDQUFOLENBQWtCVSxPQUFsQixDQUEwQixLQUExQixDQURRO0FBRW5CUCxlQUFPLEVBQUVMLDZDQUFNLENBQUNLLE9BQUQsQ0FBTixDQUFnQk8sT0FBaEIsQ0FBd0IsS0FBeEI7QUFGVSxPQUFmO0FBTnlDLEtBQTVDLENBQUwsQ0FXR2YsSUFYSCxDQVdTekIsR0FBRCxJQUFTQSxHQUFHLENBQUMwQixJQUFKLEVBWGpCLEVBWUdELElBWkgsQ0FZU3pDLElBQUQsSUFBVTtBQUNkLFVBQUlBLElBQUksSUFBSSxLQUFaLEVBQW1CO0FBQ2pCZ0gsWUFBSSxDQUFDQyxJQUFMLENBQVUsT0FBVixFQUFvQixzQkFBcEIsRUFBMkMsT0FBM0M7QUFDRCxPQUZELE1BRU87QUFDTGpCLGVBQU8sQ0FBQ0MsR0FBUixDQUFZakcsSUFBWjtBQUNBOEUscUJBQWEsQ0FDWGxDLDZDQUFNLENBQUM1QyxJQUFJLENBQUM4QyxTQUFOLENBQU4sQ0FBdUJVLE9BQXZCLENBQStCLEtBQS9CLEVBQXNDMEMsTUFBdEMsQ0FBNkMsWUFBN0MsQ0FEVyxDQUFiO0FBR0FsQixtQkFBVyxDQUFDcEMsNkNBQU0sQ0FBQzVDLElBQUksQ0FBQ2lELE9BQU4sQ0FBTixDQUFxQk8sT0FBckIsQ0FBNkIsS0FBN0IsRUFBb0MwQyxNQUFwQyxDQUEyQyxZQUEzQyxDQUFELENBQVg7QUFDQXZCLG9CQUFZLENBQ1YvQiw2Q0FBTSxDQUFDNUMsSUFBSSxDQUFDOEMsU0FBTixDQUFOLENBQXVCVSxPQUF2QixDQUErQixLQUEvQixFQUFzQzBDLE1BQXRDLENBQTZDLFlBQTdDLENBRFUsQ0FBWjtBQUdBdEIsa0JBQVUsQ0FBQ2hDLDZDQUFNLENBQUM1QyxJQUFJLENBQUNpRCxPQUFOLENBQU4sQ0FBcUJPLE9BQXJCLENBQTZCLEtBQTdCLEVBQW9DMEMsTUFBcEMsQ0FBMkMsWUFBM0MsQ0FBRCxDQUFWO0FBQ0Q7QUFDRixLQTFCSDtBQTJCRDs7QUFFRCxzQkFDRSxxRUFBQyw0Q0FBRCxDQUFPLFFBQVA7QUFBQSw0QkFDRSxxRUFBQyxnREFBRDtBQUFBLDZCQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLEVBS0doQyxJQUFJLENBQUM2QixFQUFMLEtBQVksSUFBWixnQkFDQztBQUFBLDhCQUNFLHFFQUFDLHlEQUFEO0FBQVcsaUJBQVMsRUFBQyx3QkFBckI7QUFBQSxnQ0FDRTtBQUFBLG9CQUFLbkM7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBQSxvQkFBS1k7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGLGVBR0U7QUFBQSxzQ0FDZ0JjLGFBQWEsR0FBR0YsZUFBaEIsR0FBa0NJLGVBRGxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVFFLHFFQUFDLG1EQUFEO0FBQUssaUJBQVMsRUFBQyxnQ0FBZjtBQUFBLCtCQUNFLHFFQUFDLG9EQUFEO0FBQU0sa0JBQVEsRUFBR2tCLENBQUQsSUFBT0QsUUFBUSxDQUFDQyxDQUFELENBQS9CO0FBQUEsaUNBQ0UscUVBQUMsbURBQUQ7QUFBSyxxQkFBUyxFQUFDLGdDQUFmO0FBQUEsb0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxnQkFBRSxFQUFDLE1BQVI7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBSUUscUVBQUMsbURBQUQ7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxvQkFBSSxFQUFDLE1BRFA7QUFFRSwwQkFBVSxFQUFDLFlBRmI7QUFHRSxxQkFBSyxFQUFFNUQsU0FIVDtBQUlFLHdCQUFRLEVBQUc0RCxDQUFELElBQU8vQixZQUFZLENBQUMrQixDQUFDLENBQUNRLE1BQUYsQ0FBU2pHLEtBQVY7QUFKL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkYsZUFZRSxxRUFBQyxtREFBRDtBQUFLLGdCQUFFLEVBQUMsTUFBUjtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBWkYsZUFlRSxxRUFBQyxtREFBRDtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLG9CQUFJLEVBQUMsTUFEUDtBQUVFLHFCQUFLLEVBQUVnQyxPQUZUO0FBR0UsMEJBQVUsRUFBQyxhQUhiO0FBSUUsd0JBQVEsRUFBR3lELENBQUQsSUFBTzlCLFVBQVUsQ0FBQzhCLENBQUMsQ0FBQ1EsTUFBRixDQUFTakcsS0FBVjtBQUo3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFmRixlQXdCRSxxRUFBQyxtREFBRDtBQUFBLHdCQUNHNkIsU0FBUyxJQUFJK0IsVUFBYixJQUEyQjVCLE9BQU8sSUFBSThCLFFBQXRDLGdCQUNDLHFFQUFDLHNEQUFEO0FBQVEsdUJBQU8sRUFBQyxTQUFoQjtBQUEwQixvQkFBSSxFQUFDLFFBQS9CO0FBQXdDLGtCQUFFLEVBQUMsV0FBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREQsZ0JBS0MscUVBQUMsc0RBQUQ7QUFDRSx1QkFBTyxFQUFDLFNBRFY7QUFFRSxvQkFBSSxFQUFDLFFBRlA7QUFHRSxrQkFBRSxFQUFDLFdBSEw7QUFJRSx3QkFBUSxNQUpWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTko7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkF4QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSRixlQXFERSxxRUFBQyxtREFBRDtBQUFBLGdDQUNFLHFFQUFDLG1EQUFEO0FBQUEsaUNBQ0UscUVBQUMsb0RBQUQ7QUFBTSxxQkFBUyxFQUFDLGNBQWhCO0FBQUEsb0NBQ0U7QUFBQSx3QkFDR1ksU0FBUyxDQUFDOUcsS0FBVixnQkFDQyxxRUFBQyxxREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERCxnQkFHQyxxRUFBQyw0REFBRDtBQUNFLHlCQUFTLEVBQUMsS0FEWjtBQUVFLHVCQUFPLEVBQUU4RyxTQUZYO0FBR0Usd0JBQVEsRUFBRSxPQUhaO0FBSUUseUJBQVMsRUFBRTtBQUpiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBYUUscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEscUNBQ0U7QUFBSyx5QkFBUyxFQUFDLGFBQWY7QUFBQSx3Q0FDRTtBQUFBLHFEQUF5QkwsYUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURGLGVBRUU7QUFBQSx1REFBMkJFLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGRixFQUdHRixhQUFhLElBQUlFLGVBQWpCLGdCQUNDO0FBQUEsc0RBQ3dCRixhQUFhLEdBQUdFLGVBRHhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERCxnQkFLQztBQUFBLHNEQUN3QkYsYUFBYSxHQUFHRSxlQUR4QyxFQUN5RCxHQUR6RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUkosRUFZR0osZUFBZSxJQUFJLENBQW5CLGdCQUNDO0FBQUEsdURBQTJCQSxlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREQsZ0JBR0M7QUFBQSx1REFBMkJBLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFxQ0UscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRDtBQUFNLHFCQUFTLEVBQUMsY0FBaEI7QUFBQSxvQ0FDRTtBQUFBLHdCQUNHTyxTQUFTLENBQUM5RyxLQUFWLGdCQUNDLHFFQUFDLHFEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURELGdCQUdDLHFFQUFDLDREQUFEO0FBQ0UseUJBQVMsRUFBQyxLQURaO0FBRUUsdUJBQU8sRUFBRWdILFNBRlg7QUFHRSx3QkFBUSxFQUFFLE9BSFo7QUFJRSx5QkFBUyxFQUFFO0FBSmI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFhRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxxQ0FDRTtBQUFLLHlCQUFTLEVBQUMsYUFBZjtBQUFBLHdDQUNFO0FBQUEsNkNBQWlCL0IsTUFBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURGLGVBRUU7QUFBQSwrQ0FBbUIwQixlQUFuQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRkYsRUFHRzFCLE1BQU0sSUFBSTBCLGVBQVYsZ0JBQ0M7QUFBQSw4Q0FBa0IxQixNQUFNLEdBQUcwQixlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREQsZ0JBR0M7QUFBQSw4Q0FBa0IxQixNQUFNLEdBQUcwQixlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFiRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXJDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FyREYsZUFzSEUscUVBQUMsbURBQUQ7QUFBSyxpQkFBUyxFQUFDLGdDQUFmO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxtQkFBUyxFQUFDLGdDQUFmO0FBQUEsaUNBQ0UscUVBQUMsc0RBQUQ7QUFDRSxxQkFBUyxFQUFDLFlBRFo7QUFFRSxnQkFBSSxFQUFDLElBRlA7QUFHRSxtQkFBTyxFQUFDLFNBSFY7QUFJRSxtQkFBTyxFQUFFLE1BQU16QixtREFBTSxDQUFDNUMsSUFBUCxDQUFZLGdCQUFaLENBSmpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQVdFLHFFQUFDLG1EQUFEO0FBQUEsaUNBQ0UscUVBQUMsc0RBQUQ7QUFDRSxxQkFBUyxFQUFDLFlBRFo7QUFFRSxnQkFBSSxFQUFDLElBRlA7QUFHRSxtQkFBTyxFQUFDLFdBSFY7QUFJRSxtQkFBTyxFQUFFLE1BQU00QyxtREFBTSxDQUFDNUMsSUFBUCxDQUFZLGNBQVosQ0FKakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXRIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkErSUMscUVBQUMsOENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXBKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQXlKRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN2NEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNnRyxLQUFULEdBQWlCO0FBQzlCLHNCQUNFLHFFQUFDLHdEQUFEO0FBQU0sU0FBSyxFQUFFLE9BQWI7QUFBQSwyQkFDRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyx3QkFBZjtBQUFBLDZCQUNFLHFFQUFDLG1EQUFEO0FBQUssVUFBRSxNQUFQO0FBQVEsVUFBRSxFQUFDLEdBQVg7QUFBQSwrQkFDRSxxRUFBQyxTQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQVNEOztBQUVELE1BQU1DLFNBQVMsR0FBRyxNQUFNO0FBQ3RCLFFBQU07QUFBRWxELFFBQUY7QUFBUUM7QUFBUixNQUFvQkMsd0RBQVUsQ0FBQ3RHLG9EQUFELENBQXBDO0FBRUEsUUFBTTtBQUFBLE9BQUMwRyxLQUFEO0FBQUEsT0FBUUM7QUFBUixNQUFvQjFGLHNEQUFRLENBQUMsRUFBRCxDQUFsQztBQUNBLFFBQU07QUFBQSxPQUFDc0ksUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJ2SSxzREFBUSxDQUFDLEVBQUQsQ0FBeEM7QUFDQSxRQUFNO0FBQUEsT0FBQ3dJLFFBQUQ7QUFBQSxPQUFXQztBQUFYLE1BQTBCekksc0RBQVEsQ0FBQyxLQUFELENBQXhDOztBQUVBLFdBQVMwSSxZQUFULENBQXNCZixDQUF0QixFQUF5QjtBQUN2QkEsS0FBQyxDQUFDQyxjQUFGO0FBRUEsVUFBTWUsT0FBTyxHQUFHO0FBQ2RkLFlBQU0sRUFBRSxNQURNO0FBRWRyRSxhQUFPLEVBQUU7QUFBRSx3QkFBZ0I7QUFBbEIsT0FGSztBQUdkc0UsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNuQnZDLGFBQUssRUFBRUEsS0FEWTtBQUVuQjZDLGdCQUFRLEVBQUVBO0FBRlMsT0FBZjtBQUhRLEtBQWhCO0FBU0EvRSxTQUFLLENBQUUsdUNBQUYsRUFBMENvRixPQUExQyxDQUFMLENBQ0dqRixJQURILENBQ1N6QixHQUFELElBQVNBLEdBQUcsQ0FBQzBCLElBQUosRUFEakIsRUFFR0QsSUFGSCxDQUVTekMsSUFBRCxJQUFVO0FBQ2RnRyxhQUFPLENBQUNDLEdBQVIsQ0FBWWpHLElBQVo7O0FBQ0EsVUFBSSxPQUFPQSxJQUFJLENBQUMySCxXQUFaLEtBQTRCLFdBQWhDLEVBQTZDO0FBQzNDckosb0JBQVksQ0FBQ3NKLE9BQWIsQ0FBcUIsT0FBckIsRUFBOEI1SCxJQUFJLENBQUMySCxXQUFuQztBQUNBeEQsZUFBTyxDQUFDO0FBQUU0QixZQUFFLEVBQUUvRixJQUFJLENBQUM2RDtBQUFYLFNBQUQsQ0FBUDtBQUNBRSwwREFBTSxDQUFDNUMsSUFBUCxDQUFZLEdBQVo7QUFDRCxPQUpELE1BSU87QUFDTCxZQUFJbkIsSUFBSSxDQUFDNkgsS0FBTCxLQUFlLGdCQUFuQixFQUFxQztBQUNuQ2IsNERBQUksQ0FBQ0MsSUFBTCxDQUFVLHVCQUFWLEVBQW1DLHNCQUFuQyxFQUEyRCxPQUEzRDtBQUNELFNBRkQsTUFFTyxJQUFJakgsSUFBSSxDQUFDNkgsS0FBTCxLQUFlLGtCQUFuQixFQUF1QztBQUM1Q2IsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLGtCQURGLEVBRUUsaUdBRkYsRUFHRSxPQUhGO0FBS0QsU0FOTSxNQU1BLElBQUlqSCxJQUFJLENBQUM2SCxLQUFMLEtBQWUsb0JBQW5CLEVBQXlDO0FBQzlDYiw0REFBSSxDQUFDQyxJQUFMLENBQ0UsdUJBREYsRUFFRSx1QkFGRixFQUdFLE9BSEY7QUFLRDtBQUNGO0FBQ0YsS0F6Qkg7QUEwQkQ7O0FBRUQsUUFBTWEsd0JBQXdCLEdBQUlDLFFBQUQsSUFBYztBQUM3QyxVQUFNQyxPQUFPLEdBQUc7QUFDZHBCLFlBQU0sRUFBRSxNQURNO0FBRWRyRSxhQUFPLEVBQUU7QUFBRSx3QkFBZ0I7QUFBbEIsT0FGSztBQUdkc0UsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFFa0IsZUFBTyxFQUFFRixRQUFRLENBQUNFO0FBQXBCLE9BQWY7QUFIUSxLQUFoQjtBQU1BM0YsU0FBSyxDQUFFLHdEQUFGLEVBQTJEMEYsT0FBM0QsQ0FBTCxDQUNHdkYsSUFESCxDQUNTekIsR0FBRCxJQUFTQSxHQUFHLENBQUMwQixJQUFKLEVBRGpCLEVBRUdELElBRkgsQ0FFU3pDLElBQUQsSUFBVTtBQUNkLFVBQUksT0FBT0EsSUFBSSxDQUFDMkgsV0FBWixLQUE0QixXQUFoQyxFQUE2QztBQUMzQ3JKLG9CQUFZLENBQUNzSixPQUFiLENBQXFCLE9BQXJCLEVBQThCNUgsSUFBSSxDQUFDMkgsV0FBbkM7QUFDQXhELGVBQU8sQ0FBQztBQUFFNEIsWUFBRSxFQUFFL0YsSUFBSSxDQUFDNkQ7QUFBWCxTQUFELENBQVA7QUFDQUUsMERBQU0sQ0FBQzVDLElBQVAsQ0FBWSxHQUFaO0FBQ0QsT0FKRCxNQUlPO0FBQ0wsWUFBS25CLElBQUksQ0FBQzZILEtBQUwsR0FBYSxtQkFBbEIsRUFBd0M7QUFDdENiLDREQUFJLENBQUNDLElBQUwsQ0FDRSxtQkFERixFQUVFLHlDQUZGLEVBR0UsT0FIRjtBQUtELFNBTkQsTUFNTyxJQUFLakgsSUFBSSxDQUFDNkgsS0FBTCxHQUFhLGtCQUFsQixFQUF1QztBQUM1Q2IsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLGtCQURGLEVBRUUsOERBRkYsRUFHRSxPQUhGO0FBS0Q7QUFDRjtBQUNGLEtBdEJIO0FBdUJELEdBOUJEOztBQWdDQTdILHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlvRixLQUFLLEtBQUssRUFBVixJQUFnQjZDLFFBQVEsS0FBSyxFQUFqQyxFQUFxQztBQUNuQ0csaUJBQVcsQ0FBQyxJQUFELENBQVg7QUFDRCxLQUZELE1BRU87QUFDTEEsaUJBQVcsQ0FBQyxLQUFELENBQVg7QUFDRDtBQUNGLEdBTlEsRUFNTixDQUFDaEQsS0FBRCxFQUFRNkMsUUFBUixDQU5NLENBQVQ7QUFRQSxzQkFDRSxxRUFBQyxvREFBRDtBQUFBLDRCQUNFLHFFQUFDLG9EQUFELENBQU0sTUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLDhCQUNFLHFFQUFDLGdEQUFEO0FBQUEsK0JBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBSUUscUVBQUMsb0RBQUQ7QUFBTSxnQkFBUSxFQUFHWCxDQUFELElBQU9lLFlBQVksQ0FBQ2YsQ0FBRCxDQUFuQztBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLG1CQUFTLEVBQUMsV0FBdEI7QUFBQSxrQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxnQkFBSSxFQUFDLE9BRFA7QUFFRSx1QkFBVyxFQUFDLGFBRmQ7QUFHRSxpQkFBSyxFQUFFbEMsS0FIVDtBQUlFLG9CQUFRLEVBQUdrQyxDQUFELElBQU9qQyxRQUFRLENBQUNpQyxDQUFDLENBQUNRLE1BQUYsQ0FBU2pHLEtBQVYsQ0FKM0I7QUFLRSxvQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBWUUscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksbUJBQVMsRUFBQyxVQUF0QjtBQUFBLGtDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFJLEVBQUMsVUFEUDtBQUVFLHVCQUFXLEVBQUMsVUFGZDtBQUdFLGlCQUFLLEVBQUVvRyxRQUhUO0FBSUUsb0JBQVEsRUFBR1gsQ0FBRCxJQUFPWSxXQUFXLENBQUNaLENBQUMsQ0FBQ1EsTUFBRixDQUFTakcsS0FBVixDQUo5QjtBQUtFLG9CQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBWkYsZUFzQkUscUVBQUMsbURBQUQ7QUFBSyxtQkFBUyxFQUFDLDZCQUFmO0FBQUEsb0JBQ0dzRyxRQUFRLGdCQUNQLHFFQUFDLHNEQUFEO0FBQVEscUJBQVMsRUFBQyxzQkFBbEI7QUFBeUMsZ0JBQUksRUFBQyxRQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFETyxnQkFLUCxxRUFBQyxzREFBRDtBQUFRLHFCQUFTLEVBQUMsc0JBQWxCO0FBQXlDLGdCQUFJLEVBQUMsUUFBOUM7QUFBdUQsb0JBQVEsTUFBL0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQXRCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBSkYsZUF1Q0UscUVBQUMsOERBQUQ7QUFDRSxnQkFBUSxFQUFDLDBFQURYO0FBRUUsa0JBQVUsRUFBQyxPQUZiO0FBR0UsaUJBQVMsRUFBRU8sd0JBSGI7QUFJRSxpQkFBUyxFQUFFQSx3QkFKYjtBQUtFLG9CQUFZLEVBQUUsb0JBTGhCO0FBTUUsaUJBQVMsRUFBQztBQU5aO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBdkNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQXFERCxDQTVJRCxDOzs7Ozs7Ozs7OztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNiQSxtQzs7Ozs7Ozs7Ozs7QUNBQSxzQzs7Ozs7Ozs7Ozs7QUNBQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSw0Qzs7Ozs7Ozs7Ozs7QUNBQSw0Qzs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSx3QyIsImZpbGUiOiJwYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvaW5kZXguanNcIik7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuLy8gY3JlYXRlcyBhIENvbnRleHQgT2JqZWN0XHJcbmNvbnN0IFVzZXJDb250ZXh0ID0gUmVhY3QuY3JlYXRlQ29udGV4dCgpO1xyXG5cclxuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyXHJcblxyXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDsiLCJtb2R1bGUuZXhwb3J0cyA9IHtcclxuXHRnZXRBY2Nlc3NUb2tlbjogKCkgPT4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Rva2VuJyksXHJcbn1cclxuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBEb3VnaG51dCwgUGllIH0gZnJvbSBcInJlYWN0LWNoYXJ0anMtMlwiO1xyXG5pbXBvcnQgQ29sb3JSYW5kb21pemVyIGZyb20gXCIuLi9oZWxwZXJzL0NvbG9yUmFuZG9taXplclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUGllQ2hhcnQoeyBjaGFydFR5cGUsIHJhd0RhdGEsIGxhYmVsS2V5LCBhbW91bnRLZXkgfSkge1xyXG4gIGNvbnN0IFtsYWJlbCwgc2V0TGFiZWxdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFthbW91bnQsIHNldEFtb3VudF0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2JnQ29sb3JzLCBzZXRCZ0NvbG9yc10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAocmF3RGF0YSkge1xyXG4gICAgICBzZXRMYWJlbChyYXdEYXRhLm1hcCgoZWxlbWVudCkgPT4gZWxlbWVudFtsYWJlbEtleV0pKTtcclxuICAgICAgc2V0QW1vdW50KHJhd0RhdGEubWFwKChlbGVtZW50KSA9PiBNYXRoLmFicyhlbGVtZW50W2Ftb3VudEtleV0pKSk7XHJcblxyXG4gICAgICBsZXQgY29sb3JzID0gcmF3RGF0YS5tYXAoKGVsZW1lbnQpID0+IHtcclxuICAgICAgICBpZiAoZWxlbWVudC5jb2xvcikge1xyXG4gICAgICAgICAgcmV0dXJuIGVsZW1lbnQuY29sb3I7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiBgIyR7Q29sb3JSYW5kb21pemVyKCl9YDtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgc2V0QmdDb2xvcnMoY29sb3JzKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldExhYmVsKFtdKTtcclxuICAgICAgc2V0QW1vdW50KFtdKTtcclxuICAgICAgc2V0QmdDb2xvcnMoW10pO1xyXG4gICAgfVxyXG4gIH0sIFtyYXdEYXRhXSk7XHJcblxyXG4gIGlmIChjaGFydFR5cGUgPT0gXCJQaWVcIikge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPFBpZVxyXG4gICAgICAgIGRhdGE9e3tcclxuICAgICAgICAgIGxhYmVsczogbGFiZWwsXHJcbiAgICAgICAgICBsZWdlbmQ6IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IFwibGVmdFwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBkYXRhOiBhbW91bnQsXHJcbiAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBiZ0NvbG9ycyxcclxuICAgICAgICAgICAgICBob3ZlckJhY2tncm91bmQ6IGJnQ29sb3JzLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgXSxcclxuICAgICAgICB9fVxyXG4gICAgICAgIHJlZHJhdz17ZmFsc2V9XHJcbiAgICAgIC8+XHJcbiAgICApO1xyXG4gIH0gZWxzZSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8RG91Z2hudXRcclxuICAgICAgICBkYXRhPXt7XHJcbiAgICAgICAgICBsYWJlbHM6IGxhYmVsLFxyXG4gICAgICAgICAgbGVnZW5kOiB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImxlZnRcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBkYXRhc2V0czogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgZGF0YTogYW1vdW50LFxyXG4gICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogYmdDb2xvcnMsXHJcbiAgICAgICAgICAgICAgaG92ZXJCYWNrZ3JvdW5kOiBiZ0NvbG9ycyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIF0sXHJcbiAgICAgICAgfX1cclxuICAgICAgLz5cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgeyBDb250YWluZXIgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcblxyXG4vL290aGVyIHN5bnRheFxyXG5jb25zdCBWaWV3ID0gKHsgdGl0bGUsIGNoaWxkcmVuIH0pID0+IHtcclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGUga2V5PVwidGl0bGUtdGFnXCI+e3RpdGxlfTwvdGl0bGU+XHJcbiAgICAgICAgPG1ldGFcclxuICAgICAgICAgIGtleT1cInRpdGxlLW1ldGFcIlxyXG4gICAgICAgICAgbmFtZT1cInZpZXdwb3J0XCJcclxuICAgICAgICAgIGNvbnRlbnQ9XCJpbml0aWFsLXNjYWxlPTEuMCwgd2lkdGg9ZGV2aWNlLXdpZHRoXCJcclxuICAgICAgICAvPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxDb250YWluZXIgY2xhc3NOYW1lPVwibXQtNSBwdC00IG1iLTVcIj57Y2hpbGRyZW59PC9Db250YWluZXI+XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBWaWV3O1xyXG4iLCJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAgQ29sb3JSYW5kb21pemVyICgpe1xyXG5cdHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkqMTY3NzcyMTUpLnRvU3RyaW5nKDE2KTtcclxufVxyXG5cclxuXHJcbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHN1bUJ5R3JvdXAobGFiZWxBcnJheSxsYWJlbEtleSxhcnJheSxrZXksa2V5U3VtKXtcclxuXHJcblx0XHJcblx0bGV0IHN1bUNvdW50cz1bXVxyXG5cclxuICAgIGFycmF5LnJlZHVjZShmdW5jdGlvbihyZXMsIHZhbHVlKSB7XHJcblx0XHRpZiAoIXJlc1t2YWx1ZS5ba2V5XV0pIHtcclxuXHRcdFx0cmVzW3ZhbHVlLltrZXldXSA9IHsgW2tleV06IHZhbHVlLltrZXldLCBba2V5U3VtXTogMCwgY291bnQ6MCB9O1xyXG5cdFx0XHRzdW1Db3VudHMucHVzaChyZXNbdmFsdWUuW2tleV1dKVxyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHRyZXNbdmFsdWUuW2tleV1dLltrZXlTdW1dICs9IHZhbHVlLltrZXlTdW1dXHJcblx0XHRyZXNbdmFsdWUuW2tleV1dLmNvdW50ICs9IDFcclxuXHRcdHJldHVybiByZXNcclxuXHR9LCB7fSlcclxuXHJcblxyXG5cdFxyXG5cdGlmKGxhYmVsQXJyYXkhPVtdICYgbGFiZWxLZXkhPSBcIlwiKXtcclxuICAgICAgICBcclxuICAgICAgICBsZXQgbGFiZWxTdW1Db3VudHMgPSBsYWJlbEFycmF5Lm1hcChsYWJlbD0+e1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICBsZXQgZm91bmRTdW1Db3VudCA9IHN1bUNvdW50cy5maW5kKHN1bUNvdW50ID0+IHN1bUNvdW50LltrZXldPT0gbGFiZWwuW2xhYmVsS2V5XSlcclxuXHJcbiAgICAgICAgICAgIGlmKGZvdW5kU3VtQ291bnQgPT0gdW5kZWZpbmVkKXtcclxuICAgICAgICAgICAgICAgIGxhYmVsLltrZXlTdW1dID0gMFxyXG4gICAgICAgICAgICAgICAgbGFiZWwuY291bnQgPSAwXHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgbGFiZWwuW2tleVN1bV0gPSBmb3VuZFN1bUNvdW50LltrZXlTdW1dXHJcbiAgICAgICAgICAgICAgICBsYWJlbC5jb3VudCA9IGZvdW5kU3VtQ291bnQuY291bnRcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGxhYmVsXHJcbiAgICAgICAgfSlcclxuICAgICAgICBcclxuICAgICAgICByZXR1cm4obGFiZWxTdW1Db3VudHMpXHJcblx0fWVsc2V7XHJcblx0XHRyZXR1cm4oc3VtQ291bnRzKVxyXG5cdH1cclxuXHJcblx0XHJcbn07XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgVGFibGUsIEFsZXJ0LCBDb250YWluZXIsIEJ1dHRvbiwgQ29sLCBSb3cgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBzdHlsZXMgZnJvbSBcIi4uLy4uL3N0eWxlcy9Ib21lLm1vZHVsZS5jc3NcIjtcclxuaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XHJcbmltcG9ydCBzdW1CeUdyb3VwIGZyb20gXCIuLi8uLi9oZWxwZXJzL3N1bUJ5R3JvdXBcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGNhdGVnb3J5KCkge1xyXG4gIGNvbnN0IFtjYXRlZ29yaWVzLCBzZXRDYXRlZ29yaWVzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbY3VycmVudFJlY29yZHMsIHNldEN1clJlY10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2luY29tZUNhdGVnb3JpZXMsIHNldEluY0NhdF0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2V4cGVuc2VzQ2F0ZWdvcmllcywgc2V0RXhwQ2F0XSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbaW5jb21lUm93cywgc2V0SW5jb21lUm93c10gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbZXhwZW5zZXNSb3dzLCBzZXRFeHBlbnNlc1Jvd3NdID0gdXNlU3RhdGUobnVsbCk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9kZXRhaWxzYCwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgc2V0Q2F0ZWdvcmllcyhkYXRhLmNhdGVnb3JpZXMpO1xyXG5cclxuICAgICAgICBsZXQgZGF0ZUJlZm9yZSA9IG1vbWVudChkYXRhLmN1cnJlbnREYXRlLnN0YXJ0RGF0ZSkuc3VidHJhY3QoMSwgXCJkYXlcIik7XHJcbiAgICAgICAgbGV0IGRhdGVBZnRlciA9IG1vbWVudChkYXRhLmN1cnJlbnREYXRlLmVuZERhdGUpLmFkZCgxLCBcImRheVwiKTtcclxuXHJcbiAgICAgICAgbGV0IGZpbHRlcmVkUmVjb3JkcyA9IGRhdGEucmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgbGV0IHVwZGF0ZWRPbiA9IG1vbWVudChyZWNvcmQudXBkYXRlZE9uKS5zdGFydE9mKFwiZGF5c1wiKTtcclxuICAgICAgICAgIHJldHVybiBtb21lbnQodXBkYXRlZE9uKS5pc0JldHdlZW4oZGF0ZUJlZm9yZSwgZGF0ZUFmdGVyLCBcImRheVwiKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgc2V0Q3VyUmVjKGZpbHRlcmVkUmVjb3Jkcyk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGxldCBjYXRlZ29yeVJlY29yZHMgPSBzdW1CeUdyb3VwKFxyXG4gICAgICBjYXRlZ29yaWVzLFxyXG4gICAgICBcIl9pZFwiLFxyXG4gICAgICBjdXJyZW50UmVjb3JkcyxcclxuICAgICAgXCJjYXRlZ29yeUlkXCIsXHJcbiAgICAgIFwiYW1vdW50XCJcclxuICAgICk7XHJcblxyXG4gICAgc2V0SW5jQ2F0KGNhdGVnb3J5UmVjb3Jkcy5maWx0ZXIoKGNhdGVnb3J5KSA9PiBjYXRlZ29yeS50eXBlID09IFwiaW5jb21lXCIpKTtcclxuICAgIHNldEV4cENhdChjYXRlZ29yeVJlY29yZHMuZmlsdGVyKChjYXRlZ29yeSkgPT4gY2F0ZWdvcnkudHlwZSA9PSBcImV4cGVuc2VcIikpO1xyXG4gIH0sIFtjYXRlZ29yaWVzLCBjdXJyZW50UmVjb3Jkc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgc2V0SW5jb21lUm93cyhcclxuICAgICAgaW5jb21lQ2F0ZWdvcmllcy5tYXAoKGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgIDx0ciBrZXk9e2NhdGVnb3J5Ll9pZH0+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkubmFtZX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5LmFtb3VudH08L3RkPlxyXG4gICAgICAgICAgPC90cj5cclxuICAgICAgICApO1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9LCBbaW5jb21lQ2F0ZWdvcmllc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgc2V0RXhwZW5zZXNSb3dzKFxyXG4gICAgICBleHBlbnNlc0NhdGVnb3JpZXMubWFwKChjYXRlZ29yeSkgPT4ge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICA8dHIga2V5PXtjYXRlZ29yeS5faWR9PlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5Lm5hbWV9PC90ZD5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5idWRnZXR9PC90ZD5cclxuICAgICAgICAgICAgPHRkPntNYXRoLmFicyhjYXRlZ29yeS5hbW91bnQpfTwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkuYW1vdW50ICsgY2F0ZWdvcnkuYnVkZ2V0fTwvdGQ+XHJcbiAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICk7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH0sIFtleHBlbnNlc0NhdGVnb3JpZXNdKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPiBDYXRlZ29yaWVzPC90aXRsZT5cclxuICAgICAgPC9IZWFkPlxyXG4gICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1lbmQgbXktMlwiPlxyXG4gICAgICAgIDxCdXR0b25cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgIHZhcmlhbnQ9XCJzdWNjZXNzXCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9hZGRcIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgQWRkIENhdGVnb3J5XHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgdmFyaWFudD1cInNlY29uZGFyeVwiXHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBSb3V0ZXIucHVzaChcIi4vY2F0ZWdvcnkvZWRpdFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBFZGl0IENhdGVnb3J5XHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgdmFyaWFudD1cImRhbmdlclwiXHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBSb3V0ZXIucHVzaChcIi4vY2F0ZWdvcnkvYWRkXCIpfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIERlbGV0ZSBDYXRlZ29yeVxyXG4gICAgICAgIDwvQnV0dG9uPlxyXG5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwiaW5mb1wiXHJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBSb3V0ZXIucHVzaChcIi4uL3JlY29yZC9hZGRcIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgQWRkIFJlY29yZFxyXG4gICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICA8L1Jvdz5cclxuICAgICAgPGg1PkluY29tZTo8L2g1PlxyXG4gICAgICB7aW5jb21lQ2F0ZWdvcmllcy5sZW5ndGggPT0gMCA/IChcclxuICAgICAgICA8QWxlcnQgdmFyaWFudD1cImluZm9cIj5Zb3UgaGF2ZSBubyBjYXRlZ29yaWVzIHlldC48L0FsZXJ0PlxyXG4gICAgICApIDogKFxyXG4gICAgICAgIDxUYWJsZSBzdHJpcGVkIGJvcmRlcmVkIGhvdmVyPlxyXG4gICAgICAgICAgPHRoZWFkPlxyXG4gICAgICAgICAgICA8dHI+XHJcbiAgICAgICAgICAgICAgPHRoPkNhdGVnb3J5IE5hbWU8L3RoPlxyXG4gICAgICAgICAgICAgIDx0aD5DdXJyZW50IEluY29tZTwvdGg+XHJcbiAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICA8L3RoZWFkPlxyXG5cclxuICAgICAgICAgIDx0Ym9keT57aW5jb21lUm93c308L3Rib2R5PlxyXG4gICAgICAgIDwvVGFibGU+XHJcbiAgICAgICl9XHJcbiAgICAgIDxoNT5FeHBlbnNlczo8L2g1PlxyXG4gICAgICB7ZXhwZW5zZXNDYXRlZ29yaWVzLmxlbmd0aCA9PSAwID8gKFxyXG4gICAgICAgIDxBbGVydCB2YXJpYW50PVwiaW5mb1wiPllvdSBoYXZlIG5vIGNhdGVnb3JpZXMgdW5kZXIgZXhwZW5zZXMgeWV0LjwvQWxlcnQ+XHJcbiAgICAgICkgOiAoXHJcbiAgICAgICAgPFRhYmxlIHN0cmlwZWQgYm9yZGVyZWQgaG92ZXI+XHJcbiAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICA8dGg+Q2F0ZWdvcnkgTmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgPHRoPkN1cnJlbnQgQnVkZ2V0PC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBFeHBlbnNlczwvdGg+XHJcbiAgICAgICAgICAgICAgPHRoPkN1cnJlbnQgU2F2aW5nczwvdGg+XHJcbiAgICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgICA8L3RoZWFkPlxyXG5cclxuICAgICAgICAgIDx0Ym9keT57ZXhwZW5zZXNSb3dzfTwvdGJvZHk+XHJcbiAgICAgICAgPC9UYWJsZT5cclxuICAgICAgKX1cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCB7XHJcbiAgQ29udGFpbmVyLFxyXG4gIEZvcm0sXHJcbiAgUm93LFxyXG4gIENvbCxcclxuICBCdXR0b24sXHJcbiAgSnVtYm90cm9uLFxyXG4gIENhcmQsXHJcbiAgQWxlcnQsXHJcbn0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuLi9zdHlsZXMvSG9tZS5tb2R1bGUuY3NzXCI7XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vVXNlckNvbnRleHRcIjtcclxuaW1wb3J0IExvZ2luIGZyb20gXCIuL2xvZ2luXCI7XHJcbmltcG9ydCBDYXRlZ29yeSBmcm9tIFwiLi9jYXRlZ29yeVwiO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gXCJtb21lbnRcIjtcclxuaW1wb3J0IFBpZUNoYXJ0IGZyb20gXCIuLi9jb21wb25lbnRzL1BpZUNoYXJ0XCI7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKCkge1xyXG4gIGNvbnN0IHsgdXNlciwgc2V0VXNlciB9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dCk7XHJcbiAgY29uc3QgW2xvYWRpbmcsIHNldExvYWRpbmddID0gdXNlU3RhdGUodHJ1ZSk7XHJcbiAgY29uc3QgW25hbWUsIHNldE5hbWVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2VtYWlsLCBzZXRFbWFpbF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbcmVjb3Jkcywgc2V0UmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXMsIHNldENhdGVnb3JpZXNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtzdGFydERhdGUsIHNldFN0YXJ0RGF0ZV0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbZW5kRGF0ZSwgc2V0RW5kRGF0ZV0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbc2F2ZWRTdGFydCwgc2V0U2F2ZWRTdGFydF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbc2F2ZWRFbmQsIHNldFNhdmVkRW5kXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtjdXJyZW50UmVjb3Jkcywgc2V0Q3VycmVudFJlY29yZHNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtvbGRSZWNvcmRzLCBzZXRPbGRSZWNvcmRzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbcHJldmlvdXNTYXZpbmdzLCBzZXRQcmV2aW91c1NhdmluZ3NdID0gdXNlU3RhdGUoMCk7XHJcbiAgY29uc3QgW2N1cnJlbnRJbmNvbWUsIHNldEN1cnJlbnRJbmNvbWVdID0gdXNlU3RhdGUoMCk7XHJcbiAgY29uc3QgW2N1cnJlbnRFeHBlbnNlcywgc2V0Q3VycmVudEV4cGVuc2VzXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtidWRnZXQsIHNldEJ1ZGdldF0gPSB1c2VTdGF0ZSgwKTtcclxuICBjb25zdCBbYWN0dWFsUGllLCBzZXRBY3R1YWxQaWVdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtidWRnZXRQaWUsIHNldEJ1ZGdldFBpZV0gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAodXNlci5pZCAhPSBudWxsKSB7XHJcbiAgICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2RldGFpbHNgLCB7XHJcbiAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgICB9LFxyXG4gICAgICB9KVxyXG4gICAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgc2V0U3RhcnREYXRlKFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5zdGFydERhdGUpXHJcbiAgICAgICAgICAgICAgLnN0YXJ0T2YoXCJkYXlzXCIpXHJcbiAgICAgICAgICAgICAgLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXRFbmREYXRlKFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5lbmREYXRlKVxyXG4gICAgICAgICAgICAgIC5zdGFydE9mKFwiZGF5c1wiKVxyXG4gICAgICAgICAgICAgIC5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2V0U2F2ZWRTdGFydChcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuY3VycmVudERhdGUuc3RhcnREYXRlKVxyXG4gICAgICAgICAgICAgIC5zdGFydE9mKFwiZGF5c1wiKVxyXG4gICAgICAgICAgICAgIC5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2V0U2F2ZWRFbmQoXHJcbiAgICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLmVuZERhdGUpXHJcbiAgICAgICAgICAgICAgLnN0YXJ0T2YoXCJkYXlzXCIpXHJcbiAgICAgICAgICAgICAgLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXROYW1lKGAke2RhdGEuZmlyc3ROYW1lfSAke2RhdGEubGFzdE5hbWV9YCk7XHJcbiAgICAgICAgICBzZXRFbWFpbChkYXRhLmVtYWlsKTtcclxuICAgICAgICAgIHNldFJlY29yZHMoZGF0YS5yZWNvcmRzKTtcclxuICAgICAgICAgIHNldENhdGVnb3JpZXMoZGF0YS5jYXRlZ29yaWVzKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9LCBbdXNlcl0pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKChuYW1lICE9IFwiXCIpICYgKGVtYWlsICE9IFwiXCIpKSB7XHJcbiAgICAgIHNldExvYWRpbmcoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH0sIFtuYW1lLCBlbWFpbF0pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgbGV0IGRhdGVCZWZvcmUgPSBtb21lbnQoc3RhcnREYXRlKS5zdWJ0cmFjdCgxLCBcImRheVwiKTtcclxuICAgIGxldCBkYXRlQWZ0ZXIgPSBtb21lbnQoZW5kRGF0ZSkuYWRkKDEsIFwiZGF5XCIpO1xyXG5cclxuICAgIGlmIChzdGFydERhdGUgPD0gZW5kRGF0ZSkge1xyXG4gICAgICBpZiAocmVjb3Jkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgc2V0Q3VycmVudFJlY29yZHMoXHJcbiAgICAgICAgICByZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCB1cGRhdGVkT24gPSBtb21lbnQocmVjb3JkLnVwZGF0ZWRPbikuc3RhcnRPZihcImRheXNcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBtb21lbnQodXBkYXRlZE9uKS5pc0JldHdlZW4oZGF0ZUJlZm9yZSwgZGF0ZUFmdGVyLCBcImRheVwiKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgc2V0T2xkUmVjb3JkcyhcclxuICAgICAgICAgIHJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgICAgbGV0IHVwZGF0ZWRPbiA9IG1vbWVudChyZWNvcmQudXBkYXRlZE9uKS5zdGFydE9mKFwiZGF5c1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuIG1vbWVudCh1cGRhdGVkT24pLmlzQmVmb3JlKGRhdGVCZWZvcmUsIFwiZGF5XCIpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRDdXJyZW50UmVjb3JkcyhbXSk7XHJcbiAgICAgIHNldE9sZFJlY29yZHMoW10pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChjYXRlZ29yaWVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgc2V0QnVkZ2V0KFxyXG4gICAgICAgIGNhdGVnb3JpZXMucmVkdWNlKCh0b3RhbCwgY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICAgIHJldHVybiB0b3RhbCArIGNhdGVnb3J5LmJ1ZGdldDtcclxuICAgICAgICB9LCAwKVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0QnVkZ2V0KDApO1xyXG4gICAgfVxyXG4gIH0sIFtjYXRlZ29yaWVzLCBzdGFydERhdGUsIGVuZERhdGVdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChvbGRSZWNvcmRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgc2V0UHJldmlvdXNTYXZpbmdzKFxyXG4gICAgICAgIG9sZFJlY29yZHMucmVkdWNlKCh0b3RhbCwgcmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gdG90YWwgKyByZWNvcmQuYW1vdW50O1xyXG4gICAgICAgIH0sIDApXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRQcmV2aW91c1NhdmluZ3MoMCk7XHJcbiAgICB9XHJcbiAgfSwgW29sZFJlY29yZHNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChjdXJyZW50UmVjb3Jkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGxldCBpbmNvbWUgPSBjdXJyZW50UmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmFtb3VudCA+IDApO1xyXG4gICAgICBjb25zb2xlLmxvZyhpbmNvbWUpO1xyXG4gICAgICBpZiAoaW5jb21lLmxlbmd0aCA+IDApIHtcclxuICAgICAgICBzZXRDdXJyZW50SW5jb21lKFxyXG4gICAgICAgICAgaW5jb21lLnJlZHVjZSgodG90YWwsIHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gdG90YWwgKyByZWNvcmQuYW1vdW50O1xyXG4gICAgICAgICAgfSwgMClcclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNldEN1cnJlbnRJbmNvbWUoMCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCBleHBlbnNlcyA9IGN1cnJlbnRSZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiByZWNvcmQuYW1vdW50IDwgMCk7XHJcbiAgICAgIGlmIChleHBlbnNlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgc2V0Q3VycmVudEV4cGVuc2VzKFxyXG4gICAgICAgICAgZXhwZW5zZXMucmVkdWNlKCh0b3RhbCwgcmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB0b3RhbCAtIHJlY29yZC5hbW91bnQ7XHJcbiAgICAgICAgICB9LCAwKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2V0Q3VycmVudEV4cGVuc2VzKDApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRDdXJyZW50SW5jb21lKDApO1xyXG4gICAgICBzZXRDdXJyZW50RXhwZW5zZXMoMCk7XHJcbiAgICB9XHJcbiAgfSwgW2N1cnJlbnRSZWNvcmRzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBjb25zb2xlLmxvZyhwcmV2aW91c1NhdmluZ3MpO1xyXG4gICAgY29uc29sZS5sb2coY3VycmVudEluY29tZSk7XHJcbiAgICBjb25zb2xlLmxvZyhjdXJyZW50RXhwZW5zZXMpO1xyXG4gICAgaWYgKHByZXZpb3VzU2F2aW5ncyA+PSAwKSB7XHJcbiAgICAgIGlmIChjdXJyZW50SW5jb21lID4gY3VycmVudEV4cGVuc2VzKSB7XHJcbiAgICAgICAgc2V0QWN0dWFsUGllKFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiQ3VycmVudCBTYXZpbmdzXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgY29sb3I6IFwiIzQ3YjgzZFwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiQ3VycmVudCBFeHBlbnNlc1wiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiUHJldmlvdXMgU2F2aW5nc1wiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IHByZXZpb3VzU2F2aW5ncyxcclxuICAgICAgICAgICAgY29sb3I6IFwiIzBmNTIwOVwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICBdKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoY3VycmVudEV4cGVuc2VzIDwgY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncykge1xyXG4gICAgICAgICAgc2V0QWN0dWFsUGllKFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGxhYmVsOiBcIkFjY3VtdWxhdGVkIFNhdmluZ3NcIixcclxuICAgICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgICAgY29sb3I6IFwiIzBmNTIwOVwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiQ3VycmVudCBFeHBlbnNlc1wiLFxyXG4gICAgICAgICAgICAgIGFtb3VudDogY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIF0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBzZXRBY3R1YWxQaWUoW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiQWNjdW11bGF0ZWQgRGVmaWNpdFwiLFxyXG4gICAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncyAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgICBjb2xvcjogXCIjZTMwNzA3XCIsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBsYWJlbDogXCJDb25zdW1lZCBTYXZpbmdzIGFuZCBJbmNvbWVcIixcclxuICAgICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MsXHJcbiAgICAgICAgICAgICAgY29sb3I6IFwiIzBmNTIwOVwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoY3VycmVudEluY29tZSA+IE1hdGguYWJzKHByZXZpb3VzU2F2aW5ncykgKyBjdXJyZW50RXhwZW5zZXMpIHtcclxuICAgICAgICBzZXRBY3R1YWxQaWUoW1xyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJBY2N1bXVsYXRlZCBTYXZpbmdzXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncyAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgY29sb3I6IFwiIzQ3YjgzZFwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiUHJldmlvdXMgRGVmaWNpdCArIEN1cnJlbnQgRXhwZW5zZXNcIixcclxuICAgICAgICAgICAgYW1vdW50OiBNYXRoLmFicyhwcmV2aW91c1NhdmluZ3MpICsgY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNldEFjdHVhbFBpZShbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIkNvbnN1bWVkIEluY29tZVwiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIkFjY3VtdWxhdGVkIERlZmljaXRcIixcclxuICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjZTMwNzA3XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSwgW3ByZXZpb3VzU2F2aW5ncywgY3VycmVudEluY29tZSwgY3VycmVudEV4cGVuc2VzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoYnVkZ2V0ID49IGN1cnJlbnRFeHBlbnNlcykge1xyXG4gICAgICBzZXRCdWRnZXRQaWUoW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIGxhYmVsOiBcIlNhdmluZ3NcIixcclxuICAgICAgICAgIGFtb3VudDogYnVkZ2V0IC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgY29sb3I6IFwiIzQ3YjgzZFwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgbGFiZWw6IFwiRXhwZW5zZXNcIixcclxuICAgICAgICAgIGFtb3VudDogY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIF0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0QnVkZ2V0UGllKFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJDb25zdW1lZCBCdWRnZXRcIixcclxuICAgICAgICAgIGFtb3VudDogYnVkZ2V0LFxyXG4gICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgbGFiZWw6IFwiRGVmaWNpdFwiLFxyXG4gICAgICAgICAgYW1vdW50OiBNYXRoLmFicyhidWRnZXQgLSBjdXJyZW50RXhwZW5zZXMpLFxyXG4gICAgICAgICAgY29sb3I6IFwiI2UzMDcwN1wiLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIF0pO1xyXG4gICAgfVxyXG4gIH0sIFtidWRnZXQsIGN1cnJlbnRFeHBlbnNlc10pO1xyXG5cclxuICBmdW5jdGlvbiBzYXZlRGF0ZShlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlKTtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9zZXREYXRlYCwge1xyXG4gICAgICBtZXRob2Q6IFwiUFVUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgc3RhcnREYXRlOiBtb21lbnQoc3RhcnREYXRlKS5zdGFydE9mKFwiZGF5XCIpLFxyXG4gICAgICAgIGVuZERhdGU6IG1vbWVudChlbmREYXRlKS5zdGFydE9mKFwiZGF5XCIpLFxyXG4gICAgICB9KSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEgPT0gZmFsc2UpIHtcclxuICAgICAgICAgIFN3YWwuZmlyZShcIkVycm9yXCIsIGBTb21ldGhpbmcgd2VudCB3cm9uZ2AsIFwiZXJyb3JcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgc2V0U2F2ZWRTdGFydChcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuc3RhcnREYXRlKS5zdGFydE9mKFwiZGF5XCIpLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXRTYXZlZEVuZChtb21lbnQoZGF0YS5lbmREYXRlKS5zdGFydE9mKFwiZGF5XCIpLmZvcm1hdChcInl5eXktTU0tRERcIikpO1xyXG4gICAgICAgICAgc2V0U3RhcnREYXRlKFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5zdGFydERhdGUpLnN0YXJ0T2YoXCJkYXlcIikuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldEVuZERhdGUobW9tZW50KGRhdGEuZW5kRGF0ZSkuc3RhcnRPZihcImRheVwiKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPkJ1ZGdldCBUcmFja2VyPC90aXRsZT5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAge3VzZXIuaWQgIT09IG51bGwgPyAoXHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxKdW1ib3Ryb24gY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgICAgICA8aDM+e25hbWV9PC9oMz5cclxuICAgICAgICAgICAgPGgzPntlbWFpbH08L2gzPlxyXG4gICAgICAgICAgICA8aDM+XHJcbiAgICAgICAgICAgICAgQmFsYW5jZTogUGhwIHtjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzfVxyXG4gICAgICAgICAgICA8L2gzPlxyXG4gICAgICAgICAgPC9KdW1ib3Ryb24+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICA8Rm9ybSBvblN1Ym1pdD17KGUpID0+IHNhdmVEYXRlKGUpfT5cclxuICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICAgICAgPENvbCBtZD1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+QnVkZ2V0IERhdGU6PC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImRhdGVcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGVmb3JtYXQ9XCJ5eXl5LU1NLUREXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17c3RhcnREYXRlfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U3RhcnREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgPENvbCBtZD1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+IHRvIDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17ZW5kRGF0ZX1cclxuICAgICAgICAgICAgICAgICAgICBkYXRlZm9ybWF0PVwibXl5eXktTU0tRERcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0RW5kRGF0ZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NvbD5cclxuXHJcbiAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICB7c3RhcnREYXRlICE9IHNhdmVkU3RhcnQgfHwgZW5kRGF0ZSAhPSBzYXZlZEVuZCA/IChcclxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiIGlkPVwic3VibWl0QnRuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICBTZXQgYXMgRGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzdWJtaXRCdG5cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICBTZXQgYXMgRGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICA8Q2FyZCBjbGFzc05hbWU9XCJob21lcGFnZUNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgIHthY3R1YWxQaWUubGFiZWwgPyAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEFsZXJ0Pk5vIFJlY29yZHM8L0FsZXJ0PlxyXG4gICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxQaWVDaGFydFxyXG4gICAgICAgICAgICAgICAgICAgICAgY2hhcnRUeXBlPVwiUGllXCJcclxuICAgICAgICAgICAgICAgICAgICAgIHJhd0RhdGE9e2FjdHVhbFBpZX1cclxuICAgICAgICAgICAgICAgICAgICAgIGxhYmVsS2V5PXtcImxhYmVsXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICBhbW91bnRLZXk9e1wiYW1vdW50XCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJob21lcGFnZVBpZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNT5DdXJyZW50IEluY29tZTogUGhwIHtjdXJyZW50SW5jb21lfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg1PkN1cnJlbnQgRXhwZW5zZXM6IFBocCB7Y3VycmVudEV4cGVuc2VzfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAge2N1cnJlbnRJbmNvbWUgPj0gY3VycmVudEV4cGVuc2VzID8gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg1PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDdXJyZW50IFNhdmluZ3M6IFBocCB7Y3VycmVudEluY29tZSAtIGN1cnJlbnRFeHBlbnNlc31cclxuICAgICAgICAgICAgICAgICAgICAgIDwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgQ3VycmVudCBEZWZpY2l0OiBQaHAge2N1cnJlbnRJbmNvbWUgLSBjdXJyZW50RXhwZW5zZXN9e1wiIFwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIHtwcmV2aW91c1NhdmluZ3MgPj0gMCA/IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5QcmV2aW91cyBTYXZpbmdzOiBQaHAge3ByZXZpb3VzU2F2aW5nc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDU+UHJldmlvdXMgRGVmaWNpdDogUGhwIHtwcmV2aW91c1NhdmluZ3N9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgPENhcmQgY2xhc3NOYW1lPVwiaG9tZXBhZ2VDYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICB7YWN0dWFsUGllLmxhYmVsID8gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxBbGVydD5ObyBSZWNvcmRzPC9BbGVydD5cclxuICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICA8UGllQ2hhcnRcclxuICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0VHlwZT1cIlBpZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICByYXdEYXRhPXtidWRnZXRQaWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICBsYWJlbEtleT17XCJsYWJlbFwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgYW1vdW50S2V5PXtcImFtb3VudFwifVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxDYXJkLkJvZHk+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaG9tZXBhZ2VQaWVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDU+QnVkZ2V0OiBQaHAge2J1ZGdldH08L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNT5FeHBlbnNlczogUGhwIHtjdXJyZW50RXhwZW5zZXN9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICB7YnVkZ2V0ID49IGN1cnJlbnRFeHBlbnNlcyA/IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5TYXZpbmdzOiBQaHAge2J1ZGdldCAtIGN1cnJlbnRFeHBlbnNlc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDU+RGVmaWNpdDogUGhwIHtidWRnZXQgLSBjdXJyZW50RXhwZW5zZXN9IDwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQuQm9keT5cclxuICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICA8Q29sIGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJzdWNjZXNzXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9hZGRcIil9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgQWRkIENhdGVnb3J5XHJcbiAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJzZWNvbmRhcnlcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL3JlY29yZC9hZGRcIil9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgQWRkIFJlY29yZFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApIDogKFxyXG4gICAgICAgIDxMb2dpbiAvPlxyXG4gICAgICApfVxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICApO1xyXG59XHJcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IEZvcm0sIEJ1dHRvbiwgQ2FyZCwgUm93LCBDb2wgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCB7IEdvb2dsZUxvZ2luIH0gZnJvbSBcInJlYWN0LWdvb2dsZS1sb2dpblwiO1xyXG5pbXBvcnQgU3dhbCBmcm9tIFwic3dlZXRhbGVydDJcIjtcclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgVXNlckNvbnRleHQgZnJvbSBcIi4uL1VzZXJDb250ZXh0XCI7XHJcbmltcG9ydCBWaWV3IGZyb20gXCIuLi9jb21wb25lbnRzL1ZpZXdcIjtcclxuaW1wb3J0IEFwcEhlbHBlciBmcm9tIFwiLi4vYXBwLWhlbHBlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gbG9naW4oKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxWaWV3IHRpdGxlPXtcIkxvZ2luXCJ9PlxyXG4gICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1jZW50ZXJcIj5cclxuICAgICAgICA8Q29sIHhzIG1kPVwiNlwiPlxyXG4gICAgICAgICAgPExvZ2luRm9ybSAvPlxyXG4gICAgICAgIDwvQ29sPlxyXG4gICAgICA8L1Jvdz5cclxuICAgIDwvVmlldz5cclxuICApO1xyXG59XHJcblxyXG5jb25zdCBMb2dpbkZvcm0gPSAoKSA9PiB7XHJcbiAgY29uc3QgeyB1c2VyLCBzZXRVc2VyIH0gPSB1c2VDb250ZXh0KFVzZXJDb250ZXh0KTtcclxuXHJcbiAgY29uc3QgW2VtYWlsLCBzZXRFbWFpbF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbcGFzc3dvcmQsIHNldFBhc3N3b3JkXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtpc0FjdGl2ZSwgc2V0SXNBY3RpdmVdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG5cclxuICBmdW5jdGlvbiBhdXRoZW50aWNhdGUoZSkge1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHsgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIgfSxcclxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGVtYWlsOiBlbWFpbCxcclxuICAgICAgICBwYXNzd29yZDogcGFzc3dvcmQsXHJcbiAgICAgIH0pLFxyXG4gICAgfTtcclxuXHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9sb2dpbmAsIG9wdGlvbnMpXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhLmFjY2Vzc1Rva2VuICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInRva2VuXCIsIGRhdGEuYWNjZXNzVG9rZW4pO1xyXG4gICAgICAgICAgc2V0VXNlcih7IGlkOiBkYXRhLl9pZCB9KTtcclxuICAgICAgICAgIFJvdXRlci5wdXNoKFwiL1wiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaWYgKGRhdGEuZXJyb3IgPT09IFwiZG9lcy1ub3QtZXhpc3RcIikge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXCJBdXRoZW50aWNhdGlvbiBGYWlsZWRcIiwgXCJVc2VyIGRvZXMgbm90IGV4aXN0LlwiLCBcImVycm9yXCIpO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChkYXRhLmVycm9yID09PSBcImxvZ2luLXR5cGUtZXJyb3JcIikge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJMb2dpbiBUeXBlIEVycm9yXCIsXHJcbiAgICAgICAgICAgICAgXCJZb3UgbWF5IGhhdmUgcmVnaXN0ZXJlZCB0aHJvdWdoIGEgZGlmZmVyZW50IGxvZ2luIHByb2NlZHVyZSwgdHJ5IGFuIGFsdGVybmF0aXZlIGxvZ2luIHByb2NlZHVyZVwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChkYXRhLmVycm9yID09PSBcImluY29ycmVjdC1wYXNzd29yZFwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcclxuICAgICAgICAgICAgICBcIkF1dGhlbnRpY2F0aW9uIEZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgIFwicGFzc3dvcmQgaXMgaW5jb3JyZWN0XCIsXHJcbiAgICAgICAgICAgICAgXCJlcnJvclwiXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0IGF1bnRoZW50aWNhdGVHb29nbGVUb2tlbiA9IChyZXNwb25zZSkgPT4ge1xyXG4gICAgY29uc3QgcGF5bG9hZCA9IHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRva2VuSWQ6IHJlc3BvbnNlLnRva2VuSWQgfSksXHJcbiAgICB9O1xyXG5cclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL3ZlcmlmeS1nb29nbGUtaWQtdG9rZW5gLCBwYXlsb2FkKVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIGlmICh0eXBlb2YgZGF0YS5hY2Nlc3NUb2tlbiAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ0b2tlblwiLCBkYXRhLmFjY2Vzc1Rva2VuKTtcclxuICAgICAgICAgIHNldFVzZXIoeyBpZDogZGF0YS5faWQgfSk7XHJcbiAgICAgICAgICBSb3V0ZXIucHVzaChcIi9cIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmICgoZGF0YS5lcnJvciA9IFwiZ29vZ2xlLWF1dGgtZXJyb3JcIikpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFxyXG4gICAgICAgICAgICAgIFwiR29vZ2xlIEF1dGggRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIkdvb2dsZSBhdXRoZW50aWNhdGlvbiBwcm9jZWR1cmUgZmFpbGVkLlwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICgoZGF0YS5lcnJvciA9IFwibG9naW4tdHlwZS1lcnJvclwiKSkge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJMb2dpbiBUeXBlIEVycm9yXCIsXHJcbiAgICAgICAgICAgICAgXCJZb3UgbWF5IGhhdmUgcmVnaXN0ZXJlZCB0aHJvdWdoIGEgZGlmZmVyZW50IGxvZ2luIHByb2NlZHVyZS5cIixcclxuICAgICAgICAgICAgICBcImVycm9yXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoZW1haWwgIT09IFwiXCIgJiYgcGFzc3dvcmQgIT09IFwiXCIpIHtcclxuICAgICAgc2V0SXNBY3RpdmUodHJ1ZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRJc0FjdGl2ZShmYWxzZSk7XHJcbiAgICB9XHJcbiAgfSwgW2VtYWlsLCBwYXNzd29yZF0pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPENhcmQ+XHJcbiAgICAgIDxDYXJkLkhlYWRlcj5Mb2dpbiBkZXRhaWxzPC9DYXJkLkhlYWRlcj5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8SGVhZD5cclxuICAgICAgICAgIDx0aXRsZT5CdWRnZXQgVHJhY2tlcjwvdGl0bGU+XHJcbiAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gYXV0aGVudGljYXRlKGUpfT5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInVzZXJFbWFpbFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbCBhZGRyZXNzPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgdHlwZT1cImVtYWlsXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIGVtYWlsXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17ZW1haWx9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRFbWFpbChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuXHJcbiAgICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJwYXNzd29yZFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5QYXNzd29yZDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJQYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgdmFsdWU9e3Bhc3N3b3JkfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0UGFzc3dvcmQoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1jZW50ZXIgcHgtM1wiPlxyXG4gICAgICAgICAgICB7aXNBY3RpdmUgPyAoXHJcbiAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9XCJiZy1wcmltYXJ5IGJ0bi1ibG9ja1wiIHR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgIDxCdXR0b24gY2xhc3NOYW1lPVwiYmctcHJpbWFyeSBidG4tYmxvY2tcIiB0eXBlPVwic3VibWl0XCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgKX1cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgIDwvRm9ybT5cclxuXHJcbiAgICAgICAgPEdvb2dsZUxvZ2luXHJcbiAgICAgICAgICBjbGllbnRJZD1cIjY1NjU5MzY2MjU2OS0yNGd1ZnU0NGV2ajR1anFzM2swMTEzcmdydXJxMmJucC5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbVwiXHJcbiAgICAgICAgICBidXR0b25UZXh0PVwiTG9naW5cIlxyXG4gICAgICAgICAgb25TdWNjZXNzPXthdW50aGVudGljYXRlR29vZ2xlVG9rZW59XHJcbiAgICAgICAgICBvbkZhaWx1cmU9e2F1bnRoZW50aWNhdGVHb29nbGVUb2tlbn1cclxuICAgICAgICAgIGNvb2tpZVBvbGljeT17XCJzaW5nbGVfaG9zdF9vcmlnaW5cIn1cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCJcclxuICAgICAgICAvPlxyXG4gICAgICA8L0NhcmQuQm9keT5cclxuICAgIDwvQ2FyZD5cclxuICApO1xyXG59O1xyXG4iLCIvLyBFeHBvcnRzXG5tb2R1bGUuZXhwb3J0cyA9IHtcblx0XCJjb250YWluZXJcIjogXCJIb21lX2NvbnRhaW5lcl9fMUVjc1VcIixcblx0XCJtYWluXCI6IFwiSG9tZV9tYWluX18xeDhnQ1wiLFxuXHRcImZvb3RlclwiOiBcIkhvbWVfZm9vdGVyX18xV2RoRFwiLFxuXHRcInRpdGxlXCI6IFwiSG9tZV90aXRsZV9fM0RqUjdcIixcblx0XCJkZXNjcmlwdGlvblwiOiBcIkhvbWVfZGVzY3JpcHRpb25fXzE3WjRGXCIsXG5cdFwiY29kZVwiOiBcIkhvbWVfY29kZV9fYXh4MllcIixcblx0XCJncmlkXCI6IFwiSG9tZV9ncmlkX18yRWkyRlwiLFxuXHRcImNhcmRcIjogXCJIb21lX2NhcmRfXzJTZHRCXCIsXG5cdFwibG9nb1wiOiBcIkhvbWVfbG9nb19fMVlickhcIixcblx0XCJzdGFja2VkQmFyXCI6IFwiSG9tZV9zdGFja2VkQmFyX18zVU8xbFwiLFxuXHRcImhvbWVwYWdlQ2FyZFwiOiBcIkhvbWVfaG9tZXBhZ2VDYXJkX18xdmtMTlwiXG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9tZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1ib290c3RyYXBcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtY2hhcnRqcy0yXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWdvb2dsZS1sb2dpblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic3dlZXRhbGVydDJcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==