module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/trend.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./UserContext.js":
/*!************************!*\
  !*** ./UserContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 // creates a Context Object

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "./helpers/categoryRecord.js":
/*!***********************************!*\
  !*** ./helpers/categoryRecord.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return categoryRecord; });
// Function for converting string data from the enpoints to numbers
function categoryRecord(categories, records) {
  const combinedCatRec = records.map(record => {
    const category = categories.find(category => category._id == record.categoryId);

    if (category == undefined) {
      record.categoryType = "uncategorized";
      record.categoryName = "uncategorized";
    } else {
      record.categoryName = category.name;
      record.categoryType = category.type;
    }

    return record;
  });
  return combinedCatRec;
}

/***/ }),

/***/ "./pages/trend.js":
/*!************************!*\
  !*** ./pages/trend.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return record; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _helpers_categoryRecord__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../helpers/categoryRecord */ "./helpers/categoryRecord.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_9__);

var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\trend.js";









function record() {
  const {
    0: startDate,
    1: setStartDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_8___default()().subtract(1, "years").format("yyyy-MM-DD"));
  const {
    0: endDate,
    1: setEndDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_8___default()().format("yyyy-MM-DD"));
  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: updateDates,
    1: setupdateDates
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const {
    0: trendData,
    1: setTrendData
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({});
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:4000/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      let combinedCatReg = Object(_helpers_categoryRecord__WEBPACK_IMPORTED_MODULE_7__["default"])(data.categories, data.records);
      console.log(combinedCatReg);
      setRecords(combinedCatReg);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (startDate <= endDate) {
      if (records.length > 0) {
        let dateBefore = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).subtract(1, "day");
        let dateAfter = moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).add(1, "day");
        let filteredRecords = records.filter(record => {
          let updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
        });
        console.log(filteredRecords);
        let oldRecords = records.filter(record => {
          let updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isSameOrBefore(dateBefore, "day");
        });
        let previousSavings = oldRecords.reduce((total, record) => {
          return total + record.amount;
        }, 0);
        let startMonth = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).startOf("month");
        let labelMonth = startMonth;
        let months = [];
        let incomes = [];
        let expenses = [];
        let monthlySavings = [];
        let accumulatedSavings = [];

        while (moment__WEBPACK_IMPORTED_MODULE_8___default()(labelMonth).isSameOrBefore(endDate, "month")) {
          months.push(moment__WEBPACK_IMPORTED_MODULE_8___default()(labelMonth).format("MMM-YYYY"));
          incomes.push(0);
          expenses.push(0);
          monthlySavings.push(0);
          accumulatedSavings.push(previousSavings);
          labelMonth = moment__WEBPACK_IMPORTED_MODULE_8___default()(labelMonth).add(1, "month");
        }

        filteredRecords.map(record => {
          console.log(record.updatedOn);
          let recordMonth = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("month");
          console.log(recordMonth);
          let index = moment__WEBPACK_IMPORTED_MODULE_8___default()(recordMonth).diff(moment__WEBPACK_IMPORTED_MODULE_8___default()(startMonth), "month", true);
          console.log(index);

          if (record.categoryType == "income") {
            incomes[index] += record.amount;
          } else {
            expenses[index] -= record.amount;
          }

          monthlySavings[index] = incomes[index] - expenses[index];
          let i;

          for (i = index; i < accumulatedSavings.length; i++) {
            console.log(i);
            console.log(accumulatedSavings[i]);
            accumulatedSavings[i] += record.amount;
          }
        });
        console.log(months);
        console.log(incomes);
        console.log(expenses);
        console.log(monthlySavings);
        setTrendData({
          labels: months,
          datasets: [{
            label: "Income",
            data: incomes,
            fill: false,
            borderColor: "#498ae6"
          }, {
            label: "Expenses",
            data: expenses,
            fill: false,
            borderColor: "#f56f36"
          }, {
            label: "Monthly Savings",
            data: monthlySavings,
            fill: false,
            borderColor: "#47b83d"
          }, {
            label: "Accumulated Savings",
            data: accumulatedSavings,
            fill: false,
            borderColor: "#0f5209"
          }]
        });
      }
    }
  }, [startDate, endDate, records]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Monthly Trend"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 138,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
        className: "justify-content-center  my-5",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          md: "auto",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Date:"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 142,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "date",
            value: startDate,
            onChange: e => setStartDate(e.target.value)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 146,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          md: "auto",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: " to "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 153,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 152,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "date",
            value: endDate,
            onChange: e => setEndDate(e.target.value)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 156,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 155,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 141,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_9__["Line"], {
        data: trendData
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 163,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 140,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 136,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-chartjs-2");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vaGVscGVycy9jYXRlZ29yeVJlY29yZC5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy90cmVuZC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb21lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L2hlYWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtYm9vdHN0cmFwXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtY2hhcnRqcy0yXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic3dlZXRhbGVydDJcIiJdLCJuYW1lcyI6WyJVc2VyQ29udGV4dCIsIlJlYWN0IiwiY3JlYXRlQ29udGV4dCIsIlVzZXJQcm92aWRlciIsIlByb3ZpZGVyIiwiY2F0ZWdvcnlSZWNvcmQiLCJjYXRlZ29yaWVzIiwicmVjb3JkcyIsImNvbWJpbmVkQ2F0UmVjIiwibWFwIiwicmVjb3JkIiwiY2F0ZWdvcnkiLCJmaW5kIiwiX2lkIiwiY2F0ZWdvcnlJZCIsInVuZGVmaW5lZCIsImNhdGVnb3J5VHlwZSIsImNhdGVnb3J5TmFtZSIsIm5hbWUiLCJ0eXBlIiwic3RhcnREYXRlIiwic2V0U3RhcnREYXRlIiwidXNlU3RhdGUiLCJtb21lbnQiLCJzdWJ0cmFjdCIsImZvcm1hdCIsImVuZERhdGUiLCJzZXRFbmREYXRlIiwic2V0UmVjb3JkcyIsInVwZGF0ZURhdGVzIiwic2V0dXBkYXRlRGF0ZXMiLCJ0cmVuZERhdGEiLCJzZXRUcmVuZERhdGEiLCJ1c2VFZmZlY3QiLCJmZXRjaCIsImhlYWRlcnMiLCJBdXRob3JpemF0aW9uIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNvbWJpbmVkQ2F0UmVnIiwiY29uc29sZSIsImxvZyIsImxlbmd0aCIsImRhdGVCZWZvcmUiLCJkYXRlQWZ0ZXIiLCJhZGQiLCJmaWx0ZXJlZFJlY29yZHMiLCJmaWx0ZXIiLCJ1cGRhdGVkT24iLCJzdGFydE9mIiwiaXNCZXR3ZWVuIiwib2xkUmVjb3JkcyIsImlzU2FtZU9yQmVmb3JlIiwicHJldmlvdXNTYXZpbmdzIiwicmVkdWNlIiwidG90YWwiLCJhbW91bnQiLCJzdGFydE1vbnRoIiwibGFiZWxNb250aCIsIm1vbnRocyIsImluY29tZXMiLCJleHBlbnNlcyIsIm1vbnRobHlTYXZpbmdzIiwiYWNjdW11bGF0ZWRTYXZpbmdzIiwicHVzaCIsInJlY29yZE1vbnRoIiwiaW5kZXgiLCJkaWZmIiwiaSIsImxhYmVscyIsImRhdGFzZXRzIiwibGFiZWwiLCJmaWxsIiwiYm9yZGVyQ29sb3IiLCJlIiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFBQTtBQUFBO0FBQUE7Q0FFQTs7QUFDQSxNQUFNQSxXQUFXLGdCQUFHQyw0Q0FBSyxDQUFDQyxhQUFOLEVBQXBCO0FBRU8sTUFBTUMsWUFBWSxHQUFHSCxXQUFXLENBQUNJLFFBQWpDO0FBRVFKLDBFQUFmLEU7Ozs7Ozs7Ozs7OztBQ1BBO0FBQUE7QUFBQTtBQUVlLFNBQVNLLGNBQVQsQ0FBd0JDLFVBQXhCLEVBQW9DQyxPQUFwQyxFQUE0QztBQUUxRCxRQUFNQyxjQUFjLEdBQUdELE9BQU8sQ0FBQ0UsR0FBUixDQUFZQyxNQUFNLElBQUc7QUFDM0MsVUFBTUMsUUFBUSxHQUFHTCxVQUFVLENBQUNNLElBQVgsQ0FBZ0JELFFBQVEsSUFBSUEsUUFBUSxDQUFDRSxHQUFULElBQWdCSCxNQUFNLENBQUNJLFVBQW5ELENBQWpCOztBQUVBLFFBQUdILFFBQVEsSUFBRUksU0FBYixFQUF1QjtBQUN0QkwsWUFBTSxDQUFDTSxZQUFQLEdBQXNCLGVBQXRCO0FBQ0FOLFlBQU0sQ0FBQ08sWUFBUCxHQUFzQixlQUF0QjtBQUNBLEtBSEQsTUFHSztBQUNKUCxZQUFNLENBQUNPLFlBQVAsR0FBc0JOLFFBQVEsQ0FBQ08sSUFBL0I7QUFDQVIsWUFBTSxDQUFDTSxZQUFQLEdBQXNCTCxRQUFRLENBQUNRLElBQS9CO0FBQ0E7O0FBRUQsV0FBT1QsTUFBUDtBQUNBLEdBWnNCLENBQXZCO0FBY0EsU0FBT0YsY0FBUDtBQUVBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwQkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0UsTUFBVCxHQUFrQjtBQUMvQixRQUFNO0FBQUEsT0FBQ1UsU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBNEJDLHNEQUFRLENBQ3hDQyw2Q0FBTSxHQUFHQyxRQUFULENBQWtCLENBQWxCLEVBQXFCLE9BQXJCLEVBQThCQyxNQUE5QixDQUFxQyxZQUFyQyxDQUR3QyxDQUExQztBQUdBLFFBQU07QUFBQSxPQUFDQyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3Qkwsc0RBQVEsQ0FBQ0MsNkNBQU0sR0FBR0UsTUFBVCxDQUFnQixZQUFoQixDQUFELENBQXRDO0FBQ0EsUUFBTTtBQUFBLE9BQUNsQixPQUFEO0FBQUEsT0FBVXFCO0FBQVYsTUFBd0JOLHNEQUFRLENBQUMsRUFBRCxDQUF0QztBQUNBLFFBQU07QUFBQSxPQUFDTyxXQUFEO0FBQUEsT0FBY0M7QUFBZCxNQUFnQ1Isc0RBQVEsQ0FBQyxLQUFELENBQTlDO0FBQ0EsUUFBTTtBQUFBLE9BQUNTLFNBQUQ7QUFBQSxPQUFZQztBQUFaLE1BQTRCVixzREFBUSxDQUFDLEVBQUQsQ0FBMUM7QUFFQVcseURBQVMsQ0FBQyxNQUFNO0FBQ2RDLFNBQUssQ0FBQyx5Q0FBRCxFQUE0QztBQUMvQ0MsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRHNDLEtBQTVDLENBQUwsQ0FLR0MsSUFMSCxDQUtTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUxqQixFQU1HRixJQU5ILENBTVNHLElBQUQsSUFBVTtBQUNkLFVBQUlDLGNBQWMsR0FBR3RDLHVFQUFjLENBQUNxQyxJQUFJLENBQUNwQyxVQUFOLEVBQWtCb0MsSUFBSSxDQUFDbkMsT0FBdkIsQ0FBbkM7QUFDQXFDLGFBQU8sQ0FBQ0MsR0FBUixDQUFZRixjQUFaO0FBQ0FmLGdCQUFVLENBQUNlLGNBQUQsQ0FBVjtBQUNELEtBVkg7QUFXRCxHQVpRLEVBWU4sRUFaTSxDQUFUO0FBY0FWLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUliLFNBQVMsSUFBSU0sT0FBakIsRUFBMEI7QUFDeEIsVUFBSW5CLE9BQU8sQ0FBQ3VDLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDdEIsWUFBSUMsVUFBVSxHQUFHeEIsNkNBQU0sQ0FBQ0gsU0FBRCxDQUFOLENBQWtCSSxRQUFsQixDQUEyQixDQUEzQixFQUE4QixLQUE5QixDQUFqQjtBQUNBLFlBQUl3QixTQUFTLEdBQUd6Qiw2Q0FBTSxDQUFDRyxPQUFELENBQU4sQ0FBZ0J1QixHQUFoQixDQUFvQixDQUFwQixFQUF1QixLQUF2QixDQUFoQjtBQUVBLFlBQUlDLGVBQWUsR0FBRzNDLE9BQU8sQ0FBQzRDLE1BQVIsQ0FBZ0J6QyxNQUFELElBQVk7QUFDL0MsY0FBSTBDLFNBQVMsR0FBRzdCLDZDQUFNLENBQUNiLE1BQU0sQ0FBQzBDLFNBQVIsQ0FBTixDQUF5QkMsT0FBekIsQ0FBaUMsTUFBakMsQ0FBaEI7QUFDQSxpQkFBTzlCLDZDQUFNLENBQUM2QixTQUFELENBQU4sQ0FBa0JFLFNBQWxCLENBQTRCUCxVQUE1QixFQUF3Q0MsU0FBeEMsRUFBbUQsS0FBbkQsQ0FBUDtBQUNELFNBSHFCLENBQXRCO0FBSUFKLGVBQU8sQ0FBQ0MsR0FBUixDQUFZSyxlQUFaO0FBRUEsWUFBSUssVUFBVSxHQUFHaEQsT0FBTyxDQUFDNEMsTUFBUixDQUFnQnpDLE1BQUQsSUFBWTtBQUMxQyxjQUFJMEMsU0FBUyxHQUFHN0IsNkNBQU0sQ0FBQ2IsTUFBTSxDQUFDMEMsU0FBUixDQUFOLENBQXlCQyxPQUF6QixDQUFpQyxNQUFqQyxDQUFoQjtBQUNBLGlCQUFPOUIsNkNBQU0sQ0FBQzZCLFNBQUQsQ0FBTixDQUFrQkksY0FBbEIsQ0FBaUNULFVBQWpDLEVBQTZDLEtBQTdDLENBQVA7QUFDRCxTQUhnQixDQUFqQjtBQUtBLFlBQUlVLGVBQWUsR0FBR0YsVUFBVSxDQUFDRyxNQUFYLENBQWtCLENBQUNDLEtBQUQsRUFBUWpELE1BQVIsS0FBbUI7QUFDekQsaUJBQU9pRCxLQUFLLEdBQUdqRCxNQUFNLENBQUNrRCxNQUF0QjtBQUNELFNBRnFCLEVBRW5CLENBRm1CLENBQXRCO0FBSUEsWUFBSUMsVUFBVSxHQUFHdEMsNkNBQU0sQ0FBQ0gsU0FBRCxDQUFOLENBQWtCaUMsT0FBbEIsQ0FBMEIsT0FBMUIsQ0FBakI7QUFFQSxZQUFJUyxVQUFVLEdBQUdELFVBQWpCO0FBQ0EsWUFBSUUsTUFBTSxHQUFHLEVBQWI7QUFDQSxZQUFJQyxPQUFPLEdBQUcsRUFBZDtBQUNBLFlBQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0EsWUFBSUMsY0FBYyxHQUFHLEVBQXJCO0FBQ0EsWUFBSUMsa0JBQWtCLEdBQUcsRUFBekI7O0FBQ0EsZUFBTzVDLDZDQUFNLENBQUN1QyxVQUFELENBQU4sQ0FBbUJOLGNBQW5CLENBQWtDOUIsT0FBbEMsRUFBMkMsT0FBM0MsQ0FBUCxFQUE0RDtBQUMxRHFDLGdCQUFNLENBQUNLLElBQVAsQ0FBWTdDLDZDQUFNLENBQUN1QyxVQUFELENBQU4sQ0FBbUJyQyxNQUFuQixDQUEwQixVQUExQixDQUFaO0FBQ0F1QyxpQkFBTyxDQUFDSSxJQUFSLENBQWEsQ0FBYjtBQUNBSCxrQkFBUSxDQUFDRyxJQUFULENBQWMsQ0FBZDtBQUNBRix3QkFBYyxDQUFDRSxJQUFmLENBQW9CLENBQXBCO0FBQ0FELDRCQUFrQixDQUFDQyxJQUFuQixDQUF3QlgsZUFBeEI7QUFDQUssb0JBQVUsR0FBR3ZDLDZDQUFNLENBQUN1QyxVQUFELENBQU4sQ0FBbUJiLEdBQW5CLENBQXVCLENBQXZCLEVBQTBCLE9BQTFCLENBQWI7QUFDRDs7QUFFREMsdUJBQWUsQ0FBQ3pDLEdBQWhCLENBQXFCQyxNQUFELElBQVk7QUFDOUJrQyxpQkFBTyxDQUFDQyxHQUFSLENBQVluQyxNQUFNLENBQUMwQyxTQUFuQjtBQUNBLGNBQUlpQixXQUFXLEdBQUc5Qyw2Q0FBTSxDQUFDYixNQUFNLENBQUMwQyxTQUFSLENBQU4sQ0FBeUJDLE9BQXpCLENBQWlDLE9BQWpDLENBQWxCO0FBQ0FULGlCQUFPLENBQUNDLEdBQVIsQ0FBWXdCLFdBQVo7QUFDQSxjQUFJQyxLQUFLLEdBQUcvQyw2Q0FBTSxDQUFDOEMsV0FBRCxDQUFOLENBQW9CRSxJQUFwQixDQUNWaEQsNkNBQU0sQ0FBQ3NDLFVBQUQsQ0FESSxFQUVWLE9BRlUsRUFHVixJQUhVLENBQVo7QUFLQWpCLGlCQUFPLENBQUNDLEdBQVIsQ0FBWXlCLEtBQVo7O0FBQ0EsY0FBSTVELE1BQU0sQ0FBQ00sWUFBUCxJQUF1QixRQUEzQixFQUFxQztBQUNuQ2dELG1CQUFPLENBQUNNLEtBQUQsQ0FBUCxJQUFrQjVELE1BQU0sQ0FBQ2tELE1BQXpCO0FBQ0QsV0FGRCxNQUVPO0FBQ0xLLG9CQUFRLENBQUNLLEtBQUQsQ0FBUixJQUFtQjVELE1BQU0sQ0FBQ2tELE1BQTFCO0FBQ0Q7O0FBQ0RNLHdCQUFjLENBQUNJLEtBQUQsQ0FBZCxHQUF3Qk4sT0FBTyxDQUFDTSxLQUFELENBQVAsR0FBaUJMLFFBQVEsQ0FBQ0ssS0FBRCxDQUFqRDtBQUVBLGNBQUlFLENBQUo7O0FBQ0EsZUFBS0EsQ0FBQyxHQUFHRixLQUFULEVBQWdCRSxDQUFDLEdBQUdMLGtCQUFrQixDQUFDckIsTUFBdkMsRUFBK0MwQixDQUFDLEVBQWhELEVBQW9EO0FBQ2xENUIsbUJBQU8sQ0FBQ0MsR0FBUixDQUFZMkIsQ0FBWjtBQUNBNUIsbUJBQU8sQ0FBQ0MsR0FBUixDQUFZc0Isa0JBQWtCLENBQUNLLENBQUQsQ0FBOUI7QUFDQUwsOEJBQWtCLENBQUNLLENBQUQsQ0FBbEIsSUFBeUI5RCxNQUFNLENBQUNrRCxNQUFoQztBQUNEO0FBQ0YsU0F2QkQ7QUF5QkFoQixlQUFPLENBQUNDLEdBQVIsQ0FBWWtCLE1BQVo7QUFDQW5CLGVBQU8sQ0FBQ0MsR0FBUixDQUFZbUIsT0FBWjtBQUNBcEIsZUFBTyxDQUFDQyxHQUFSLENBQVlvQixRQUFaO0FBQ0FyQixlQUFPLENBQUNDLEdBQVIsQ0FBWXFCLGNBQVo7QUFFQWxDLG9CQUFZLENBQUM7QUFDWHlDLGdCQUFNLEVBQUVWLE1BREc7QUFFWFcsa0JBQVEsRUFBRSxDQUNSO0FBQ0VDLGlCQUFLLEVBQUUsUUFEVDtBQUVFakMsZ0JBQUksRUFBRXNCLE9BRlI7QUFHRVksZ0JBQUksRUFBRSxLQUhSO0FBSUVDLHVCQUFXLEVBQUU7QUFKZixXQURRLEVBT1I7QUFDRUYsaUJBQUssRUFBRSxVQURUO0FBRUVqQyxnQkFBSSxFQUFFdUIsUUFGUjtBQUdFVyxnQkFBSSxFQUFFLEtBSFI7QUFJRUMsdUJBQVcsRUFBRTtBQUpmLFdBUFEsRUFhUjtBQUNFRixpQkFBSyxFQUFFLGlCQURUO0FBRUVqQyxnQkFBSSxFQUFFd0IsY0FGUjtBQUdFVSxnQkFBSSxFQUFFLEtBSFI7QUFJRUMsdUJBQVcsRUFBRTtBQUpmLFdBYlEsRUFtQlI7QUFDRUYsaUJBQUssRUFBRSxxQkFEVDtBQUVFakMsZ0JBQUksRUFBRXlCLGtCQUZSO0FBR0VTLGdCQUFJLEVBQUUsS0FIUjtBQUlFQyx1QkFBVyxFQUFFO0FBSmYsV0FuQlE7QUFGQyxTQUFELENBQVo7QUE2QkQ7QUFDRjtBQUNGLEdBbkdRLEVBbUdOLENBQUN6RCxTQUFELEVBQVlNLE9BQVosRUFBcUJuQixPQUFyQixDQW5HTSxDQUFUO0FBcUdBLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxvREFBRDtBQUFBLDhCQUNFLHFFQUFDLG1EQUFEO0FBQUssaUJBQVMsRUFBQyw4QkFBZjtBQUFBLGdDQUNFLHFFQUFDLG1EQUFEO0FBQUssWUFBRSxFQUFDLE1BQVI7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBSUUscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxnQkFBSSxFQUFDLE1BRFA7QUFFRSxpQkFBSyxFQUFFYSxTQUZUO0FBR0Usb0JBQVEsRUFBRzBELENBQUQsSUFBT3pELFlBQVksQ0FBQ3lELENBQUMsQ0FBQ0MsTUFBRixDQUFTQyxLQUFWO0FBSC9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUpGLGVBV0UscUVBQUMsbURBQUQ7QUFBSyxZQUFFLEVBQUMsTUFBUjtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBWEYsZUFjRSxxRUFBQyxtREFBRDtBQUFBLGlDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFJLEVBQUMsTUFEUDtBQUVFLGlCQUFLLEVBQUV0RCxPQUZUO0FBR0Usb0JBQVEsRUFBR29ELENBQUQsSUFBT25ELFVBQVUsQ0FBQ21ELENBQUMsQ0FBQ0MsTUFBRixDQUFTQyxLQUFWO0FBSDdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBdUJFLHFFQUFDLG9EQUFEO0FBQU0sWUFBSSxFQUFFakQ7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBdkJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBZ0NELEM7Ozs7Ozs7Ozs7O0FDdEtELG1DOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLHdDIiwiZmlsZSI6InBhZ2VzL3RyZW5kLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy90cmVuZC5qc1wiKTtcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcblxyXG4vLyBjcmVhdGVzIGEgQ29udGV4dCBPYmplY3RcclxuY29uc3QgVXNlckNvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KCk7XHJcblxyXG5leHBvcnQgY29uc3QgVXNlclByb3ZpZGVyID0gVXNlckNvbnRleHQuUHJvdmlkZXJcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFVzZXJDb250ZXh0OyIsIi8vIEZ1bmN0aW9uIGZvciBjb252ZXJ0aW5nIHN0cmluZyBkYXRhIGZyb20gdGhlIGVucG9pbnRzIHRvIG51bWJlcnNcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGNhdGVnb3J5UmVjb3JkKGNhdGVnb3JpZXMsIHJlY29yZHMpe1xyXG5cclxuXHRjb25zdCBjb21iaW5lZENhdFJlYyA9IHJlY29yZHMubWFwKHJlY29yZCA9PntcclxuXHRcdGNvbnN0IGNhdGVnb3J5ID0gY2F0ZWdvcmllcy5maW5kKGNhdGVnb3J5ID0+IGNhdGVnb3J5Ll9pZCA9PSByZWNvcmQuY2F0ZWdvcnlJZClcclxuXHJcblx0XHRpZihjYXRlZ29yeT09dW5kZWZpbmVkKXtcclxuXHRcdFx0cmVjb3JkLmNhdGVnb3J5VHlwZSA9IFwidW5jYXRlZ29yaXplZFwiXHJcblx0XHRcdHJlY29yZC5jYXRlZ29yeU5hbWUgPSBcInVuY2F0ZWdvcml6ZWRcIlxyXG5cdFx0fWVsc2V7XHJcblx0XHRcdHJlY29yZC5jYXRlZ29yeU5hbWUgPSBjYXRlZ29yeS5uYW1lXHJcblx0XHRcdHJlY29yZC5jYXRlZ29yeVR5cGUgPSBjYXRlZ29yeS50eXBlXHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHJlY29yZFxyXG5cdH0pXHJcblxyXG5cdHJldHVybiBjb21iaW5lZENhdFJlY1xyXG5cclxufSIsImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IEZvcm0sIEJ1dHRvbiwgVGFibGUsIFJvdywgQ29sLCBBbGVydCB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgU3dhbCBmcm9tIFwic3dlZXRhbGVydDJcIjtcclxuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gXCIuLi9Vc2VyQ29udGV4dFwiO1xyXG5pbXBvcnQgY2F0ZWdvcnlSZWNvcmQgZnJvbSBcIi4uL2hlbHBlcnMvY2F0ZWdvcnlSZWNvcmRcIjtcclxuaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XHJcbmltcG9ydCB7IExpbmUgfSBmcm9tIFwicmVhY3QtY2hhcnRqcy0yXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiByZWNvcmQoKSB7XHJcbiAgY29uc3QgW3N0YXJ0RGF0ZSwgc2V0U3RhcnREYXRlXSA9IHVzZVN0YXRlKFxyXG4gICAgbW9tZW50KCkuc3VidHJhY3QoMSwgXCJ5ZWFyc1wiKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgKTtcclxuICBjb25zdCBbZW5kRGF0ZSwgc2V0RW5kRGF0ZV0gPSB1c2VTdGF0ZShtb21lbnQoKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpKTtcclxuICBjb25zdCBbcmVjb3Jkcywgc2V0UmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW3VwZGF0ZURhdGVzLCBzZXR1cGRhdGVEYXRlc10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW3RyZW5kRGF0YSwgc2V0VHJlbmREYXRhXSA9IHVzZVN0YXRlKHt9KTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9kZXRhaWxzXCIsIHtcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgIH0sXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIGxldCBjb21iaW5lZENhdFJlZyA9IGNhdGVnb3J5UmVjb3JkKGRhdGEuY2F0ZWdvcmllcywgZGF0YS5yZWNvcmRzKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhjb21iaW5lZENhdFJlZyk7XHJcbiAgICAgICAgc2V0UmVjb3Jkcyhjb21iaW5lZENhdFJlZyk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChzdGFydERhdGUgPD0gZW5kRGF0ZSkge1xyXG4gICAgICBpZiAocmVjb3Jkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgbGV0IGRhdGVCZWZvcmUgPSBtb21lbnQoc3RhcnREYXRlKS5zdWJ0cmFjdCgxLCBcImRheVwiKTtcclxuICAgICAgICBsZXQgZGF0ZUFmdGVyID0gbW9tZW50KGVuZERhdGUpLmFkZCgxLCBcImRheVwiKTtcclxuXHJcbiAgICAgICAgbGV0IGZpbHRlcmVkUmVjb3JkcyA9IHJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgIGxldCB1cGRhdGVkT24gPSBtb21lbnQocmVjb3JkLnVwZGF0ZWRPbikuc3RhcnRPZihcImRheXNcIik7XHJcbiAgICAgICAgICByZXR1cm4gbW9tZW50KHVwZGF0ZWRPbikuaXNCZXR3ZWVuKGRhdGVCZWZvcmUsIGRhdGVBZnRlciwgXCJkYXlcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc29sZS5sb2coZmlsdGVyZWRSZWNvcmRzKTtcclxuXHJcbiAgICAgICAgbGV0IG9sZFJlY29yZHMgPSByZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJkYXlzXCIpO1xyXG4gICAgICAgICAgcmV0dXJuIG1vbWVudCh1cGRhdGVkT24pLmlzU2FtZU9yQmVmb3JlKGRhdGVCZWZvcmUsIFwiZGF5XCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBsZXQgcHJldmlvdXNTYXZpbmdzID0gb2xkUmVjb3Jkcy5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICAgIHJldHVybiB0b3RhbCArIHJlY29yZC5hbW91bnQ7XHJcbiAgICAgICAgfSwgMCk7XHJcblxyXG4gICAgICAgIGxldCBzdGFydE1vbnRoID0gbW9tZW50KHN0YXJ0RGF0ZSkuc3RhcnRPZihcIm1vbnRoXCIpO1xyXG5cclxuICAgICAgICBsZXQgbGFiZWxNb250aCA9IHN0YXJ0TW9udGg7XHJcbiAgICAgICAgbGV0IG1vbnRocyA9IFtdO1xyXG4gICAgICAgIGxldCBpbmNvbWVzID0gW107XHJcbiAgICAgICAgbGV0IGV4cGVuc2VzID0gW107XHJcbiAgICAgICAgbGV0IG1vbnRobHlTYXZpbmdzID0gW107XHJcbiAgICAgICAgbGV0IGFjY3VtdWxhdGVkU2F2aW5ncyA9IFtdO1xyXG4gICAgICAgIHdoaWxlIChtb21lbnQobGFiZWxNb250aCkuaXNTYW1lT3JCZWZvcmUoZW5kRGF0ZSwgXCJtb250aFwiKSkge1xyXG4gICAgICAgICAgbW9udGhzLnB1c2gobW9tZW50KGxhYmVsTW9udGgpLmZvcm1hdChcIk1NTS1ZWVlZXCIpKTtcclxuICAgICAgICAgIGluY29tZXMucHVzaCgwKTtcclxuICAgICAgICAgIGV4cGVuc2VzLnB1c2goMCk7XHJcbiAgICAgICAgICBtb250aGx5U2F2aW5ncy5wdXNoKDApO1xyXG4gICAgICAgICAgYWNjdW11bGF0ZWRTYXZpbmdzLnB1c2gocHJldmlvdXNTYXZpbmdzKTtcclxuICAgICAgICAgIGxhYmVsTW9udGggPSBtb21lbnQobGFiZWxNb250aCkuYWRkKDEsIFwibW9udGhcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmaWx0ZXJlZFJlY29yZHMubWFwKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHJlY29yZC51cGRhdGVkT24pO1xyXG4gICAgICAgICAgbGV0IHJlY29yZE1vbnRoID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJtb250aFwiKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHJlY29yZE1vbnRoKTtcclxuICAgICAgICAgIGxldCBpbmRleCA9IG1vbWVudChyZWNvcmRNb250aCkuZGlmZihcclxuICAgICAgICAgICAgbW9tZW50KHN0YXJ0TW9udGgpLFxyXG4gICAgICAgICAgICBcIm1vbnRoXCIsXHJcbiAgICAgICAgICAgIHRydWVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhpbmRleCk7XHJcbiAgICAgICAgICBpZiAocmVjb3JkLmNhdGVnb3J5VHlwZSA9PSBcImluY29tZVwiKSB7XHJcbiAgICAgICAgICAgIGluY29tZXNbaW5kZXhdICs9IHJlY29yZC5hbW91bnQ7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBleHBlbnNlc1tpbmRleF0gLT0gcmVjb3JkLmFtb3VudDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIG1vbnRobHlTYXZpbmdzW2luZGV4XSA9IGluY29tZXNbaW5kZXhdIC0gZXhwZW5zZXNbaW5kZXhdO1xyXG5cclxuICAgICAgICAgIGxldCBpO1xyXG4gICAgICAgICAgZm9yIChpID0gaW5kZXg7IGkgPCBhY2N1bXVsYXRlZFNhdmluZ3MubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coaSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGFjY3VtdWxhdGVkU2F2aW5nc1tpXSk7XHJcbiAgICAgICAgICAgIGFjY3VtdWxhdGVkU2F2aW5nc1tpXSArPSByZWNvcmQuYW1vdW50O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhtb250aHMpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGluY29tZXMpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGV4cGVuc2VzKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhtb250aGx5U2F2aW5ncyk7XHJcblxyXG4gICAgICAgIHNldFRyZW5kRGF0YSh7XHJcbiAgICAgICAgICBsYWJlbHM6IG1vbnRocyxcclxuICAgICAgICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBsYWJlbDogXCJJbmNvbWVcIixcclxuICAgICAgICAgICAgICBkYXRhOiBpbmNvbWVzLFxyXG4gICAgICAgICAgICAgIGZpbGw6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIGJvcmRlckNvbG9yOiBcIiM0OThhZTZcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGxhYmVsOiBcIkV4cGVuc2VzXCIsXHJcbiAgICAgICAgICAgICAgZGF0YTogZXhwZW5zZXMsXHJcbiAgICAgICAgICAgICAgZmlsbDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiTW9udGhseSBTYXZpbmdzXCIsXHJcbiAgICAgICAgICAgICAgZGF0YTogbW9udGhseVNhdmluZ3MsXHJcbiAgICAgICAgICAgICAgZmlsbDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IFwiIzQ3YjgzZFwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiQWNjdW11bGF0ZWQgU2F2aW5nc1wiLFxyXG4gICAgICAgICAgICAgIGRhdGE6IGFjY3VtdWxhdGVkU2F2aW5ncyxcclxuICAgICAgICAgICAgICBmaWxsOiBmYWxzZSxcclxuICAgICAgICAgICAgICBib3JkZXJDb2xvcjogXCIjMGY1MjA5XCIsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICBdLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSwgW3N0YXJ0RGF0ZSwgZW5kRGF0ZSwgcmVjb3Jkc10pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+TW9udGhseSBUcmVuZDwvdGl0bGU+XHJcbiAgICAgIDwvSGVhZD5cclxuICAgICAgPEZvcm0+XHJcbiAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyICBteS01XCI+XHJcbiAgICAgICAgICA8Q29sIG1kPVwiYXV0b1wiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5EYXRlOjwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17c3RhcnREYXRlfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U3RhcnREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPENvbCBtZD1cImF1dG9cIj5cclxuICAgICAgICAgICAgPEZvcm0uTGFiZWw+IHRvIDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17ZW5kRGF0ZX1cclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEVuZERhdGUoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgPExpbmUgZGF0YT17dHJlbmREYXRhfSAvPlxyXG4gICAgICA8L0Zvcm0+XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn1cclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9tZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1ib290c3RyYXBcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtY2hhcnRqcy0yXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9