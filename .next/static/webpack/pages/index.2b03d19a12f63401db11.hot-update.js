webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login */ "./pages/login.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./category */ "./pages/category/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_PieChart__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/PieChart */ "./components/PieChart.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_10__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\index.js",
    _s = $RefreshSig$();











function Home() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_5__["default"]),
      user = _useContext.user,
      setUser = _useContext.setUser;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      loading = _useState[0],
      setLoading = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      name = _useState2[0],
      setName = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState3[0],
      setEmail = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      records = _useState4[0],
      setRecords = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      categories = _useState5[0],
      setCategories = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      startDate = _useState6[0],
      setStartDate = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      endDate = _useState7[0],
      setEndDate = _useState7[1];

  var _useState8 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      savedStart = _useState8[0],
      setSavedStart = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      savedEnd = _useState9[0],
      setSavedEnd = _useState9[1];

  var _useState10 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      currentRecords = _useState10[0],
      setCurrentRecords = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      oldRecords = _useState11[0],
      setOldRecords = _useState11[1];

  var _useState12 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      previousSavings = _useState12[0],
      setPreviousSavings = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      currentIncome = _useState13[0],
      setCurrentIncome = _useState13[1];

  var _useState14 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      currentExpenses = _useState14[0],
      setCurrentExpenses = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      budget = _useState15[0],
      setBudget = _useState15[1];

  var _useState16 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      actualPie = _useState16[0],
      setActualPie = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      budgetPie = _useState17[0],
      setBudgetPie = _useState17[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (user.id != null) {
      fetch("http://localhost:4000/api/users/details", {
        headers: {
          Authorization: "Bearer ".concat(localStorage.getItem("token"))
        }
      }).then(function (res) {
        return res.json();
      }).then(function (data) {
        console.log(data);
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setName("".concat(data.firstName, " ").concat(data.lastName));
        setEmail(data.email);
        setRecords(data.records);
        setCategories(data.categories);
      });
    }
  }, [user]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (name != "" & email != "") {
      setLoading(false);
    }
  }, [name, email]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var dateBefore = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).subtract(1, "day");
    var dateAfter = moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).add(1, "day");

    if (startDate <= endDate) {
      if (records.length > 0) {
        setCurrentRecords(records.filter(function (record) {
          var updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
        }));
        setOldRecords(records.filter(function (record) {
          var updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBefore(dateBefore, "day");
        }));
      }
    } else {
      setCurrentRecords([]);
      setOldRecords([]);
    }

    if (categories.length > 0) {
      setBudget(categories.reduce(function (total, category) {
        return total + category.budget;
      }, 0));
    } else {
      setBudget(0);
    }
  }, [categories, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (oldRecords.length > 0) {
      setPreviousSavings(oldRecords.reduce(function (total, record) {
        return total + record.amount;
      }, 0));
    } else {
      setPreviousSavings(0);
    }
  }, [oldRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (currentRecords.length > 0) {
      var income = currentRecords.filter(function (record) {
        return record.amount > 0;
      });
      console.log(income);

      if (income.length > 0) {
        setCurrentIncome(income.reduce(function (total, record) {
          return total + record.amount;
        }, 0));
      } else {
        setCurrentIncome(0);
      }

      var expenses = currentRecords.filter(function (record) {
        return record.amount < 0;
      });

      if (expenses.length > 0) {
        setCurrentExpenses(expenses.reduce(function (total, record) {
          return total - record.amount;
        }, 0));
      } else {
        setCurrentExpenses(0);
      }
    } else {
      setCurrentIncome(0);
      setCurrentExpenses(0);
    }
  }, [currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    console.log(previousSavings);
    console.log(currentIncome);
    console.log(currentExpenses);

    if (previousSavings >= 0) {
      if (currentIncome > currentExpenses) {
        setActualPie([{
          label: "Current Savings",
          amount: currentIncome - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Current Expenses",
          amount: currentExpenses,
          color: "#f56f36"
        }, {
          label: "Previous Savings",
          amount: previousSavings,
          color: "#0f5209"
        }]);
      } else {
        if (currentExpenses < currentIncome + previousSavings) {
          setActualPie([{
            label: "Accumulated Savings",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#0f5209"
          }, {
            label: "Current Expenses",
            amount: currentExpenses,
            color: "#f56f36"
          }]);
        } else {
          setActualPie([{
            label: "Accumulated Deficit",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#e30707"
          }, {
            label: "Consumed Savings and Income",
            amount: currentIncome + previousSavings,
            color: "#0f5209"
          }]);
        }
      }
    } else {
      if (currentIncome > Math.abs(previousSavings) + currentExpenses) {
        setActualPie([{
          label: "Accumulated Savings",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Previous Deficit + Current Expenses",
          amount: Math.abs(previousSavings) + currentExpenses,
          color: "#f56f36"
        }]);
      } else {
        setActualPie([{
          label: "Consumed Income",
          amount: currentIncome,
          color: "#f56f36"
        }, {
          label: "Accumulated Deficit",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#e30707"
        }]);
      }
    }
  }, [previousSavings, currentIncome, currentExpenses]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (budget >= currentExpenses) {
      setBudgetPie([{
        label: "Savings",
        amount: budget - currentExpenses,
        color: "#47b83d"
      }, {
        label: "Expenses",
        amount: currentExpenses,
        color: "#f56f36"
      }]);
    } else {
      setBudgetPie([{
        label: "Consumed Budget",
        amount: budget,
        color: "#f56f36"
      }, {
        label: "Deficit",
        amount: Math.abs(budget - currentExpenses),
        color: "#e30707"
      }]);
    }
  }, [budget, currentExpenses]);

  function saveDate(e) {
    console.log(e);
    e.preventDefault();
    fetch("http://localhost:4000/api/users/setDate", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      },
      body: JSON.stringify({
        startDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).startOf("day"),
        endDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).startOf("day")
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (data == false) {
        Swal.fire("Error", "Something went wrong", "error");
      } else {
        console.log(data);
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Budget Tracker"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 312,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 311,
      columnNumber: 7
    }, this), user.id !== null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
        className: "justify-content-center",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 318,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: email
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 319,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: ["Balance: Php ", currentIncome + previousSavings - currentExpenses]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 320,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 317,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
          onSubmit: function onSubmit(e) {
            return saveDate(e);
          },
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
            className: "justify-content-md-center my-2",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: "Budget Date:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 328,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 327,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                dateformat: "yyyy-MM-DD",
                value: startDate,
                onChange: function onChange(e) {
                  return setStartDate(e.target.value);
                }
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 331,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 330,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: " to "
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 339,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 338,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                value: endDate,
                dateformat: "myyyy-MM-DD",
                onChange: function onChange(e) {
                  return setEndDate(e.target.value);
                }
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 342,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 341,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: startDate != savedStart || endDate != savedEnd ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 352,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                disabled: true,
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 356,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 350,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 326,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 325,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 324,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 374,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: actualPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 376,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 372,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Income: Php ", currentIncome]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 386,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 387,
                  columnNumber: 21
                }, this), currentIncome >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Savings: Php ", currentIncome - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 389,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Deficit: Php ", currentIncome - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 393,
                  columnNumber: 23
                }, this), previousSavings >= 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Savings: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 398,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Deficit: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 400,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 385,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 384,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 371,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 370,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 410,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: budgetPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 412,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 408,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Budget: Php ", budget]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 422,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 423,
                  columnNumber: 21
                }, this), budget >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Savings: Php ", budget - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 425,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Deficit: Php ", budget - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 427,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 421,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 420,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 407,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 406,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 369,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          className: "justify-content-md-center my-2",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "success",
            onClick: function onClick() {
              return next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./category/add");
            },
            children: "Add Category"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 436,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 435,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "secondary",
            onClick: function onClick() {
              return next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./record/add");
            },
            children: "Add Record"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 446,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 445,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 434,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 316,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_login__WEBPACK_IMPORTED_MODULE_6__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 458,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 310,
    columnNumber: 5
  }, this);
}

_s(Home, "89t6n/MJkOlUE3yv2I91v03nCNY=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSG9tZSIsInVzZUNvbnRleHQiLCJVc2VyQ29udGV4dCIsInVzZXIiLCJzZXRVc2VyIiwidXNlU3RhdGUiLCJsb2FkaW5nIiwic2V0TG9hZGluZyIsIm5hbWUiLCJzZXROYW1lIiwiZW1haWwiLCJzZXRFbWFpbCIsInJlY29yZHMiLCJzZXRSZWNvcmRzIiwiY2F0ZWdvcmllcyIsInNldENhdGVnb3JpZXMiLCJzdGFydERhdGUiLCJzZXRTdGFydERhdGUiLCJlbmREYXRlIiwic2V0RW5kRGF0ZSIsInNhdmVkU3RhcnQiLCJzZXRTYXZlZFN0YXJ0Iiwic2F2ZWRFbmQiLCJzZXRTYXZlZEVuZCIsImN1cnJlbnRSZWNvcmRzIiwic2V0Q3VycmVudFJlY29yZHMiLCJvbGRSZWNvcmRzIiwic2V0T2xkUmVjb3JkcyIsInByZXZpb3VzU2F2aW5ncyIsInNldFByZXZpb3VzU2F2aW5ncyIsImN1cnJlbnRJbmNvbWUiLCJzZXRDdXJyZW50SW5jb21lIiwiY3VycmVudEV4cGVuc2VzIiwic2V0Q3VycmVudEV4cGVuc2VzIiwiYnVkZ2V0Iiwic2V0QnVkZ2V0IiwiYWN0dWFsUGllIiwic2V0QWN0dWFsUGllIiwiYnVkZ2V0UGllIiwic2V0QnVkZ2V0UGllIiwidXNlRWZmZWN0IiwiaWQiLCJmZXRjaCIsImhlYWRlcnMiLCJBdXRob3JpemF0aW9uIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNvbnNvbGUiLCJsb2ciLCJtb21lbnQiLCJjdXJyZW50RGF0ZSIsInN0YXJ0T2YiLCJmb3JtYXQiLCJmaXJzdE5hbWUiLCJsYXN0TmFtZSIsImRhdGVCZWZvcmUiLCJzdWJ0cmFjdCIsImRhdGVBZnRlciIsImFkZCIsImxlbmd0aCIsImZpbHRlciIsInJlY29yZCIsInVwZGF0ZWRPbiIsImlzQmV0d2VlbiIsImlzQmVmb3JlIiwicmVkdWNlIiwidG90YWwiLCJjYXRlZ29yeSIsImFtb3VudCIsImluY29tZSIsImV4cGVuc2VzIiwibGFiZWwiLCJjb2xvciIsIk1hdGgiLCJhYnMiLCJzYXZlRGF0ZSIsImUiLCJwcmV2ZW50RGVmYXVsdCIsIm1ldGhvZCIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiU3dhbCIsImZpcmUiLCJ0YXJnZXQiLCJ2YWx1ZSIsIlJvdXRlciIsInB1c2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLElBQVQsR0FBZ0I7QUFBQTs7QUFBQSxvQkFDSEMsd0RBQVUsQ0FBQ0Msb0RBQUQsQ0FEUDtBQUFBLE1BQ3JCQyxJQURxQixlQUNyQkEsSUFEcUI7QUFBQSxNQUNmQyxPQURlLGVBQ2ZBLE9BRGU7O0FBQUEsa0JBRUNDLHNEQUFRLENBQUMsSUFBRCxDQUZUO0FBQUEsTUFFdEJDLE9BRnNCO0FBQUEsTUFFYkMsVUFGYTs7QUFBQSxtQkFHTEYsc0RBQVEsQ0FBQyxFQUFELENBSEg7QUFBQSxNQUd0QkcsSUFIc0I7QUFBQSxNQUdoQkMsT0FIZ0I7O0FBQUEsbUJBSUhKLHNEQUFRLENBQUMsRUFBRCxDQUpMO0FBQUEsTUFJdEJLLEtBSnNCO0FBQUEsTUFJZkMsUUFKZTs7QUFBQSxtQkFLQ04sc0RBQVEsQ0FBQyxFQUFELENBTFQ7QUFBQSxNQUt0Qk8sT0FMc0I7QUFBQSxNQUtiQyxVQUxhOztBQUFBLG1CQU1PUixzREFBUSxDQUFDLEVBQUQsQ0FOZjtBQUFBLE1BTXRCUyxVQU5zQjtBQUFBLE1BTVZDLGFBTlU7O0FBQUEsbUJBT0tWLHNEQUFRLENBQUMsRUFBRCxDQVBiO0FBQUEsTUFPdEJXLFNBUHNCO0FBQUEsTUFPWEMsWUFQVzs7QUFBQSxtQkFRQ1osc0RBQVEsQ0FBQyxFQUFELENBUlQ7QUFBQSxNQVF0QmEsT0FSc0I7QUFBQSxNQVFiQyxVQVJhOztBQUFBLG1CQVNPZCxzREFBUSxDQUFDLEVBQUQsQ0FUZjtBQUFBLE1BU3RCZSxVQVRzQjtBQUFBLE1BU1ZDLGFBVFU7O0FBQUEsbUJBVUdoQixzREFBUSxDQUFDLEVBQUQsQ0FWWDtBQUFBLE1BVXRCaUIsUUFWc0I7QUFBQSxNQVVaQyxXQVZZOztBQUFBLG9CQVdlbEIsc0RBQVEsQ0FBQyxFQUFELENBWHZCO0FBQUEsTUFXdEJtQixjQVhzQjtBQUFBLE1BV05DLGlCQVhNOztBQUFBLG9CQVlPcEIsc0RBQVEsQ0FBQyxFQUFELENBWmY7QUFBQSxNQVl0QnFCLFVBWnNCO0FBQUEsTUFZVkMsYUFaVTs7QUFBQSxvQkFhaUJ0QixzREFBUSxDQUFDLENBQUQsQ0FiekI7QUFBQSxNQWF0QnVCLGVBYnNCO0FBQUEsTUFhTEMsa0JBYks7O0FBQUEsb0JBY2F4QixzREFBUSxDQUFDLENBQUQsQ0FkckI7QUFBQSxNQWN0QnlCLGFBZHNCO0FBQUEsTUFjUEMsZ0JBZE87O0FBQUEsb0JBZWlCMUIsc0RBQVEsQ0FBQyxDQUFELENBZnpCO0FBQUEsTUFldEIyQixlQWZzQjtBQUFBLE1BZUxDLGtCQWZLOztBQUFBLG9CQWdCRDVCLHNEQUFRLENBQUMsQ0FBRCxDQWhCUDtBQUFBLE1BZ0J0QjZCLE1BaEJzQjtBQUFBLE1BZ0JkQyxTQWhCYzs7QUFBQSxvQkFpQks5QixzREFBUSxDQUFDLEVBQUQsQ0FqQmI7QUFBQSxNQWlCdEIrQixTQWpCc0I7QUFBQSxNQWlCWEMsWUFqQlc7O0FBQUEsb0JBa0JLaEMsc0RBQVEsQ0FBQyxFQUFELENBbEJiO0FBQUEsTUFrQnRCaUMsU0FsQnNCO0FBQUEsTUFrQlhDLFlBbEJXOztBQW9CN0JDLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUlyQyxJQUFJLENBQUNzQyxFQUFMLElBQVcsSUFBZixFQUFxQjtBQUNuQkMsV0FBSyw0Q0FBNEM7QUFDL0NDLGVBQU8sRUFBRTtBQUNQQyx1QkFBYSxtQkFBWUMsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQVo7QUFETjtBQURzQyxPQUE1QyxDQUFMLENBS0dDLElBTEgsQ0FLUSxVQUFDQyxHQUFEO0FBQUEsZUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxPQUxSLEVBTUdGLElBTkgsQ0FNUSxVQUFDRyxJQUFELEVBQVU7QUFDZEMsZUFBTyxDQUFDQyxHQUFSLENBQVlGLElBQVo7QUFDQWpDLG9CQUFZLENBQ1ZvQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJ0QyxTQUFsQixDQUFOLENBQ0d1QyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURVLENBQVo7QUFLQXJDLGtCQUFVLENBQ1JrQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJwQyxPQUFsQixDQUFOLENBQ0dxQyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURRLENBQVY7QUFLQW5DLHFCQUFhLENBQ1hnQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJ0QyxTQUFsQixDQUFOLENBQ0d1QyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURXLENBQWI7QUFLQWpDLG1CQUFXLENBQ1Q4Qiw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJwQyxPQUFsQixDQUFOLENBQ0dxQyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURTLENBQVg7QUFLQS9DLGVBQU8sV0FBSXlDLElBQUksQ0FBQ08sU0FBVCxjQUFzQlAsSUFBSSxDQUFDUSxRQUEzQixFQUFQO0FBQ0EvQyxnQkFBUSxDQUFDdUMsSUFBSSxDQUFDeEMsS0FBTixDQUFSO0FBQ0FHLGtCQUFVLENBQUNxQyxJQUFJLENBQUN0QyxPQUFOLENBQVY7QUFDQUcscUJBQWEsQ0FBQ21DLElBQUksQ0FBQ3BDLFVBQU4sQ0FBYjtBQUNELE9BaENIO0FBaUNEO0FBQ0YsR0FwQ1EsRUFvQ04sQ0FBQ1gsSUFBRCxDQXBDTSxDQUFUO0FBc0NBcUMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBS2hDLElBQUksSUFBSSxFQUFULEdBQWdCRSxLQUFLLElBQUksRUFBN0IsRUFBa0M7QUFDaENILGdCQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0Q7QUFDRixHQUpRLEVBSU4sQ0FBQ0MsSUFBRCxFQUFPRSxLQUFQLENBSk0sQ0FBVDtBQU1BOEIseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSW1CLFVBQVUsR0FBR04sNkNBQU0sQ0FBQ3JDLFNBQUQsQ0FBTixDQUFrQjRDLFFBQWxCLENBQTJCLENBQTNCLEVBQThCLEtBQTlCLENBQWpCO0FBQ0EsUUFBSUMsU0FBUyxHQUFHUiw2Q0FBTSxDQUFDbkMsT0FBRCxDQUFOLENBQWdCNEMsR0FBaEIsQ0FBb0IsQ0FBcEIsRUFBdUIsS0FBdkIsQ0FBaEI7O0FBRUEsUUFBSTlDLFNBQVMsSUFBSUUsT0FBakIsRUFBMEI7QUFDeEIsVUFBSU4sT0FBTyxDQUFDbUQsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUN0QnRDLHlCQUFpQixDQUNmYixPQUFPLENBQUNvRCxNQUFSLENBQWUsVUFBQ0MsTUFBRCxFQUFZO0FBQ3pCLGNBQUlDLFNBQVMsR0FBR2IsNkNBQU0sQ0FBQ1ksTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJYLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsaUJBQU9GLDZDQUFNLENBQUNhLFNBQUQsQ0FBTixDQUFrQkMsU0FBbEIsQ0FBNEJSLFVBQTVCLEVBQXdDRSxTQUF4QyxFQUFtRCxLQUFuRCxDQUFQO0FBQ0QsU0FIRCxDQURlLENBQWpCO0FBT0FsQyxxQkFBYSxDQUNYZixPQUFPLENBQUNvRCxNQUFSLENBQWUsVUFBQ0MsTUFBRCxFQUFZO0FBQ3pCLGNBQUlDLFNBQVMsR0FBR2IsNkNBQU0sQ0FBQ1ksTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJYLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsaUJBQU9GLDZDQUFNLENBQUNhLFNBQUQsQ0FBTixDQUFrQkUsUUFBbEIsQ0FBMkJULFVBQTNCLEVBQXVDLEtBQXZDLENBQVA7QUFDRCxTQUhELENBRFcsQ0FBYjtBQU1EO0FBQ0YsS0FoQkQsTUFnQk87QUFDTGxDLHVCQUFpQixDQUFDLEVBQUQsQ0FBakI7QUFDQUUsbUJBQWEsQ0FBQyxFQUFELENBQWI7QUFDRDs7QUFFRCxRQUFJYixVQUFVLENBQUNpRCxNQUFYLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3pCNUIsZUFBUyxDQUNQckIsVUFBVSxDQUFDdUQsTUFBWCxDQUFrQixVQUFDQyxLQUFELEVBQVFDLFFBQVIsRUFBcUI7QUFDckMsZUFBT0QsS0FBSyxHQUFHQyxRQUFRLENBQUNyQyxNQUF4QjtBQUNELE9BRkQsRUFFRyxDQUZILENBRE8sQ0FBVDtBQUtELEtBTkQsTUFNTztBQUNMQyxlQUFTLENBQUMsQ0FBRCxDQUFUO0FBQ0Q7QUFDRixHQWxDUSxFQWtDTixDQUFDckIsVUFBRCxFQUFhRSxTQUFiLEVBQXdCRSxPQUF4QixDQWxDTSxDQUFUO0FBb0NBc0IseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSWQsVUFBVSxDQUFDcUMsTUFBWCxHQUFvQixDQUF4QixFQUEyQjtBQUN6QmxDLHdCQUFrQixDQUNoQkgsVUFBVSxDQUFDMkMsTUFBWCxDQUFrQixVQUFDQyxLQUFELEVBQVFMLE1BQVIsRUFBbUI7QUFDbkMsZUFBT0ssS0FBSyxHQUFHTCxNQUFNLENBQUNPLE1BQXRCO0FBQ0QsT0FGRCxFQUVHLENBRkgsQ0FEZ0IsQ0FBbEI7QUFLRCxLQU5ELE1BTU87QUFDTDNDLHdCQUFrQixDQUFDLENBQUQsQ0FBbEI7QUFDRDtBQUNGLEdBVlEsRUFVTixDQUFDSCxVQUFELENBVk0sQ0FBVDtBQVlBYyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJaEIsY0FBYyxDQUFDdUMsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QixVQUFJVSxNQUFNLEdBQUdqRCxjQUFjLENBQUN3QyxNQUFmLENBQXNCLFVBQUNDLE1BQUQ7QUFBQSxlQUFZQSxNQUFNLENBQUNPLE1BQVAsR0FBZ0IsQ0FBNUI7QUFBQSxPQUF0QixDQUFiO0FBQ0FyQixhQUFPLENBQUNDLEdBQVIsQ0FBWXFCLE1BQVo7O0FBQ0EsVUFBSUEsTUFBTSxDQUFDVixNQUFQLEdBQWdCLENBQXBCLEVBQXVCO0FBQ3JCaEMsd0JBQWdCLENBQ2QwQyxNQUFNLENBQUNKLE1BQVAsQ0FBYyxVQUFDQyxLQUFELEVBQVFMLE1BQVIsRUFBbUI7QUFDL0IsaUJBQU9LLEtBQUssR0FBR0wsTUFBTSxDQUFDTyxNQUF0QjtBQUNELFNBRkQsRUFFRyxDQUZILENBRGMsQ0FBaEI7QUFLRCxPQU5ELE1BTU87QUFDTHpDLHdCQUFnQixDQUFDLENBQUQsQ0FBaEI7QUFDRDs7QUFFRCxVQUFJMkMsUUFBUSxHQUFHbEQsY0FBYyxDQUFDd0MsTUFBZixDQUFzQixVQUFDQyxNQUFEO0FBQUEsZUFBWUEsTUFBTSxDQUFDTyxNQUFQLEdBQWdCLENBQTVCO0FBQUEsT0FBdEIsQ0FBZjs7QUFDQSxVQUFJRSxRQUFRLENBQUNYLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkI5QiwwQkFBa0IsQ0FDaEJ5QyxRQUFRLENBQUNMLE1BQVQsQ0FBZ0IsVUFBQ0MsS0FBRCxFQUFRTCxNQUFSLEVBQW1CO0FBQ2pDLGlCQUFPSyxLQUFLLEdBQUdMLE1BQU0sQ0FBQ08sTUFBdEI7QUFDRCxTQUZELEVBRUcsQ0FGSCxDQURnQixDQUFsQjtBQUtELE9BTkQsTUFNTztBQUNMdkMsMEJBQWtCLENBQUMsQ0FBRCxDQUFsQjtBQUNEO0FBQ0YsS0F2QkQsTUF1Qk87QUFDTEYsc0JBQWdCLENBQUMsQ0FBRCxDQUFoQjtBQUNBRSx3QkFBa0IsQ0FBQyxDQUFELENBQWxCO0FBQ0Q7QUFDRixHQTVCUSxFQTRCTixDQUFDVCxjQUFELENBNUJNLENBQVQ7QUE4QkFnQix5REFBUyxDQUFDLFlBQU07QUFDZFcsV0FBTyxDQUFDQyxHQUFSLENBQVl4QixlQUFaO0FBQ0F1QixXQUFPLENBQUNDLEdBQVIsQ0FBWXRCLGFBQVo7QUFDQXFCLFdBQU8sQ0FBQ0MsR0FBUixDQUFZcEIsZUFBWjs7QUFDQSxRQUFJSixlQUFlLElBQUksQ0FBdkIsRUFBMEI7QUFDeEIsVUFBSUUsYUFBYSxHQUFHRSxlQUFwQixFQUFxQztBQUNuQ0ssb0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxlQUFLLEVBQUUsaUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBQWEsR0FBR0UsZUFGMUI7QUFHRTRDLGVBQUssRUFBRTtBQUhULFNBRFcsRUFNWDtBQUNFRCxlQUFLLEVBQUUsa0JBRFQ7QUFFRUgsZ0JBQU0sRUFBRXhDLGVBRlY7QUFHRTRDLGVBQUssRUFBRTtBQUhULFNBTlcsRUFXWDtBQUNFRCxlQUFLLEVBQUUsa0JBRFQ7QUFFRUgsZ0JBQU0sRUFBRTVDLGVBRlY7QUFHRWdELGVBQUssRUFBRTtBQUhULFNBWFcsQ0FBRCxDQUFaO0FBaUJELE9BbEJELE1Ba0JPO0FBQ0wsWUFBSTVDLGVBQWUsR0FBR0YsYUFBYSxHQUFHRixlQUF0QyxFQUF1RDtBQUNyRFMsc0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxpQkFBSyxFQUFFLHFCQURUO0FBRUVILGtCQUFNLEVBQUUxQyxhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQUY1QztBQUdFNEMsaUJBQUssRUFBRTtBQUhULFdBRFcsRUFNWDtBQUNFRCxpQkFBSyxFQUFFLGtCQURUO0FBRUVILGtCQUFNLEVBQUV4QyxlQUZWO0FBR0U0QyxpQkFBSyxFQUFFO0FBSFQsV0FOVyxDQUFELENBQVo7QUFZRCxTQWJELE1BYU87QUFDTHZDLHNCQUFZLENBQUMsQ0FDWDtBQUNFc0MsaUJBQUssRUFBRSxxQkFEVDtBQUVFSCxrQkFBTSxFQUFFMUMsYUFBYSxHQUFHRixlQUFoQixHQUFrQ0ksZUFGNUM7QUFHRTRDLGlCQUFLLEVBQUU7QUFIVCxXQURXLEVBTVg7QUFDRUQsaUJBQUssRUFBRSw2QkFEVDtBQUVFSCxrQkFBTSxFQUFFMUMsYUFBYSxHQUFHRixlQUYxQjtBQUdFZ0QsaUJBQUssRUFBRTtBQUhULFdBTlcsQ0FBRCxDQUFaO0FBWUQ7QUFDRjtBQUNGLEtBaERELE1BZ0RPO0FBQ0wsVUFBSTlDLGFBQWEsR0FBRytDLElBQUksQ0FBQ0MsR0FBTCxDQUFTbEQsZUFBVCxJQUE0QkksZUFBaEQsRUFBaUU7QUFDL0RLLG9CQUFZLENBQUMsQ0FDWDtBQUNFc0MsZUFBSyxFQUFFLHFCQURUO0FBRUVILGdCQUFNLEVBQUUxQyxhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQUY1QztBQUdFNEMsZUFBSyxFQUFFO0FBSFQsU0FEVyxFQU1YO0FBQ0VELGVBQUssRUFBRSxxQ0FEVDtBQUVFSCxnQkFBTSxFQUFFSyxJQUFJLENBQUNDLEdBQUwsQ0FBU2xELGVBQVQsSUFBNEJJLGVBRnRDO0FBR0U0QyxlQUFLLEVBQUU7QUFIVCxTQU5XLENBQUQsQ0FBWjtBQVlELE9BYkQsTUFhTztBQUNMdkMsb0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxlQUFLLEVBQUUsaUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBRlY7QUFHRThDLGVBQUssRUFBRTtBQUhULFNBRFcsRUFNWDtBQUNFRCxlQUFLLEVBQUUscUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBQWEsR0FBR0YsZUFBaEIsR0FBa0NJLGVBRjVDO0FBR0U0QyxlQUFLLEVBQUU7QUFIVCxTQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0Y7QUFDRixHQWpGUSxFQWlGTixDQUFDaEQsZUFBRCxFQUFrQkUsYUFBbEIsRUFBaUNFLGVBQWpDLENBakZNLENBQVQ7QUFtRkFRLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUlOLE1BQU0sSUFBSUYsZUFBZCxFQUErQjtBQUM3Qk8sa0JBQVksQ0FBQyxDQUNYO0FBQ0VvQyxhQUFLLEVBQUUsU0FEVDtBQUVFSCxjQUFNLEVBQUV0QyxNQUFNLEdBQUdGLGVBRm5CO0FBR0U0QyxhQUFLLEVBQUU7QUFIVCxPQURXLEVBTVg7QUFDRUQsYUFBSyxFQUFFLFVBRFQ7QUFFRUgsY0FBTSxFQUFFeEMsZUFGVjtBQUdFNEMsYUFBSyxFQUFFO0FBSFQsT0FOVyxDQUFELENBQVo7QUFZRCxLQWJELE1BYU87QUFDTHJDLGtCQUFZLENBQUMsQ0FDWDtBQUNFb0MsYUFBSyxFQUFFLGlCQURUO0FBRUVILGNBQU0sRUFBRXRDLE1BRlY7QUFHRTBDLGFBQUssRUFBRTtBQUhULE9BRFcsRUFNWDtBQUNFRCxhQUFLLEVBQUUsU0FEVDtBQUVFSCxjQUFNLEVBQUVLLElBQUksQ0FBQ0MsR0FBTCxDQUFTNUMsTUFBTSxHQUFHRixlQUFsQixDQUZWO0FBR0U0QyxhQUFLLEVBQUU7QUFIVCxPQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0YsR0E1QlEsRUE0Qk4sQ0FBQzFDLE1BQUQsRUFBU0YsZUFBVCxDQTVCTSxDQUFUOztBQThCQSxXQUFTK0MsUUFBVCxDQUFrQkMsQ0FBbEIsRUFBcUI7QUFDbkI3QixXQUFPLENBQUNDLEdBQVIsQ0FBWTRCLENBQVo7QUFDQUEsS0FBQyxDQUFDQyxjQUFGO0FBRUF2QyxTQUFLLDRDQUE0QztBQUMvQ3dDLFlBQU0sRUFBRSxLQUR1QztBQUUvQ3ZDLGFBQU8sRUFBRTtBQUNQLHdCQUFnQixrQkFEVDtBQUVQQyxxQkFBYSxtQkFBWUMsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQVo7QUFGTixPQUZzQztBQU0vQ3FDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDbkJyRSxpQkFBUyxFQUFFcUMsNkNBQU0sQ0FBQ3JDLFNBQUQsQ0FBTixDQUFrQnVDLE9BQWxCLENBQTBCLEtBQTFCLENBRFE7QUFFbkJyQyxlQUFPLEVBQUVtQyw2Q0FBTSxDQUFDbkMsT0FBRCxDQUFOLENBQWdCcUMsT0FBaEIsQ0FBd0IsS0FBeEI7QUFGVSxPQUFmO0FBTnlDLEtBQTVDLENBQUwsQ0FXR1IsSUFYSCxDQVdRLFVBQUNDLEdBQUQ7QUFBQSxhQUFTQSxHQUFHLENBQUNDLElBQUosRUFBVDtBQUFBLEtBWFIsRUFZR0YsSUFaSCxDQVlRLFVBQUNHLElBQUQsRUFBVTtBQUNkLFVBQUlBLElBQUksSUFBSSxLQUFaLEVBQW1CO0FBQ2pCb0MsWUFBSSxDQUFDQyxJQUFMLENBQVUsT0FBViwwQkFBMkMsT0FBM0M7QUFDRCxPQUZELE1BRU87QUFDTHBDLGVBQU8sQ0FBQ0MsR0FBUixDQUFZRixJQUFaO0FBQ0E3QixxQkFBYSxDQUNYZ0MsNkNBQU0sQ0FBQ0gsSUFBSSxDQUFDbEMsU0FBTixDQUFOLENBQXVCdUMsT0FBdkIsQ0FBK0IsS0FBL0IsRUFBc0NDLE1BQXRDLENBQTZDLFlBQTdDLENBRFcsQ0FBYjtBQUdBakMsbUJBQVcsQ0FBQzhCLDZDQUFNLENBQUNILElBQUksQ0FBQ2hDLE9BQU4sQ0FBTixDQUFxQnFDLE9BQXJCLENBQTZCLEtBQTdCLEVBQW9DQyxNQUFwQyxDQUEyQyxZQUEzQyxDQUFELENBQVg7QUFDQXZDLG9CQUFZLENBQ1ZvQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNsQyxTQUFOLENBQU4sQ0FBdUJ1QyxPQUF2QixDQUErQixLQUEvQixFQUFzQ0MsTUFBdEMsQ0FBNkMsWUFBN0MsQ0FEVSxDQUFaO0FBR0FyQyxrQkFBVSxDQUFDa0MsNkNBQU0sQ0FBQ0gsSUFBSSxDQUFDaEMsT0FBTixDQUFOLENBQXFCcUMsT0FBckIsQ0FBNkIsS0FBN0IsRUFBb0NDLE1BQXBDLENBQTJDLFlBQTNDLENBQUQsQ0FBVjtBQUNEO0FBQ0YsS0ExQkg7QUEyQkQ7O0FBRUQsc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixFQUtHckQsSUFBSSxDQUFDc0MsRUFBTCxLQUFZLElBQVosZ0JBQ0M7QUFBQSw4QkFDRSxxRUFBQyx5REFBRDtBQUFXLGlCQUFTLEVBQUMsd0JBQXJCO0FBQUEsZ0NBQ0U7QUFBQSxvQkFBS2pDO0FBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFO0FBQUEsb0JBQUtFO0FBQUw7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRixlQUdFO0FBQUEsc0NBQ2dCb0IsYUFBYSxHQUFHRixlQUFoQixHQUFrQ0ksZUFEbEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBUUUscUVBQUMsbURBQUQ7QUFBSyxpQkFBUyxFQUFDLGdDQUFmO0FBQUEsK0JBQ0UscUVBQUMsb0RBQUQ7QUFBTSxrQkFBUSxFQUFFLGtCQUFDZ0QsQ0FBRDtBQUFBLG1CQUFPRCxRQUFRLENBQUNDLENBQUQsQ0FBZjtBQUFBLFdBQWhCO0FBQUEsaUNBQ0UscUVBQUMsbURBQUQ7QUFBSyxxQkFBUyxFQUFDLGdDQUFmO0FBQUEsb0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxnQkFBRSxFQUFDLE1BQVI7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBSUUscUVBQUMsbURBQUQ7QUFBQSxxQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxvQkFBSSxFQUFDLE1BRFA7QUFFRSwwQkFBVSxFQUFDLFlBRmI7QUFHRSxxQkFBSyxFQUFFaEUsU0FIVDtBQUlFLHdCQUFRLEVBQUUsa0JBQUNnRSxDQUFEO0FBQUEseUJBQU8vRCxZQUFZLENBQUMrRCxDQUFDLENBQUNRLE1BQUYsQ0FBU0MsS0FBVixDQUFuQjtBQUFBO0FBSlo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBSkYsZUFZRSxxRUFBQyxtREFBRDtBQUFLLGdCQUFFLEVBQUMsTUFBUjtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBWkYsZUFlRSxxRUFBQyxtREFBRDtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLG9CQUFJLEVBQUMsTUFEUDtBQUVFLHFCQUFLLEVBQUV2RSxPQUZUO0FBR0UsMEJBQVUsRUFBQyxhQUhiO0FBSUUsd0JBQVEsRUFBRSxrQkFBQzhELENBQUQ7QUFBQSx5QkFBTzdELFVBQVUsQ0FBQzZELENBQUMsQ0FBQ1EsTUFBRixDQUFTQyxLQUFWLENBQWpCO0FBQUE7QUFKWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFmRixlQXdCRSxxRUFBQyxtREFBRDtBQUFBLHdCQUNHekUsU0FBUyxJQUFJSSxVQUFiLElBQTJCRixPQUFPLElBQUlJLFFBQXRDLGdCQUNDLHFFQUFDLHNEQUFEO0FBQVEsdUJBQU8sRUFBQyxTQUFoQjtBQUEwQixvQkFBSSxFQUFDLFFBQS9CO0FBQXdDLGtCQUFFLEVBQUMsV0FBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREQsZ0JBS0MscUVBQUMsc0RBQUQ7QUFDRSx1QkFBTyxFQUFDLFNBRFY7QUFFRSxvQkFBSSxFQUFDLFFBRlA7QUFHRSxrQkFBRSxFQUFDLFdBSEw7QUFJRSx3QkFBUSxNQUpWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTko7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkF4QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSRixlQXFERSxxRUFBQyxtREFBRDtBQUFBLGdDQUNFLHFFQUFDLG1EQUFEO0FBQUEsaUNBQ0UscUVBQUMsb0RBQUQ7QUFBTSxxQkFBUyxFQUFDLGNBQWhCO0FBQUEsb0NBQ0U7QUFBQSx3QkFDR2MsU0FBUyxDQUFDdUMsS0FBVixnQkFDQyxxRUFBQyxxREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERCxnQkFHQyxxRUFBQyw0REFBRDtBQUNFLHlCQUFTLEVBQUMsS0FEWjtBQUVFLHVCQUFPLEVBQUV2QyxTQUZYO0FBR0Usd0JBQVEsRUFBRSxPQUhaO0FBSUUseUJBQVMsRUFBRTtBQUpiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBYUUscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEscUNBQ0U7QUFBSyx5QkFBUyxFQUFDLGFBQWY7QUFBQSx3Q0FDRTtBQUFBLHFEQUF5Qk4sYUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURGLGVBRUU7QUFBQSx1REFBMkJFLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGRixFQUdHRixhQUFhLElBQUlFLGVBQWpCLGdCQUNDO0FBQUEsc0RBQ3dCRixhQUFhLEdBQUdFLGVBRHhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERCxnQkFLQztBQUFBLHNEQUN3QkYsYUFBYSxHQUFHRSxlQUR4QyxFQUN5RCxHQUR6RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUkosRUFZR0osZUFBZSxJQUFJLENBQW5CLGdCQUNDO0FBQUEsdURBQTJCQSxlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREQsZ0JBR0M7QUFBQSx1REFBMkJBLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFmSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFxQ0UscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRDtBQUFNLHFCQUFTLEVBQUMsY0FBaEI7QUFBQSxvQ0FDRTtBQUFBLHdCQUNHUSxTQUFTLENBQUN1QyxLQUFWLGdCQUNDLHFFQUFDLHFEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURELGdCQUdDLHFFQUFDLDREQUFEO0FBQ0UseUJBQVMsRUFBQyxLQURaO0FBRUUsdUJBQU8sRUFBRXJDLFNBRlg7QUFHRSx3QkFBUSxFQUFFLE9BSFo7QUFJRSx5QkFBUyxFQUFFO0FBSmI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFhRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxxQ0FDRTtBQUFLLHlCQUFTLEVBQUMsYUFBZjtBQUFBLHdDQUNFO0FBQUEsNkNBQWlCSixNQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREYsZUFFRTtBQUFBLCtDQUFtQkYsZUFBbkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUZGLEVBR0dFLE1BQU0sSUFBSUYsZUFBVixnQkFDQztBQUFBLDhDQUFrQkUsTUFBTSxHQUFHRixlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREQsZ0JBR0M7QUFBQSw4Q0FBa0JFLE1BQU0sR0FBR0YsZUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBYkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFyQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBckRGLGVBc0hFLHFFQUFDLG1EQUFEO0FBQUssaUJBQVMsRUFBQyxnQ0FBZjtBQUFBLGdDQUNFLHFFQUFDLG1EQUFEO0FBQUssbUJBQVMsRUFBQyxnQ0FBZjtBQUFBLGlDQUNFLHFFQUFDLHNEQUFEO0FBQ0UscUJBQVMsRUFBQyxZQURaO0FBRUUsZ0JBQUksRUFBQyxJQUZQO0FBR0UsbUJBQU8sRUFBQyxTQUhWO0FBSUUsbUJBQU8sRUFBRTtBQUFBLHFCQUFNMEQsbURBQU0sQ0FBQ0MsSUFBUCxDQUFZLGdCQUFaLENBQU47QUFBQSxhQUpYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQVdFLHFFQUFDLG1EQUFEO0FBQUEsaUNBQ0UscUVBQUMsc0RBQUQ7QUFDRSxxQkFBUyxFQUFDLFlBRFo7QUFFRSxnQkFBSSxFQUFDLElBRlA7QUFHRSxtQkFBTyxFQUFDLFdBSFY7QUFJRSxtQkFBTyxFQUFFO0FBQUEscUJBQU1ELG1EQUFNLENBQUNDLElBQVAsQ0FBWSxjQUFaLENBQU47QUFBQSxhQUpYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFYRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0F0SEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREQsZ0JBK0lDLHFFQUFDLDhDQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFwSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUF5SkQ7O0dBemJ1QjNGLEk7O0tBQUFBLEkiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguMmIwM2QxOWExMmY2MzQwMWRiMTEuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVN0YXRlLCB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IHtcclxuICBDb250YWluZXIsXHJcbiAgRm9ybSxcclxuICBSb3csXHJcbiAgQ29sLFxyXG4gIEJ1dHRvbixcclxuICBKdW1ib3Ryb24sXHJcbiAgQ2FyZCxcclxuICBBbGVydCxcclxufSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBzdHlsZXMgZnJvbSBcIi4uL3N0eWxlcy9Ib21lLm1vZHVsZS5jc3NcIjtcclxuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gXCIuLi9Vc2VyQ29udGV4dFwiO1xyXG5pbXBvcnQgTG9naW4gZnJvbSBcIi4vbG9naW5cIjtcclxuaW1wb3J0IENhdGVnb3J5IGZyb20gXCIuL2NhdGVnb3J5XCI7XHJcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG5pbXBvcnQgUGllQ2hhcnQgZnJvbSBcIi4uL2NvbXBvbmVudHMvUGllQ2hhcnRcIjtcclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhvbWUoKSB7XHJcbiAgY29uc3QgeyB1c2VyLCBzZXRVc2VyIH0gPSB1c2VDb250ZXh0KFVzZXJDb250ZXh0KTtcclxuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZSh0cnVlKTtcclxuICBjb25zdCBbbmFtZSwgc2V0TmFtZV0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtyZWNvcmRzLCBzZXRSZWNvcmRzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbY2F0ZWdvcmllcywgc2V0Q2F0ZWdvcmllc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW3N0YXJ0RGF0ZSwgc2V0U3RhcnREYXRlXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtlbmREYXRlLCBzZXRFbmREYXRlXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtzYXZlZFN0YXJ0LCBzZXRTYXZlZFN0YXJ0XSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtzYXZlZEVuZCwgc2V0U2F2ZWRFbmRdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2N1cnJlbnRSZWNvcmRzLCBzZXRDdXJyZW50UmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW29sZFJlY29yZHMsIHNldE9sZFJlY29yZHNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtwcmV2aW91c1NhdmluZ3MsIHNldFByZXZpb3VzU2F2aW5nc10gPSB1c2VTdGF0ZSgwKTtcclxuICBjb25zdCBbY3VycmVudEluY29tZSwgc2V0Q3VycmVudEluY29tZV0gPSB1c2VTdGF0ZSgwKTtcclxuICBjb25zdCBbY3VycmVudEV4cGVuc2VzLCBzZXRDdXJyZW50RXhwZW5zZXNdID0gdXNlU3RhdGUoMCk7XHJcbiAgY29uc3QgW2J1ZGdldCwgc2V0QnVkZ2V0XSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFthY3R1YWxQaWUsIHNldEFjdHVhbFBpZV0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2J1ZGdldFBpZSwgc2V0QnVkZ2V0UGllXSA9IHVzZVN0YXRlKFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmICh1c2VyLmlkICE9IG51bGwpIHtcclxuICAgICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvZGV0YWlsc2AsIHtcclxuICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICBzZXRTdGFydERhdGUoXHJcbiAgICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLnN0YXJ0RGF0ZSlcclxuICAgICAgICAgICAgICAuc3RhcnRPZihcImRheXNcIilcclxuICAgICAgICAgICAgICAuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldEVuZERhdGUoXHJcbiAgICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLmVuZERhdGUpXHJcbiAgICAgICAgICAgICAgLnN0YXJ0T2YoXCJkYXlzXCIpXHJcbiAgICAgICAgICAgICAgLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXRTYXZlZFN0YXJ0KFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5zdGFydERhdGUpXHJcbiAgICAgICAgICAgICAgLnN0YXJ0T2YoXCJkYXlzXCIpXHJcbiAgICAgICAgICAgICAgLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXRTYXZlZEVuZChcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuY3VycmVudERhdGUuZW5kRGF0ZSlcclxuICAgICAgICAgICAgICAuc3RhcnRPZihcImRheXNcIilcclxuICAgICAgICAgICAgICAuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldE5hbWUoYCR7ZGF0YS5maXJzdE5hbWV9ICR7ZGF0YS5sYXN0TmFtZX1gKTtcclxuICAgICAgICAgIHNldEVtYWlsKGRhdGEuZW1haWwpO1xyXG4gICAgICAgICAgc2V0UmVjb3JkcyhkYXRhLnJlY29yZHMpO1xyXG4gICAgICAgICAgc2V0Q2F0ZWdvcmllcyhkYXRhLmNhdGVnb3JpZXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH0sIFt1c2VyXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoKG5hbWUgIT0gXCJcIikgJiAoZW1haWwgIT0gXCJcIikpIHtcclxuICAgICAgc2V0TG9hZGluZyhmYWxzZSk7XHJcbiAgICB9XHJcbiAgfSwgW25hbWUsIGVtYWlsXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBsZXQgZGF0ZUJlZm9yZSA9IG1vbWVudChzdGFydERhdGUpLnN1YnRyYWN0KDEsIFwiZGF5XCIpO1xyXG4gICAgbGV0IGRhdGVBZnRlciA9IG1vbWVudChlbmREYXRlKS5hZGQoMSwgXCJkYXlcIik7XHJcblxyXG4gICAgaWYgKHN0YXJ0RGF0ZSA8PSBlbmREYXRlKSB7XHJcbiAgICAgIGlmIChyZWNvcmRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICBzZXRDdXJyZW50UmVjb3JkcyhcclxuICAgICAgICAgIHJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHtcclxuICAgICAgICAgICAgbGV0IHVwZGF0ZWRPbiA9IG1vbWVudChyZWNvcmQudXBkYXRlZE9uKS5zdGFydE9mKFwiZGF5c1wiKTtcclxuICAgICAgICAgICAgcmV0dXJuIG1vbWVudCh1cGRhdGVkT24pLmlzQmV0d2VlbihkYXRlQmVmb3JlLCBkYXRlQWZ0ZXIsIFwiZGF5XCIpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICBzZXRPbGRSZWNvcmRzKFxyXG4gICAgICAgICAgcmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJkYXlzXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gbW9tZW50KHVwZGF0ZWRPbikuaXNCZWZvcmUoZGF0ZUJlZm9yZSwgXCJkYXlcIik7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEN1cnJlbnRSZWNvcmRzKFtdKTtcclxuICAgICAgc2V0T2xkUmVjb3JkcyhbXSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGNhdGVnb3JpZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICBzZXRCdWRnZXQoXHJcbiAgICAgICAgY2F0ZWdvcmllcy5yZWR1Y2UoKHRvdGFsLCBjYXRlZ29yeSkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHRvdGFsICsgY2F0ZWdvcnkuYnVkZ2V0O1xyXG4gICAgICAgIH0sIDApXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRCdWRnZXQoMCk7XHJcbiAgICB9XHJcbiAgfSwgW2NhdGVnb3JpZXMsIHN0YXJ0RGF0ZSwgZW5kRGF0ZV0pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKG9sZFJlY29yZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICBzZXRQcmV2aW91c1NhdmluZ3MoXHJcbiAgICAgICAgb2xkUmVjb3Jkcy5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICAgIHJldHVybiB0b3RhbCArIHJlY29yZC5hbW91bnQ7XHJcbiAgICAgICAgfSwgMClcclxuICAgICAgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldFByZXZpb3VzU2F2aW5ncygwKTtcclxuICAgIH1cclxuICB9LCBbb2xkUmVjb3Jkc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGN1cnJlbnRSZWNvcmRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgbGV0IGluY29tZSA9IGN1cnJlbnRSZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiByZWNvcmQuYW1vdW50ID4gMCk7XHJcbiAgICAgIGNvbnNvbGUubG9nKGluY29tZSk7XHJcbiAgICAgIGlmIChpbmNvbWUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHNldEN1cnJlbnRJbmNvbWUoXHJcbiAgICAgICAgICBpbmNvbWUucmVkdWNlKCh0b3RhbCwgcmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB0b3RhbCArIHJlY29yZC5hbW91bnQ7XHJcbiAgICAgICAgICB9LCAwKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2V0Q3VycmVudEluY29tZSgwKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IGV4cGVuc2VzID0gY3VycmVudFJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHJlY29yZC5hbW91bnQgPCAwKTtcclxuICAgICAgaWYgKGV4cGVuc2VzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICBzZXRDdXJyZW50RXhwZW5zZXMoXHJcbiAgICAgICAgICBleHBlbnNlcy5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRvdGFsIC0gcmVjb3JkLmFtb3VudDtcclxuICAgICAgICAgIH0sIDApXHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZXRDdXJyZW50RXhwZW5zZXMoMCk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEN1cnJlbnRJbmNvbWUoMCk7XHJcbiAgICAgIHNldEN1cnJlbnRFeHBlbnNlcygwKTtcclxuICAgIH1cclxuICB9LCBbY3VycmVudFJlY29yZHNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGNvbnNvbGUubG9nKHByZXZpb3VzU2F2aW5ncyk7XHJcbiAgICBjb25zb2xlLmxvZyhjdXJyZW50SW5jb21lKTtcclxuICAgIGNvbnNvbGUubG9nKGN1cnJlbnRFeHBlbnNlcyk7XHJcbiAgICBpZiAocHJldmlvdXNTYXZpbmdzID49IDApIHtcclxuICAgICAgaWYgKGN1cnJlbnRJbmNvbWUgPiBjdXJyZW50RXhwZW5zZXMpIHtcclxuICAgICAgICBzZXRBY3R1YWxQaWUoW1xyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJDdXJyZW50IFNhdmluZ3NcIixcclxuICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lIC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjNDdiODNkXCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJDdXJyZW50IEV4cGVuc2VzXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJQcmV2aW91cyBTYXZpbmdzXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogcHJldmlvdXNTYXZpbmdzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjMGY1MjA5XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIF0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChjdXJyZW50RXhwZW5zZXMgPCBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzKSB7XHJcbiAgICAgICAgICBzZXRBY3R1YWxQaWUoW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiQWNjdW11bGF0ZWQgU2F2aW5nc1wiLFxyXG4gICAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncyAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgICBjb2xvcjogXCIjMGY1MjA5XCIsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBsYWJlbDogXCJDdXJyZW50IEV4cGVuc2VzXCIsXHJcbiAgICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgXSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHNldEFjdHVhbFBpZShbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBsYWJlbDogXCJBY2N1bXVsYXRlZCBEZWZpY2l0XCIsXHJcbiAgICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiNlMzA3MDdcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGxhYmVsOiBcIkNvbnN1bWVkIFNhdmluZ3MgYW5kIEluY29tZVwiLFxyXG4gICAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncyxcclxuICAgICAgICAgICAgICBjb2xvcjogXCIjMGY1MjA5XCIsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICBdKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChjdXJyZW50SW5jb21lID4gTWF0aC5hYnMocHJldmlvdXNTYXZpbmdzKSArIGN1cnJlbnRFeHBlbnNlcykge1xyXG4gICAgICAgIHNldEFjdHVhbFBpZShbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIkFjY3VtdWxhdGVkIFNhdmluZ3NcIixcclxuICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjNDdiODNkXCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJQcmV2aW91cyBEZWZpY2l0ICsgQ3VycmVudCBFeHBlbnNlc1wiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IE1hdGguYWJzKHByZXZpb3VzU2F2aW5ncykgKyBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgXSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2V0QWN0dWFsUGllKFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiQ29uc3VtZWQgSW5jb21lXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSxcclxuICAgICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiQWNjdW11bGF0ZWQgRGVmaWNpdFwiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiNlMzA3MDdcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgXSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9LCBbcHJldmlvdXNTYXZpbmdzLCBjdXJyZW50SW5jb21lLCBjdXJyZW50RXhwZW5zZXNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChidWRnZXQgPj0gY3VycmVudEV4cGVuc2VzKSB7XHJcbiAgICAgIHNldEJ1ZGdldFBpZShbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgbGFiZWw6IFwiU2F2aW5nc1wiLFxyXG4gICAgICAgICAgYW1vdW50OiBidWRnZXQgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICBjb2xvcjogXCIjNDdiODNkXCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJFeHBlbnNlc1wiLFxyXG4gICAgICAgICAgYW1vdW50OiBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgXSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRCdWRnZXRQaWUoW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIGxhYmVsOiBcIkNvbnN1bWVkIEJ1ZGdldFwiLFxyXG4gICAgICAgICAgYW1vdW50OiBidWRnZXQsXHJcbiAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJEZWZpY2l0XCIsXHJcbiAgICAgICAgICBhbW91bnQ6IE1hdGguYWJzKGJ1ZGdldCAtIGN1cnJlbnRFeHBlbnNlcyksXHJcbiAgICAgICAgICBjb2xvcjogXCIjZTMwNzA3XCIsXHJcbiAgICAgICAgfSxcclxuICAgICAgXSk7XHJcbiAgICB9XHJcbiAgfSwgW2J1ZGdldCwgY3VycmVudEV4cGVuc2VzXSk7XHJcblxyXG4gIGZ1bmN0aW9uIHNhdmVEYXRlKGUpIHtcclxuICAgIGNvbnNvbGUubG9nKGUpO1xyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL3NldERhdGVgLCB7XHJcbiAgICAgIG1ldGhvZDogXCJQVVRcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBzdGFydERhdGU6IG1vbWVudChzdGFydERhdGUpLnN0YXJ0T2YoXCJkYXlcIiksXHJcbiAgICAgICAgZW5kRGF0ZTogbW9tZW50KGVuZERhdGUpLnN0YXJ0T2YoXCJkYXlcIiksXHJcbiAgICAgIH0pLFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBpZiAoZGF0YSA9PSBmYWxzZSkge1xyXG4gICAgICAgICAgU3dhbC5maXJlKFwiRXJyb3JcIiwgYFNvbWV0aGluZyB3ZW50IHdyb25nYCwgXCJlcnJvclwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICBzZXRTYXZlZFN0YXJ0KFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5zdGFydERhdGUpLnN0YXJ0T2YoXCJkYXlcIikuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldFNhdmVkRW5kKG1vbWVudChkYXRhLmVuZERhdGUpLnN0YXJ0T2YoXCJkYXlcIikuZm9ybWF0KFwieXl5eS1NTS1ERFwiKSk7XHJcbiAgICAgICAgICBzZXRTdGFydERhdGUoXHJcbiAgICAgICAgICAgIG1vbWVudChkYXRhLnN0YXJ0RGF0ZSkuc3RhcnRPZihcImRheVwiKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2V0RW5kRGF0ZShtb21lbnQoZGF0YS5lbmREYXRlKS5zdGFydE9mKFwiZGF5XCIpLmZvcm1hdChcInl5eXktTU0tRERcIikpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+QnVkZ2V0IFRyYWNrZXI8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcblxyXG4gICAgICB7dXNlci5pZCAhPT0gbnVsbCA/IChcclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPEp1bWJvdHJvbiBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgIDxoMz57bmFtZX08L2gzPlxyXG4gICAgICAgICAgICA8aDM+e2VtYWlsfTwvaDM+XHJcbiAgICAgICAgICAgIDxoMz5cclxuICAgICAgICAgICAgICBCYWxhbmNlOiBQaHAge2N1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MgLSBjdXJyZW50RXhwZW5zZXN9XHJcbiAgICAgICAgICAgIDwvaDM+XHJcbiAgICAgICAgICA8L0p1bWJvdHJvbj5cclxuICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gc2F2ZURhdGUoZSl9PlxyXG4gICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICAgICAgICA8Q29sIG1kPVwiYXV0b1wiPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD5CdWRnZXQgRGF0ZTo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiZGF0ZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZWZvcm1hdD1cInl5eXktTU0tRERcIlxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtzdGFydERhdGV9XHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRTdGFydERhdGUoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICA8Q29sIG1kPVwiYXV0b1wiPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD4gdG8gPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImRhdGVcIlxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtlbmREYXRlfVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGVmb3JtYXQ9XCJteXl5eS1NTS1ERFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRFbmREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ29sPlxyXG5cclxuICAgICAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgICAgIHtzdGFydERhdGUgIT0gc2F2ZWRTdGFydCB8fCBlbmREYXRlICE9IHNhdmVkRW5kID8gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cInByaW1hcnlcIiB0eXBlPVwic3VibWl0XCIgaWQ9XCJzdWJtaXRCdG5cIj5cclxuICAgICAgICAgICAgICAgICAgICAgIFNldCBhcyBEZWZhdWx0XHJcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgdmFyaWFudD1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICBpZD1cInN1Ym1pdEJ0blwiXHJcbiAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZFxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgIFNldCBhcyBEZWZhdWx0XHJcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICAgICAgPC9Gb3JtPlxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICA8Um93PlxyXG4gICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgIDxDYXJkIGNsYXNzTmFtZT1cImhvbWVwYWdlQ2FyZFwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAge2FjdHVhbFBpZS5sYWJlbCA/IChcclxuICAgICAgICAgICAgICAgICAgICA8QWxlcnQ+Tm8gUmVjb3JkczwvQWxlcnQ+XHJcbiAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFBpZUNoYXJ0XHJcbiAgICAgICAgICAgICAgICAgICAgICBjaGFydFR5cGU9XCJQaWVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgcmF3RGF0YT17YWN0dWFsUGllfVxyXG4gICAgICAgICAgICAgICAgICAgICAgbGFiZWxLZXk9e1wibGFiZWxcIn1cclxuICAgICAgICAgICAgICAgICAgICAgIGFtb3VudEtleT17XCJhbW91bnRcIn1cclxuICAgICAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhvbWVwYWdlUGllXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg1PkN1cnJlbnQgSW5jb21lOiBQaHAge2N1cnJlbnRJbmNvbWV9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICA8aDU+Q3VycmVudCBFeHBlbnNlczogUGhwIHtjdXJyZW50RXhwZW5zZXN9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICB7Y3VycmVudEluY29tZSA+PSBjdXJyZW50RXhwZW5zZXMgPyAoXHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDU+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEN1cnJlbnQgU2F2aW5nczogUGhwIHtjdXJyZW50SW5jb21lIC0gY3VycmVudEV4cGVuc2VzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg1PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDdXJyZW50IERlZmljaXQ6IFBocCB7Y3VycmVudEluY29tZSAtIGN1cnJlbnRFeHBlbnNlc317XCIgXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICAgICAge3ByZXZpb3VzU2F2aW5ncyA+PSAwID8gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg1PlByZXZpb3VzIFNhdmluZ3M6IFBocCB7cHJldmlvdXNTYXZpbmdzfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5QcmV2aW91cyBEZWZpY2l0OiBQaHAge3ByZXZpb3VzU2F2aW5nc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9DYXJkLkJvZHk+XHJcbiAgICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICA8Q2FyZCBjbGFzc05hbWU9XCJob21lcGFnZUNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgIHthY3R1YWxQaWUubGFiZWwgPyAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEFsZXJ0Pk5vIFJlY29yZHM8L0FsZXJ0PlxyXG4gICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxQaWVDaGFydFxyXG4gICAgICAgICAgICAgICAgICAgICAgY2hhcnRUeXBlPVwiUGllXCJcclxuICAgICAgICAgICAgICAgICAgICAgIHJhd0RhdGE9e2J1ZGdldFBpZX1cclxuICAgICAgICAgICAgICAgICAgICAgIGxhYmVsS2V5PXtcImxhYmVsXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICBhbW91bnRLZXk9e1wiYW1vdW50XCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJob21lcGFnZVBpZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNT5CdWRnZXQ6IFBocCB7YnVkZ2V0fTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg1PkV4cGVuc2VzOiBQaHAge2N1cnJlbnRFeHBlbnNlc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgIHtidWRnZXQgPj0gY3VycmVudEV4cGVuc2VzID8gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg1PlNhdmluZ3M6IFBocCB7YnVkZ2V0IC0gY3VycmVudEV4cGVuc2VzfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5EZWZpY2l0OiBQaHAge2J1ZGdldCAtIGN1cnJlbnRFeHBlbnNlc30gPC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWNlbnRlciBteS0yXCI+XHJcbiAgICAgICAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgICAgICAgdmFyaWFudD1cInN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL2NhdGVnb3J5L2FkZFwiKX1cclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICBBZGQgQ2F0ZWdvcnlcclxuICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgICAgICAgdmFyaWFudD1cInNlY29uZGFyeVwiXHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBSb3V0ZXIucHVzaChcIi4vcmVjb3JkL2FkZFwiKX1cclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICBBZGQgUmVjb3JkXHJcbiAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICkgOiAoXHJcbiAgICAgICAgPExvZ2luIC8+XHJcbiAgICAgICl9XHJcbiAgICA8L1JlYWN0LkZyYWdtZW50PlxyXG4gICk7XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==