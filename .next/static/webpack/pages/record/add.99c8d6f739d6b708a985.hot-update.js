webpackHotUpdate_N_E("pages/record/add",{

/***/ "./node_modules/process/browser.js":
false,

/***/ "./pages/record/add.js":
/*!*****************************!*\
  !*** ./pages/record/add.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return newCategory; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_bootstrap_DropdownButton__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap/DropdownButton */ "./node_modules/react-bootstrap/esm/DropdownButton.js");
/* harmony import */ var react_bootstrap_Dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-bootstrap/Dropdown */ "./node_modules/react-bootstrap/esm/Dropdown.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../UserContext */ "./UserContext.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_9__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\record\\add.js",
    _s = $RefreshSig$();









 // import moment from 'moment'

function newCategory() {
  _s();

  var _this = this;

  // const{user} = useContext(UserContext)
  // Form input state hooks
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      type = _useState[0],
      setType = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      categoryId = _useState2[0],
      setCategoryId = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      description = _useState3[0],
      setDescription = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      amount = _useState4[0],
      setAmount = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      amountToSave = _useState5[0],
      setAmountToSave = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      categories = _useState6[0],
      setCategories = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      catOption = _useState7[0],
      setCatOption = _useState7[1];

  var _useState8 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isActive = _useState8[0],
      setIsActive = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_9___default()().format("YYYY-MM-DD")),
      recordDate = _useState9[0],
      setRecordDate = _useState9[1];

  var _useState10 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(moment__WEBPACK_IMPORTED_MODULE_9___default()().format("HH:mm")),
      recordTime = _useState10[0],
      setRecordTime = _useState10[1]; // Validate form input whenever email, password1, or password2 is changed


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    fetch("http://localhost:4000/api/users/details", {
      headers: {
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      }
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      console.log(data);
      setCategories(data.categories);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var categoryList = categories.filter(function (category) {
      return category.type == type;
    });
    var catOnType = categoryList.map(function (category) {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
        value: category._id,
        children: category.name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 14
      }, _this);
    });
    console.log(catOnType);
    setCatOption(catOnType);
    setCategoryId("");
  }, [type]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (type == "expense") {
      setAmountToSave(0 - Math.abs(amount));
    } else {
      setAmountToSave(Math.abs(amount));
    }
  }, [type, amount]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (type !== "" && categoryId !== "" && description !== "" && amount > 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [type, categoryId, description, amount]);

  function addRecord(e) {
    e.preventDefault();
    console.log(categoryId);
    console.log(description);
    console.log(amount);
    var updatedOn = moment__WEBPACK_IMPORTED_MODULE_9___default()(recordDate + " " + recordTime);
    console.log(updatedOn);
    fetch("http://localhost:4000/api/users/addRecord", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      },
      body: JSON.stringify({
        categoryId: categoryId,
        description: description,
        amount: amountToSave,
        updatedOn: updatedOn
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (data == true) {
        next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push(".");
      } else {
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire("Error", "Something went wrong", "error");
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Add Record"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 101,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      onSubmit: function onSubmit(e) {
        return addRecord(e);
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "type",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Category Type"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 105,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          as: "select",
          onChange: function onChange(e) {
            setType(e.target.value);
          },
          required: true,
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
            value: "",
            disabled: true,
            selected: true,
            children: "Select Category Type"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 113,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
            value: "income",
            children: "Income"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 116,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
            value: "expense",
            children: "Expense"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 117,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 106,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 104,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "categoryId",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Category Name"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 122,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          as: "select",
          value: categoryId,
          onChange: function onChange(e) {
            return setCategoryId(e.target.value);
          },
          required: true,
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
            value: "",
            disabled: true,
            selected: true,
            children: "Select Category Type"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 129,
            columnNumber: 13
          }, this), catOption]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 123,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 121,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "description",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Description"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 137,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "string",
          placeholder: "Enter Description",
          value: description,
          onChange: function onChange(e) {
            return setDescription(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 138,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 136,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "amount",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Amount"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "number",
          placeholder: "Insert Amount",
          value: amount,
          onChange: function onChange(e) {
            return setAmount(e.target.value);
          }
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 149,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 147,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "timestamp",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Date"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 158,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              type: "date",
              value: recordDate,
              onChange: function onChange(e) {
                return setRecordDate(e.target.value);
              }
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 161,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 160,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              type: "time",
              value: recordTime,
              onChange: function onChange(e) {
                return setRecordTime(e.target.value);
              }
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 168,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 167,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 159,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 157,
        columnNumber: 9
      }, this), isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 178,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        disabled: true,
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 99,
    columnNumber: 5
  }, this);
}

_s(newCategory, "aNPFlNdrupVSlGRTeIXMg3mKcrM=");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvcmVjb3JkL2FkZC5qcyJdLCJuYW1lcyI6WyJuZXdDYXRlZ29yeSIsInVzZVN0YXRlIiwidHlwZSIsInNldFR5cGUiLCJjYXRlZ29yeUlkIiwic2V0Q2F0ZWdvcnlJZCIsImRlc2NyaXB0aW9uIiwic2V0RGVzY3JpcHRpb24iLCJhbW91bnQiLCJzZXRBbW91bnQiLCJhbW91bnRUb1NhdmUiLCJzZXRBbW91bnRUb1NhdmUiLCJjYXRlZ29yaWVzIiwic2V0Q2F0ZWdvcmllcyIsImNhdE9wdGlvbiIsInNldENhdE9wdGlvbiIsImlzQWN0aXZlIiwic2V0SXNBY3RpdmUiLCJtb21lbnQiLCJmb3JtYXQiLCJyZWNvcmREYXRlIiwic2V0UmVjb3JkRGF0ZSIsInJlY29yZFRpbWUiLCJzZXRSZWNvcmRUaW1lIiwidXNlRWZmZWN0IiwiZmV0Y2giLCJoZWFkZXJzIiwiQXV0aG9yaXphdGlvbiIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJjb25zb2xlIiwibG9nIiwiY2F0ZWdvcnlMaXN0IiwiZmlsdGVyIiwiY2F0ZWdvcnkiLCJjYXRPblR5cGUiLCJtYXAiLCJfaWQiLCJuYW1lIiwiTWF0aCIsImFicyIsImFkZFJlY29yZCIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInVwZGF0ZWRPbiIsIm1ldGhvZCIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiUm91dGVyIiwicHVzaCIsIlN3YWwiLCJmaXJlIiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Q0FFQTs7QUFFZSxTQUFTQSxXQUFULEdBQXVCO0FBQUE7O0FBQUE7O0FBQ3BDO0FBQ0E7QUFGb0Msa0JBR1pDLHNEQUFRLENBQUMsRUFBRCxDQUhJO0FBQUEsTUFHN0JDLElBSDZCO0FBQUEsTUFHdkJDLE9BSHVCOztBQUFBLG1CQUlBRixzREFBUSxDQUFDLEVBQUQsQ0FKUjtBQUFBLE1BSTdCRyxVQUo2QjtBQUFBLE1BSWpCQyxhQUppQjs7QUFBQSxtQkFLRUosc0RBQVEsQ0FBQyxFQUFELENBTFY7QUFBQSxNQUs3QkssV0FMNkI7QUFBQSxNQUtoQkMsY0FMZ0I7O0FBQUEsbUJBTVJOLHNEQUFRLENBQUMsQ0FBRCxDQU5BO0FBQUEsTUFNN0JPLE1BTjZCO0FBQUEsTUFNckJDLFNBTnFCOztBQUFBLG1CQU9JUixzREFBUSxDQUFDLENBQUQsQ0FQWjtBQUFBLE1BTzdCUyxZQVA2QjtBQUFBLE1BT2ZDLGVBUGU7O0FBQUEsbUJBUUFWLHNEQUFRLENBQUMsRUFBRCxDQVJSO0FBQUEsTUFRN0JXLFVBUjZCO0FBQUEsTUFRakJDLGFBUmlCOztBQUFBLG1CQVNGWixzREFBUSxDQUFDLElBQUQsQ0FUTjtBQUFBLE1BUzdCYSxTQVQ2QjtBQUFBLE1BU2xCQyxZQVRrQjs7QUFBQSxtQkFVSmQsc0RBQVEsQ0FBQyxLQUFELENBVko7QUFBQSxNQVU3QmUsUUFWNkI7QUFBQSxNQVVuQkMsV0FWbUI7O0FBQUEsbUJBV0FoQixzREFBUSxDQUFDaUIsNkNBQU0sR0FBR0MsTUFBVCxDQUFnQixZQUFoQixDQUFELENBWFI7QUFBQSxNQVc3QkMsVUFYNkI7QUFBQSxNQVdqQkMsYUFYaUI7O0FBQUEsb0JBWUFwQixzREFBUSxDQUFDaUIsNkNBQU0sR0FBR0MsTUFBVCxDQUFnQixPQUFoQixDQUFELENBWlI7QUFBQSxNQVk3QkcsVUFaNkI7QUFBQSxNQVlqQkMsYUFaaUIsbUJBY3BDOzs7QUFDQUMseURBQVMsQ0FBQyxZQUFNO0FBQ2RDLFNBQUssNENBQTRDO0FBQy9DQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsbUJBQVlDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFaO0FBRE47QUFEc0MsS0FBNUMsQ0FBTCxDQUtHQyxJQUxILENBS1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FMUixFQU1HRixJQU5ILENBTVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2RDLGFBQU8sQ0FBQ0MsR0FBUixDQUFZRixJQUFaO0FBQ0FwQixtQkFBYSxDQUFDb0IsSUFBSSxDQUFDckIsVUFBTixDQUFiO0FBQ0QsS0FUSDtBQVVELEdBWFEsRUFXTixFQVhNLENBQVQ7QUFhQVkseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSVksWUFBWSxHQUFHeEIsVUFBVSxDQUFDeUIsTUFBWCxDQUFrQixVQUFDQyxRQUFEO0FBQUEsYUFBY0EsUUFBUSxDQUFDcEMsSUFBVCxJQUFpQkEsSUFBL0I7QUFBQSxLQUFsQixDQUFuQjtBQUVBLFFBQUlxQyxTQUFTLEdBQUdILFlBQVksQ0FBQ0ksR0FBYixDQUFpQixVQUFDRixRQUFELEVBQWM7QUFDN0MsMEJBQU87QUFBUSxhQUFLLEVBQUVBLFFBQVEsQ0FBQ0csR0FBeEI7QUFBQSxrQkFBOEJILFFBQVEsQ0FBQ0k7QUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUFQO0FBQ0QsS0FGZSxDQUFoQjtBQUlBUixXQUFPLENBQUNDLEdBQVIsQ0FBWUksU0FBWjtBQUNBeEIsZ0JBQVksQ0FBQ3dCLFNBQUQsQ0FBWjtBQUNBbEMsaUJBQWEsQ0FBQyxFQUFELENBQWI7QUFDRCxHQVZRLEVBVU4sQ0FBQ0gsSUFBRCxDQVZNLENBQVQ7QUFZQXNCLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUl0QixJQUFJLElBQUksU0FBWixFQUF1QjtBQUNyQlMscUJBQWUsQ0FBQyxJQUFJZ0MsSUFBSSxDQUFDQyxHQUFMLENBQVNwQyxNQUFULENBQUwsQ0FBZjtBQUNELEtBRkQsTUFFTztBQUNMRyxxQkFBZSxDQUFDZ0MsSUFBSSxDQUFDQyxHQUFMLENBQVNwQyxNQUFULENBQUQsQ0FBZjtBQUNEO0FBQ0YsR0FOUSxFQU1OLENBQUNOLElBQUQsRUFBT00sTUFBUCxDQU5NLENBQVQ7QUFRQWdCLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUl0QixJQUFJLEtBQUssRUFBVCxJQUFlRSxVQUFVLEtBQUssRUFBOUIsSUFBb0NFLFdBQVcsS0FBSyxFQUFwRCxJQUEwREUsTUFBTSxHQUFHLENBQXZFLEVBQTBFO0FBQ3hFUyxpQkFBVyxDQUFDLElBQUQsQ0FBWDtBQUNELEtBRkQsTUFFTztBQUNMQSxpQkFBVyxDQUFDLEtBQUQsQ0FBWDtBQUNEO0FBQ0YsR0FOUSxFQU1OLENBQUNmLElBQUQsRUFBT0UsVUFBUCxFQUFtQkUsV0FBbkIsRUFBZ0NFLE1BQWhDLENBTk0sQ0FBVDs7QUFRQSxXQUFTcUMsU0FBVCxDQUFtQkMsQ0FBbkIsRUFBc0I7QUFDcEJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBYixXQUFPLENBQUNDLEdBQVIsQ0FBWS9CLFVBQVo7QUFDQThCLFdBQU8sQ0FBQ0MsR0FBUixDQUFZN0IsV0FBWjtBQUNBNEIsV0FBTyxDQUFDQyxHQUFSLENBQVkzQixNQUFaO0FBQ0EsUUFBSXdDLFNBQVMsR0FBRzlCLDZDQUFNLENBQUNFLFVBQVUsR0FBRyxHQUFiLEdBQW1CRSxVQUFwQixDQUF0QjtBQUNBWSxXQUFPLENBQUNDLEdBQVIsQ0FBWWEsU0FBWjtBQUNBdkIsU0FBSyw4Q0FBOEM7QUFDakR3QixZQUFNLEVBQUUsS0FEeUM7QUFFakR2QixhQUFPLEVBQUU7QUFDUCx3QkFBZ0Isa0JBRFQ7QUFFUEMscUJBQWEsbUJBQVlDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFaO0FBRk4sT0FGd0M7QUFNakRxQixVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CaEQsa0JBQVUsRUFBRUEsVUFETztBQUVuQkUsbUJBQVcsRUFBRUEsV0FGTTtBQUduQkUsY0FBTSxFQUFFRSxZQUhXO0FBSW5Cc0MsaUJBQVMsRUFBRUE7QUFKUSxPQUFmO0FBTjJDLEtBQTlDLENBQUwsQ0FhR2xCLElBYkgsQ0FhUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQWJSLEVBY0dGLElBZEgsQ0FjUSxVQUFDRyxJQUFELEVBQVU7QUFDZCxVQUFJQSxJQUFJLElBQUksSUFBWixFQUFrQjtBQUNoQm9CLDBEQUFNLENBQUNDLElBQVAsQ0FBWSxHQUFaO0FBQ0QsT0FGRCxNQUVPO0FBQ0xDLDBEQUFJLENBQUNDLElBQUwsQ0FBVSxPQUFWLDBCQUEyQyxPQUEzQztBQUNEO0FBQ0YsS0FwQkg7QUFxQkQ7O0FBRUQsc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQUlFLHFFQUFDLG9EQUFEO0FBQU0sY0FBUSxFQUFFLGtCQUFDVixDQUFEO0FBQUEsZUFBT0QsU0FBUyxDQUFDQyxDQUFELENBQWhCO0FBQUEsT0FBaEI7QUFBQSw4QkFDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxpQkFBUyxFQUFDLE1BQXRCO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsWUFBRSxFQUFDLFFBREw7QUFFRSxrQkFBUSxFQUFFLGtCQUFDQSxDQUFELEVBQU87QUFDZjNDLG1CQUFPLENBQUMyQyxDQUFDLENBQUNXLE1BQUYsQ0FBU0MsS0FBVixDQUFQO0FBQ0QsV0FKSDtBQUtFLGtCQUFRLE1BTFY7QUFBQSxrQ0FPRTtBQUFRLGlCQUFLLEVBQUMsRUFBZDtBQUFpQixvQkFBUSxNQUF6QjtBQUEwQixvQkFBUSxNQUFsQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFQRixlQVVFO0FBQVEsaUJBQUssRUFBQyxRQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVZGLGVBV0U7QUFBUSxpQkFBSyxFQUFDLFNBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBWEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBa0JFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGlCQUFTLEVBQUMsWUFBdEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxZQUFFLEVBQUMsUUFETDtBQUVFLGVBQUssRUFBRXRELFVBRlQ7QUFHRSxrQkFBUSxFQUFFLGtCQUFDMEMsQ0FBRDtBQUFBLG1CQUFPekMsYUFBYSxDQUFDeUMsQ0FBQyxDQUFDVyxNQUFGLENBQVNDLEtBQVYsQ0FBcEI7QUFBQSxXQUhaO0FBSUUsa0JBQVEsTUFKVjtBQUFBLGtDQU1FO0FBQVEsaUJBQUssRUFBQyxFQUFkO0FBQWlCLG9CQUFRLE1BQXpCO0FBQTBCLG9CQUFRLE1BQWxDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQU5GLEVBU0c1QyxTQVRIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FsQkYsZUFpQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxhQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxRQURQO0FBRUUscUJBQVcsRUFBQyxtQkFGZDtBQUdFLGVBQUssRUFBRVIsV0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUN3QyxDQUFEO0FBQUEsbUJBQU92QyxjQUFjLENBQUN1QyxDQUFDLENBQUNXLE1BQUYsQ0FBU0MsS0FBVixDQUFyQjtBQUFBLFdBSlo7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakNGLGVBNENFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGlCQUFTLEVBQUMsUUFBdEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxjQUFJLEVBQUMsUUFEUDtBQUVFLHFCQUFXLEVBQUMsZUFGZDtBQUdFLGVBQUssRUFBRWxELE1BSFQ7QUFJRSxrQkFBUSxFQUFFLGtCQUFDc0MsQ0FBRDtBQUFBLG1CQUFPckMsU0FBUyxDQUFDcUMsQ0FBQyxDQUFDVyxNQUFGLENBQVNDLEtBQVYsQ0FBaEI7QUFBQTtBQUpaO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBNUNGLGVBc0RFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGlCQUFTLEVBQUMsV0FBdEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxtREFBRDtBQUFBLGtDQUNFLHFFQUFDLG1EQUFEO0FBQUEsbUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0Usa0JBQUksRUFBQyxNQURQO0FBRUUsbUJBQUssRUFBRXRDLFVBRlQ7QUFHRSxzQkFBUSxFQUFFLGtCQUFDMEIsQ0FBRDtBQUFBLHVCQUFPekIsYUFBYSxDQUFDeUIsQ0FBQyxDQUFDVyxNQUFGLENBQVNDLEtBQVYsQ0FBcEI7QUFBQTtBQUhaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBUUUscUVBQUMsbURBQUQ7QUFBQSxtQ0FDRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxrQkFBSSxFQUFDLE1BRFA7QUFFRSxtQkFBSyxFQUFFcEMsVUFGVDtBQUdFLHNCQUFRLEVBQUUsa0JBQUN3QixDQUFEO0FBQUEsdUJBQU92QixhQUFhLENBQUN1QixDQUFDLENBQUNXLE1BQUYsQ0FBU0MsS0FBVixDQUFwQjtBQUFBO0FBSFo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXRERixFQTBFRzFDLFFBQVEsZ0JBQ1AscUVBQUMsc0RBQUQ7QUFBUSxlQUFPLEVBQUMsU0FBaEI7QUFBMEIsWUFBSSxFQUFDLFFBQS9CO0FBQXdDLFVBQUUsRUFBQyxXQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURPLGdCQUtQLHFFQUFDLHNEQUFEO0FBQVEsZUFBTyxFQUFDLFNBQWhCO0FBQTBCLFlBQUksRUFBQyxRQUEvQjtBQUF3QyxVQUFFLEVBQUMsV0FBM0M7QUFBdUQsZ0JBQVEsTUFBL0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0EvRUo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEyRkQ7O0dBakx1QmhCLFciLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcmVjb3JkL2FkZC45OWM4ZDZmNzM5ZDZiNzA4YTk4NS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZVJlZiB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGb3JtLCBCdXR0b24sIFJvdywgQ29sIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBEcm9wZG93bkJ1dHRvbiBmcm9tIFwicmVhY3QtYm9vdHN0cmFwL0Ryb3Bkb3duQnV0dG9uXCI7XHJcbmltcG9ydCBEcm9wZG93biBmcm9tIFwicmVhY3QtYm9vdHN0cmFwL0Ryb3Bkb3duXCI7XHJcbmltcG9ydCBTd2FsIGZyb20gXCJzd2VldGFsZXJ0MlwiO1xyXG5pbXBvcnQgVXNlckNvbnRleHQgZnJvbSBcIi4uLy4uL1VzZXJDb250ZXh0XCI7XHJcbmltcG9ydCBtb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG4vLyBpbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIG5ld0NhdGVnb3J5KCkge1xyXG4gIC8vIGNvbnN0e3VzZXJ9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dClcclxuICAvLyBGb3JtIGlucHV0IHN0YXRlIGhvb2tzXHJcbiAgY29uc3QgW3R5cGUsIHNldFR5cGVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2NhdGVnb3J5SWQsIHNldENhdGVnb3J5SWRdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2Rlc2NyaXB0aW9uLCBzZXREZXNjcmlwdGlvbl0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbYW1vdW50LCBzZXRBbW91bnRdID0gdXNlU3RhdGUoMCk7XHJcbiAgY29uc3QgW2Ftb3VudFRvU2F2ZSwgc2V0QW1vdW50VG9TYXZlXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtjYXRlZ29yaWVzLCBzZXRDYXRlZ29yaWVzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbY2F0T3B0aW9uLCBzZXRDYXRPcHRpb25dID0gdXNlU3RhdGUobnVsbCk7XHJcbiAgY29uc3QgW2lzQWN0aXZlLCBzZXRJc0FjdGl2ZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW3JlY29yZERhdGUsIHNldFJlY29yZERhdGVdID0gdXNlU3RhdGUobW9tZW50KCkuZm9ybWF0KFwiWVlZWS1NTS1ERFwiKSk7XHJcbiAgY29uc3QgW3JlY29yZFRpbWUsIHNldFJlY29yZFRpbWVdID0gdXNlU3RhdGUobW9tZW50KCkuZm9ybWF0KFwiSEg6bW1cIikpO1xyXG5cclxuICAvLyBWYWxpZGF0ZSBmb3JtIGlucHV0IHdoZW5ldmVyIGVtYWlsLCBwYXNzd29yZDEsIG9yIHBhc3N3b3JkMiBpcyBjaGFuZ2VkXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2RldGFpbHNgLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICBzZXRDYXRlZ29yaWVzKGRhdGEuY2F0ZWdvcmllcyk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGxldCBjYXRlZ29yeUxpc3QgPSBjYXRlZ29yaWVzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gdHlwZSk7XHJcblxyXG4gICAgbGV0IGNhdE9uVHlwZSA9IGNhdGVnb3J5TGlzdC5tYXAoKGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgIHJldHVybiA8b3B0aW9uIHZhbHVlPXtjYXRlZ29yeS5faWR9PntjYXRlZ29yeS5uYW1lfTwvb3B0aW9uPjtcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKGNhdE9uVHlwZSk7XHJcbiAgICBzZXRDYXRPcHRpb24oY2F0T25UeXBlKTtcclxuICAgIHNldENhdGVnb3J5SWQoXCJcIik7XHJcbiAgfSwgW3R5cGVdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmICh0eXBlID09IFwiZXhwZW5zZVwiKSB7XHJcbiAgICAgIHNldEFtb3VudFRvU2F2ZSgwIC0gTWF0aC5hYnMoYW1vdW50KSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRBbW91bnRUb1NhdmUoTWF0aC5hYnMoYW1vdW50KSk7XHJcbiAgICB9XHJcbiAgfSwgW3R5cGUsIGFtb3VudF0pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKHR5cGUgIT09IFwiXCIgJiYgY2F0ZWdvcnlJZCAhPT0gXCJcIiAmJiBkZXNjcmlwdGlvbiAhPT0gXCJcIiAmJiBhbW91bnQgPiAwKSB7XHJcbiAgICAgIHNldElzQWN0aXZlKHRydWUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0SXNBY3RpdmUoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH0sIFt0eXBlLCBjYXRlZ29yeUlkLCBkZXNjcmlwdGlvbiwgYW1vdW50XSk7XHJcblxyXG4gIGZ1bmN0aW9uIGFkZFJlY29yZChlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBjb25zb2xlLmxvZyhjYXRlZ29yeUlkKTtcclxuICAgIGNvbnNvbGUubG9nKGRlc2NyaXB0aW9uKTtcclxuICAgIGNvbnNvbGUubG9nKGFtb3VudCk7XHJcbiAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZERhdGUgKyBcIiBcIiArIHJlY29yZFRpbWUpO1xyXG4gICAgY29uc29sZS5sb2codXBkYXRlZE9uKTtcclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2FkZFJlY29yZGAsIHtcclxuICAgICAgbWV0aG9kOiBcIlBVVFwiLFxyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGNhdGVnb3J5SWQ6IGNhdGVnb3J5SWQsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uLFxyXG4gICAgICAgIGFtb3VudDogYW1vdW50VG9TYXZlLFxyXG4gICAgICAgIHVwZGF0ZWRPbjogdXBkYXRlZE9uLFxyXG4gICAgICB9KSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgUm91dGVyLnB1c2goXCIuXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBTd2FsLmZpcmUoXCJFcnJvclwiLCBgU29tZXRoaW5nIHdlbnQgd3JvbmdgLCBcImVycm9yXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+QWRkIFJlY29yZDwvdGl0bGU+XHJcbiAgICAgIDwvSGVhZD5cclxuICAgICAgPEZvcm0gb25TdWJtaXQ9eyhlKSA9PiBhZGRSZWNvcmQoZSl9PlxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInR5cGVcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPkNhdGVnb3J5IFR5cGU8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiB7XHJcbiAgICAgICAgICAgICAgc2V0VHlwZShlLnRhcmdldC52YWx1ZSk7XHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJcIiBkaXNhYmxlZCBzZWxlY3RlZD5cclxuICAgICAgICAgICAgICBTZWxlY3QgQ2F0ZWdvcnkgVHlwZVxyXG4gICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cImluY29tZVwiPkluY29tZTwvb3B0aW9uPlxyXG4gICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiZXhwZW5zZVwiPkV4cGVuc2U8L29wdGlvbj5cclxuICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxyXG4gICAgICAgIDwvRm9ybS5Hcm91cD5cclxuXHJcbiAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwiY2F0ZWdvcnlJZFwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+Q2F0ZWdvcnkgTmFtZTwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxyXG4gICAgICAgICAgICB2YWx1ZT17Y2F0ZWdvcnlJZH1cclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRDYXRlZ29yeUlkKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlwiIGRpc2FibGVkIHNlbGVjdGVkPlxyXG4gICAgICAgICAgICAgIFNlbGVjdCBDYXRlZ29yeSBUeXBlXHJcbiAgICAgICAgICAgIDwvb3B0aW9uPlxyXG4gICAgICAgICAgICB7Y2F0T3B0aW9ufVxyXG4gICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJkZXNjcmlwdGlvblwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+RGVzY3JpcHRpb248L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJzdHJpbmdcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIERlc2NyaXB0aW9uXCJcclxuICAgICAgICAgICAgdmFsdWU9e2Rlc2NyaXB0aW9ufVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldERlc2NyaXB0aW9uKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJhbW91bnRcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPkFtb3VudDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgdHlwZT1cIm51bWJlclwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiSW5zZXJ0IEFtb3VudFwiXHJcbiAgICAgICAgICAgIHZhbHVlPXthbW91bnR9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0QW1vdW50KGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJ0aW1lc3RhbXBcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPkRhdGU8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Um93PlxyXG4gICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICAgIHZhbHVlPXtyZWNvcmREYXRlfVxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRSZWNvcmREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwidGltZVwiXHJcbiAgICAgICAgICAgICAgICB2YWx1ZT17cmVjb3JkVGltZX1cclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0UmVjb3JkVGltZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICA8L1Jvdz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcblxyXG4gICAgICAgIHtpc0FjdGl2ZSA/IChcclxuICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cInByaW1hcnlcIiB0eXBlPVwic3VibWl0XCIgaWQ9XCJzdWJtaXRCdG5cIj5cclxuICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICApIDogKFxyXG4gICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIHR5cGU9XCJzdWJtaXRcIiBpZD1cInN1Ym1pdEJ0blwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvRm9ybT5cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9