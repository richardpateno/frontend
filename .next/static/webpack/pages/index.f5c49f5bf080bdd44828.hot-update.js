webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/login.js":
/*!************************!*\
  !*** ./pages/login.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return login; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-google-login */ "./node_modules/react-google-login/dist/google-login.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _components_View__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/View */ "./components/View.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app-helper */ "./app-helper.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_app_helper__WEBPACK_IMPORTED_MODULE_9__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\login.js",
    _this = undefined,
    _s = $RefreshSig$();










function login() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_View__WEBPACK_IMPORTED_MODULE_8__["default"], {
    title: "Login",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      className: "justify-content-center",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: true,
        md: "6",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(LoginForm, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

var LoginForm = function LoginForm() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_7__["default"]),
      user = _useContext.user,
      setUser = _useContext.setUser;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState[0],
      setEmail = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password = _useState2[0],
      setPassword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isActive = _useState3[0],
      setIsActive = _useState3[1];

  function authenticate(e) {
    e.preventDefault();
    var options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    };
    fetch("http://localhost:4000/api/users/login", options).then(function (res) {
      return res.json();
    }).then(function (data) {
      console.log(data);

      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error === "does-not-exist") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "User does not exist.", "error");
        } else if (data.error === "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure, try an alternative login procedure", "error");
        } else if (data.error === "incorrect-password") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "password is incorrect", "error");
        }
      }
    });
  }

  var aunthenticateGoogleToken = function aunthenticateGoogleToken(response) {
    var payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    };
    fetch("http://localhost:4000/api/users/verify-google-id-token", payload).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error = "google-auth-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Google Auth Error", "Google authentication procedure failed.", "error");
        } else if (data.error = "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure.", "error");
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Header, {
      children: "Login details"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_6___default.a, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
          children: "Budget Tracker"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 115,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 114,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
        onSubmit: function onSubmit(e) {
          return authenticate(e);
        },
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "userEmail",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Email address"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 119,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "email",
            placeholder: "Enter email",
            value: email,
            onChange: function onChange(e) {
              return setEmail(e.target.value);
            },
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 120,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 118,
          columnNumber: 11
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "password",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Password"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "password",
            placeholder: "Password",
            value: password,
            onChange: function onChange(e) {
              return setPassword(e.target.value);
            },
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 131,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 11
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          className: "justify-content-center px-3",
          children: isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 141,
            columnNumber: 15
          }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            disabled: true,
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 139,
          columnNumber: 11
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_google_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLogin"], {
        clientId: "656593662569-24gufu44evj4ujqs3k0113rgrurq2bnp.apps.googleusercontent.com",
        buttonText: "Login",
        onSuccess: aunthenticateGoogleToken,
        onFailure: aunthenticateGoogleToken,
        cookiePolicy: "single_host_origin",
        className: "w-100 text-center d-flex justify-content-center"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 152,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 111,
    columnNumber: 5
  }, _this);
};

_s(LoginForm, "A6LN1EkaZmgXDaI+XK75hYWiYQs=");

_c = LoginForm;

var _c;

$RefreshReg$(_c, "LoginForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvbG9naW4uanMiXSwibmFtZXMiOlsibG9naW4iLCJMb2dpbkZvcm0iLCJ1c2VDb250ZXh0IiwiVXNlckNvbnRleHQiLCJ1c2VyIiwic2V0VXNlciIsInVzZVN0YXRlIiwiZW1haWwiLCJzZXRFbWFpbCIsInBhc3N3b3JkIiwic2V0UGFzc3dvcmQiLCJpc0FjdGl2ZSIsInNldElzQWN0aXZlIiwiYXV0aGVudGljYXRlIiwiZSIsInByZXZlbnREZWZhdWx0Iiwib3B0aW9ucyIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsImZldGNoIiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiY29uc29sZSIsImxvZyIsImFjY2Vzc1Rva2VuIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsImlkIiwiX2lkIiwiUm91dGVyIiwicHVzaCIsImVycm9yIiwiU3dhbCIsImZpcmUiLCJhdW50aGVudGljYXRlR29vZ2xlVG9rZW4iLCJyZXNwb25zZSIsInBheWxvYWQiLCJ0b2tlbklkIiwidXNlRWZmZWN0IiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0EsS0FBVCxHQUFpQjtBQUM5QixzQkFDRSxxRUFBQyx3REFBRDtBQUFNLFNBQUssRUFBRSxPQUFiO0FBQUEsMkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxlQUFTLEVBQUMsd0JBQWY7QUFBQSw2QkFDRSxxRUFBQyxtREFBRDtBQUFLLFVBQUUsTUFBUDtBQUFRLFVBQUUsRUFBQyxHQUFYO0FBQUEsK0JBQ0UscUVBQUMsU0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFTRDs7QUFFRCxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQUFNO0FBQUE7O0FBQUEsb0JBQ0lDLHdEQUFVLENBQUNDLG9EQUFELENBRGQ7QUFBQSxNQUNkQyxJQURjLGVBQ2RBLElBRGM7QUFBQSxNQUNSQyxPQURRLGVBQ1JBLE9BRFE7O0FBQUEsa0JBR0lDLHNEQUFRLENBQUMsRUFBRCxDQUhaO0FBQUEsTUFHZkMsS0FIZTtBQUFBLE1BR1JDLFFBSFE7O0FBQUEsbUJBSVVGLHNEQUFRLENBQUMsRUFBRCxDQUpsQjtBQUFBLE1BSWZHLFFBSmU7QUFBQSxNQUlMQyxXQUpLOztBQUFBLG1CQUtVSixzREFBUSxDQUFDLEtBQUQsQ0FMbEI7QUFBQSxNQUtmSyxRQUxlO0FBQUEsTUFLTEMsV0FMSzs7QUFPdEIsV0FBU0MsWUFBVCxDQUFzQkMsQ0FBdEIsRUFBeUI7QUFDdkJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBLFFBQU1DLE9BQU8sR0FBRztBQUNkQyxZQUFNLEVBQUUsTUFETTtBQUVkQyxhQUFPLEVBQUU7QUFBRSx3QkFBZ0I7QUFBbEIsT0FGSztBQUdkQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CZCxhQUFLLEVBQUVBLEtBRFk7QUFFbkJFLGdCQUFRLEVBQUVBO0FBRlMsT0FBZjtBQUhRLEtBQWhCO0FBU0FhLFNBQUssMENBQTBDTixPQUExQyxDQUFMLENBQ0dPLElBREgsQ0FDUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQURSLEVBRUdGLElBRkgsQ0FFUSxVQUFDRyxJQUFELEVBQVU7QUFDZEMsYUFBTyxDQUFDQyxHQUFSLENBQVlGLElBQVo7O0FBQ0EsVUFBSSxPQUFPQSxJQUFJLENBQUNHLFdBQVosS0FBNEIsV0FBaEMsRUFBNkM7QUFDM0NDLG9CQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsRUFBOEJMLElBQUksQ0FBQ0csV0FBbkM7QUFDQXhCLGVBQU8sQ0FBQztBQUFFMkIsWUFBRSxFQUFFTixJQUFJLENBQUNPO0FBQVgsU0FBRCxDQUFQO0FBQ0FDLDBEQUFNLENBQUNDLElBQVAsQ0FBWSxHQUFaO0FBQ0QsT0FKRCxNQUlPO0FBQ0wsWUFBSVQsSUFBSSxDQUFDVSxLQUFMLEtBQWUsZ0JBQW5CLEVBQXFDO0FBQ25DQyw0REFBSSxDQUFDQyxJQUFMLENBQVUsdUJBQVYsRUFBbUMsc0JBQW5DLEVBQTJELE9BQTNEO0FBQ0QsU0FGRCxNQUVPLElBQUlaLElBQUksQ0FBQ1UsS0FBTCxLQUFlLGtCQUFuQixFQUF1QztBQUM1Q0MsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLGtCQURGLEVBRUUsaUdBRkYsRUFHRSxPQUhGO0FBS0QsU0FOTSxNQU1BLElBQUlaLElBQUksQ0FBQ1UsS0FBTCxLQUFlLG9CQUFuQixFQUF5QztBQUM5Q0MsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLHVCQURGLEVBRUUsdUJBRkYsRUFHRSxPQUhGO0FBS0Q7QUFDRjtBQUNGLEtBekJIO0FBMEJEOztBQUVELE1BQU1DLHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsQ0FBQ0MsUUFBRCxFQUFjO0FBQzdDLFFBQU1DLE9BQU8sR0FBRztBQUNkeEIsWUFBTSxFQUFFLE1BRE07QUFFZEMsYUFBTyxFQUFFO0FBQUUsd0JBQWdCO0FBQWxCLE9BRks7QUFHZEMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFFcUIsZUFBTyxFQUFFRixRQUFRLENBQUNFO0FBQXBCLE9BQWY7QUFIUSxLQUFoQjtBQU1BcEIsU0FBSywyREFBMkRtQixPQUEzRCxDQUFMLENBQ0dsQixJQURILENBQ1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FEUixFQUVHRixJQUZILENBRVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2QsVUFBSSxPQUFPQSxJQUFJLENBQUNHLFdBQVosS0FBNEIsV0FBaEMsRUFBNkM7QUFDM0NDLG9CQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsRUFBOEJMLElBQUksQ0FBQ0csV0FBbkM7QUFDQXhCLGVBQU8sQ0FBQztBQUFFMkIsWUFBRSxFQUFFTixJQUFJLENBQUNPO0FBQVgsU0FBRCxDQUFQO0FBQ0FDLDBEQUFNLENBQUNDLElBQVAsQ0FBWSxHQUFaO0FBQ0QsT0FKRCxNQUlPO0FBQ0wsWUFBS1QsSUFBSSxDQUFDVSxLQUFMLEdBQWEsbUJBQWxCLEVBQXdDO0FBQ3RDQyw0REFBSSxDQUFDQyxJQUFMLENBQ0UsbUJBREYsRUFFRSx5Q0FGRixFQUdFLE9BSEY7QUFLRCxTQU5ELE1BTU8sSUFBS1osSUFBSSxDQUFDVSxLQUFMLEdBQWEsa0JBQWxCLEVBQXVDO0FBQzVDQyw0REFBSSxDQUFDQyxJQUFMLENBQ0Usa0JBREYsRUFFRSw4REFGRixFQUdFLE9BSEY7QUFLRDtBQUNGO0FBQ0YsS0F0Qkg7QUF1QkQsR0E5QkQ7O0FBZ0NBSyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJcEMsS0FBSyxLQUFLLEVBQVYsSUFBZ0JFLFFBQVEsS0FBSyxFQUFqQyxFQUFxQztBQUNuQ0csaUJBQVcsQ0FBQyxJQUFELENBQVg7QUFDRCxLQUZELE1BRU87QUFDTEEsaUJBQVcsQ0FBQyxLQUFELENBQVg7QUFDRDtBQUNGLEdBTlEsRUFNTixDQUFDTCxLQUFELEVBQVFFLFFBQVIsQ0FOTSxDQUFUO0FBUUEsc0JBQ0UscUVBQUMsb0RBQUQ7QUFBQSw0QkFDRSxxRUFBQyxvREFBRCxDQUFNLE1BQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLDhCQUNFLHFFQUFDLGdEQUFEO0FBQUEsK0JBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREYsZUFJRSxxRUFBQyxvREFBRDtBQUFNLGdCQUFRLEVBQUUsa0JBQUNLLENBQUQ7QUFBQSxpQkFBT0QsWUFBWSxDQUFDQyxDQUFELENBQW5CO0FBQUEsU0FBaEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxtQkFBUyxFQUFDLFdBQXRCO0FBQUEsa0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsZ0JBQUksRUFBQyxPQURQO0FBRUUsdUJBQVcsRUFBQyxhQUZkO0FBR0UsaUJBQUssRUFBRVAsS0FIVDtBQUlFLG9CQUFRLEVBQUUsa0JBQUNPLENBQUQ7QUFBQSxxQkFBT04sUUFBUSxDQUFDTSxDQUFDLENBQUM4QixNQUFGLENBQVNDLEtBQVYsQ0FBZjtBQUFBLGFBSlo7QUFLRSxvQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBWUUscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksbUJBQVMsRUFBQyxVQUF0QjtBQUFBLGtDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGdCQUFJLEVBQUMsVUFEUDtBQUVFLHVCQUFXLEVBQUMsVUFGZDtBQUdFLGlCQUFLLEVBQUVwQyxRQUhUO0FBSUUsb0JBQVEsRUFBRSxrQkFBQ0ssQ0FBRDtBQUFBLHFCQUFPSixXQUFXLENBQUNJLENBQUMsQ0FBQzhCLE1BQUYsQ0FBU0MsS0FBVixDQUFsQjtBQUFBLGFBSlo7QUFLRSxvQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVpGLGVBc0JFLHFFQUFDLG1EQUFEO0FBQUssbUJBQVMsRUFBQyw2QkFBZjtBQUFBLG9CQUNHbEMsUUFBUSxnQkFDUCxxRUFBQyxzREFBRDtBQUFRLHFCQUFTLEVBQUMsc0JBQWxCO0FBQXlDLGdCQUFJLEVBQUMsUUFBOUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRE8sZ0JBS1AscUVBQUMsc0RBQUQ7QUFBUSxxQkFBUyxFQUFDLHNCQUFsQjtBQUF5QyxnQkFBSSxFQUFDLFFBQTlDO0FBQXVELG9CQUFRLE1BQS9EO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTko7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF0QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBSkYsZUF1Q0UscUVBQUMsOERBQUQ7QUFDRSxnQkFBUSxFQUFDLDBFQURYO0FBRUUsa0JBQVUsRUFBQyxPQUZiO0FBR0UsaUJBQVMsRUFBRTRCLHdCQUhiO0FBSUUsaUJBQVMsRUFBRUEsd0JBSmI7QUFLRSxvQkFBWSxFQUFFLG9CQUxoQjtBQU1FLGlCQUFTLEVBQUM7QUFOWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBdkNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURGO0FBcURELENBNUlEOztHQUFNdEMsUzs7S0FBQUEsUyIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC5mNWM0OWY1YmYwODBiZGQ0NDgyOC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgRm9ybSwgQnV0dG9uLCBDYXJkLCBSb3csIENvbCB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHsgR29vZ2xlTG9naW4gfSBmcm9tIFwicmVhY3QtZ29vZ2xlLWxvZ2luXCI7XHJcbmltcG9ydCBTd2FsIGZyb20gXCJzd2VldGFsZXJ0MlwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vVXNlckNvbnRleHRcIjtcclxuaW1wb3J0IFZpZXcgZnJvbSBcIi4uL2NvbXBvbmVudHMvVmlld1wiO1xyXG5pbXBvcnQgQXBwSGVscGVyIGZyb20gXCIuLi9hcHAtaGVscGVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBsb2dpbigpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPFZpZXcgdGl0bGU9e1wiTG9naW5cIn0+XHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgIDxDb2wgeHMgbWQ9XCI2XCI+XHJcbiAgICAgICAgICA8TG9naW5Gb3JtIC8+XHJcbiAgICAgICAgPC9Db2w+XHJcbiAgICAgIDwvUm93PlxyXG4gICAgPC9WaWV3PlxyXG4gICk7XHJcbn1cclxuXHJcbmNvbnN0IExvZ2luRm9ybSA9ICgpID0+IHtcclxuICBjb25zdCB7IHVzZXIsIHNldFVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpO1xyXG5cclxuICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtwYXNzd29yZCwgc2V0UGFzc3dvcmRdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2lzQWN0aXZlLCBzZXRJc0FjdGl2ZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIGZ1bmN0aW9uIGF1dGhlbnRpY2F0ZShlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgY29uc3Qgb3B0aW9ucyA9IHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczogeyBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIiB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgZW1haWw6IGVtYWlsLFxyXG4gICAgICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcclxuICAgICAgfSksXHJcbiAgICB9O1xyXG5cclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2xvZ2luYCwgb3B0aW9ucylcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICBpZiAodHlwZW9mIGRhdGEuYWNjZXNzVG9rZW4gIT09IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwidG9rZW5cIiwgZGF0YS5hY2Nlc3NUb2tlbik7XHJcbiAgICAgICAgICBzZXRVc2VyKHsgaWQ6IGRhdGEuX2lkIH0pO1xyXG4gICAgICAgICAgUm91dGVyLnB1c2goXCIvXCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoZGF0YS5lcnJvciA9PT0gXCJkb2VzLW5vdC1leGlzdFwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcIkF1dGhlbnRpY2F0aW9uIEZhaWxlZFwiLCBcIlVzZXIgZG9lcyBub3QgZXhpc3QuXCIsIFwiZXJyb3JcIik7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09IFwibG9naW4tdHlwZS1lcnJvclwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcclxuICAgICAgICAgICAgICBcIkxvZ2luIFR5cGUgRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIllvdSBtYXkgaGF2ZSByZWdpc3RlcmVkIHRocm91Z2ggYSBkaWZmZXJlbnQgbG9naW4gcHJvY2VkdXJlLCB0cnkgYW4gYWx0ZXJuYXRpdmUgbG9naW4gcHJvY2VkdXJlXCIsXHJcbiAgICAgICAgICAgICAgXCJlcnJvclwiXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09IFwiaW5jb3JyZWN0LXBhc3N3b3JkXCIpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFxyXG4gICAgICAgICAgICAgIFwiQXV0aGVudGljYXRpb24gRmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgXCJwYXNzd29yZCBpcyBpbmNvcnJlY3RcIixcclxuICAgICAgICAgICAgICBcImVycm9yXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29uc3QgYXVudGhlbnRpY2F0ZUdvb2dsZVRva2VuID0gKHJlc3BvbnNlKSA9PiB7XHJcbiAgICBjb25zdCBwYXlsb2FkID0ge1xyXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICBoZWFkZXJzOiB7IFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgdG9rZW5JZDogcmVzcG9uc2UudG9rZW5JZCB9KSxcclxuICAgIH07XHJcblxyXG4gICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvdmVyaWZ5LWdvb2dsZS1pZC10b2tlbmAsIHBheWxvYWQpXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhLmFjY2Vzc1Rva2VuICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInRva2VuXCIsIGRhdGEuYWNjZXNzVG9rZW4pO1xyXG4gICAgICAgICAgc2V0VXNlcih7IGlkOiBkYXRhLl9pZCB9KTtcclxuICAgICAgICAgIFJvdXRlci5wdXNoKFwiL1wiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaWYgKChkYXRhLmVycm9yID0gXCJnb29nbGUtYXV0aC1lcnJvclwiKSkge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJHb29nbGUgQXV0aCBFcnJvclwiLFxyXG4gICAgICAgICAgICAgIFwiR29vZ2xlIGF1dGhlbnRpY2F0aW9uIHByb2NlZHVyZSBmYWlsZWQuXCIsXHJcbiAgICAgICAgICAgICAgXCJlcnJvclwiXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKChkYXRhLmVycm9yID0gXCJsb2dpbi10eXBlLWVycm9yXCIpKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZShcclxuICAgICAgICAgICAgICBcIkxvZ2luIFR5cGUgRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIllvdSBtYXkgaGF2ZSByZWdpc3RlcmVkIHRocm91Z2ggYSBkaWZmZXJlbnQgbG9naW4gcHJvY2VkdXJlLlwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChlbWFpbCAhPT0gXCJcIiAmJiBwYXNzd29yZCAhPT0gXCJcIikge1xyXG4gICAgICBzZXRJc0FjdGl2ZSh0cnVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldElzQWN0aXZlKGZhbHNlKTtcclxuICAgIH1cclxuICB9LCBbZW1haWwsIHBhc3N3b3JkXSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Q2FyZD5cclxuICAgICAgPENhcmQuSGVhZGVyPkxvZ2luIGRldGFpbHM8L0NhcmQuSGVhZGVyPlxyXG4gICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgPHRpdGxlPkJ1ZGdldCBUcmFja2VyPC90aXRsZT5cclxuICAgICAgICA8L0hlYWQ+XHJcbiAgICAgICAgPEZvcm0gb25TdWJtaXQ9eyhlKSA9PiBhdXRoZW50aWNhdGUoZSl9PlxyXG4gICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwidXNlckVtYWlsXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPkVtYWlsIGFkZHJlc3M8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIlxyXG4gICAgICAgICAgICAgIHZhbHVlPXtlbWFpbH1cclxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEVtYWlsKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInBhc3N3b3JkXCI+XHJcbiAgICAgICAgICAgIDxGb3JtLkxhYmVsPlBhc3N3b3JkPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17cGFzc3dvcmR9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRQYXNzd29yZChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlciBweC0zXCI+XHJcbiAgICAgICAgICAgIHtpc0FjdGl2ZSA/IChcclxuICAgICAgICAgICAgICA8QnV0dG9uIGNsYXNzTmFtZT1cImJnLXByaW1hcnkgYnRuLWJsb2NrXCIgdHlwZT1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9XCJiZy1wcmltYXJ5IGJ0bi1ibG9ja1wiIHR5cGU9XCJzdWJtaXRcIiBkaXNhYmxlZD5cclxuICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICApfVxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgPC9Gb3JtPlxyXG5cclxuICAgICAgICA8R29vZ2xlTG9naW5cclxuICAgICAgICAgIGNsaWVudElkPVwiNjU2NTkzNjYyNTY5LTI0Z3VmdTQ0ZXZqNHVqcXMzazAxMTNyZ3J1cnEyYm5wLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tXCJcclxuICAgICAgICAgIGJ1dHRvblRleHQ9XCJMb2dpblwiXHJcbiAgICAgICAgICBvblN1Y2Nlc3M9e2F1bnRoZW50aWNhdGVHb29nbGVUb2tlbn1cclxuICAgICAgICAgIG9uRmFpbHVyZT17YXVudGhlbnRpY2F0ZUdvb2dsZVRva2VufVxyXG4gICAgICAgICAgY29va2llUG9saWN5PXtcInNpbmdsZV9ob3N0X29yaWdpblwifVxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwidy0xMDAgdGV4dC1jZW50ZXIgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIlxyXG4gICAgICAgIC8+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICk7XHJcbn07XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=