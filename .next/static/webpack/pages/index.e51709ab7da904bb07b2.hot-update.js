webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/category/index.js":
/*!*********************************!*\
  !*** ./pages/category/index.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../helpers/sumByGroup */ "./helpers/sumByGroup.js");


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\category\\index.js",
    _s = $RefreshSig$();








function category() {
  _s();

  var _this = this;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      categories = _useState[0],
      setCategories = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      currentRecords = _useState2[0],
      setCurRec = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      incomeCategories = _useState3[0],
      setIncCat = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      expensesCategories = _useState4[0],
      setExpCat = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      incomeRows = _useState5[0],
      setIncomeRows = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(null),
      expensesRows = _useState6[0],
      setExpensesRows = _useState6[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    fetch("http://localhost:4000/api/users/details", {
      headers: {
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      }
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      setCategories(data.categories);
      var dateBefore = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.startDate).subtract(1, "day");
      var dateAfter = moment__WEBPACK_IMPORTED_MODULE_6___default()(data.currentDate.endDate).add(1, "day");
      var filteredRecords = data.records.filter(function (record) {
        var updatedOn = moment__WEBPACK_IMPORTED_MODULE_6___default()(record.updatedOn).startOf("days");
        return moment__WEBPACK_IMPORTED_MODULE_6___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
      });
      setCurRec(filteredRecords);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var categoryRecords = Object(_helpers_sumByGroup__WEBPACK_IMPORTED_MODULE_7__["default"])(categories, "_id", currentRecords, "categoryId", "amount");
    setIncCat(categoryRecords.filter(function (category) {
      return category.type == "income";
    }));
    setExpCat(categoryRecords.filter(function (category) {
      return category.type == "expense";
    }));
  }, [categories, currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setIncomeRows(incomeCategories.map(function (category) {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 57,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 13
        }, _this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 11
      }, _this);
    }));
  }, [incomeCategories]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setExpensesRows(expensesCategories.map(function (category) {
      return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: Math.abs(category.amount)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
          children: category.amount + category.budget
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 13
        }, _this)]
      }, category._id, true, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 11
      }, _this);
    }));
  }, [expensesCategories]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: " Categories"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], {
      className: "justify-content-md-end my-2",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "success",
        onClick: function onClick() {
          return next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add");
        },
        children: "Add Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "secondary",
        onClick: function onClick() {
          return next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/edit");
        },
        children: "Edit Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "danger",
        onClick: function onClick() {
          return next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("./category/add");
        },
        children: "Delete Category"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 102,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pull-right",
        size: "lg",
        variant: "info",
        onClick: function onClick() {
          return next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("../record/add");
        },
        children: "Add Record"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 111,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Income:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 7
    }, this), incomeCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 122,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 127,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Income"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 128,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: incomeRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 132,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 9
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
      children: "Expenses:"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 135,
      columnNumber: 7
    }, this), expensesCategories.length == 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Alert"], {
      variant: "info",
      children: "You have no categories under expenses yet."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Category Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 142,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Budget"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 143,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Expenses"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 15
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("th", {
            children: "Current Savings"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 145,
            columnNumber: 15
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 141,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tbody", {
        children: expensesRows
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 149,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 5
  }, this);
}

_s(category, "1mm0mE+mGN5Nmzg1vaDwoF5bTw8=");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvY2F0ZWdvcnkvaW5kZXguanMiXSwibmFtZXMiOlsiY2F0ZWdvcnkiLCJ1c2VTdGF0ZSIsImNhdGVnb3JpZXMiLCJzZXRDYXRlZ29yaWVzIiwiY3VycmVudFJlY29yZHMiLCJzZXRDdXJSZWMiLCJpbmNvbWVDYXRlZ29yaWVzIiwic2V0SW5jQ2F0IiwiZXhwZW5zZXNDYXRlZ29yaWVzIiwic2V0RXhwQ2F0IiwiaW5jb21lUm93cyIsInNldEluY29tZVJvd3MiLCJleHBlbnNlc1Jvd3MiLCJzZXRFeHBlbnNlc1Jvd3MiLCJ1c2VFZmZlY3QiLCJmZXRjaCIsImhlYWRlcnMiLCJBdXRob3JpemF0aW9uIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImRhdGVCZWZvcmUiLCJtb21lbnQiLCJjdXJyZW50RGF0ZSIsInN0YXJ0RGF0ZSIsInN1YnRyYWN0IiwiZGF0ZUFmdGVyIiwiZW5kRGF0ZSIsImFkZCIsImZpbHRlcmVkUmVjb3JkcyIsInJlY29yZHMiLCJmaWx0ZXIiLCJyZWNvcmQiLCJ1cGRhdGVkT24iLCJzdGFydE9mIiwiaXNCZXR3ZWVuIiwiY2F0ZWdvcnlSZWNvcmRzIiwic3VtQnlHcm91cCIsInR5cGUiLCJtYXAiLCJuYW1lIiwiYW1vdW50IiwiX2lkIiwiYnVkZ2V0IiwiTWF0aCIsImFicyIsIlJvdXRlciIsInB1c2giLCJsZW5ndGgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLFFBQVQsR0FBb0I7QUFBQTs7QUFBQTs7QUFBQSxrQkFDR0Msc0RBQVEsQ0FBQyxFQUFELENBRFg7QUFBQSxNQUMxQkMsVUFEMEI7QUFBQSxNQUNkQyxhQURjOztBQUFBLG1CQUVHRixzREFBUSxDQUFDLEVBQUQsQ0FGWDtBQUFBLE1BRTFCRyxjQUYwQjtBQUFBLE1BRVZDLFNBRlU7O0FBQUEsbUJBR0tKLHNEQUFRLENBQUMsRUFBRCxDQUhiO0FBQUEsTUFHMUJLLGdCQUgwQjtBQUFBLE1BR1JDLFNBSFE7O0FBQUEsbUJBSU9OLHNEQUFRLENBQUMsRUFBRCxDQUpmO0FBQUEsTUFJMUJPLGtCQUowQjtBQUFBLE1BSU5DLFNBSk07O0FBQUEsbUJBS0dSLHNEQUFRLENBQUMsSUFBRCxDQUxYO0FBQUEsTUFLMUJTLFVBTDBCO0FBQUEsTUFLZEMsYUFMYzs7QUFBQSxtQkFNT1Ysc0RBQVEsQ0FBQyxJQUFELENBTmY7QUFBQSxNQU0xQlcsWUFOMEI7QUFBQSxNQU1aQyxlQU5ZOztBQVFqQ0MseURBQVMsQ0FBQyxZQUFNO0FBQ2RDLFNBQUssNENBQTRDO0FBQy9DQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsbUJBQVlDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFaO0FBRE47QUFEc0MsS0FBNUMsQ0FBTCxDQUtHQyxJQUxILENBS1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FMUixFQU1HRixJQU5ILENBTVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2RwQixtQkFBYSxDQUFDb0IsSUFBSSxDQUFDckIsVUFBTixDQUFiO0FBRUEsVUFBSXNCLFVBQVUsR0FBR0MsNkNBQU0sQ0FBQ0YsSUFBSSxDQUFDRyxXQUFMLENBQWlCQyxTQUFsQixDQUFOLENBQW1DQyxRQUFuQyxDQUE0QyxDQUE1QyxFQUErQyxLQUEvQyxDQUFqQjtBQUNBLFVBQUlDLFNBQVMsR0FBR0osNkNBQU0sQ0FBQ0YsSUFBSSxDQUFDRyxXQUFMLENBQWlCSSxPQUFsQixDQUFOLENBQWlDQyxHQUFqQyxDQUFxQyxDQUFyQyxFQUF3QyxLQUF4QyxDQUFoQjtBQUVBLFVBQUlDLGVBQWUsR0FBR1QsSUFBSSxDQUFDVSxPQUFMLENBQWFDLE1BQWIsQ0FBb0IsVUFBQ0MsTUFBRCxFQUFZO0FBQ3BELFlBQUlDLFNBQVMsR0FBR1gsNkNBQU0sQ0FBQ1UsTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJDLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsZUFBT1osNkNBQU0sQ0FBQ1csU0FBRCxDQUFOLENBQWtCRSxTQUFsQixDQUE0QmQsVUFBNUIsRUFBd0NLLFNBQXhDLEVBQW1ELEtBQW5ELENBQVA7QUFDRCxPQUhxQixDQUF0QjtBQUtBeEIsZUFBUyxDQUFDMkIsZUFBRCxDQUFUO0FBQ0QsS0FsQkg7QUFtQkQsR0FwQlEsRUFvQk4sRUFwQk0sQ0FBVDtBQXNCQWxCLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUl5QixlQUFlLEdBQUdDLG1FQUFVLENBQzlCdEMsVUFEOEIsRUFFOUIsS0FGOEIsRUFHOUJFLGNBSDhCLEVBSTlCLFlBSjhCLEVBSzlCLFFBTDhCLENBQWhDO0FBUUFHLGFBQVMsQ0FBQ2dDLGVBQWUsQ0FBQ0wsTUFBaEIsQ0FBdUIsVUFBQ2xDLFFBQUQ7QUFBQSxhQUFjQSxRQUFRLENBQUN5QyxJQUFULElBQWlCLFFBQS9CO0FBQUEsS0FBdkIsQ0FBRCxDQUFUO0FBQ0FoQyxhQUFTLENBQUM4QixlQUFlLENBQUNMLE1BQWhCLENBQXVCLFVBQUNsQyxRQUFEO0FBQUEsYUFBY0EsUUFBUSxDQUFDeUMsSUFBVCxJQUFpQixTQUEvQjtBQUFBLEtBQXZCLENBQUQsQ0FBVDtBQUNELEdBWFEsRUFXTixDQUFDdkMsVUFBRCxFQUFhRSxjQUFiLENBWE0sQ0FBVDtBQWFBVSx5REFBUyxDQUFDLFlBQU07QUFDZEgsaUJBQWEsQ0FDWEwsZ0JBQWdCLENBQUNvQyxHQUFqQixDQUFxQixVQUFDMUMsUUFBRCxFQUFjO0FBQ2pDLDBCQUNFO0FBQUEsZ0NBQ0U7QUFBQSxvQkFBS0EsUUFBUSxDQUFDMkM7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBRUU7QUFBQSxvQkFBSzNDLFFBQVEsQ0FBQzRDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUFBLFNBQVM1QyxRQUFRLENBQUM2QyxHQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFNRCxLQVBELENBRFcsQ0FBYjtBQVVELEdBWFEsRUFXTixDQUFDdkMsZ0JBQUQsQ0FYTSxDQUFUO0FBYUFRLHlEQUFTLENBQUMsWUFBTTtBQUNkRCxtQkFBZSxDQUNiTCxrQkFBa0IsQ0FBQ2tDLEdBQW5CLENBQXVCLFVBQUMxQyxRQUFELEVBQWM7QUFDbkMsMEJBQ0U7QUFBQSxnQ0FDRTtBQUFBLG9CQUFLQSxRQUFRLENBQUMyQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFFRTtBQUFBLG9CQUFLM0MsUUFBUSxDQUFDOEM7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZGLGVBR0U7QUFBQSxvQkFBS0MsSUFBSSxDQUFDQyxHQUFMLENBQVNoRCxRQUFRLENBQUM0QyxNQUFsQjtBQUFMO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSEYsZUFJRTtBQUFBLG9CQUFLNUMsUUFBUSxDQUFDNEMsTUFBVCxHQUFrQjVDLFFBQVEsQ0FBQzhDO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSkY7QUFBQSxTQUFTOUMsUUFBUSxDQUFDNkMsR0FBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGO0FBUUQsS0FURCxDQURhLENBQWY7QUFZRCxHQWJRLEVBYU4sQ0FBQ3JDLGtCQUFELENBYk0sQ0FBVDtBQWVBLHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFLLGVBQVMsRUFBQyw2QkFBZjtBQUFBLDhCQUNFLHFFQUFDLHNEQUFEO0FBQ0UsaUJBQVMsRUFBQyxZQURaO0FBRUUsWUFBSSxFQUFDLElBRlA7QUFHRSxlQUFPLEVBQUMsU0FIVjtBQUlFLGVBQU8sRUFBRTtBQUFBLGlCQUFNeUMsa0RBQU0sQ0FBQ0MsSUFBUCxDQUFZLGdCQUFaLENBQU47QUFBQSxTQUpYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFTRSxxRUFBQyxzREFBRDtBQUNFLGlCQUFTLEVBQUMsWUFEWjtBQUVFLFlBQUksRUFBQyxJQUZQO0FBR0UsZUFBTyxFQUFDLFdBSFY7QUFJRSxlQUFPLEVBQUU7QUFBQSxpQkFBTUQsa0RBQU0sQ0FBQ0MsSUFBUCxDQUFZLGlCQUFaLENBQU47QUFBQSxTQUpYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBVEYsZUFpQkUscUVBQUMsc0RBQUQ7QUFDRSxpQkFBUyxFQUFDLFlBRFo7QUFFRSxZQUFJLEVBQUMsSUFGUDtBQUdFLGVBQU8sRUFBQyxRQUhWO0FBSUUsZUFBTyxFQUFFO0FBQUEsaUJBQU1ELGtEQUFNLENBQUNDLElBQVAsQ0FBWSxnQkFBWixDQUFOO0FBQUEsU0FKWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWpCRixlQTBCRSxxRUFBQyxzREFBRDtBQUNFLGlCQUFTLEVBQUMsWUFEWjtBQUVFLFlBQUksRUFBQyxJQUZQO0FBR0UsZUFBTyxFQUFDLE1BSFY7QUFJRSxlQUFPLEVBQUU7QUFBQSxpQkFBTUQsa0RBQU0sQ0FBQ0MsSUFBUCxDQUFZLGVBQVosQ0FBTjtBQUFBLFNBSlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0ExQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkYsZUF1Q0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF2Q0YsRUF3Q0c1QyxnQkFBZ0IsQ0FBQzZDLE1BQWpCLElBQTJCLENBQTNCLGdCQUNDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxFQUFDLE1BQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkFHQyxxRUFBQyxxREFBRDtBQUFPLGFBQU8sTUFBZDtBQUFlLGNBQVEsTUFBdkI7QUFBd0IsV0FBSyxNQUE3QjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBQSxrQ0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVFFO0FBQUEsa0JBQVF6QztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUEzQ0osZUFzREU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF0REYsRUF1REdGLGtCQUFrQixDQUFDMkMsTUFBbkIsSUFBNkIsQ0FBN0IsZ0JBQ0MscUVBQUMscURBQUQ7QUFBTyxhQUFPLEVBQUMsTUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURELGdCQUdDLHFFQUFDLHFEQUFEO0FBQU8sYUFBTyxNQUFkO0FBQWUsY0FBUSxNQUF2QjtBQUF3QixXQUFLLE1BQTdCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkYsZUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFIRixlQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQVVFO0FBQUEsa0JBQVF2QztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUExREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEwRUQ7O0dBakp1QlosUSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC5lNTE3MDlhYjdkYTkwNGJiMDdiMi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgeyBUYWJsZSwgQWxlcnQsIENvbnRhaW5lciwgQnV0dG9uLCBDb2wsIFJvdyB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHN0eWxlcyBmcm9tIFwiLi4vLi4vc3R5bGVzL0hvbWUubW9kdWxlLmNzc1wiO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gXCJtb21lbnRcIjtcclxuaW1wb3J0IHN1bUJ5R3JvdXAgZnJvbSBcIi4uLy4uL2hlbHBlcnMvc3VtQnlHcm91cFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY2F0ZWdvcnkoKSB7XHJcbiAgY29uc3QgW2NhdGVnb3JpZXMsIHNldENhdGVnb3JpZXNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtjdXJyZW50UmVjb3Jkcywgc2V0Q3VyUmVjXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbaW5jb21lQ2F0ZWdvcmllcywgc2V0SW5jQ2F0XSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbZXhwZW5zZXNDYXRlZ29yaWVzLCBzZXRFeHBDYXRdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtpbmNvbWVSb3dzLCBzZXRJbmNvbWVSb3dzXSA9IHVzZVN0YXRlKG51bGwpO1xyXG4gIGNvbnN0IFtleHBlbnNlc1Jvd3MsIHNldEV4cGVuc2VzUm93c10gPSB1c2VTdGF0ZShudWxsKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKGBodHRwOi8vbG9jYWxob3N0OjQwMDAvYXBpL3VzZXJzL2RldGFpbHNgLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBzZXRDYXRlZ29yaWVzKGRhdGEuY2F0ZWdvcmllcyk7XHJcblxyXG4gICAgICAgIGxldCBkYXRlQmVmb3JlID0gbW9tZW50KGRhdGEuY3VycmVudERhdGUuc3RhcnREYXRlKS5zdWJ0cmFjdCgxLCBcImRheVwiKTtcclxuICAgICAgICBsZXQgZGF0ZUFmdGVyID0gbW9tZW50KGRhdGEuY3VycmVudERhdGUuZW5kRGF0ZSkuYWRkKDEsIFwiZGF5XCIpO1xyXG5cclxuICAgICAgICBsZXQgZmlsdGVyZWRSZWNvcmRzID0gZGF0YS5yZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJkYXlzXCIpO1xyXG4gICAgICAgICAgcmV0dXJuIG1vbWVudCh1cGRhdGVkT24pLmlzQmV0d2VlbihkYXRlQmVmb3JlLCBkYXRlQWZ0ZXIsIFwiZGF5XCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBzZXRDdXJSZWMoZmlsdGVyZWRSZWNvcmRzKTtcclxuICAgICAgfSk7XHJcbiAgfSwgW10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgbGV0IGNhdGVnb3J5UmVjb3JkcyA9IHN1bUJ5R3JvdXAoXHJcbiAgICAgIGNhdGVnb3JpZXMsXHJcbiAgICAgIFwiX2lkXCIsXHJcbiAgICAgIGN1cnJlbnRSZWNvcmRzLFxyXG4gICAgICBcImNhdGVnb3J5SWRcIixcclxuICAgICAgXCJhbW91bnRcIlxyXG4gICAgKTtcclxuXHJcbiAgICBzZXRJbmNDYXQoY2F0ZWdvcnlSZWNvcmRzLmZpbHRlcigoY2F0ZWdvcnkpID0+IGNhdGVnb3J5LnR5cGUgPT0gXCJpbmNvbWVcIikpO1xyXG4gICAgc2V0RXhwQ2F0KGNhdGVnb3J5UmVjb3Jkcy5maWx0ZXIoKGNhdGVnb3J5KSA9PiBjYXRlZ29yeS50eXBlID09IFwiZXhwZW5zZVwiKSk7XHJcbiAgfSwgW2NhdGVnb3JpZXMsIGN1cnJlbnRSZWNvcmRzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRJbmNvbWVSb3dzKFxyXG4gICAgICBpbmNvbWVDYXRlZ29yaWVzLm1hcCgoY2F0ZWdvcnkpID0+IHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgPHRyIGtleT17Y2F0ZWdvcnkuX2lkfT5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5uYW1lfTwvdGQ+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkuYW1vdW50fTwvdGQ+XHJcbiAgICAgICAgICA8L3RyPlxyXG4gICAgICAgICk7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH0sIFtpbmNvbWVDYXRlZ29yaWVzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRFeHBlbnNlc1Jvd3MoXHJcbiAgICAgIGV4cGVuc2VzQ2F0ZWdvcmllcy5tYXAoKGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgIDx0ciBrZXk9e2NhdGVnb3J5Ll9pZH0+XHJcbiAgICAgICAgICAgIDx0ZD57Y2F0ZWdvcnkubmFtZX08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e2NhdGVnb3J5LmJ1ZGdldH08L3RkPlxyXG4gICAgICAgICAgICA8dGQ+e01hdGguYWJzKGNhdGVnb3J5LmFtb3VudCl9PC90ZD5cclxuICAgICAgICAgICAgPHRkPntjYXRlZ29yeS5hbW91bnQgKyBjYXRlZ29yeS5idWRnZXR9PC90ZD5cclxuICAgICAgICAgIDwvdHI+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW2V4cGVuc2VzQ2F0ZWdvcmllc10pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+IENhdGVnb3JpZXM8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LW1kLWVuZCBteS0yXCI+XHJcbiAgICAgICAgPEJ1dHRvblxyXG4gICAgICAgICAgY2xhc3NOYW1lPVwicHVsbC1yaWdodFwiXHJcbiAgICAgICAgICBzaXplPVwibGdcIlxyXG4gICAgICAgICAgdmFyaWFudD1cInN1Y2Nlc3NcIlxyXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL2NhdGVnb3J5L2FkZFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBBZGQgQ2F0ZWdvcnlcclxuICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwic2Vjb25kYXJ5XCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9lZGl0XCIpfVxyXG4gICAgICAgID5cclxuICAgICAgICAgIEVkaXQgQ2F0ZWdvcnlcclxuICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICBjbGFzc05hbWU9XCJwdWxsLXJpZ2h0XCJcclxuICAgICAgICAgIHNpemU9XCJsZ1wiXHJcbiAgICAgICAgICB2YXJpYW50PVwiZGFuZ2VyXCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9hZGRcIil9XHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgRGVsZXRlIENhdGVnb3J5XHJcbiAgICAgICAgPC9CdXR0b24+XHJcblxyXG4gICAgICAgIDxCdXR0b25cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgIHZhcmlhbnQ9XCJpbmZvXCJcclxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi4vcmVjb3JkL2FkZFwiKX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICBBZGQgUmVjb3JkXHJcbiAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgIDwvUm93PlxyXG4gICAgICA8aDU+SW5jb21lOjwvaDU+XHJcbiAgICAgIHtpbmNvbWVDYXRlZ29yaWVzLmxlbmd0aCA9PSAwID8gKFxyXG4gICAgICAgIDxBbGVydCB2YXJpYW50PVwiaW5mb1wiPllvdSBoYXZlIG5vIGNhdGVnb3JpZXMgeWV0LjwvQWxlcnQ+XHJcbiAgICAgICkgOiAoXHJcbiAgICAgICAgPFRhYmxlIHN0cmlwZWQgYm9yZGVyZWQgaG92ZXI+XHJcbiAgICAgICAgICA8dGhlYWQ+XHJcbiAgICAgICAgICAgIDx0cj5cclxuICAgICAgICAgICAgICA8dGg+Q2F0ZWdvcnkgTmFtZTwvdGg+XHJcbiAgICAgICAgICAgICAgPHRoPkN1cnJlbnQgSW5jb21lPC90aD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIDwvdGhlYWQ+XHJcblxyXG4gICAgICAgICAgPHRib2R5PntpbmNvbWVSb3dzfTwvdGJvZHk+XHJcbiAgICAgICAgPC9UYWJsZT5cclxuICAgICAgKX1cclxuICAgICAgPGg1PkV4cGVuc2VzOjwvaDU+XHJcbiAgICAgIHtleHBlbnNlc0NhdGVnb3JpZXMubGVuZ3RoID09IDAgPyAoXHJcbiAgICAgICAgPEFsZXJ0IHZhcmlhbnQ9XCJpbmZvXCI+WW91IGhhdmUgbm8gY2F0ZWdvcmllcyB1bmRlciBleHBlbnNlcyB5ZXQuPC9BbGVydD5cclxuICAgICAgKSA6IChcclxuICAgICAgICA8VGFibGUgc3RyaXBlZCBib3JkZXJlZCBob3Zlcj5cclxuICAgICAgICAgIDx0aGVhZD5cclxuICAgICAgICAgICAgPHRyPlxyXG4gICAgICAgICAgICAgIDx0aD5DYXRlZ29yeSBOYW1lPC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBCdWRnZXQ8L3RoPlxyXG4gICAgICAgICAgICAgIDx0aD5DdXJyZW50IEV4cGVuc2VzPC90aD5cclxuICAgICAgICAgICAgICA8dGg+Q3VycmVudCBTYXZpbmdzPC90aD5cclxuICAgICAgICAgICAgPC90cj5cclxuICAgICAgICAgIDwvdGhlYWQ+XHJcblxyXG4gICAgICAgICAgPHRib2R5PntleHBlbnNlc1Jvd3N9PC90Ym9keT5cclxuICAgICAgICA8L1RhYmxlPlxyXG4gICAgICApfVxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICApO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=