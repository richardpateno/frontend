webpackHotUpdate_N_E("pages/register",{

/***/ "./pages/register.js":
/*!***************************!*\
  !*** ./pages/register.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process, module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Register; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\register.js",
    _s = $RefreshSig$();





function Register() {
  _s();

  // Form input state hooks
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      firstName = _useState[0],
      setFirstName = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      lastName = _useState2[0],
      setLastName = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState3[0],
      setEmail = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password1 = _useState4[0],
      setPassword1 = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password2 = _useState5[0],
      setPassword2 = _useState5[1]; // State to determine whether submit button is enabled or not


  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isActive = _useState6[0],
      setIsActive = _useState6[1]; // Validate form input whenever email, password1, or password2 is changed


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (firstName != "" && lastName != "" && email != "" && password1 !== "" && password2 !== "" && password2 === password1) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, password1, password2]); // Function to register user

  function registerUser(e) {
    e.preventDefault(); // Check for duplicate email in database first

    fetch("http://localhost:4000/api/users/email-exists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      // If no duplicates found
      if (data === false) {
        fetch("".concat(process.env.NEXT_PUBLIC_BASE_URL, "/api/users"), {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password1
          })
        }).then(function (res) {
          return res.json();
        }).then(function (data) {
          // Registration successful
          if (data === true) {
            // Redirect to login
            next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/login");
          } else {
            // Error in creating registration, redirect to error page
            next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/error");
          }
        });
      } else {
        // Duplicate email found
        next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/error");
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Register"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      onSubmit: function onSubmit(e) {
        return registerUser(e);
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "firstName",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "First Name"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 89,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "string",
          placeholder: "Enter First Name",
          value: firstName,
          onChange: function onChange(e) {
            return setFirstName(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "lastName",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Last Name"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 99,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "string",
          placeholder: "Enter Last Name",
          value: lastName,
          onChange: function onChange(e) {
            return setLastName(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 100,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "userEmail",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Email address"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 109,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "email",
          placeholder: "Enter email",
          value: email,
          onChange: function onChange(e) {
            return setEmail(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 108,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "password1",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Password"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "password",
          placeholder: "Password",
          value: password1,
          onChange: function onChange(e) {
            return setPassword1(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 121,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 119,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "password2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Verify Password"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 131,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "password",
          placeholder: "Verify Password",
          value: password2,
          onChange: function onChange(e) {
            return setPassword2(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 132,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 130,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Text, {
        className: "text-muted",
        children: "We'll never share your details with anyone else."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 9
      }, this), isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 146,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        disabled: true,
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 150,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 83,
    columnNumber: 5
  }, this);
}

_s(Register, "VXG5vUUVCeuoZLvKVJr+ONT+1RU=");

_c = Register;

var _c;

$RefreshReg$(_c, "Register");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/process/browser.js */ "./node_modules/process/browser.js"), __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvcmVnaXN0ZXIuanMiXSwibmFtZXMiOlsiUmVnaXN0ZXIiLCJ1c2VTdGF0ZSIsImZpcnN0TmFtZSIsInNldEZpcnN0TmFtZSIsImxhc3ROYW1lIiwic2V0TGFzdE5hbWUiLCJlbWFpbCIsInNldEVtYWlsIiwicGFzc3dvcmQxIiwic2V0UGFzc3dvcmQxIiwicGFzc3dvcmQyIiwic2V0UGFzc3dvcmQyIiwiaXNBY3RpdmUiLCJzZXRJc0FjdGl2ZSIsInVzZUVmZmVjdCIsInJlZ2lzdGVyVXNlciIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImZldGNoIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0JBU0VfVVJMIiwicGFzc3dvcmQiLCJSb3V0ZXIiLCJwdXNoIiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFZSxTQUFTQSxRQUFULEdBQW9CO0FBQUE7O0FBQ2pDO0FBRGlDLGtCQUVDQyxzREFBUSxDQUFDLEVBQUQsQ0FGVDtBQUFBLE1BRTFCQyxTQUYwQjtBQUFBLE1BRWZDLFlBRmU7O0FBQUEsbUJBR0RGLHNEQUFRLENBQUMsRUFBRCxDQUhQO0FBQUEsTUFHMUJHLFFBSDBCO0FBQUEsTUFHaEJDLFdBSGdCOztBQUFBLG1CQUlQSixzREFBUSxDQUFDLEVBQUQsQ0FKRDtBQUFBLE1BSTFCSyxLQUowQjtBQUFBLE1BSW5CQyxRQUptQjs7QUFBQSxtQkFLQ04sc0RBQVEsQ0FBQyxFQUFELENBTFQ7QUFBQSxNQUsxQk8sU0FMMEI7QUFBQSxNQUtmQyxZQUxlOztBQUFBLG1CQU1DUixzREFBUSxDQUFDLEVBQUQsQ0FOVDtBQUFBLE1BTTFCUyxTQU4wQjtBQUFBLE1BTWZDLFlBTmUsa0JBUWpDOzs7QUFSaUMsbUJBU0RWLHNEQUFRLENBQUMsS0FBRCxDQVRQO0FBQUEsTUFTMUJXLFFBVDBCO0FBQUEsTUFTaEJDLFdBVGdCLGtCQVdqQzs7O0FBQ0FDLHlEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0EsUUFDRVosU0FBUyxJQUFJLEVBQWIsSUFDQUUsUUFBUSxJQUFJLEVBRFosSUFFQUUsS0FBSyxJQUFJLEVBRlQsSUFHQUUsU0FBUyxLQUFLLEVBSGQsSUFJQUUsU0FBUyxLQUFLLEVBSmQsSUFLQUEsU0FBUyxLQUFLRixTQU5oQixFQU9FO0FBQ0FLLGlCQUFXLENBQUMsSUFBRCxDQUFYO0FBQ0QsS0FURCxNQVNPO0FBQ0xBLGlCQUFXLENBQUMsS0FBRCxDQUFYO0FBQ0Q7QUFDRixHQWRRLEVBY04sQ0FBQ1gsU0FBRCxFQUFZRSxRQUFaLEVBQXNCSSxTQUF0QixFQUFpQ0UsU0FBakMsQ0FkTSxDQUFULENBWmlDLENBNEJqQzs7QUFDQSxXQUFTSyxZQUFULENBQXNCQyxDQUF0QixFQUF5QjtBQUN2QkEsS0FBQyxDQUFDQyxjQUFGLEdBRHVCLENBR3ZCOztBQUNBQyxTQUFLLGlEQUFpRDtBQUNwREMsWUFBTSxFQUFFLE1BRDRDO0FBRXBEQyxhQUFPLEVBQUU7QUFDUCx3QkFBZ0I7QUFEVCxPQUYyQztBQUtwREMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNuQmpCLGFBQUssRUFBRUE7QUFEWSxPQUFmO0FBTDhDLEtBQWpELENBQUwsQ0FTR2tCLElBVEgsQ0FTUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQVRSLEVBVUdGLElBVkgsQ0FVUSxVQUFDRyxJQUFELEVBQVU7QUFDZDtBQUNBLFVBQUlBLElBQUksS0FBSyxLQUFiLEVBQW9CO0FBQ2xCVCxhQUFLLFdBQUlVLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxvQkFBaEIsaUJBQWtEO0FBQ3JEWCxnQkFBTSxFQUFFLE1BRDZDO0FBRXJEQyxpQkFBTyxFQUFFO0FBQ1AsNEJBQWdCO0FBRFQsV0FGNEM7QUFLckRDLGNBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDbkJyQixxQkFBUyxFQUFFQSxTQURRO0FBRW5CRSxvQkFBUSxFQUFFQSxRQUZTO0FBR25CRSxpQkFBSyxFQUFFQSxLQUhZO0FBSW5CeUIsb0JBQVEsRUFBRXZCO0FBSlMsV0FBZjtBQUwrQyxTQUFsRCxDQUFMLENBWUdnQixJQVpILENBWVEsVUFBQ0MsR0FBRDtBQUFBLGlCQUFTQSxHQUFHLENBQUNDLElBQUosRUFBVDtBQUFBLFNBWlIsRUFhR0YsSUFiSCxDQWFRLFVBQUNHLElBQUQsRUFBVTtBQUNkO0FBQ0EsY0FBSUEsSUFBSSxLQUFLLElBQWIsRUFBbUI7QUFDakI7QUFDQUssOERBQU0sQ0FBQ0MsSUFBUCxDQUFZLFFBQVo7QUFDRCxXQUhELE1BR087QUFDTDtBQUNBRCw4REFBTSxDQUFDQyxJQUFQLENBQVksUUFBWjtBQUNEO0FBQ0YsU0F0Qkg7QUF1QkQsT0F4QkQsTUF3Qk87QUFDTDtBQUNBRCwwREFBTSxDQUFDQyxJQUFQLENBQVksUUFBWjtBQUNEO0FBQ0YsS0F4Q0g7QUF5Q0Q7O0FBRUQsc0JBQ0UscUVBQUMsNENBQUQsQ0FBTyxRQUFQO0FBQUEsNEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSw2QkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQUlFLHFFQUFDLG9EQUFEO0FBQU0sY0FBUSxFQUFFLGtCQUFDakIsQ0FBRDtBQUFBLGVBQU9ELFlBQVksQ0FBQ0MsQ0FBRCxDQUFuQjtBQUFBLE9BQWhCO0FBQUEsOEJBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxXQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxRQURQO0FBRUUscUJBQVcsRUFBQyxrQkFGZDtBQUdFLGVBQUssRUFBRWQsU0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNjLENBQUQ7QUFBQSxtQkFBT2IsWUFBWSxDQUFDYSxDQUFDLENBQUNrQixNQUFGLENBQVNDLEtBQVYsQ0FBbkI7QUFBQSxXQUpaO0FBS0Usa0JBQVE7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBV0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxVQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxRQURQO0FBRUUscUJBQVcsRUFBQyxpQkFGZDtBQUdFLGVBQUssRUFBRS9CLFFBSFQ7QUFJRSxrQkFBUSxFQUFFLGtCQUFDWSxDQUFEO0FBQUEsbUJBQU9YLFdBQVcsQ0FBQ1csQ0FBQyxDQUFDa0IsTUFBRixDQUFTQyxLQUFWLENBQWxCO0FBQUEsV0FKWjtBQUtFLGtCQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FYRixlQXFCRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxpQkFBUyxFQUFDLFdBQXRCO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsY0FBSSxFQUFDLE9BRFA7QUFFRSxxQkFBVyxFQUFDLGFBRmQ7QUFHRSxlQUFLLEVBQUU3QixLQUhUO0FBSUUsa0JBQVEsRUFBRSxrQkFBQ1UsQ0FBRDtBQUFBLG1CQUFPVCxRQUFRLENBQUNTLENBQUMsQ0FBQ2tCLE1BQUYsQ0FBU0MsS0FBVixDQUFmO0FBQUEsV0FKWjtBQUtFLGtCQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FyQkYsZUFnQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxXQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxVQURQO0FBRUUscUJBQVcsRUFBQyxVQUZkO0FBR0UsZUFBSyxFQUFFM0IsU0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNRLENBQUQ7QUFBQSxtQkFBT1AsWUFBWSxDQUFDTyxDQUFDLENBQUNrQixNQUFGLENBQVNDLEtBQVYsQ0FBbkI7QUFBQSxXQUpaO0FBS0Usa0JBQVE7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWhDRixlQTJDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxpQkFBUyxFQUFDLFdBQXRCO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsY0FBSSxFQUFDLFVBRFA7QUFFRSxxQkFBVyxFQUFDLGlCQUZkO0FBR0UsZUFBSyxFQUFFekIsU0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNNLENBQUQ7QUFBQSxtQkFBT0wsWUFBWSxDQUFDSyxDQUFDLENBQUNrQixNQUFGLENBQVNDLEtBQVYsQ0FBbkI7QUFBQSxXQUpaO0FBS0Usa0JBQVE7QUFMVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTNDRixlQXFERSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBVyxpQkFBUyxFQUFDLFlBQXJCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBckRGLEVBMERHdkIsUUFBUSxnQkFDUCxxRUFBQyxzREFBRDtBQUFRLGVBQU8sRUFBQyxTQUFoQjtBQUEwQixZQUFJLEVBQUMsUUFBL0I7QUFBd0MsVUFBRSxFQUFDLFdBQTNDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRE8sZ0JBS1AscUVBQUMsc0RBQUQ7QUFBUSxlQUFPLEVBQUMsU0FBaEI7QUFBMEIsWUFBSSxFQUFDLFFBQS9CO0FBQXdDLFVBQUUsRUFBQyxXQUEzQztBQUF1RCxnQkFBUSxNQUEvRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQS9ESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQTJFRDs7R0F2SnVCWixROztLQUFBQSxRIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL3JlZ2lzdGVyLjZlZTBkZGFiNWQ4Y2RhM2Q5NjEzLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGb3JtLCBCdXR0b24gfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFJlZ2lzdGVyKCkge1xyXG4gIC8vIEZvcm0gaW5wdXQgc3RhdGUgaG9va3NcclxuICBjb25zdCBbZmlyc3ROYW1lLCBzZXRGaXJzdE5hbWVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2xhc3ROYW1lLCBzZXRMYXN0TmFtZV0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtwYXNzd29yZDEsIHNldFBhc3N3b3JkMV0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbcGFzc3dvcmQyLCBzZXRQYXNzd29yZDJdID0gdXNlU3RhdGUoXCJcIik7XHJcblxyXG4gIC8vIFN0YXRlIHRvIGRldGVybWluZSB3aGV0aGVyIHN1Ym1pdCBidXR0b24gaXMgZW5hYmxlZCBvciBub3RcclxuICBjb25zdCBbaXNBY3RpdmUsIHNldElzQWN0aXZlXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuXHJcbiAgLy8gVmFsaWRhdGUgZm9ybSBpbnB1dCB3aGVuZXZlciBlbWFpbCwgcGFzc3dvcmQxLCBvciBwYXNzd29yZDIgaXMgY2hhbmdlZFxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAvLyBWYWxpZGF0aW9uIHRvIGVuYWJsZSBzdWJtaXQgYnV0dG9uIHdoZW4gYWxsIGZpZWxkcyBhcmUgcG9wdWxhdGVkIGFuZCBib3RoIHBhc3N3b3JkcyBtYXRjaFxyXG4gICAgaWYgKFxyXG4gICAgICBmaXJzdE5hbWUgIT0gXCJcIiAmJlxyXG4gICAgICBsYXN0TmFtZSAhPSBcIlwiICYmXHJcbiAgICAgIGVtYWlsICE9IFwiXCIgJiZcclxuICAgICAgcGFzc3dvcmQxICE9PSBcIlwiICYmXHJcbiAgICAgIHBhc3N3b3JkMiAhPT0gXCJcIiAmJlxyXG4gICAgICBwYXNzd29yZDIgPT09IHBhc3N3b3JkMVxyXG4gICAgKSB7XHJcbiAgICAgIHNldElzQWN0aXZlKHRydWUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0SXNBY3RpdmUoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH0sIFtmaXJzdE5hbWUsIGxhc3ROYW1lLCBwYXNzd29yZDEsIHBhc3N3b3JkMl0pO1xyXG5cclxuICAvLyBGdW5jdGlvbiB0byByZWdpc3RlciB1c2VyXHJcbiAgZnVuY3Rpb24gcmVnaXN0ZXJVc2VyKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAvLyBDaGVjayBmb3IgZHVwbGljYXRlIGVtYWlsIGluIGRhdGFiYXNlIGZpcnN0XHJcbiAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9lbWFpbC1leGlzdHNgLCB7XHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgfSxcclxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgIGVtYWlsOiBlbWFpbCxcclxuICAgICAgfSksXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIC8vIElmIG5vIGR1cGxpY2F0ZXMgZm91bmRcclxuICAgICAgICBpZiAoZGF0YSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgIGZldGNoKGAke3Byb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0JBU0VfVVJMfS9hcGkvdXNlcnNgLCB7XHJcbiAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICAgIGZpcnN0TmFtZTogZmlyc3ROYW1lLFxyXG4gICAgICAgICAgICAgIGxhc3ROYW1lOiBsYXN0TmFtZSxcclxuICAgICAgICAgICAgICBlbWFpbDogZW1haWwsXHJcbiAgICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkMSxcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgIC8vIFJlZ2lzdHJhdGlvbiBzdWNjZXNzZnVsXHJcbiAgICAgICAgICAgICAgaWYgKGRhdGEgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgIC8vIFJlZGlyZWN0IHRvIGxvZ2luXHJcbiAgICAgICAgICAgICAgICBSb3V0ZXIucHVzaChcIi9sb2dpblwiKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gRXJyb3IgaW4gY3JlYXRpbmcgcmVnaXN0cmF0aW9uLCByZWRpcmVjdCB0byBlcnJvciBwYWdlXHJcbiAgICAgICAgICAgICAgICBSb3V0ZXIucHVzaChcIi9lcnJvclwiKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAvLyBEdXBsaWNhdGUgZW1haWwgZm91bmRcclxuICAgICAgICAgIFJvdXRlci5wdXNoKFwiL2Vycm9yXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPFJlYWN0LkZyYWdtZW50PlxyXG4gICAgICA8SGVhZD5cclxuICAgICAgICA8dGl0bGU+UmVnaXN0ZXI8L3RpdGxlPlxyXG4gICAgICA8L0hlYWQ+XHJcbiAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gcmVnaXN0ZXJVc2VyKGUpfT5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJmaXJzdE5hbWVcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPkZpcnN0IE5hbWU8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJzdHJpbmdcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIEZpcnN0IE5hbWVcIlxyXG4gICAgICAgICAgICB2YWx1ZT17Zmlyc3ROYW1lfVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEZpcnN0TmFtZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJsYXN0TmFtZVwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+TGFzdCBOYW1lPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICB0eXBlPVwic3RyaW5nXCJcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJFbnRlciBMYXN0IE5hbWVcIlxyXG4gICAgICAgICAgICB2YWx1ZT17bGFzdE5hbWV9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0TGFzdE5hbWUoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwidXNlckVtYWlsXCI+XHJcbiAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbCBhZGRyZXNzPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIGVtYWlsXCJcclxuICAgICAgICAgICAgdmFsdWU9e2VtYWlsfVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldEVtYWlsKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJwYXNzd29yZDFcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPlBhc3N3b3JkPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkXCJcclxuICAgICAgICAgICAgdmFsdWU9e3Bhc3N3b3JkMX1cclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRQYXNzd29yZDEoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcblxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInBhc3N3b3JkMlwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+VmVyaWZ5IFBhc3N3b3JkPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlZlcmlmeSBQYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHZhbHVlPXtwYXNzd29yZDJ9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0UGFzc3dvcmQyKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgIDxGb3JtLlRleHQgY2xhc3NOYW1lPVwidGV4dC1tdXRlZFwiPlxyXG4gICAgICAgICAgV2UnbGwgbmV2ZXIgc2hhcmUgeW91ciBkZXRhaWxzIHdpdGggYW55b25lIGVsc2UuXHJcbiAgICAgICAgPC9Gb3JtLlRleHQ+XHJcblxyXG4gICAgICAgIHsvKiBDb25kaXRpb25hbGx5IHJlbmRlciBzdWJtaXQgYnV0dG9uIGJhc2VkIG9uIGlzQWN0aXZlIHN0YXRlICovfVxyXG4gICAgICAgIHtpc0FjdGl2ZSA/IChcclxuICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cInByaW1hcnlcIiB0eXBlPVwic3VibWl0XCIgaWQ9XCJzdWJtaXRCdG5cIj5cclxuICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICApIDogKFxyXG4gICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIHR5cGU9XCJzdWJtaXRcIiBpZD1cInN1Ym1pdEJ0blwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvRm9ybT5cclxuICAgIDwvUmVhY3QuRnJhZ21lbnQ+XHJcbiAgKTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9