webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process, module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/Home.module.css */ "./styles/Home.module.css");
/* harmony import */ var _styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_Home_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login */ "./pages/login.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./category */ "./pages/category/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _components_PieChart__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/PieChart */ "./components/PieChart.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_10__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\index.js",
    _s = $RefreshSig$();











function Home() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_5__["default"]),
      user = _useContext.user,
      setUser = _useContext.setUser;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      loading = _useState[0],
      setLoading = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      name = _useState2[0],
      setName = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState3[0],
      setEmail = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      records = _useState4[0],
      setRecords = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      categories = _useState5[0],
      setCategories = _useState5[1];

  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      startDate = _useState6[0],
      setStartDate = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      endDate = _useState7[0],
      setEndDate = _useState7[1];

  var _useState8 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      savedStart = _useState8[0],
      setSavedStart = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      savedEnd = _useState9[0],
      setSavedEnd = _useState9[1];

  var _useState10 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      currentRecords = _useState10[0],
      setCurrentRecords = _useState10[1];

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      oldRecords = _useState11[0],
      setOldRecords = _useState11[1];

  var _useState12 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      previousSavings = _useState12[0],
      setPreviousSavings = _useState12[1];

  var _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      currentIncome = _useState13[0],
      setCurrentIncome = _useState13[1];

  var _useState14 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      currentExpenses = _useState14[0],
      setCurrentExpenses = _useState14[1];

  var _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      budget = _useState15[0],
      setBudget = _useState15[1];

  var _useState16 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      actualPie = _useState16[0],
      setActualPie = _useState16[1];

  var _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      budgetPie = _useState17[0],
      setBudgetPie = _useState17[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (user.id != null) {
      fetch("http://localhost:4000/api/users/details", {
        headers: {
          Authorization: "Bearer ".concat(localStorage.getItem("token"))
        }
      }).then(function (res) {
        return res.json();
      }).then(function (data) {
        console.log(data);
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.startDate).startOf("days").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD"));
        setName("".concat(data.firstName, " ").concat(data.lastName));
        setEmail(data.email);
        setRecords(data.records);
        setCategories(data.categories);
      });
    }
  }, [user]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (name != "" & email != "") {
      setLoading(false);
    }
  }, [name, email]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    var dateBefore = moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).subtract(1, "day");
    var dateAfter = moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).add(1, "day");

    if (startDate <= endDate) {
      if (records.length > 0) {
        setCurrentRecords(records.filter(function (record) {
          var updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBetween(dateBefore, dateAfter, "day");
        }));
        setOldRecords(records.filter(function (record) {
          var updatedOn = moment__WEBPACK_IMPORTED_MODULE_8___default()(record.updatedOn).startOf("days");
          return moment__WEBPACK_IMPORTED_MODULE_8___default()(updatedOn).isBefore(dateBefore, "day");
        }));
      }
    } else {
      setCurrentRecords([]);
      setOldRecords([]);
    }

    if (categories.length > 0) {
      setBudget(categories.reduce(function (total, category) {
        return total + category.budget;
      }, 0));
    } else {
      setBudget(0);
    }
  }, [categories, startDate, endDate]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (oldRecords.length > 0) {
      setPreviousSavings(oldRecords.reduce(function (total, record) {
        return total + record.amount;
      }, 0));
    } else {
      setPreviousSavings(0);
    }
  }, [oldRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (currentRecords.length > 0) {
      var income = currentRecords.filter(function (record) {
        return record.amount > 0;
      });
      console.log(income);

      if (income.length > 0) {
        setCurrentIncome(income.reduce(function (total, record) {
          return total + record.amount;
        }, 0));
      } else {
        setCurrentIncome(0);
      }

      var expenses = currentRecords.filter(function (record) {
        return record.amount < 0;
      });

      if (expenses.length > 0) {
        setCurrentExpenses(expenses.reduce(function (total, record) {
          return total - record.amount;
        }, 0));
      } else {
        setCurrentExpenses(0);
      }
    } else {
      setCurrentIncome(0);
      setCurrentExpenses(0);
    }
  }, [currentRecords]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    console.log(previousSavings);
    console.log(currentIncome);
    console.log(currentExpenses);

    if (previousSavings >= 0) {
      if (currentIncome > currentExpenses) {
        setActualPie([{
          label: "Current Savings",
          amount: currentIncome - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Current Expenses",
          amount: currentExpenses,
          color: "#f56f36"
        }, {
          label: "Previous Savings",
          amount: previousSavings,
          color: "#0f5209"
        }]);
      } else {
        if (currentExpenses < currentIncome + previousSavings) {
          setActualPie([{
            label: "Accumulated Savings",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#0f5209"
          }, {
            label: "Current Expenses",
            amount: currentExpenses,
            color: "#f56f36"
          }]);
        } else {
          setActualPie([{
            label: "Accumulated Deficit",
            amount: currentIncome + previousSavings - currentExpenses,
            color: "#e30707"
          }, {
            label: "Consumed Savings and Income",
            amount: currentIncome + previousSavings,
            color: "#0f5209"
          }]);
        }
      }
    } else {
      if (currentIncome > Math.abs(previousSavings) + currentExpenses) {
        setActualPie([{
          label: "Accumulated Savings",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#47b83d"
        }, {
          label: "Previous Deficit + Current Expenses",
          amount: Math.abs(previousSavings) + currentExpenses,
          color: "#f56f36"
        }]);
      } else {
        setActualPie([{
          label: "Consumed Income",
          amount: currentIncome,
          color: "#f56f36"
        }, {
          label: "Accumulated Deficit",
          amount: currentIncome + previousSavings - currentExpenses,
          color: "#e30707"
        }]);
      }
    }
  }, [previousSavings, currentIncome, currentExpenses]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (budget >= currentExpenses) {
      setBudgetPie([{
        label: "Savings",
        amount: budget - currentExpenses,
        color: "#47b83d"
      }, {
        label: "Expenses",
        amount: currentExpenses,
        color: "#f56f36"
      }]);
    } else {
      setBudgetPie([{
        label: "Consumed Budget",
        amount: budget,
        color: "#f56f36"
      }, {
        label: "Deficit",
        amount: Math.abs(budget - currentExpenses),
        color: "#e30707"
      }]);
    }
  }, [budget, currentExpenses]);

  function saveDate(e) {
    console.log(e);
    e.preventDefault();
    fetch("".concat(process.env.NEXT_PUBLIC_BASE_URL, "/api/users/setDate"), {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      },
      body: JSON.stringify({
        startDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(startDate).startOf("day"),
        endDate: moment__WEBPACK_IMPORTED_MODULE_8___default()(endDate).startOf("day")
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (data == false) {
        Swal.fire("Error", "Something went wrong", "error");
      } else {
        console.log(data);
        setSavedStart(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setSavedEnd(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
        setStartDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.startDate).startOf("day").format("yyyy-MM-DD"));
        setEndDate(moment__WEBPACK_IMPORTED_MODULE_8___default()(data.endDate).startOf("day").format("yyyy-MM-DD"));
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Budget Tracker"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 312,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 311,
      columnNumber: 7
    }, this), user.id !== null ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
        className: "justify-content-center",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 318,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: email
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 319,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
          children: ["Balance: Php ", currentIncome + previousSavings - currentExpenses]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 320,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 317,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
          onSubmit: function onSubmit(e) {
            return saveDate(e);
          },
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
            className: "justify-content-md-center my-2",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: "Budget Date:"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 328,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 327,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                dateformat: "yyyy-MM-DD",
                value: startDate,
                onChange: function onChange(e) {
                  return setStartDate(e.target.value);
                }
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 331,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 330,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              md: "auto",
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: " to "
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 339,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 338,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "date",
                value: endDate,
                dateformat: "myyyy-MM-DD",
                onChange: function onChange(e) {
                  return setEndDate(e.target.value);
                }
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 342,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 341,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
              children: startDate != savedStart || endDate != savedEnd ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 352,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                variant: "primary",
                type: "submit",
                id: "submitBtn",
                disabled: true,
                children: "Set as Default"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 356,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 350,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 326,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 325,
          columnNumber: 13
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 324,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 374,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: actualPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 376,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 372,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Income: Php ", currentIncome]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 386,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 387,
                  columnNumber: 21
                }, this), currentIncome >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Savings: Php ", currentIncome - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 389,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Current Deficit: Php ", currentIncome - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 393,
                  columnNumber: 23
                }, this), previousSavings >= 0 ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Savings: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 398,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Previous Deficit: Php ", previousSavings]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 400,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 385,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 384,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 371,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 370,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
            className: "homepageCard",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
              children: actualPie.label ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Alert"], {
                children: "No Records"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 410,
                columnNumber: 21
              }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_9__["default"], {
                chartType: "Pie",
                rawData: budgetPie,
                labelKey: "label",
                amountKey: "amount"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 412,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 408,
              columnNumber: 17
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
              children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
                className: "homepagePie",
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Budget: Php ", budget]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 422,
                  columnNumber: 21
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Expenses: Php ", currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 423,
                  columnNumber: 21
                }, this), budget >= currentExpenses ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Savings: Php ", budget - currentExpenses]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 425,
                  columnNumber: 23
                }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
                  children: ["Deficit: Php ", budget - currentExpenses, " "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 427,
                  columnNumber: 23
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 421,
                columnNumber: 19
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 420,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 407,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 406,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 369,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        className: "justify-content-md-center my-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          className: "justify-content-md-center my-2",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "success",
            onClick: function onClick() {
              return next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./category/add");
            },
            children: "Add Category"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 436,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 435,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            className: "pull-right",
            size: "lg",
            variant: "secondary",
            onClick: function onClick() {
              return next_router__WEBPACK_IMPORTED_MODULE_10___default.a.push("./record/add");
            },
            children: "Add Record"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 446,
            columnNumber: 15
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 445,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 434,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 316,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_login__WEBPACK_IMPORTED_MODULE_6__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 458,
      columnNumber: 9
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 310,
    columnNumber: 5
  }, this);
}

_s(Home, "89t6n/MJkOlUE3yv2I91v03nCNY=");

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/process/browser.js */ "./node_modules/process/browser.js"), __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSG9tZSIsInVzZUNvbnRleHQiLCJVc2VyQ29udGV4dCIsInVzZXIiLCJzZXRVc2VyIiwidXNlU3RhdGUiLCJsb2FkaW5nIiwic2V0TG9hZGluZyIsIm5hbWUiLCJzZXROYW1lIiwiZW1haWwiLCJzZXRFbWFpbCIsInJlY29yZHMiLCJzZXRSZWNvcmRzIiwiY2F0ZWdvcmllcyIsInNldENhdGVnb3JpZXMiLCJzdGFydERhdGUiLCJzZXRTdGFydERhdGUiLCJlbmREYXRlIiwic2V0RW5kRGF0ZSIsInNhdmVkU3RhcnQiLCJzZXRTYXZlZFN0YXJ0Iiwic2F2ZWRFbmQiLCJzZXRTYXZlZEVuZCIsImN1cnJlbnRSZWNvcmRzIiwic2V0Q3VycmVudFJlY29yZHMiLCJvbGRSZWNvcmRzIiwic2V0T2xkUmVjb3JkcyIsInByZXZpb3VzU2F2aW5ncyIsInNldFByZXZpb3VzU2F2aW5ncyIsImN1cnJlbnRJbmNvbWUiLCJzZXRDdXJyZW50SW5jb21lIiwiY3VycmVudEV4cGVuc2VzIiwic2V0Q3VycmVudEV4cGVuc2VzIiwiYnVkZ2V0Iiwic2V0QnVkZ2V0IiwiYWN0dWFsUGllIiwic2V0QWN0dWFsUGllIiwiYnVkZ2V0UGllIiwic2V0QnVkZ2V0UGllIiwidXNlRWZmZWN0IiwiaWQiLCJmZXRjaCIsImhlYWRlcnMiLCJBdXRob3JpemF0aW9uIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImNvbnNvbGUiLCJsb2ciLCJtb21lbnQiLCJjdXJyZW50RGF0ZSIsInN0YXJ0T2YiLCJmb3JtYXQiLCJmaXJzdE5hbWUiLCJsYXN0TmFtZSIsImRhdGVCZWZvcmUiLCJzdWJ0cmFjdCIsImRhdGVBZnRlciIsImFkZCIsImxlbmd0aCIsImZpbHRlciIsInJlY29yZCIsInVwZGF0ZWRPbiIsImlzQmV0d2VlbiIsImlzQmVmb3JlIiwicmVkdWNlIiwidG90YWwiLCJjYXRlZ29yeSIsImFtb3VudCIsImluY29tZSIsImV4cGVuc2VzIiwibGFiZWwiLCJjb2xvciIsIk1hdGgiLCJhYnMiLCJzYXZlRGF0ZSIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInByb2Nlc3MiLCJlbnYiLCJORVhUX1BVQkxJQ19CQVNFX1VSTCIsIm1ldGhvZCIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiU3dhbCIsImZpcmUiLCJ0YXJnZXQiLCJ2YWx1ZSIsIlJvdXRlciIsInB1c2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLElBQVQsR0FBZ0I7QUFBQTs7QUFBQSxvQkFDSEMsd0RBQVUsQ0FBQ0Msb0RBQUQsQ0FEUDtBQUFBLE1BQ3JCQyxJQURxQixlQUNyQkEsSUFEcUI7QUFBQSxNQUNmQyxPQURlLGVBQ2ZBLE9BRGU7O0FBQUEsa0JBRUNDLHNEQUFRLENBQUMsSUFBRCxDQUZUO0FBQUEsTUFFdEJDLE9BRnNCO0FBQUEsTUFFYkMsVUFGYTs7QUFBQSxtQkFHTEYsc0RBQVEsQ0FBQyxFQUFELENBSEg7QUFBQSxNQUd0QkcsSUFIc0I7QUFBQSxNQUdoQkMsT0FIZ0I7O0FBQUEsbUJBSUhKLHNEQUFRLENBQUMsRUFBRCxDQUpMO0FBQUEsTUFJdEJLLEtBSnNCO0FBQUEsTUFJZkMsUUFKZTs7QUFBQSxtQkFLQ04sc0RBQVEsQ0FBQyxFQUFELENBTFQ7QUFBQSxNQUt0Qk8sT0FMc0I7QUFBQSxNQUtiQyxVQUxhOztBQUFBLG1CQU1PUixzREFBUSxDQUFDLEVBQUQsQ0FOZjtBQUFBLE1BTXRCUyxVQU5zQjtBQUFBLE1BTVZDLGFBTlU7O0FBQUEsbUJBT0tWLHNEQUFRLENBQUMsRUFBRCxDQVBiO0FBQUEsTUFPdEJXLFNBUHNCO0FBQUEsTUFPWEMsWUFQVzs7QUFBQSxtQkFRQ1osc0RBQVEsQ0FBQyxFQUFELENBUlQ7QUFBQSxNQVF0QmEsT0FSc0I7QUFBQSxNQVFiQyxVQVJhOztBQUFBLG1CQVNPZCxzREFBUSxDQUFDLEVBQUQsQ0FUZjtBQUFBLE1BU3RCZSxVQVRzQjtBQUFBLE1BU1ZDLGFBVFU7O0FBQUEsbUJBVUdoQixzREFBUSxDQUFDLEVBQUQsQ0FWWDtBQUFBLE1BVXRCaUIsUUFWc0I7QUFBQSxNQVVaQyxXQVZZOztBQUFBLG9CQVdlbEIsc0RBQVEsQ0FBQyxFQUFELENBWHZCO0FBQUEsTUFXdEJtQixjQVhzQjtBQUFBLE1BV05DLGlCQVhNOztBQUFBLG9CQVlPcEIsc0RBQVEsQ0FBQyxFQUFELENBWmY7QUFBQSxNQVl0QnFCLFVBWnNCO0FBQUEsTUFZVkMsYUFaVTs7QUFBQSxvQkFhaUJ0QixzREFBUSxDQUFDLENBQUQsQ0FiekI7QUFBQSxNQWF0QnVCLGVBYnNCO0FBQUEsTUFhTEMsa0JBYks7O0FBQUEsb0JBY2F4QixzREFBUSxDQUFDLENBQUQsQ0FkckI7QUFBQSxNQWN0QnlCLGFBZHNCO0FBQUEsTUFjUEMsZ0JBZE87O0FBQUEsb0JBZWlCMUIsc0RBQVEsQ0FBQyxDQUFELENBZnpCO0FBQUEsTUFldEIyQixlQWZzQjtBQUFBLE1BZUxDLGtCQWZLOztBQUFBLG9CQWdCRDVCLHNEQUFRLENBQUMsQ0FBRCxDQWhCUDtBQUFBLE1BZ0J0QjZCLE1BaEJzQjtBQUFBLE1BZ0JkQyxTQWhCYzs7QUFBQSxvQkFpQks5QixzREFBUSxDQUFDLEVBQUQsQ0FqQmI7QUFBQSxNQWlCdEIrQixTQWpCc0I7QUFBQSxNQWlCWEMsWUFqQlc7O0FBQUEsb0JBa0JLaEMsc0RBQVEsQ0FBQyxFQUFELENBbEJiO0FBQUEsTUFrQnRCaUMsU0FsQnNCO0FBQUEsTUFrQlhDLFlBbEJXOztBQW9CN0JDLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUlyQyxJQUFJLENBQUNzQyxFQUFMLElBQVcsSUFBZixFQUFxQjtBQUNuQkMsV0FBSyw0Q0FBNEM7QUFDL0NDLGVBQU8sRUFBRTtBQUNQQyx1QkFBYSxtQkFBWUMsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQVo7QUFETjtBQURzQyxPQUE1QyxDQUFMLENBS0dDLElBTEgsQ0FLUSxVQUFDQyxHQUFEO0FBQUEsZUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxPQUxSLEVBTUdGLElBTkgsQ0FNUSxVQUFDRyxJQUFELEVBQVU7QUFDZEMsZUFBTyxDQUFDQyxHQUFSLENBQVlGLElBQVo7QUFDQWpDLG9CQUFZLENBQ1ZvQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJ0QyxTQUFsQixDQUFOLENBQ0d1QyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURVLENBQVo7QUFLQXJDLGtCQUFVLENBQ1JrQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJwQyxPQUFsQixDQUFOLENBQ0dxQyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURRLENBQVY7QUFLQW5DLHFCQUFhLENBQ1hnQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJ0QyxTQUFsQixDQUFOLENBQ0d1QyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURXLENBQWI7QUFLQWpDLG1CQUFXLENBQ1Q4Qiw2Q0FBTSxDQUFDSCxJQUFJLENBQUNJLFdBQUwsQ0FBaUJwQyxPQUFsQixDQUFOLENBQ0dxQyxPQURILENBQ1csTUFEWCxFQUVHQyxNQUZILENBRVUsWUFGVixDQURTLENBQVg7QUFLQS9DLGVBQU8sV0FBSXlDLElBQUksQ0FBQ08sU0FBVCxjQUFzQlAsSUFBSSxDQUFDUSxRQUEzQixFQUFQO0FBQ0EvQyxnQkFBUSxDQUFDdUMsSUFBSSxDQUFDeEMsS0FBTixDQUFSO0FBQ0FHLGtCQUFVLENBQUNxQyxJQUFJLENBQUN0QyxPQUFOLENBQVY7QUFDQUcscUJBQWEsQ0FBQ21DLElBQUksQ0FBQ3BDLFVBQU4sQ0FBYjtBQUNELE9BaENIO0FBaUNEO0FBQ0YsR0FwQ1EsRUFvQ04sQ0FBQ1gsSUFBRCxDQXBDTSxDQUFUO0FBc0NBcUMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBS2hDLElBQUksSUFBSSxFQUFULEdBQWdCRSxLQUFLLElBQUksRUFBN0IsRUFBa0M7QUFDaENILGdCQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0Q7QUFDRixHQUpRLEVBSU4sQ0FBQ0MsSUFBRCxFQUFPRSxLQUFQLENBSk0sQ0FBVDtBQU1BOEIseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSW1CLFVBQVUsR0FBR04sNkNBQU0sQ0FBQ3JDLFNBQUQsQ0FBTixDQUFrQjRDLFFBQWxCLENBQTJCLENBQTNCLEVBQThCLEtBQTlCLENBQWpCO0FBQ0EsUUFBSUMsU0FBUyxHQUFHUiw2Q0FBTSxDQUFDbkMsT0FBRCxDQUFOLENBQWdCNEMsR0FBaEIsQ0FBb0IsQ0FBcEIsRUFBdUIsS0FBdkIsQ0FBaEI7O0FBRUEsUUFBSTlDLFNBQVMsSUFBSUUsT0FBakIsRUFBMEI7QUFDeEIsVUFBSU4sT0FBTyxDQUFDbUQsTUFBUixHQUFpQixDQUFyQixFQUF3QjtBQUN0QnRDLHlCQUFpQixDQUNmYixPQUFPLENBQUNvRCxNQUFSLENBQWUsVUFBQ0MsTUFBRCxFQUFZO0FBQ3pCLGNBQUlDLFNBQVMsR0FBR2IsNkNBQU0sQ0FBQ1ksTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJYLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsaUJBQU9GLDZDQUFNLENBQUNhLFNBQUQsQ0FBTixDQUFrQkMsU0FBbEIsQ0FBNEJSLFVBQTVCLEVBQXdDRSxTQUF4QyxFQUFtRCxLQUFuRCxDQUFQO0FBQ0QsU0FIRCxDQURlLENBQWpCO0FBT0FsQyxxQkFBYSxDQUNYZixPQUFPLENBQUNvRCxNQUFSLENBQWUsVUFBQ0MsTUFBRCxFQUFZO0FBQ3pCLGNBQUlDLFNBQVMsR0FBR2IsNkNBQU0sQ0FBQ1ksTUFBTSxDQUFDQyxTQUFSLENBQU4sQ0FBeUJYLE9BQXpCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0EsaUJBQU9GLDZDQUFNLENBQUNhLFNBQUQsQ0FBTixDQUFrQkUsUUFBbEIsQ0FBMkJULFVBQTNCLEVBQXVDLEtBQXZDLENBQVA7QUFDRCxTQUhELENBRFcsQ0FBYjtBQU1EO0FBQ0YsS0FoQkQsTUFnQk87QUFDTGxDLHVCQUFpQixDQUFDLEVBQUQsQ0FBakI7QUFDQUUsbUJBQWEsQ0FBQyxFQUFELENBQWI7QUFDRDs7QUFFRCxRQUFJYixVQUFVLENBQUNpRCxNQUFYLEdBQW9CLENBQXhCLEVBQTJCO0FBQ3pCNUIsZUFBUyxDQUNQckIsVUFBVSxDQUFDdUQsTUFBWCxDQUFrQixVQUFDQyxLQUFELEVBQVFDLFFBQVIsRUFBcUI7QUFDckMsZUFBT0QsS0FBSyxHQUFHQyxRQUFRLENBQUNyQyxNQUF4QjtBQUNELE9BRkQsRUFFRyxDQUZILENBRE8sQ0FBVDtBQUtELEtBTkQsTUFNTztBQUNMQyxlQUFTLENBQUMsQ0FBRCxDQUFUO0FBQ0Q7QUFDRixHQWxDUSxFQWtDTixDQUFDckIsVUFBRCxFQUFhRSxTQUFiLEVBQXdCRSxPQUF4QixDQWxDTSxDQUFUO0FBb0NBc0IseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSWQsVUFBVSxDQUFDcUMsTUFBWCxHQUFvQixDQUF4QixFQUEyQjtBQUN6QmxDLHdCQUFrQixDQUNoQkgsVUFBVSxDQUFDMkMsTUFBWCxDQUFrQixVQUFDQyxLQUFELEVBQVFMLE1BQVIsRUFBbUI7QUFDbkMsZUFBT0ssS0FBSyxHQUFHTCxNQUFNLENBQUNPLE1BQXRCO0FBQ0QsT0FGRCxFQUVHLENBRkgsQ0FEZ0IsQ0FBbEI7QUFLRCxLQU5ELE1BTU87QUFDTDNDLHdCQUFrQixDQUFDLENBQUQsQ0FBbEI7QUFDRDtBQUNGLEdBVlEsRUFVTixDQUFDSCxVQUFELENBVk0sQ0FBVDtBQVlBYyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJaEIsY0FBYyxDQUFDdUMsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QixVQUFJVSxNQUFNLEdBQUdqRCxjQUFjLENBQUN3QyxNQUFmLENBQXNCLFVBQUNDLE1BQUQ7QUFBQSxlQUFZQSxNQUFNLENBQUNPLE1BQVAsR0FBZ0IsQ0FBNUI7QUFBQSxPQUF0QixDQUFiO0FBQ0FyQixhQUFPLENBQUNDLEdBQVIsQ0FBWXFCLE1BQVo7O0FBQ0EsVUFBSUEsTUFBTSxDQUFDVixNQUFQLEdBQWdCLENBQXBCLEVBQXVCO0FBQ3JCaEMsd0JBQWdCLENBQ2QwQyxNQUFNLENBQUNKLE1BQVAsQ0FBYyxVQUFDQyxLQUFELEVBQVFMLE1BQVIsRUFBbUI7QUFDL0IsaUJBQU9LLEtBQUssR0FBR0wsTUFBTSxDQUFDTyxNQUF0QjtBQUNELFNBRkQsRUFFRyxDQUZILENBRGMsQ0FBaEI7QUFLRCxPQU5ELE1BTU87QUFDTHpDLHdCQUFnQixDQUFDLENBQUQsQ0FBaEI7QUFDRDs7QUFFRCxVQUFJMkMsUUFBUSxHQUFHbEQsY0FBYyxDQUFDd0MsTUFBZixDQUFzQixVQUFDQyxNQUFEO0FBQUEsZUFBWUEsTUFBTSxDQUFDTyxNQUFQLEdBQWdCLENBQTVCO0FBQUEsT0FBdEIsQ0FBZjs7QUFDQSxVQUFJRSxRQUFRLENBQUNYLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUI7QUFDdkI5QiwwQkFBa0IsQ0FDaEJ5QyxRQUFRLENBQUNMLE1BQVQsQ0FBZ0IsVUFBQ0MsS0FBRCxFQUFRTCxNQUFSLEVBQW1CO0FBQ2pDLGlCQUFPSyxLQUFLLEdBQUdMLE1BQU0sQ0FBQ08sTUFBdEI7QUFDRCxTQUZELEVBRUcsQ0FGSCxDQURnQixDQUFsQjtBQUtELE9BTkQsTUFNTztBQUNMdkMsMEJBQWtCLENBQUMsQ0FBRCxDQUFsQjtBQUNEO0FBQ0YsS0F2QkQsTUF1Qk87QUFDTEYsc0JBQWdCLENBQUMsQ0FBRCxDQUFoQjtBQUNBRSx3QkFBa0IsQ0FBQyxDQUFELENBQWxCO0FBQ0Q7QUFDRixHQTVCUSxFQTRCTixDQUFDVCxjQUFELENBNUJNLENBQVQ7QUE4QkFnQix5REFBUyxDQUFDLFlBQU07QUFDZFcsV0FBTyxDQUFDQyxHQUFSLENBQVl4QixlQUFaO0FBQ0F1QixXQUFPLENBQUNDLEdBQVIsQ0FBWXRCLGFBQVo7QUFDQXFCLFdBQU8sQ0FBQ0MsR0FBUixDQUFZcEIsZUFBWjs7QUFDQSxRQUFJSixlQUFlLElBQUksQ0FBdkIsRUFBMEI7QUFDeEIsVUFBSUUsYUFBYSxHQUFHRSxlQUFwQixFQUFxQztBQUNuQ0ssb0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxlQUFLLEVBQUUsaUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBQWEsR0FBR0UsZUFGMUI7QUFHRTRDLGVBQUssRUFBRTtBQUhULFNBRFcsRUFNWDtBQUNFRCxlQUFLLEVBQUUsa0JBRFQ7QUFFRUgsZ0JBQU0sRUFBRXhDLGVBRlY7QUFHRTRDLGVBQUssRUFBRTtBQUhULFNBTlcsRUFXWDtBQUNFRCxlQUFLLEVBQUUsa0JBRFQ7QUFFRUgsZ0JBQU0sRUFBRTVDLGVBRlY7QUFHRWdELGVBQUssRUFBRTtBQUhULFNBWFcsQ0FBRCxDQUFaO0FBaUJELE9BbEJELE1Ba0JPO0FBQ0wsWUFBSTVDLGVBQWUsR0FBR0YsYUFBYSxHQUFHRixlQUF0QyxFQUF1RDtBQUNyRFMsc0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxpQkFBSyxFQUFFLHFCQURUO0FBRUVILGtCQUFNLEVBQUUxQyxhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQUY1QztBQUdFNEMsaUJBQUssRUFBRTtBQUhULFdBRFcsRUFNWDtBQUNFRCxpQkFBSyxFQUFFLGtCQURUO0FBRUVILGtCQUFNLEVBQUV4QyxlQUZWO0FBR0U0QyxpQkFBSyxFQUFFO0FBSFQsV0FOVyxDQUFELENBQVo7QUFZRCxTQWJELE1BYU87QUFDTHZDLHNCQUFZLENBQUMsQ0FDWDtBQUNFc0MsaUJBQUssRUFBRSxxQkFEVDtBQUVFSCxrQkFBTSxFQUFFMUMsYUFBYSxHQUFHRixlQUFoQixHQUFrQ0ksZUFGNUM7QUFHRTRDLGlCQUFLLEVBQUU7QUFIVCxXQURXLEVBTVg7QUFDRUQsaUJBQUssRUFBRSw2QkFEVDtBQUVFSCxrQkFBTSxFQUFFMUMsYUFBYSxHQUFHRixlQUYxQjtBQUdFZ0QsaUJBQUssRUFBRTtBQUhULFdBTlcsQ0FBRCxDQUFaO0FBWUQ7QUFDRjtBQUNGLEtBaERELE1BZ0RPO0FBQ0wsVUFBSTlDLGFBQWEsR0FBRytDLElBQUksQ0FBQ0MsR0FBTCxDQUFTbEQsZUFBVCxJQUE0QkksZUFBaEQsRUFBaUU7QUFDL0RLLG9CQUFZLENBQUMsQ0FDWDtBQUNFc0MsZUFBSyxFQUFFLHFCQURUO0FBRUVILGdCQUFNLEVBQUUxQyxhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQUY1QztBQUdFNEMsZUFBSyxFQUFFO0FBSFQsU0FEVyxFQU1YO0FBQ0VELGVBQUssRUFBRSxxQ0FEVDtBQUVFSCxnQkFBTSxFQUFFSyxJQUFJLENBQUNDLEdBQUwsQ0FBU2xELGVBQVQsSUFBNEJJLGVBRnRDO0FBR0U0QyxlQUFLLEVBQUU7QUFIVCxTQU5XLENBQUQsQ0FBWjtBQVlELE9BYkQsTUFhTztBQUNMdkMsb0JBQVksQ0FBQyxDQUNYO0FBQ0VzQyxlQUFLLEVBQUUsaUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBRlY7QUFHRThDLGVBQUssRUFBRTtBQUhULFNBRFcsRUFNWDtBQUNFRCxlQUFLLEVBQUUscUJBRFQ7QUFFRUgsZ0JBQU0sRUFBRTFDLGFBQWEsR0FBR0YsZUFBaEIsR0FBa0NJLGVBRjVDO0FBR0U0QyxlQUFLLEVBQUU7QUFIVCxTQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0Y7QUFDRixHQWpGUSxFQWlGTixDQUFDaEQsZUFBRCxFQUFrQkUsYUFBbEIsRUFBaUNFLGVBQWpDLENBakZNLENBQVQ7QUFtRkFRLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUlOLE1BQU0sSUFBSUYsZUFBZCxFQUErQjtBQUM3Qk8sa0JBQVksQ0FBQyxDQUNYO0FBQ0VvQyxhQUFLLEVBQUUsU0FEVDtBQUVFSCxjQUFNLEVBQUV0QyxNQUFNLEdBQUdGLGVBRm5CO0FBR0U0QyxhQUFLLEVBQUU7QUFIVCxPQURXLEVBTVg7QUFDRUQsYUFBSyxFQUFFLFVBRFQ7QUFFRUgsY0FBTSxFQUFFeEMsZUFGVjtBQUdFNEMsYUFBSyxFQUFFO0FBSFQsT0FOVyxDQUFELENBQVo7QUFZRCxLQWJELE1BYU87QUFDTHJDLGtCQUFZLENBQUMsQ0FDWDtBQUNFb0MsYUFBSyxFQUFFLGlCQURUO0FBRUVILGNBQU0sRUFBRXRDLE1BRlY7QUFHRTBDLGFBQUssRUFBRTtBQUhULE9BRFcsRUFNWDtBQUNFRCxhQUFLLEVBQUUsU0FEVDtBQUVFSCxjQUFNLEVBQUVLLElBQUksQ0FBQ0MsR0FBTCxDQUFTNUMsTUFBTSxHQUFHRixlQUFsQixDQUZWO0FBR0U0QyxhQUFLLEVBQUU7QUFIVCxPQU5XLENBQUQsQ0FBWjtBQVlEO0FBQ0YsR0E1QlEsRUE0Qk4sQ0FBQzFDLE1BQUQsRUFBU0YsZUFBVCxDQTVCTSxDQUFUOztBQThCQSxXQUFTK0MsUUFBVCxDQUFrQkMsQ0FBbEIsRUFBcUI7QUFDbkI3QixXQUFPLENBQUNDLEdBQVIsQ0FBWTRCLENBQVo7QUFDQUEsS0FBQyxDQUFDQyxjQUFGO0FBRUF2QyxTQUFLLFdBQUl3QyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsb0JBQWhCLHlCQUEwRDtBQUM3REMsWUFBTSxFQUFFLEtBRHFEO0FBRTdEMUMsYUFBTyxFQUFFO0FBQ1Asd0JBQWdCLGtCQURUO0FBRVBDLHFCQUFhLG1CQUFZQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBWjtBQUZOLE9BRm9EO0FBTTdEd0MsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNuQnhFLGlCQUFTLEVBQUVxQyw2Q0FBTSxDQUFDckMsU0FBRCxDQUFOLENBQWtCdUMsT0FBbEIsQ0FBMEIsS0FBMUIsQ0FEUTtBQUVuQnJDLGVBQU8sRUFBRW1DLDZDQUFNLENBQUNuQyxPQUFELENBQU4sQ0FBZ0JxQyxPQUFoQixDQUF3QixLQUF4QjtBQUZVLE9BQWY7QUFOdUQsS0FBMUQsQ0FBTCxDQVdHUixJQVhILENBV1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FYUixFQVlHRixJQVpILENBWVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2QsVUFBSUEsSUFBSSxJQUFJLEtBQVosRUFBbUI7QUFDakJ1QyxZQUFJLENBQUNDLElBQUwsQ0FBVSxPQUFWLDBCQUEyQyxPQUEzQztBQUNELE9BRkQsTUFFTztBQUNMdkMsZUFBTyxDQUFDQyxHQUFSLENBQVlGLElBQVo7QUFDQTdCLHFCQUFhLENBQ1hnQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNsQyxTQUFOLENBQU4sQ0FBdUJ1QyxPQUF2QixDQUErQixLQUEvQixFQUFzQ0MsTUFBdEMsQ0FBNkMsWUFBN0MsQ0FEVyxDQUFiO0FBR0FqQyxtQkFBVyxDQUFDOEIsNkNBQU0sQ0FBQ0gsSUFBSSxDQUFDaEMsT0FBTixDQUFOLENBQXFCcUMsT0FBckIsQ0FBNkIsS0FBN0IsRUFBb0NDLE1BQXBDLENBQTJDLFlBQTNDLENBQUQsQ0FBWDtBQUNBdkMsb0JBQVksQ0FDVm9DLDZDQUFNLENBQUNILElBQUksQ0FBQ2xDLFNBQU4sQ0FBTixDQUF1QnVDLE9BQXZCLENBQStCLEtBQS9CLEVBQXNDQyxNQUF0QyxDQUE2QyxZQUE3QyxDQURVLENBQVo7QUFHQXJDLGtCQUFVLENBQUNrQyw2Q0FBTSxDQUFDSCxJQUFJLENBQUNoQyxPQUFOLENBQU4sQ0FBcUJxQyxPQUFyQixDQUE2QixLQUE3QixFQUFvQ0MsTUFBcEMsQ0FBMkMsWUFBM0MsQ0FBRCxDQUFWO0FBQ0Q7QUFDRixLQTFCSDtBQTJCRDs7QUFFRCxzQkFDRSxxRUFBQyw0Q0FBRCxDQUFPLFFBQVA7QUFBQSw0QkFDRSxxRUFBQyxnREFBRDtBQUFBLDZCQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLEVBS0dyRCxJQUFJLENBQUNzQyxFQUFMLEtBQVksSUFBWixnQkFDQztBQUFBLDhCQUNFLHFFQUFDLHlEQUFEO0FBQVcsaUJBQVMsRUFBQyx3QkFBckI7QUFBQSxnQ0FDRTtBQUFBLG9CQUFLakM7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBQSxvQkFBS0U7QUFBTDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGLGVBR0U7QUFBQSxzQ0FDZ0JvQixhQUFhLEdBQUdGLGVBQWhCLEdBQWtDSSxlQURsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFRRSxxRUFBQyxtREFBRDtBQUFLLGlCQUFTLEVBQUMsZ0NBQWY7QUFBQSwrQkFDRSxxRUFBQyxvREFBRDtBQUFNLGtCQUFRLEVBQUUsa0JBQUNnRCxDQUFEO0FBQUEsbUJBQU9ELFFBQVEsQ0FBQ0MsQ0FBRCxDQUFmO0FBQUEsV0FBaEI7QUFBQSxpQ0FDRSxxRUFBQyxtREFBRDtBQUFLLHFCQUFTLEVBQUMsZ0NBQWY7QUFBQSxvQ0FDRSxxRUFBQyxtREFBRDtBQUFLLGdCQUFFLEVBQUMsTUFBUjtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFJRSxxRUFBQyxtREFBRDtBQUFBLHFDQUNFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLG9CQUFJLEVBQUMsTUFEUDtBQUVFLDBCQUFVLEVBQUMsWUFGYjtBQUdFLHFCQUFLLEVBQUVoRSxTQUhUO0FBSUUsd0JBQVEsRUFBRSxrQkFBQ2dFLENBQUQ7QUFBQSx5QkFBTy9ELFlBQVksQ0FBQytELENBQUMsQ0FBQ1csTUFBRixDQUFTQyxLQUFWLENBQW5CO0FBQUE7QUFKWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFKRixlQVlFLHFFQUFDLG1EQUFEO0FBQUssZ0JBQUUsRUFBQyxNQUFSO0FBQUEscUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFaRixlQWVFLHFFQUFDLG1EQUFEO0FBQUEscUNBQ0UscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0Usb0JBQUksRUFBQyxNQURQO0FBRUUscUJBQUssRUFBRTFFLE9BRlQ7QUFHRSwwQkFBVSxFQUFDLGFBSGI7QUFJRSx3QkFBUSxFQUFFLGtCQUFDOEQsQ0FBRDtBQUFBLHlCQUFPN0QsVUFBVSxDQUFDNkQsQ0FBQyxDQUFDVyxNQUFGLENBQVNDLEtBQVYsQ0FBakI7QUFBQTtBQUpaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQWZGLGVBd0JFLHFFQUFDLG1EQUFEO0FBQUEsd0JBQ0c1RSxTQUFTLElBQUlJLFVBQWIsSUFBMkJGLE9BQU8sSUFBSUksUUFBdEMsZ0JBQ0MscUVBQUMsc0RBQUQ7QUFBUSx1QkFBTyxFQUFDLFNBQWhCO0FBQTBCLG9CQUFJLEVBQUMsUUFBL0I7QUFBd0Msa0JBQUUsRUFBQyxXQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERCxnQkFLQyxxRUFBQyxzREFBRDtBQUNFLHVCQUFPLEVBQUMsU0FEVjtBQUVFLG9CQUFJLEVBQUMsUUFGUDtBQUdFLGtCQUFFLEVBQUMsV0FITDtBQUlFLHdCQUFRLE1BSlY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQXhCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVJGLGVBcURFLHFFQUFDLG1EQUFEO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRDtBQUFNLHFCQUFTLEVBQUMsY0FBaEI7QUFBQSxvQ0FDRTtBQUFBLHdCQUNHYyxTQUFTLENBQUN1QyxLQUFWLGdCQUNDLHFFQUFDLHFEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURELGdCQUdDLHFFQUFDLDREQUFEO0FBQ0UseUJBQVMsRUFBQyxLQURaO0FBRUUsdUJBQU8sRUFBRXZDLFNBRlg7QUFHRSx3QkFBUSxFQUFFLE9BSFo7QUFJRSx5QkFBUyxFQUFFO0FBSmI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFhRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxxQ0FDRTtBQUFLLHlCQUFTLEVBQUMsYUFBZjtBQUFBLHdDQUNFO0FBQUEscURBQXlCTixhQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREYsZUFFRTtBQUFBLHVEQUEyQkUsZUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUZGLEVBR0dGLGFBQWEsSUFBSUUsZUFBakIsZ0JBQ0M7QUFBQSxzREFDd0JGLGFBQWEsR0FBR0UsZUFEeEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURELGdCQUtDO0FBQUEsc0RBQ3dCRixhQUFhLEdBQUdFLGVBRHhDLEVBQ3lELEdBRHpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFSSixFQVlHSixlQUFlLElBQUksQ0FBbkIsZ0JBQ0M7QUFBQSx1REFBMkJBLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERCxnQkFHQztBQUFBLHVEQUEyQkEsZUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQWZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBYkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQXFDRSxxRUFBQyxtREFBRDtBQUFBLGlDQUNFLHFFQUFDLG9EQUFEO0FBQU0scUJBQVMsRUFBQyxjQUFoQjtBQUFBLG9DQUNFO0FBQUEsd0JBQ0dRLFNBQVMsQ0FBQ3VDLEtBQVYsZ0JBQ0MscUVBQUMscURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREQsZ0JBR0MscUVBQUMsNERBQUQ7QUFDRSx5QkFBUyxFQUFDLEtBRFo7QUFFRSx1QkFBTyxFQUFFckMsU0FGWDtBQUdFLHdCQUFRLEVBQUUsT0FIWjtBQUlFLHlCQUFTLEVBQUU7QUFKYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSko7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixlQWFFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLHFDQUNFO0FBQUsseUJBQVMsRUFBQyxhQUFmO0FBQUEsd0NBQ0U7QUFBQSw2Q0FBaUJKLE1BQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERixlQUVFO0FBQUEsK0NBQW1CRixlQUFuQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRkYsRUFHR0UsTUFBTSxJQUFJRixlQUFWLGdCQUNDO0FBQUEsOENBQWtCRSxNQUFNLEdBQUdGLGVBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFERCxnQkFHQztBQUFBLDhDQUFrQkUsTUFBTSxHQUFHRixlQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFiRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXJDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FyREYsZUFzSEUscUVBQUMsbURBQUQ7QUFBSyxpQkFBUyxFQUFDLGdDQUFmO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxtQkFBUyxFQUFDLGdDQUFmO0FBQUEsaUNBQ0UscUVBQUMsc0RBQUQ7QUFDRSxxQkFBUyxFQUFDLFlBRFo7QUFFRSxnQkFBSSxFQUFDLElBRlA7QUFHRSxtQkFBTyxFQUFDLFNBSFY7QUFJRSxtQkFBTyxFQUFFO0FBQUEscUJBQU02RCxtREFBTSxDQUFDQyxJQUFQLENBQVksZ0JBQVosQ0FBTjtBQUFBLGFBSlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBV0UscUVBQUMsbURBQUQ7QUFBQSxpQ0FDRSxxRUFBQyxzREFBRDtBQUNFLHFCQUFTLEVBQUMsWUFEWjtBQUVFLGdCQUFJLEVBQUMsSUFGUDtBQUdFLG1CQUFPLEVBQUMsV0FIVjtBQUlFLG1CQUFPLEVBQUU7QUFBQSxxQkFBTUQsbURBQU0sQ0FBQ0MsSUFBUCxDQUFZLGNBQVosQ0FBTjtBQUFBLGFBSlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXRIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERCxnQkErSUMscUVBQUMsOENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXBKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQXlKRDs7R0F6YnVCOUYsSTs7S0FBQUEsSSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC4wM2JjZTc2ZWFhN2Q3MWNmODFkYy5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUsIHVzZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQge1xyXG4gIENvbnRhaW5lcixcclxuICBGb3JtLFxyXG4gIFJvdyxcclxuICBDb2wsXHJcbiAgQnV0dG9uLFxyXG4gIEp1bWJvdHJvbixcclxuICBDYXJkLFxyXG4gIEFsZXJ0LFxyXG59IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IHN0eWxlcyBmcm9tIFwiLi4vc3R5bGVzL0hvbWUubW9kdWxlLmNzc1wiO1xyXG5pbXBvcnQgVXNlckNvbnRleHQgZnJvbSBcIi4uL1VzZXJDb250ZXh0XCI7XHJcbmltcG9ydCBMb2dpbiBmcm9tIFwiLi9sb2dpblwiO1xyXG5pbXBvcnQgQ2F0ZWdvcnkgZnJvbSBcIi4vY2F0ZWdvcnlcIjtcclxuaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XHJcbmltcG9ydCBQaWVDaGFydCBmcm9tIFwiLi4vY29tcG9uZW50cy9QaWVDaGFydFwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcclxuICBjb25zdCB7IHVzZXIsIHNldFVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpO1xyXG4gIGNvbnN0IFtsb2FkaW5nLCBzZXRMb2FkaW5nXSA9IHVzZVN0YXRlKHRydWUpO1xyXG4gIGNvbnN0IFtuYW1lLCBzZXROYW1lXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtlbWFpbCwgc2V0RW1haWxdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3JlY29yZHMsIHNldFJlY29yZHNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFtjYXRlZ29yaWVzLCBzZXRDYXRlZ29yaWVzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbc3RhcnREYXRlLCBzZXRTdGFydERhdGVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2VuZERhdGUsIHNldEVuZERhdGVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3NhdmVkU3RhcnQsIHNldFNhdmVkU3RhcnRdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3NhdmVkRW5kLCBzZXRTYXZlZEVuZF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbY3VycmVudFJlY29yZHMsIHNldEN1cnJlbnRSZWNvcmRzXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbb2xkUmVjb3Jkcywgc2V0T2xkUmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW3ByZXZpb3VzU2F2aW5ncywgc2V0UHJldmlvdXNTYXZpbmdzXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtjdXJyZW50SW5jb21lLCBzZXRDdXJyZW50SW5jb21lXSA9IHVzZVN0YXRlKDApO1xyXG4gIGNvbnN0IFtjdXJyZW50RXhwZW5zZXMsIHNldEN1cnJlbnRFeHBlbnNlc10gPSB1c2VTdGF0ZSgwKTtcclxuICBjb25zdCBbYnVkZ2V0LCBzZXRCdWRnZXRdID0gdXNlU3RhdGUoMCk7XHJcbiAgY29uc3QgW2FjdHVhbFBpZSwgc2V0QWN0dWFsUGllXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbYnVkZ2V0UGllLCBzZXRCdWRnZXRQaWVdID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKHVzZXIuaWQgIT0gbnVsbCkge1xyXG4gICAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vycy9kZXRhaWxzYCwge1xyXG4gICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSlcclxuICAgICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgIHNldFN0YXJ0RGF0ZShcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuY3VycmVudERhdGUuc3RhcnREYXRlKVxyXG4gICAgICAgICAgICAgIC5zdGFydE9mKFwiZGF5c1wiKVxyXG4gICAgICAgICAgICAgIC5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2V0RW5kRGF0ZShcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuY3VycmVudERhdGUuZW5kRGF0ZSlcclxuICAgICAgICAgICAgICAuc3RhcnRPZihcImRheXNcIilcclxuICAgICAgICAgICAgICAuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldFNhdmVkU3RhcnQoXHJcbiAgICAgICAgICAgIG1vbWVudChkYXRhLmN1cnJlbnREYXRlLnN0YXJ0RGF0ZSlcclxuICAgICAgICAgICAgICAuc3RhcnRPZihcImRheXNcIilcclxuICAgICAgICAgICAgICAuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldFNhdmVkRW5kKFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5jdXJyZW50RGF0ZS5lbmREYXRlKVxyXG4gICAgICAgICAgICAgIC5zdGFydE9mKFwiZGF5c1wiKVxyXG4gICAgICAgICAgICAgIC5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2V0TmFtZShgJHtkYXRhLmZpcnN0TmFtZX0gJHtkYXRhLmxhc3ROYW1lfWApO1xyXG4gICAgICAgICAgc2V0RW1haWwoZGF0YS5lbWFpbCk7XHJcbiAgICAgICAgICBzZXRSZWNvcmRzKGRhdGEucmVjb3Jkcyk7XHJcbiAgICAgICAgICBzZXRDYXRlZ29yaWVzKGRhdGEuY2F0ZWdvcmllcyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfSwgW3VzZXJdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmICgobmFtZSAhPSBcIlwiKSAmIChlbWFpbCAhPSBcIlwiKSkge1xyXG4gICAgICBzZXRMb2FkaW5nKGZhbHNlKTtcclxuICAgIH1cclxuICB9LCBbbmFtZSwgZW1haWxdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGxldCBkYXRlQmVmb3JlID0gbW9tZW50KHN0YXJ0RGF0ZSkuc3VidHJhY3QoMSwgXCJkYXlcIik7XHJcbiAgICBsZXQgZGF0ZUFmdGVyID0gbW9tZW50KGVuZERhdGUpLmFkZCgxLCBcImRheVwiKTtcclxuXHJcbiAgICBpZiAoc3RhcnREYXRlIDw9IGVuZERhdGUpIHtcclxuICAgICAgaWYgKHJlY29yZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHNldEN1cnJlbnRSZWNvcmRzKFxyXG4gICAgICAgICAgcmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgdXBkYXRlZE9uID0gbW9tZW50KHJlY29yZC51cGRhdGVkT24pLnN0YXJ0T2YoXCJkYXlzXCIpO1xyXG4gICAgICAgICAgICByZXR1cm4gbW9tZW50KHVwZGF0ZWRPbikuaXNCZXR3ZWVuKGRhdGVCZWZvcmUsIGRhdGVBZnRlciwgXCJkYXlcIik7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHNldE9sZFJlY29yZHMoXHJcbiAgICAgICAgICByZWNvcmRzLmZpbHRlcigocmVjb3JkKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCB1cGRhdGVkT24gPSBtb21lbnQocmVjb3JkLnVwZGF0ZWRPbikuc3RhcnRPZihcImRheXNcIik7XHJcbiAgICAgICAgICAgIHJldHVybiBtb21lbnQodXBkYXRlZE9uKS5pc0JlZm9yZShkYXRlQmVmb3JlLCBcImRheVwiKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0Q3VycmVudFJlY29yZHMoW10pO1xyXG4gICAgICBzZXRPbGRSZWNvcmRzKFtdKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoY2F0ZWdvcmllcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHNldEJ1ZGdldChcclxuICAgICAgICBjYXRlZ29yaWVzLnJlZHVjZSgodG90YWwsIGNhdGVnb3J5KSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4gdG90YWwgKyBjYXRlZ29yeS5idWRnZXQ7XHJcbiAgICAgICAgfSwgMClcclxuICAgICAgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEJ1ZGdldCgwKTtcclxuICAgIH1cclxuICB9LCBbY2F0ZWdvcmllcywgc3RhcnREYXRlLCBlbmREYXRlXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAob2xkUmVjb3Jkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHNldFByZXZpb3VzU2F2aW5ncyhcclxuICAgICAgICBvbGRSZWNvcmRzLnJlZHVjZSgodG90YWwsIHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHRvdGFsICsgcmVjb3JkLmFtb3VudDtcclxuICAgICAgICB9LCAwKVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0UHJldmlvdXNTYXZpbmdzKDApO1xyXG4gICAgfVxyXG4gIH0sIFtvbGRSZWNvcmRzXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoY3VycmVudFJlY29yZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICBsZXQgaW5jb21lID0gY3VycmVudFJlY29yZHMuZmlsdGVyKChyZWNvcmQpID0+IHJlY29yZC5hbW91bnQgPiAwKTtcclxuICAgICAgY29uc29sZS5sb2coaW5jb21lKTtcclxuICAgICAgaWYgKGluY29tZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgc2V0Q3VycmVudEluY29tZShcclxuICAgICAgICAgIGluY29tZS5yZWR1Y2UoKHRvdGFsLCByZWNvcmQpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRvdGFsICsgcmVjb3JkLmFtb3VudDtcclxuICAgICAgICAgIH0sIDApXHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZXRDdXJyZW50SW5jb21lKDApO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgZXhwZW5zZXMgPSBjdXJyZW50UmVjb3Jkcy5maWx0ZXIoKHJlY29yZCkgPT4gcmVjb3JkLmFtb3VudCA8IDApO1xyXG4gICAgICBpZiAoZXhwZW5zZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHNldEN1cnJlbnRFeHBlbnNlcyhcclxuICAgICAgICAgIGV4cGVuc2VzLnJlZHVjZSgodG90YWwsIHJlY29yZCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gdG90YWwgLSByZWNvcmQuYW1vdW50O1xyXG4gICAgICAgICAgfSwgMClcclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNldEN1cnJlbnRFeHBlbnNlcygwKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0Q3VycmVudEluY29tZSgwKTtcclxuICAgICAgc2V0Q3VycmVudEV4cGVuc2VzKDApO1xyXG4gICAgfVxyXG4gIH0sIFtjdXJyZW50UmVjb3Jkc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgY29uc29sZS5sb2cocHJldmlvdXNTYXZpbmdzKTtcclxuICAgIGNvbnNvbGUubG9nKGN1cnJlbnRJbmNvbWUpO1xyXG4gICAgY29uc29sZS5sb2coY3VycmVudEV4cGVuc2VzKTtcclxuICAgIGlmIChwcmV2aW91c1NhdmluZ3MgPj0gMCkge1xyXG4gICAgICBpZiAoY3VycmVudEluY29tZSA+IGN1cnJlbnRFeHBlbnNlcykge1xyXG4gICAgICAgIHNldEFjdHVhbFBpZShbXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIkN1cnJlbnQgU2F2aW5nc1wiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiM0N2I4M2RcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIkN1cnJlbnQgRXhwZW5zZXNcIixcclxuICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIlByZXZpb3VzIFNhdmluZ3NcIixcclxuICAgICAgICAgICAgYW1vdW50OiBwcmV2aW91c1NhdmluZ3MsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiMwZjUyMDlcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgXSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKGN1cnJlbnRFeHBlbnNlcyA8IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MpIHtcclxuICAgICAgICAgIHNldEFjdHVhbFBpZShbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBsYWJlbDogXCJBY2N1bXVsYXRlZCBTYXZpbmdzXCIsXHJcbiAgICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiMwZjUyMDlcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGxhYmVsOiBcIkN1cnJlbnQgRXhwZW5zZXNcIixcclxuICAgICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICBdKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgc2V0QWN0dWFsUGllKFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGxhYmVsOiBcIkFjY3VtdWxhdGVkIERlZmljaXRcIixcclxuICAgICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgICAgY29sb3I6IFwiI2UzMDcwN1wiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgbGFiZWw6IFwiQ29uc3VtZWQgU2F2aW5ncyBhbmQgSW5jb21lXCIsXHJcbiAgICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzLFxyXG4gICAgICAgICAgICAgIGNvbG9yOiBcIiMwZjUyMDlcIixcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIF0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKGN1cnJlbnRJbmNvbWUgPiBNYXRoLmFicyhwcmV2aW91c1NhdmluZ3MpICsgY3VycmVudEV4cGVuc2VzKSB7XHJcbiAgICAgICAgc2V0QWN0dWFsUGllKFtcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgbGFiZWw6IFwiQWNjdW11bGF0ZWQgU2F2aW5nc1wiLFxyXG4gICAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRJbmNvbWUgKyBwcmV2aW91c1NhdmluZ3MgLSBjdXJyZW50RXhwZW5zZXMsXHJcbiAgICAgICAgICAgIGNvbG9yOiBcIiM0N2I4M2RcIixcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIGxhYmVsOiBcIlByZXZpb3VzIERlZmljaXQgKyBDdXJyZW50IEV4cGVuc2VzXCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogTWF0aC5hYnMocHJldmlvdXNTYXZpbmdzKSArIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgY29sb3I6IFwiI2Y1NmYzNlwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICBdKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZXRBY3R1YWxQaWUoW1xyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJDb25zdW1lZCBJbmNvbWVcIixcclxuICAgICAgICAgICAgYW1vdW50OiBjdXJyZW50SW5jb21lLFxyXG4gICAgICAgICAgICBjb2xvcjogXCIjZjU2ZjM2XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAge1xyXG4gICAgICAgICAgICBsYWJlbDogXCJBY2N1bXVsYXRlZCBEZWZpY2l0XCIsXHJcbiAgICAgICAgICAgIGFtb3VudDogY3VycmVudEluY29tZSArIHByZXZpb3VzU2F2aW5ncyAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgICAgY29sb3I6IFwiI2UzMDcwN1wiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICBdKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH0sIFtwcmV2aW91c1NhdmluZ3MsIGN1cnJlbnRJbmNvbWUsIGN1cnJlbnRFeHBlbnNlc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGJ1ZGdldCA+PSBjdXJyZW50RXhwZW5zZXMpIHtcclxuICAgICAgc2V0QnVkZ2V0UGllKFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBsYWJlbDogXCJTYXZpbmdzXCIsXHJcbiAgICAgICAgICBhbW91bnQ6IGJ1ZGdldCAtIGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgIGNvbG9yOiBcIiM0N2I4M2RcIixcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGxhYmVsOiBcIkV4cGVuc2VzXCIsXHJcbiAgICAgICAgICBhbW91bnQ6IGN1cnJlbnRFeHBlbnNlcyxcclxuICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICB9LFxyXG4gICAgICBdKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldEJ1ZGdldFBpZShbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgbGFiZWw6IFwiQ29uc3VtZWQgQnVkZ2V0XCIsXHJcbiAgICAgICAgICBhbW91bnQ6IGJ1ZGdldCxcclxuICAgICAgICAgIGNvbG9yOiBcIiNmNTZmMzZcIixcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIGxhYmVsOiBcIkRlZmljaXRcIixcclxuICAgICAgICAgIGFtb3VudDogTWF0aC5hYnMoYnVkZ2V0IC0gY3VycmVudEV4cGVuc2VzKSxcclxuICAgICAgICAgIGNvbG9yOiBcIiNlMzA3MDdcIixcclxuICAgICAgICB9LFxyXG4gICAgICBdKTtcclxuICAgIH1cclxuICB9LCBbYnVkZ2V0LCBjdXJyZW50RXhwZW5zZXNdKTtcclxuXHJcbiAgZnVuY3Rpb24gc2F2ZURhdGUoZSkge1xyXG4gICAgY29uc29sZS5sb2coZSk7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgZmV0Y2goYCR7cHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQkFTRV9VUkx9L2FwaS91c2Vycy9zZXREYXRlYCwge1xyXG4gICAgICBtZXRob2Q6IFwiUFVUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgc3RhcnREYXRlOiBtb21lbnQoc3RhcnREYXRlKS5zdGFydE9mKFwiZGF5XCIpLFxyXG4gICAgICAgIGVuZERhdGU6IG1vbWVudChlbmREYXRlKS5zdGFydE9mKFwiZGF5XCIpLFxyXG4gICAgICB9KSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEgPT0gZmFsc2UpIHtcclxuICAgICAgICAgIFN3YWwuZmlyZShcIkVycm9yXCIsIGBTb21ldGhpbmcgd2VudCB3cm9uZ2AsIFwiZXJyb3JcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgc2V0U2F2ZWRTdGFydChcclxuICAgICAgICAgICAgbW9tZW50KGRhdGEuc3RhcnREYXRlKS5zdGFydE9mKFwiZGF5XCIpLmZvcm1hdChcInl5eXktTU0tRERcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZXRTYXZlZEVuZChtb21lbnQoZGF0YS5lbmREYXRlKS5zdGFydE9mKFwiZGF5XCIpLmZvcm1hdChcInl5eXktTU0tRERcIikpO1xyXG4gICAgICAgICAgc2V0U3RhcnREYXRlKFxyXG4gICAgICAgICAgICBtb21lbnQoZGF0YS5zdGFydERhdGUpLnN0YXJ0T2YoXCJkYXlcIikuZm9ybWF0KFwieXl5eS1NTS1ERFwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNldEVuZERhdGUobW9tZW50KGRhdGEuZW5kRGF0ZSkuc3RhcnRPZihcImRheVwiKS5mb3JtYXQoXCJ5eXl5LU1NLUREXCIpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxSZWFjdC5GcmFnbWVudD5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPkJ1ZGdldCBUcmFja2VyPC90aXRsZT5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAge3VzZXIuaWQgIT09IG51bGwgPyAoXHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxKdW1ib3Ryb24gY2xhc3NOYW1lPVwianVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxyXG4gICAgICAgICAgICA8aDM+e25hbWV9PC9oMz5cclxuICAgICAgICAgICAgPGgzPntlbWFpbH08L2gzPlxyXG4gICAgICAgICAgICA8aDM+XHJcbiAgICAgICAgICAgICAgQmFsYW5jZTogUGhwIHtjdXJyZW50SW5jb21lICsgcHJldmlvdXNTYXZpbmdzIC0gY3VycmVudEV4cGVuc2VzfVxyXG4gICAgICAgICAgICA8L2gzPlxyXG4gICAgICAgICAgPC9KdW1ib3Ryb24+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICA8Rm9ybSBvblN1Ym1pdD17KGUpID0+IHNhdmVEYXRlKGUpfT5cclxuICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICAgICAgPENvbCBtZD1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+QnVkZ2V0IERhdGU6PC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImRhdGVcIlxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGVmb3JtYXQ9XCJ5eXl5LU1NLUREXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17c3RhcnREYXRlfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U3RhcnREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgPENvbCBtZD1cImF1dG9cIj5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+IHRvIDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJkYXRlXCJcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17ZW5kRGF0ZX1cclxuICAgICAgICAgICAgICAgICAgICBkYXRlZm9ybWF0PVwibXl5eXktTU0tRERcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0RW5kRGF0ZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NvbD5cclxuXHJcbiAgICAgICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgICAgICB7c3RhcnREYXRlICE9IHNhdmVkU3RhcnQgfHwgZW5kRGF0ZSAhPSBzYXZlZEVuZCA/IChcclxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiIGlkPVwic3VibWl0QnRuXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICBTZXQgYXMgRGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzdWJtaXRCdG5cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICBTZXQgYXMgRGVmYXVsdFxyXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgICAgPFJvdz5cclxuICAgICAgICAgICAgPENvbD5cclxuICAgICAgICAgICAgICA8Q2FyZCBjbGFzc05hbWU9XCJob21lcGFnZUNhcmRcIj5cclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgIHthY3R1YWxQaWUubGFiZWwgPyAoXHJcbiAgICAgICAgICAgICAgICAgICAgPEFsZXJ0Pk5vIFJlY29yZHM8L0FsZXJ0PlxyXG4gICAgICAgICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgICAgICAgIDxQaWVDaGFydFxyXG4gICAgICAgICAgICAgICAgICAgICAgY2hhcnRUeXBlPVwiUGllXCJcclxuICAgICAgICAgICAgICAgICAgICAgIHJhd0RhdGE9e2FjdHVhbFBpZX1cclxuICAgICAgICAgICAgICAgICAgICAgIGxhYmVsS2V5PXtcImxhYmVsXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICBhbW91bnRLZXk9e1wiYW1vdW50XCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJob21lcGFnZVBpZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNT5DdXJyZW50IEluY29tZTogUGhwIHtjdXJyZW50SW5jb21lfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg1PkN1cnJlbnQgRXhwZW5zZXM6IFBocCB7Y3VycmVudEV4cGVuc2VzfTwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAge2N1cnJlbnRJbmNvbWUgPj0gY3VycmVudEV4cGVuc2VzID8gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgPGg1PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDdXJyZW50IFNhdmluZ3M6IFBocCB7Y3VycmVudEluY29tZSAtIGN1cnJlbnRFeHBlbnNlc31cclxuICAgICAgICAgICAgICAgICAgICAgIDwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgQ3VycmVudCBEZWZpY2l0OiBQaHAge2N1cnJlbnRJbmNvbWUgLSBjdXJyZW50RXhwZW5zZXN9e1wiIFwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgPC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIHtwcmV2aW91c1NhdmluZ3MgPj0gMCA/IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5QcmV2aW91cyBTYXZpbmdzOiBQaHAge3ByZXZpb3VzU2F2aW5nc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDU+UHJldmlvdXMgRGVmaWNpdDogUGhwIHtwcmV2aW91c1NhdmluZ3N9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgICAgPENhcmQgY2xhc3NOYW1lPVwiaG9tZXBhZ2VDYXJkXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICB7YWN0dWFsUGllLmxhYmVsID8gKFxyXG4gICAgICAgICAgICAgICAgICAgIDxBbGVydD5ObyBSZWNvcmRzPC9BbGVydD5cclxuICAgICAgICAgICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgICAgICAgICA8UGllQ2hhcnRcclxuICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0VHlwZT1cIlBpZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICByYXdEYXRhPXtidWRnZXRQaWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICBsYWJlbEtleT17XCJsYWJlbFwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgYW1vdW50S2V5PXtcImFtb3VudFwifVxyXG4gICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxDYXJkLkJvZHk+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaG9tZXBhZ2VQaWVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aDU+QnVkZ2V0OiBQaHAge2J1ZGdldH08L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgIDxoNT5FeHBlbnNlczogUGhwIHtjdXJyZW50RXhwZW5zZXN9PC9oNT5cclxuICAgICAgICAgICAgICAgICAgICB7YnVkZ2V0ID49IGN1cnJlbnRFeHBlbnNlcyA/IChcclxuICAgICAgICAgICAgICAgICAgICAgIDxoNT5TYXZpbmdzOiBQaHAge2J1ZGdldCAtIGN1cnJlbnRFeHBlbnNlc308L2g1PlxyXG4gICAgICAgICAgICAgICAgICAgICkgOiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICA8aDU+RGVmaWNpdDogUGhwIHtidWRnZXQgLSBjdXJyZW50RXhwZW5zZXN9IDwvaDU+XHJcbiAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQuQm9keT5cclxuICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgPC9Sb3c+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICA8Q29sIGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1tZC1jZW50ZXIgbXktMlwiPlxyXG4gICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJzdWNjZXNzXCJcclxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IFJvdXRlci5wdXNoKFwiLi9jYXRlZ29yeS9hZGRcIil9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgQWRkIENhdGVnb3J5XHJcbiAgICAgICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInB1bGwtcmlnaHRcIlxyXG4gICAgICAgICAgICAgICAgc2l6ZT1cImxnXCJcclxuICAgICAgICAgICAgICAgIHZhcmlhbnQ9XCJzZWNvbmRhcnlcIlxyXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gUm91dGVyLnB1c2goXCIuL3JlY29yZC9hZGRcIil9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgQWRkIFJlY29yZFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICApIDogKFxyXG4gICAgICAgIDxMb2dpbiAvPlxyXG4gICAgICApfVxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICApO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=