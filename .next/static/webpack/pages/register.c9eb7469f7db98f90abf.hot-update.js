webpackHotUpdate_N_E("pages/register",{

/***/ "./node_modules/process/browser.js":
false,

/***/ "./pages/register.js":
/*!***************************!*\
  !*** ./pages/register.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Register; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\register.js",
    _s = $RefreshSig$();





function Register() {
  _s();

  // Form input state hooks
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      firstName = _useState[0],
      setFirstName = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      lastName = _useState2[0],
      setLastName = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState3[0],
      setEmail = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password1 = _useState4[0],
      setPassword1 = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password2 = _useState5[0],
      setPassword2 = _useState5[1]; // State to determine whether submit button is enabled or not


  var _useState6 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isActive = _useState6[0],
      setIsActive = _useState6[1]; // Validate form input whenever email, password1, or password2 is changed


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (firstName != "" && lastName != "" && email != "" && password1 !== "" && password2 !== "" && password2 === password1) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, password1, password2]); // Function to register user

  function registerUser(e) {
    e.preventDefault(); // Check for duplicate email in database first

    fetch("http://localhost:4000/api/users/email-exists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      // If no duplicates found
      if (data === false) {
        fetch("http://localhost:4000/api/users", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password1
          })
        }).then(function (res) {
          return res.json();
        }).then(function (data) {
          // Registration successful
          if (data === true) {
            // Redirect to login
            next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/login");
          } else {
            // Error in creating registration, redirect to error page
            next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/error");
          }
        });
      } else {
        // Duplicate email found
        next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/error");
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_4___default.a, {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Register"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      onSubmit: function onSubmit(e) {
        return registerUser(e);
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "firstName",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "First Name"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 89,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "string",
          placeholder: "Enter First Name",
          value: firstName,
          onChange: function onChange(e) {
            return setFirstName(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "lastName",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Last Name"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 99,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "string",
          placeholder: "Enter Last Name",
          value: lastName,
          onChange: function onChange(e) {
            return setLastName(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 100,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "userEmail",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Email address"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 109,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "email",
          placeholder: "Enter email",
          value: email,
          onChange: function onChange(e) {
            return setEmail(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 108,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "password1",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Password"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "password",
          placeholder: "Password",
          value: password1,
          onChange: function onChange(e) {
            return setPassword1(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 121,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 119,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "password2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Verify Password"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 131,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "password",
          placeholder: "Verify Password",
          value: password2,
          onChange: function onChange(e) {
            return setPassword2(e.target.value);
          },
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 132,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 130,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Text, {
        className: "text-muted",
        children: "We'll never share your details with anyone else."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 140,
        columnNumber: 9
      }, this), isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 146,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        id: "submitBtn",
        disabled: true,
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 150,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 83,
    columnNumber: 5
  }, this);
}

_s(Register, "VXG5vUUVCeuoZLvKVJr+ONT+1RU=");

_c = Register;

var _c;

$RefreshReg$(_c, "Register");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvcmVnaXN0ZXIuanMiXSwibmFtZXMiOlsiUmVnaXN0ZXIiLCJ1c2VTdGF0ZSIsImZpcnN0TmFtZSIsInNldEZpcnN0TmFtZSIsImxhc3ROYW1lIiwic2V0TGFzdE5hbWUiLCJlbWFpbCIsInNldEVtYWlsIiwicGFzc3dvcmQxIiwic2V0UGFzc3dvcmQxIiwicGFzc3dvcmQyIiwic2V0UGFzc3dvcmQyIiwiaXNBY3RpdmUiLCJzZXRJc0FjdGl2ZSIsInVzZUVmZmVjdCIsInJlZ2lzdGVyVXNlciIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImZldGNoIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwicGFzc3dvcmQiLCJSb3V0ZXIiLCJwdXNoIiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFZSxTQUFTQSxRQUFULEdBQW9CO0FBQUE7O0FBQ2pDO0FBRGlDLGtCQUVDQyxzREFBUSxDQUFDLEVBQUQsQ0FGVDtBQUFBLE1BRTFCQyxTQUYwQjtBQUFBLE1BRWZDLFlBRmU7O0FBQUEsbUJBR0RGLHNEQUFRLENBQUMsRUFBRCxDQUhQO0FBQUEsTUFHMUJHLFFBSDBCO0FBQUEsTUFHaEJDLFdBSGdCOztBQUFBLG1CQUlQSixzREFBUSxDQUFDLEVBQUQsQ0FKRDtBQUFBLE1BSTFCSyxLQUowQjtBQUFBLE1BSW5CQyxRQUptQjs7QUFBQSxtQkFLQ04sc0RBQVEsQ0FBQyxFQUFELENBTFQ7QUFBQSxNQUsxQk8sU0FMMEI7QUFBQSxNQUtmQyxZQUxlOztBQUFBLG1CQU1DUixzREFBUSxDQUFDLEVBQUQsQ0FOVDtBQUFBLE1BTTFCUyxTQU4wQjtBQUFBLE1BTWZDLFlBTmUsa0JBUWpDOzs7QUFSaUMsbUJBU0RWLHNEQUFRLENBQUMsS0FBRCxDQVRQO0FBQUEsTUFTMUJXLFFBVDBCO0FBQUEsTUFTaEJDLFdBVGdCLGtCQVdqQzs7O0FBQ0FDLHlEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0EsUUFDRVosU0FBUyxJQUFJLEVBQWIsSUFDQUUsUUFBUSxJQUFJLEVBRFosSUFFQUUsS0FBSyxJQUFJLEVBRlQsSUFHQUUsU0FBUyxLQUFLLEVBSGQsSUFJQUUsU0FBUyxLQUFLLEVBSmQsSUFLQUEsU0FBUyxLQUFLRixTQU5oQixFQU9FO0FBQ0FLLGlCQUFXLENBQUMsSUFBRCxDQUFYO0FBQ0QsS0FURCxNQVNPO0FBQ0xBLGlCQUFXLENBQUMsS0FBRCxDQUFYO0FBQ0Q7QUFDRixHQWRRLEVBY04sQ0FBQ1gsU0FBRCxFQUFZRSxRQUFaLEVBQXNCSSxTQUF0QixFQUFpQ0UsU0FBakMsQ0FkTSxDQUFULENBWmlDLENBNEJqQzs7QUFDQSxXQUFTSyxZQUFULENBQXNCQyxDQUF0QixFQUF5QjtBQUN2QkEsS0FBQyxDQUFDQyxjQUFGLEdBRHVCLENBR3ZCOztBQUNBQyxTQUFLLGlEQUFpRDtBQUNwREMsWUFBTSxFQUFFLE1BRDRDO0FBRXBEQyxhQUFPLEVBQUU7QUFDUCx3QkFBZ0I7QUFEVCxPQUYyQztBQUtwREMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUNuQmpCLGFBQUssRUFBRUE7QUFEWSxPQUFmO0FBTDhDLEtBQWpELENBQUwsQ0FTR2tCLElBVEgsQ0FTUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQVRSLEVBVUdGLElBVkgsQ0FVUSxVQUFDRyxJQUFELEVBQVU7QUFDZDtBQUNBLFVBQUlBLElBQUksS0FBSyxLQUFiLEVBQW9CO0FBQ2xCVCxhQUFLLG9DQUFvQztBQUN2Q0MsZ0JBQU0sRUFBRSxNQUQrQjtBQUV2Q0MsaUJBQU8sRUFBRTtBQUNQLDRCQUFnQjtBQURULFdBRjhCO0FBS3ZDQyxjQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CckIscUJBQVMsRUFBRUEsU0FEUTtBQUVuQkUsb0JBQVEsRUFBRUEsUUFGUztBQUduQkUsaUJBQUssRUFBRUEsS0FIWTtBQUluQnNCLG9CQUFRLEVBQUVwQjtBQUpTLFdBQWY7QUFMaUMsU0FBcEMsQ0FBTCxDQVlHZ0IsSUFaSCxDQVlRLFVBQUNDLEdBQUQ7QUFBQSxpQkFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxTQVpSLEVBYUdGLElBYkgsQ0FhUSxVQUFDRyxJQUFELEVBQVU7QUFDZDtBQUNBLGNBQUlBLElBQUksS0FBSyxJQUFiLEVBQW1CO0FBQ2pCO0FBQ0FFLDhEQUFNLENBQUNDLElBQVAsQ0FBWSxRQUFaO0FBQ0QsV0FIRCxNQUdPO0FBQ0w7QUFDQUQsOERBQU0sQ0FBQ0MsSUFBUCxDQUFZLFFBQVo7QUFDRDtBQUNGLFNBdEJIO0FBdUJELE9BeEJELE1Bd0JPO0FBQ0w7QUFDQUQsMERBQU0sQ0FBQ0MsSUFBUCxDQUFZLFFBQVo7QUFDRDtBQUNGLEtBeENIO0FBeUNEOztBQUVELHNCQUNFLHFFQUFDLDRDQUFELENBQU8sUUFBUDtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsNkJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFJRSxxRUFBQyxvREFBRDtBQUFNLGNBQVEsRUFBRSxrQkFBQ2QsQ0FBRDtBQUFBLGVBQU9ELFlBQVksQ0FBQ0MsQ0FBRCxDQUFuQjtBQUFBLE9BQWhCO0FBQUEsOEJBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxXQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxRQURQO0FBRUUscUJBQVcsRUFBQyxrQkFGZDtBQUdFLGVBQUssRUFBRWQsU0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNjLENBQUQ7QUFBQSxtQkFBT2IsWUFBWSxDQUFDYSxDQUFDLENBQUNlLE1BQUYsQ0FBU0MsS0FBVixDQUFuQjtBQUFBLFdBSlo7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFXRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxpQkFBUyxFQUFDLFVBQXRCO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsY0FBSSxFQUFDLFFBRFA7QUFFRSxxQkFBVyxFQUFDLGlCQUZkO0FBR0UsZUFBSyxFQUFFNUIsUUFIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNZLENBQUQ7QUFBQSxtQkFBT1gsV0FBVyxDQUFDVyxDQUFDLENBQUNlLE1BQUYsQ0FBU0MsS0FBVixDQUFsQjtBQUFBLFdBSlo7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBWEYsZUFxQkUscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxXQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxPQURQO0FBRUUscUJBQVcsRUFBQyxhQUZkO0FBR0UsZUFBSyxFQUFFMUIsS0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNVLENBQUQ7QUFBQSxtQkFBT1QsUUFBUSxDQUFDUyxDQUFDLENBQUNlLE1BQUYsQ0FBU0MsS0FBVixDQUFmO0FBQUEsV0FKWjtBQUtFLGtCQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FyQkYsZUFnQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQVksaUJBQVMsRUFBQyxXQUF0QjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGNBQUksRUFBQyxVQURQO0FBRUUscUJBQVcsRUFBQyxVQUZkO0FBR0UsZUFBSyxFQUFFeEIsU0FIVDtBQUlFLGtCQUFRLEVBQUUsa0JBQUNRLENBQUQ7QUFBQSxtQkFBT1AsWUFBWSxDQUFDTyxDQUFDLENBQUNlLE1BQUYsQ0FBU0MsS0FBVixDQUFuQjtBQUFBLFdBSlo7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBaENGLGVBMkNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGlCQUFTLEVBQUMsV0FBdEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxjQUFJLEVBQUMsVUFEUDtBQUVFLHFCQUFXLEVBQUMsaUJBRmQ7QUFHRSxlQUFLLEVBQUV0QixTQUhUO0FBSUUsa0JBQVEsRUFBRSxrQkFBQ00sQ0FBRDtBQUFBLG1CQUFPTCxZQUFZLENBQUNLLENBQUMsQ0FBQ2UsTUFBRixDQUFTQyxLQUFWLENBQW5CO0FBQUEsV0FKWjtBQUtFLGtCQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0EzQ0YsZUFxREUscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQVcsaUJBQVMsRUFBQyxZQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXJERixFQTBER3BCLFFBQVEsZ0JBQ1AscUVBQUMsc0RBQUQ7QUFBUSxlQUFPLEVBQUMsU0FBaEI7QUFBMEIsWUFBSSxFQUFDLFFBQS9CO0FBQXdDLFVBQUUsRUFBQyxXQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURPLGdCQUtQLHFFQUFDLHNEQUFEO0FBQVEsZUFBTyxFQUFDLFNBQWhCO0FBQTBCLFlBQUksRUFBQyxRQUEvQjtBQUF3QyxVQUFFLEVBQUMsV0FBM0M7QUFBdUQsZ0JBQVEsTUFBL0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0EvREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUEyRUQ7O0dBdkp1QlosUTs7S0FBQUEsUSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9yZWdpc3Rlci5jOWViNzQ2OWY3ZGI5OGY5MGFiZi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgRm9ybSwgQnV0dG9uIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBSZWdpc3RlcigpIHtcclxuICAvLyBGb3JtIGlucHV0IHN0YXRlIGhvb2tzXHJcbiAgY29uc3QgW2ZpcnN0TmFtZSwgc2V0Rmlyc3ROYW1lXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtsYXN0TmFtZSwgc2V0TGFzdE5hbWVdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2VtYWlsLCBzZXRFbWFpbF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbcGFzc3dvcmQxLCBzZXRQYXNzd29yZDFdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3Bhc3N3b3JkMiwgc2V0UGFzc3dvcmQyXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG5cclxuICAvLyBTdGF0ZSB0byBkZXRlcm1pbmUgd2hldGhlciBzdWJtaXQgYnV0dG9uIGlzIGVuYWJsZWQgb3Igbm90XHJcbiAgY29uc3QgW2lzQWN0aXZlLCBzZXRJc0FjdGl2ZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIC8vIFZhbGlkYXRlIGZvcm0gaW5wdXQgd2hlbmV2ZXIgZW1haWwsIHBhc3N3b3JkMSwgb3IgcGFzc3dvcmQyIGlzIGNoYW5nZWRcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgLy8gVmFsaWRhdGlvbiB0byBlbmFibGUgc3VibWl0IGJ1dHRvbiB3aGVuIGFsbCBmaWVsZHMgYXJlIHBvcHVsYXRlZCBhbmQgYm90aCBwYXNzd29yZHMgbWF0Y2hcclxuICAgIGlmIChcclxuICAgICAgZmlyc3ROYW1lICE9IFwiXCIgJiZcclxuICAgICAgbGFzdE5hbWUgIT0gXCJcIiAmJlxyXG4gICAgICBlbWFpbCAhPSBcIlwiICYmXHJcbiAgICAgIHBhc3N3b3JkMSAhPT0gXCJcIiAmJlxyXG4gICAgICBwYXNzd29yZDIgIT09IFwiXCIgJiZcclxuICAgICAgcGFzc3dvcmQyID09PSBwYXNzd29yZDFcclxuICAgICkge1xyXG4gICAgICBzZXRJc0FjdGl2ZSh0cnVlKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNldElzQWN0aXZlKGZhbHNlKTtcclxuICAgIH1cclxuICB9LCBbZmlyc3ROYW1lLCBsYXN0TmFtZSwgcGFzc3dvcmQxLCBwYXNzd29yZDJdKTtcclxuXHJcbiAgLy8gRnVuY3Rpb24gdG8gcmVnaXN0ZXIgdXNlclxyXG4gIGZ1bmN0aW9uIHJlZ2lzdGVyVXNlcihlKSB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgLy8gQ2hlY2sgZm9yIGR1cGxpY2F0ZSBlbWFpbCBpbiBkYXRhYmFzZSBmaXJzdFxyXG4gICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvZW1haWwtZXhpc3RzYCwge1xyXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBlbWFpbDogZW1haWwsXHJcbiAgICAgIH0pLFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAvLyBJZiBubyBkdXBsaWNhdGVzIGZvdW5kXHJcbiAgICAgICAgaWYgKGRhdGEgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICBmZXRjaChgaHR0cDovL2xvY2FsaG9zdDo0MDAwL2FwaS91c2Vyc2AsIHtcclxuICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgZmlyc3ROYW1lOiBmaXJzdE5hbWUsXHJcbiAgICAgICAgICAgICAgbGFzdE5hbWU6IGxhc3ROYW1lLFxyXG4gICAgICAgICAgICAgIGVtYWlsOiBlbWFpbCxcclxuICAgICAgICAgICAgICBwYXNzd29yZDogcGFzc3dvcmQxLFxyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgLy8gUmVnaXN0cmF0aW9uIHN1Y2Nlc3NmdWxcclxuICAgICAgICAgICAgICBpZiAoZGF0YSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgLy8gUmVkaXJlY3QgdG8gbG9naW5cclxuICAgICAgICAgICAgICAgIFJvdXRlci5wdXNoKFwiL2xvZ2luXCIpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBFcnJvciBpbiBjcmVhdGluZyByZWdpc3RyYXRpb24sIHJlZGlyZWN0IHRvIGVycm9yIHBhZ2VcclxuICAgICAgICAgICAgICAgIFJvdXRlci5wdXNoKFwiL2Vycm9yXCIpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIER1cGxpY2F0ZSBlbWFpbCBmb3VuZFxyXG4gICAgICAgICAgUm91dGVyLnB1c2goXCIvZXJyb3JcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8UmVhY3QuRnJhZ21lbnQ+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDx0aXRsZT5SZWdpc3RlcjwvdGl0bGU+XHJcbiAgICAgIDwvSGVhZD5cclxuICAgICAgPEZvcm0gb25TdWJtaXQ9eyhlKSA9PiByZWdpc3RlclVzZXIoZSl9PlxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImZpcnN0TmFtZVwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+Rmlyc3QgTmFtZTwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgdHlwZT1cInN0cmluZ1wiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgRmlyc3QgTmFtZVwiXHJcbiAgICAgICAgICAgIHZhbHVlPXtmaXJzdE5hbWV9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0Rmlyc3ROYW1lKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImxhc3ROYW1lXCI+XHJcbiAgICAgICAgICA8Rm9ybS5MYWJlbD5MYXN0IE5hbWU8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJzdHJpbmdcIlxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIExhc3QgTmFtZVwiXHJcbiAgICAgICAgICAgIHZhbHVlPXtsYXN0TmFtZX1cclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRMYXN0TmFtZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJ1c2VyRW1haWxcIj5cclxuICAgICAgICAgIDxGb3JtLkxhYmVsPkVtYWlsIGFkZHJlc3M8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgZW1haWxcIlxyXG4gICAgICAgICAgICB2YWx1ZT17ZW1haWx9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0RW1haWwoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcblxyXG4gICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInBhc3N3b3JkMVwiPlxyXG4gICAgICAgICAgPEZvcm0uTGFiZWw+UGFzc3dvcmQ8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmRcIlxyXG4gICAgICAgICAgICB2YWx1ZT17cGFzc3dvcmQxfVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFBhc3N3b3JkMShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgIDwvRm9ybS5Hcm91cD5cclxuXHJcbiAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwicGFzc3dvcmQyXCI+XHJcbiAgICAgICAgICA8Rm9ybS5MYWJlbD5WZXJpZnkgUGFzc3dvcmQ8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiVmVyaWZ5IFBhc3N3b3JkXCJcclxuICAgICAgICAgICAgdmFsdWU9e3Bhc3N3b3JkMn1cclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRQYXNzd29yZDIoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgPEZvcm0uVGV4dCBjbGFzc05hbWU9XCJ0ZXh0LW11dGVkXCI+XHJcbiAgICAgICAgICBXZSdsbCBuZXZlciBzaGFyZSB5b3VyIGRldGFpbHMgd2l0aCBhbnlvbmUgZWxzZS5cclxuICAgICAgICA8L0Zvcm0uVGV4dD5cclxuXHJcbiAgICAgICAgey8qIENvbmRpdGlvbmFsbHkgcmVuZGVyIHN1Ym1pdCBidXR0b24gYmFzZWQgb24gaXNBY3RpdmUgc3RhdGUgKi99XHJcbiAgICAgICAge2lzQWN0aXZlID8gKFxyXG4gICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIHR5cGU9XCJzdWJtaXRcIiBpZD1cInN1Ym1pdEJ0blwiPlxyXG4gICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiIGlkPVwic3VibWl0QnRuXCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgPC9CdXR0b24+XHJcbiAgICAgICAgKX1cclxuICAgICAgPC9Gb3JtPlxyXG4gICAgPC9SZWFjdC5GcmFnbWVudD5cclxuICApO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=