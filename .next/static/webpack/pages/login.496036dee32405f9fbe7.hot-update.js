webpackHotUpdate_N_E("pages/login",{

/***/ "./pages/login.js":
/*!************************!*\
  !*** ./pages/login.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process, module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return login; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-google-login */ "./node_modules/react-google-login/dist/google-login.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _UserContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../UserContext */ "./UserContext.js");
/* harmony import */ var _components_View__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/View */ "./components/View.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../app-helper */ "./app-helper.js");
/* harmony import */ var _app_helper__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_app_helper__WEBPACK_IMPORTED_MODULE_9__);


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\pages\\login.js",
    _this = undefined,
    _s = $RefreshSig$();










function login() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_View__WEBPACK_IMPORTED_MODULE_8__["default"], {
    title: "Login",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      className: "justify-content-center",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: true,
        md: "6",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(LoginForm, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

var LoginForm = function LoginForm() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_UserContext__WEBPACK_IMPORTED_MODULE_7__["default"]),
      user = _useContext.user,
      setUser = _useContext.setUser;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      email = _useState[0],
      setEmail = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""),
      password = _useState2[0],
      setPassword = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false),
      isActive = _useState3[0],
      setIsActive = _useState3[1];

  function authenticate(e) {
    e.preventDefault();
    var options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    };
    fetch("http://localhost:4000/api/users/login", options).then(function (res) {
      return res.json();
    }).then(function (data) {
      console.log(data);

      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error === "does-not-exist") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "User does not exist.", "error");
        } else if (data.error === "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure, try an alternative login procedure", "error");
        } else if (data.error === "incorrect-password") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Authentication Failed", "password is incorrect", "error");
        }
      }
    });
  }

  var aunthenticateGoogleToken = function aunthenticateGoogleToken(response) {
    var payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    };
    fetch("".concat(process.env.NEXT_PUBLIC_BASE_URL, "/api/users/verify-google-id-token"), payload).then(function (res) {
      return res.json();
    }).then(function (data) {
      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        setUser({
          id: data._id
        });
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push("/");
      } else {
        if (data.error = "google-auth-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Google Auth Error", "Google authentication procedure failed.", "error");
        } else if (data.error = "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire("Login Type Error", "You may have registered through a different login procedure.", "error");
        }
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Header, {
      children: "Login details"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 7
    }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_6___default.a, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
          children: "Budget Tracker"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 118,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
        onSubmit: function onSubmit(e) {
          return authenticate(e);
        },
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "userEmail",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Email address"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 122,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "email",
            placeholder: "Enter email",
            value: email,
            onChange: function onChange(e) {
              return setEmail(e.target.value);
            },
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 123,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 121,
          columnNumber: 11
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
          controlId: "password",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
            children: "Password"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 133,
            columnNumber: 13
          }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
            type: "password",
            placeholder: "Password",
            value: password,
            onChange: function onChange(e) {
              return setPassword(e.target.value);
            },
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 134,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 132,
          columnNumber: 11
        }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
          className: "justify-content-center px-3",
          children: isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 15
          }, _this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: "bg-primary btn-block",
            type: "submit",
            disabled: true,
            children: "Submit"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 148,
            columnNumber: 15
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 142,
          columnNumber: 11
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 120,
        columnNumber: 9
      }, _this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_google_login__WEBPACK_IMPORTED_MODULE_3__["GoogleLogin"], {
        clientId: "705089561550-sh5j061fh45spu9s4gpg2noov5e14qjv.apps.googleusercontent.com",
        buttonText: "Login",
        onSuccess: aunthenticateGoogleToken,
        onFailure: aunthenticateGoogleToken,
        cookiePolicy: "single_host_origin",
        className: "w-100 text-center d-flex justify-content-center"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 155,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 116,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 114,
    columnNumber: 5
  }, _this);
};

_s(LoginForm, "A6LN1EkaZmgXDaI+XK75hYWiYQs=");

_c = LoginForm;

var _c;

$RefreshReg$(_c, "LoginForm");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/process/browser.js */ "./node_modules/process/browser.js"), __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvbG9naW4uanMiXSwibmFtZXMiOlsibG9naW4iLCJMb2dpbkZvcm0iLCJ1c2VDb250ZXh0IiwiVXNlckNvbnRleHQiLCJ1c2VyIiwic2V0VXNlciIsInVzZVN0YXRlIiwiZW1haWwiLCJzZXRFbWFpbCIsInBhc3N3b3JkIiwic2V0UGFzc3dvcmQiLCJpc0FjdGl2ZSIsInNldElzQWN0aXZlIiwiYXV0aGVudGljYXRlIiwiZSIsInByZXZlbnREZWZhdWx0Iiwib3B0aW9ucyIsIm1ldGhvZCIsImhlYWRlcnMiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsImZldGNoIiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiY29uc29sZSIsImxvZyIsImFjY2Vzc1Rva2VuIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsImlkIiwiX2lkIiwiUm91dGVyIiwicHVzaCIsImVycm9yIiwiU3dhbCIsImZpcmUiLCJhdW50aGVudGljYXRlR29vZ2xlVG9rZW4iLCJyZXNwb25zZSIsInBheWxvYWQiLCJ0b2tlbklkIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0JBU0VfVVJMIiwidXNlRWZmZWN0IiwidGFyZ2V0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0EsS0FBVCxHQUFpQjtBQUM5QixzQkFDRSxxRUFBQyx3REFBRDtBQUFNLFNBQUssRUFBRSxPQUFiO0FBQUEsMkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxlQUFTLEVBQUMsd0JBQWY7QUFBQSw2QkFDRSxxRUFBQyxtREFBRDtBQUFLLFVBQUUsTUFBUDtBQUFRLFVBQUUsRUFBQyxHQUFYO0FBQUEsK0JBQ0UscUVBQUMsU0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFTRDs7QUFFRCxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQUFNO0FBQUE7O0FBQUEsb0JBQ0lDLHdEQUFVLENBQUNDLG9EQUFELENBRGQ7QUFBQSxNQUNkQyxJQURjLGVBQ2RBLElBRGM7QUFBQSxNQUNSQyxPQURRLGVBQ1JBLE9BRFE7O0FBQUEsa0JBR0lDLHNEQUFRLENBQUMsRUFBRCxDQUhaO0FBQUEsTUFHZkMsS0FIZTtBQUFBLE1BR1JDLFFBSFE7O0FBQUEsbUJBSVVGLHNEQUFRLENBQUMsRUFBRCxDQUpsQjtBQUFBLE1BSWZHLFFBSmU7QUFBQSxNQUlMQyxXQUpLOztBQUFBLG1CQUtVSixzREFBUSxDQUFDLEtBQUQsQ0FMbEI7QUFBQSxNQUtmSyxRQUxlO0FBQUEsTUFLTEMsV0FMSzs7QUFPdEIsV0FBU0MsWUFBVCxDQUFzQkMsQ0FBdEIsRUFBeUI7QUFDdkJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUVBLFFBQU1DLE9BQU8sR0FBRztBQUNkQyxZQUFNLEVBQUUsTUFETTtBQUVkQyxhQUFPLEVBQUU7QUFBRSx3QkFBZ0I7QUFBbEIsT0FGSztBQUdkQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CZCxhQUFLLEVBQUVBLEtBRFk7QUFFbkJFLGdCQUFRLEVBQUVBO0FBRlMsT0FBZjtBQUhRLEtBQWhCO0FBU0FhLFNBQUssMENBQTBDTixPQUExQyxDQUFMLENBQ0dPLElBREgsQ0FDUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQURSLEVBRUdGLElBRkgsQ0FFUSxVQUFDRyxJQUFELEVBQVU7QUFDZEMsYUFBTyxDQUFDQyxHQUFSLENBQVlGLElBQVo7O0FBQ0EsVUFBSSxPQUFPQSxJQUFJLENBQUNHLFdBQVosS0FBNEIsV0FBaEMsRUFBNkM7QUFDM0NDLG9CQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsRUFBOEJMLElBQUksQ0FBQ0csV0FBbkM7QUFDQXhCLGVBQU8sQ0FBQztBQUFFMkIsWUFBRSxFQUFFTixJQUFJLENBQUNPO0FBQVgsU0FBRCxDQUFQO0FBQ0FDLDBEQUFNLENBQUNDLElBQVAsQ0FBWSxHQUFaO0FBQ0QsT0FKRCxNQUlPO0FBQ0wsWUFBSVQsSUFBSSxDQUFDVSxLQUFMLEtBQWUsZ0JBQW5CLEVBQXFDO0FBQ25DQyw0REFBSSxDQUFDQyxJQUFMLENBQVUsdUJBQVYsRUFBbUMsc0JBQW5DLEVBQTJELE9BQTNEO0FBQ0QsU0FGRCxNQUVPLElBQUlaLElBQUksQ0FBQ1UsS0FBTCxLQUFlLGtCQUFuQixFQUF1QztBQUM1Q0MsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLGtCQURGLEVBRUUsaUdBRkYsRUFHRSxPQUhGO0FBS0QsU0FOTSxNQU1BLElBQUlaLElBQUksQ0FBQ1UsS0FBTCxLQUFlLG9CQUFuQixFQUF5QztBQUM5Q0MsNERBQUksQ0FBQ0MsSUFBTCxDQUNFLHVCQURGLEVBRUUsdUJBRkYsRUFHRSxPQUhGO0FBS0Q7QUFDRjtBQUNGLEtBekJIO0FBMEJEOztBQUVELE1BQU1DLHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsQ0FBQ0MsUUFBRCxFQUFjO0FBQzdDLFFBQU1DLE9BQU8sR0FBRztBQUNkeEIsWUFBTSxFQUFFLE1BRE07QUFFZEMsYUFBTyxFQUFFO0FBQUUsd0JBQWdCO0FBQWxCLE9BRks7QUFHZEMsVUFBSSxFQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTtBQUFFcUIsZUFBTyxFQUFFRixRQUFRLENBQUNFO0FBQXBCLE9BQWY7QUFIUSxLQUFoQjtBQU1BcEIsU0FBSyxXQUNBcUIsT0FBTyxDQUFDQyxHQUFSLENBQVlDLG9CQURaLHdDQUVISixPQUZHLENBQUwsQ0FJR2xCLElBSkgsQ0FJUSxVQUFDQyxHQUFEO0FBQUEsYUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBQVQ7QUFBQSxLQUpSLEVBS0dGLElBTEgsQ0FLUSxVQUFDRyxJQUFELEVBQVU7QUFDZCxVQUFJLE9BQU9BLElBQUksQ0FBQ0csV0FBWixLQUE0QixXQUFoQyxFQUE2QztBQUMzQ0Msb0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4QkwsSUFBSSxDQUFDRyxXQUFuQztBQUNBeEIsZUFBTyxDQUFDO0FBQUUyQixZQUFFLEVBQUVOLElBQUksQ0FBQ087QUFBWCxTQUFELENBQVA7QUFDQUMsMERBQU0sQ0FBQ0MsSUFBUCxDQUFZLEdBQVo7QUFDRCxPQUpELE1BSU87QUFDTCxZQUFLVCxJQUFJLENBQUNVLEtBQUwsR0FBYSxtQkFBbEIsRUFBd0M7QUFDdENDLDREQUFJLENBQUNDLElBQUwsQ0FDRSxtQkFERixFQUVFLHlDQUZGLEVBR0UsT0FIRjtBQUtELFNBTkQsTUFNTyxJQUFLWixJQUFJLENBQUNVLEtBQUwsR0FBYSxrQkFBbEIsRUFBdUM7QUFDNUNDLDREQUFJLENBQUNDLElBQUwsQ0FDRSxrQkFERixFQUVFLDhEQUZGLEVBR0UsT0FIRjtBQUtEO0FBQ0Y7QUFDRixLQXpCSDtBQTBCRCxHQWpDRDs7QUFtQ0FRLHlEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUl2QyxLQUFLLEtBQUssRUFBVixJQUFnQkUsUUFBUSxLQUFLLEVBQWpDLEVBQXFDO0FBQ25DRyxpQkFBVyxDQUFDLElBQUQsQ0FBWDtBQUNELEtBRkQsTUFFTztBQUNMQSxpQkFBVyxDQUFDLEtBQUQsQ0FBWDtBQUNEO0FBQ0YsR0FOUSxFQU1OLENBQUNMLEtBQUQsRUFBUUUsUUFBUixDQU5NLENBQVQ7QUFRQSxzQkFDRSxxRUFBQyxvREFBRDtBQUFBLDRCQUNFLHFFQUFDLG9EQUFELENBQU0sTUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsOEJBQ0UscUVBQUMsZ0RBQUQ7QUFBQSwrQkFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQUlFLHFFQUFDLG9EQUFEO0FBQU0sZ0JBQVEsRUFBRSxrQkFBQ0ssQ0FBRDtBQUFBLGlCQUFPRCxZQUFZLENBQUNDLENBQUQsQ0FBbkI7QUFBQSxTQUFoQjtBQUFBLGdDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLG1CQUFTLEVBQUMsV0FBdEI7QUFBQSxrQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxnQkFBSSxFQUFDLE9BRFA7QUFFRSx1QkFBVyxFQUFDLGFBRmQ7QUFHRSxpQkFBSyxFQUFFUCxLQUhUO0FBSUUsb0JBQVEsRUFBRSxrQkFBQ08sQ0FBRDtBQUFBLHFCQUFPTixRQUFRLENBQUNNLENBQUMsQ0FBQ2lDLE1BQUYsQ0FBU0MsS0FBVixDQUFmO0FBQUEsYUFKWjtBQUtFLG9CQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFZRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxtQkFBUyxFQUFDLFVBQXRCO0FBQUEsa0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsZ0JBQUksRUFBQyxVQURQO0FBRUUsdUJBQVcsRUFBQyxVQUZkO0FBR0UsaUJBQUssRUFBRXZDLFFBSFQ7QUFJRSxvQkFBUSxFQUFFLGtCQUFDSyxDQUFEO0FBQUEscUJBQU9KLFdBQVcsQ0FBQ0ksQ0FBQyxDQUFDaUMsTUFBRixDQUFTQyxLQUFWLENBQWxCO0FBQUEsYUFKWjtBQUtFLG9CQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBWkYsZUFzQkUscUVBQUMsbURBQUQ7QUFBSyxtQkFBUyxFQUFDLDZCQUFmO0FBQUEsb0JBQ0dyQyxRQUFRLGdCQUNQLHFFQUFDLHNEQUFEO0FBQVEscUJBQVMsRUFBQyxzQkFBbEI7QUFBeUMsZ0JBQUksRUFBQyxRQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFETyxnQkFLUCxxRUFBQyxzREFBRDtBQUFRLHFCQUFTLEVBQUMsc0JBQWxCO0FBQXlDLGdCQUFJLEVBQUMsUUFBOUM7QUFBdUQsb0JBQVEsTUFBL0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXRCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFKRixlQXVDRSxxRUFBQyw4REFBRDtBQUNFLGdCQUFRLEVBQUMsMEVBRFg7QUFFRSxrQkFBVSxFQUFDLE9BRmI7QUFHRSxpQkFBUyxFQUFFNEIsd0JBSGI7QUFJRSxpQkFBUyxFQUFFQSx3QkFKYjtBQUtFLG9CQUFZLEVBQUUsb0JBTGhCO0FBTUUsaUJBQVMsRUFBQztBQU5aO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUF2Q0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREY7QUFxREQsQ0EvSUQ7O0dBQU10QyxTOztLQUFBQSxTIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2xvZ2luLjQ5NjAzNmRlZTMyNDA1ZjlmYmU3LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGb3JtLCBCdXR0b24sIENhcmQsIFJvdywgQ29sIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgeyBHb29nbGVMb2dpbiB9IGZyb20gXCJyZWFjdC1nb29nbGUtbG9naW5cIjtcclxuaW1wb3J0IFN3YWwgZnJvbSBcInN3ZWV0YWxlcnQyXCI7XHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gXCIuLi9Vc2VyQ29udGV4dFwiO1xyXG5pbXBvcnQgVmlldyBmcm9tIFwiLi4vY29tcG9uZW50cy9WaWV3XCI7XHJcbmltcG9ydCBBcHBIZWxwZXIgZnJvbSBcIi4uL2FwcC1oZWxwZXJcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGxvZ2luKCkge1xyXG4gIHJldHVybiAoXHJcbiAgICA8VmlldyB0aXRsZT17XCJMb2dpblwifT5cclxuICAgICAgPFJvdyBjbGFzc05hbWU9XCJqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCI+XHJcbiAgICAgICAgPENvbCB4cyBtZD1cIjZcIj5cclxuICAgICAgICAgIDxMb2dpbkZvcm0gLz5cclxuICAgICAgICA8L0NvbD5cclxuICAgICAgPC9Sb3c+XHJcbiAgICA8L1ZpZXc+XHJcbiAgKTtcclxufVxyXG5cclxuY29uc3QgTG9naW5Gb3JtID0gKCkgPT4ge1xyXG4gIGNvbnN0IHsgdXNlciwgc2V0VXNlciB9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dCk7XHJcblxyXG4gIGNvbnN0IFtlbWFpbCwgc2V0RW1haWxdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW3Bhc3N3b3JkLCBzZXRQYXNzd29yZF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbaXNBY3RpdmUsIHNldElzQWN0aXZlXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuXHJcbiAgZnVuY3Rpb24gYXV0aGVudGljYXRlKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICBjb25zdCBvcHRpb25zID0ge1xyXG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgICBoZWFkZXJzOiB7IFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBlbWFpbDogZW1haWwsXHJcbiAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkLFxyXG4gICAgICB9KSxcclxuICAgIH07XHJcblxyXG4gICAgZmV0Y2goYGh0dHA6Ly9sb2NhbGhvc3Q6NDAwMC9hcGkvdXNlcnMvbG9naW5gLCBvcHRpb25zKVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgIGlmICh0eXBlb2YgZGF0YS5hY2Nlc3NUb2tlbiAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ0b2tlblwiLCBkYXRhLmFjY2Vzc1Rva2VuKTtcclxuICAgICAgICAgIHNldFVzZXIoeyBpZDogZGF0YS5faWQgfSk7XHJcbiAgICAgICAgICBSb3V0ZXIucHVzaChcIi9cIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmIChkYXRhLmVycm9yID09PSBcImRvZXMtbm90LWV4aXN0XCIpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFwiQXV0aGVudGljYXRpb24gRmFpbGVkXCIsIFwiVXNlciBkb2VzIG5vdCBleGlzdC5cIiwgXCJlcnJvclwiKTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YS5lcnJvciA9PT0gXCJsb2dpbi10eXBlLWVycm9yXCIpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFxyXG4gICAgICAgICAgICAgIFwiTG9naW4gVHlwZSBFcnJvclwiLFxyXG4gICAgICAgICAgICAgIFwiWW91IG1heSBoYXZlIHJlZ2lzdGVyZWQgdGhyb3VnaCBhIGRpZmZlcmVudCBsb2dpbiBwcm9jZWR1cmUsIHRyeSBhbiBhbHRlcm5hdGl2ZSBsb2dpbiBwcm9jZWR1cmVcIixcclxuICAgICAgICAgICAgICBcImVycm9yXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YS5lcnJvciA9PT0gXCJpbmNvcnJlY3QtcGFzc3dvcmRcIikge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJBdXRoZW50aWNhdGlvbiBGYWlsZWRcIixcclxuICAgICAgICAgICAgICBcInBhc3N3b3JkIGlzIGluY29ycmVjdFwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdCBhdW50aGVudGljYXRlR29vZ2xlVG9rZW4gPSAocmVzcG9uc2UpID0+IHtcclxuICAgIGNvbnN0IHBheWxvYWQgPSB7XHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHsgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIgfSxcclxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyB0b2tlbklkOiByZXNwb25zZS50b2tlbklkIH0pLFxyXG4gICAgfTtcclxuXHJcbiAgICBmZXRjaChcclxuICAgICAgYCR7cHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQkFTRV9VUkx9L2FwaS91c2Vycy92ZXJpZnktZ29vZ2xlLWlkLXRva2VuYCxcclxuICAgICAgcGF5bG9hZFxyXG4gICAgKVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIGlmICh0eXBlb2YgZGF0YS5hY2Nlc3NUb2tlbiAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ0b2tlblwiLCBkYXRhLmFjY2Vzc1Rva2VuKTtcclxuICAgICAgICAgIHNldFVzZXIoeyBpZDogZGF0YS5faWQgfSk7XHJcbiAgICAgICAgICBSb3V0ZXIucHVzaChcIi9cIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmICgoZGF0YS5lcnJvciA9IFwiZ29vZ2xlLWF1dGgtZXJyb3JcIikpIHtcclxuICAgICAgICAgICAgU3dhbC5maXJlKFxyXG4gICAgICAgICAgICAgIFwiR29vZ2xlIEF1dGggRXJyb3JcIixcclxuICAgICAgICAgICAgICBcIkdvb2dsZSBhdXRoZW50aWNhdGlvbiBwcm9jZWR1cmUgZmFpbGVkLlwiLFxyXG4gICAgICAgICAgICAgIFwiZXJyb3JcIlxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICgoZGF0YS5lcnJvciA9IFwibG9naW4tdHlwZS1lcnJvclwiKSkge1xyXG4gICAgICAgICAgICBTd2FsLmZpcmUoXHJcbiAgICAgICAgICAgICAgXCJMb2dpbiBUeXBlIEVycm9yXCIsXHJcbiAgICAgICAgICAgICAgXCJZb3UgbWF5IGhhdmUgcmVnaXN0ZXJlZCB0aHJvdWdoIGEgZGlmZmVyZW50IGxvZ2luIHByb2NlZHVyZS5cIixcclxuICAgICAgICAgICAgICBcImVycm9yXCJcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoZW1haWwgIT09IFwiXCIgJiYgcGFzc3dvcmQgIT09IFwiXCIpIHtcclxuICAgICAgc2V0SXNBY3RpdmUodHJ1ZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRJc0FjdGl2ZShmYWxzZSk7XHJcbiAgICB9XHJcbiAgfSwgW2VtYWlsLCBwYXNzd29yZF0pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPENhcmQ+XHJcbiAgICAgIDxDYXJkLkhlYWRlcj5Mb2dpbiBkZXRhaWxzPC9DYXJkLkhlYWRlcj5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8SGVhZD5cclxuICAgICAgICAgIDx0aXRsZT5CdWRnZXQgVHJhY2tlcjwvdGl0bGU+XHJcbiAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgIDxGb3JtIG9uU3VibWl0PXsoZSkgPT4gYXV0aGVudGljYXRlKGUpfT5cclxuICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInVzZXJFbWFpbFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbCBhZGRyZXNzPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgdHlwZT1cImVtYWlsXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIGVtYWlsXCJcclxuICAgICAgICAgICAgICB2YWx1ZT17ZW1haWx9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRFbWFpbChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuXHJcbiAgICAgICAgICA8Rm9ybS5Hcm91cCBjb250cm9sSWQ9XCJwYXNzd29yZFwiPlxyXG4gICAgICAgICAgICA8Rm9ybS5MYWJlbD5QYXNzd29yZDwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJQYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgdmFsdWU9e3Bhc3N3b3JkfVxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0UGFzc3dvcmQoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImp1c3RpZnktY29udGVudC1jZW50ZXIgcHgtM1wiPlxyXG4gICAgICAgICAgICB7aXNBY3RpdmUgPyAoXHJcbiAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzc05hbWU9XCJiZy1wcmltYXJ5IGJ0bi1ibG9ja1wiIHR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgIFN1Ym1pdFxyXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxyXG4gICAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICAgIDxCdXR0b24gY2xhc3NOYW1lPVwiYmctcHJpbWFyeSBidG4tYmxvY2tcIiB0eXBlPVwic3VibWl0XCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICBTdWJtaXRcclxuICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgKX1cclxuICAgICAgICAgIDwvUm93PlxyXG4gICAgICAgIDwvRm9ybT5cclxuXHJcbiAgICAgICAgPEdvb2dsZUxvZ2luXHJcbiAgICAgICAgICBjbGllbnRJZD1cIjcwNTA4OTU2MTU1MC1zaDVqMDYxZmg0NXNwdTlzNGdwZzJub292NWUxNHFqdi5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbVwiXHJcbiAgICAgICAgICBidXR0b25UZXh0PVwiTG9naW5cIlxyXG4gICAgICAgICAgb25TdWNjZXNzPXthdW50aGVudGljYXRlR29vZ2xlVG9rZW59XHJcbiAgICAgICAgICBvbkZhaWx1cmU9e2F1bnRoZW50aWNhdGVHb29nbGVUb2tlbn1cclxuICAgICAgICAgIGNvb2tpZVBvbGljeT17XCJzaW5nbGVfaG9zdF9vcmlnaW5cIn1cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCJcclxuICAgICAgICAvPlxyXG4gICAgICA8L0NhcmQuQm9keT5cclxuICAgIDwvQ2FyZD5cclxuICApO1xyXG59O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9