webpackHotUpdate_N_E("pages/index",{

/***/ "./components/PieChart.js":
/*!********************************!*\
  !*** ./components/PieChart.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChart; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "./node_modules/react-chartjs-2/es/index.js");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/ColorRandomizer */ "./helpers/ColorRandomizer.js");


var _jsxFileName = "C:\\Users\\richa\\OneDrive\\Desktop\\Caps 3\\capstone-3-frontend\\components\\PieChart.js",
    _s = $RefreshSig$();




function PieChart(_ref) {
  _s();

  var chartType = _ref.chartType,
      rawData = _ref.rawData,
      labelKey = _ref.labelKey,
      amountKey = _ref.amountKey;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      label = _useState[0],
      setLabel = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      amount = _useState2[0],
      setAmount = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      bgColors = _useState3[0],
      setBgColors = _useState3[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (rawData) {
      setLabel(rawData.map(function (element) {
        return element[labelKey];
      }));
      setAmount(rawData.map(function (element) {
        return Math.abs(element[amountKey]);
      }));
      var colors = rawData.map(function (element) {
        if (element.color) {
          return element.color;
        } else {
          return "#".concat(Object(_helpers_ColorRandomizer__WEBPACK_IMPORTED_MODULE_3__["default"])());
        }
      });
      setBgColors(colors);
    } else {
      setLabel([]);
      setAmount([]);
      setBgColors([]);
    }
  }, [rawData]);

  if (chartType == "Pie") {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      },
      redraw: false
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 7
    }, this);
  } else {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Doughnut"], {
      data: {
        labels: label,
        legend: {
          position: "left"
        },
        datasets: [{
          data: amount,
          backgroundColor: bgColors,
          hoverBackground: bgColors
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 7
    }, this);
  }
}

_s(PieChart, "wU8AlrnnEQs/CoFwitN29kubuKE=");

_c = PieChart;

var _c;

$RefreshReg$(_c, "PieChart");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9QaWVDaGFydC5qcyJdLCJuYW1lcyI6WyJQaWVDaGFydCIsImNoYXJ0VHlwZSIsInJhd0RhdGEiLCJsYWJlbEtleSIsImFtb3VudEtleSIsInVzZVN0YXRlIiwibGFiZWwiLCJzZXRMYWJlbCIsImFtb3VudCIsInNldEFtb3VudCIsImJnQ29sb3JzIiwic2V0QmdDb2xvcnMiLCJ1c2VFZmZlY3QiLCJtYXAiLCJlbGVtZW50IiwiTWF0aCIsImFicyIsImNvbG9ycyIsImNvbG9yIiwiQ29sb3JSYW5kb21pemVyIiwibGFiZWxzIiwibGVnZW5kIiwicG9zaXRpb24iLCJkYXRhc2V0cyIsImRhdGEiLCJiYWNrZ3JvdW5kQ29sb3IiLCJob3ZlckJhY2tncm91bmQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLFFBQVQsT0FBK0Q7QUFBQTs7QUFBQSxNQUEzQ0MsU0FBMkMsUUFBM0NBLFNBQTJDO0FBQUEsTUFBaENDLE9BQWdDLFFBQWhDQSxPQUFnQztBQUFBLE1BQXZCQyxRQUF1QixRQUF2QkEsUUFBdUI7QUFBQSxNQUFiQyxTQUFhLFFBQWJBLFNBQWE7O0FBQUEsa0JBQ2xEQyxzREFBUSxDQUFDLEVBQUQsQ0FEMEM7QUFBQSxNQUNyRUMsS0FEcUU7QUFBQSxNQUM5REMsUUFEOEQ7O0FBQUEsbUJBRWhERixzREFBUSxDQUFDLEVBQUQsQ0FGd0M7QUFBQSxNQUVyRUcsTUFGcUU7QUFBQSxNQUU3REMsU0FGNkQ7O0FBQUEsbUJBRzVDSixzREFBUSxDQUFDLEVBQUQsQ0FIb0M7QUFBQSxNQUdyRUssUUFIcUU7QUFBQSxNQUczREMsV0FIMkQ7O0FBSzVFQyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJVixPQUFKLEVBQWE7QUFDWEssY0FBUSxDQUFDTCxPQUFPLENBQUNXLEdBQVIsQ0FBWSxVQUFDQyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDWCxRQUFELENBQXBCO0FBQUEsT0FBWixDQUFELENBQVI7QUFDQU0sZUFBUyxDQUFDUCxPQUFPLENBQUNXLEdBQVIsQ0FBWSxVQUFDQyxPQUFEO0FBQUEsZUFBYUMsSUFBSSxDQUFDQyxHQUFMLENBQVNGLE9BQU8sQ0FBQ1YsU0FBRCxDQUFoQixDQUFiO0FBQUEsT0FBWixDQUFELENBQVQ7QUFFQSxVQUFJYSxNQUFNLEdBQUdmLE9BQU8sQ0FBQ1csR0FBUixDQUFZLFVBQUNDLE9BQUQsRUFBYTtBQUNwQyxZQUFJQSxPQUFPLENBQUNJLEtBQVosRUFBbUI7QUFDakIsaUJBQU9KLE9BQU8sQ0FBQ0ksS0FBZjtBQUNELFNBRkQsTUFFTztBQUNMLDRCQUFXQyx3RUFBZSxFQUExQjtBQUNEO0FBQ0YsT0FOWSxDQUFiO0FBUUFSLGlCQUFXLENBQUNNLE1BQUQsQ0FBWDtBQUNELEtBYkQsTUFhTztBQUNMVixjQUFRLENBQUMsRUFBRCxDQUFSO0FBQ0FFLGVBQVMsQ0FBQyxFQUFELENBQVQ7QUFDQUUsaUJBQVcsQ0FBQyxFQUFELENBQVg7QUFDRDtBQUNGLEdBbkJRLEVBbUJOLENBQUNULE9BQUQsQ0FuQk0sQ0FBVDs7QUFxQkEsTUFBSUQsU0FBUyxJQUFJLEtBQWpCLEVBQXdCO0FBQ3RCLHdCQUNFLHFFQUFDLG1EQUFEO0FBQ0UsVUFBSSxFQUFFO0FBQ0ptQixjQUFNLEVBQUVkLEtBREo7QUFFSmUsY0FBTSxFQUFFO0FBQ05DLGtCQUFRLEVBQUU7QUFESixTQUZKO0FBS0pDLGdCQUFRLEVBQUUsQ0FDUjtBQUNFQyxjQUFJLEVBQUVoQixNQURSO0FBRUVpQix5QkFBZSxFQUFFZixRQUZuQjtBQUdFZ0IseUJBQWUsRUFBRWhCO0FBSG5CLFNBRFE7QUFMTixPQURSO0FBY0UsWUFBTSxFQUFFO0FBZFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGO0FBa0JELEdBbkJELE1BbUJPO0FBQ0wsd0JBQ0UscUVBQUMsd0RBQUQ7QUFDRSxVQUFJLEVBQUU7QUFDSlUsY0FBTSxFQUFFZCxLQURKO0FBRUplLGNBQU0sRUFBRTtBQUNOQyxrQkFBUSxFQUFFO0FBREosU0FGSjtBQUtKQyxnQkFBUSxFQUFFLENBQ1I7QUFDRUMsY0FBSSxFQUFFaEIsTUFEUjtBQUVFaUIseUJBQWUsRUFBRWYsUUFGbkI7QUFHRWdCLHlCQUFlLEVBQUVoQjtBQUhuQixTQURRO0FBTE47QUFEUjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREY7QUFpQkQ7QUFDRjs7R0FoRXVCVixROztLQUFBQSxRIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4Ljc2OGQxNmQxZjZiNDA2ODU5NjI2LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IERvdWdobnV0LCBQaWUgfSBmcm9tIFwicmVhY3QtY2hhcnRqcy0yXCI7XHJcbmltcG9ydCBDb2xvclJhbmRvbWl6ZXIgZnJvbSBcIi4uL2hlbHBlcnMvQ29sb3JSYW5kb21pemVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQaWVDaGFydCh7IGNoYXJ0VHlwZSwgcmF3RGF0YSwgbGFiZWxLZXksIGFtb3VudEtleSB9KSB7XHJcbiAgY29uc3QgW2xhYmVsLCBzZXRMYWJlbF0gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2Ftb3VudCwgc2V0QW1vdW50XSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbYmdDb2xvcnMsIHNldEJnQ29sb3JzXSA9IHVzZVN0YXRlKFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChyYXdEYXRhKSB7XHJcbiAgICAgIHNldExhYmVsKHJhd0RhdGEubWFwKChlbGVtZW50KSA9PiBlbGVtZW50W2xhYmVsS2V5XSkpO1xyXG4gICAgICBzZXRBbW91bnQocmF3RGF0YS5tYXAoKGVsZW1lbnQpID0+IE1hdGguYWJzKGVsZW1lbnRbYW1vdW50S2V5XSkpKTtcclxuXHJcbiAgICAgIGxldCBjb2xvcnMgPSByYXdEYXRhLm1hcCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgIGlmIChlbGVtZW50LmNvbG9yKSB7XHJcbiAgICAgICAgICByZXR1cm4gZWxlbWVudC5jb2xvcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmV0dXJuIGAjJHtDb2xvclJhbmRvbWl6ZXIoKX1gO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICBzZXRCZ0NvbG9ycyhjb2xvcnMpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgc2V0TGFiZWwoW10pO1xyXG4gICAgICBzZXRBbW91bnQoW10pO1xyXG4gICAgICBzZXRCZ0NvbG9ycyhbXSk7XHJcbiAgICB9XHJcbiAgfSwgW3Jhd0RhdGFdKTtcclxuXHJcbiAgaWYgKGNoYXJ0VHlwZSA9PSBcIlBpZVwiKSB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8UGllXHJcbiAgICAgICAgZGF0YT17e1xyXG4gICAgICAgICAgbGFiZWxzOiBsYWJlbCxcclxuICAgICAgICAgIGxlZ2VuZDoge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJsZWZ0XCIsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZGF0YXNldHM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGRhdGE6IGFtb3VudCxcclxuICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IGJnQ29sb3JzLFxyXG4gICAgICAgICAgICAgIGhvdmVyQmFja2dyb3VuZDogYmdDb2xvcnMsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICBdLFxyXG4gICAgICAgIH19XHJcbiAgICAgICAgcmVkcmF3PXtmYWxzZX1cclxuICAgICAgLz5cclxuICAgICk7XHJcbiAgfSBlbHNlIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxEb3VnaG51dFxyXG4gICAgICAgIGRhdGE9e3tcclxuICAgICAgICAgIGxhYmVsczogbGFiZWwsXHJcbiAgICAgICAgICBsZWdlbmQ6IHtcclxuICAgICAgICAgICAgcG9zaXRpb246IFwibGVmdFwiLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBkYXRhOiBhbW91bnQsXHJcbiAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBiZ0NvbG9ycyxcclxuICAgICAgICAgICAgICBob3ZlckJhY2tncm91bmQ6IGJnQ29sb3JzLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgXSxcclxuICAgICAgICB9fVxyXG4gICAgICAvPlxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==